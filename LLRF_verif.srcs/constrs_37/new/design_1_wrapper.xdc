set_property PACKAGE_PIN T1 [get_ports ADC_A0_P]
set_property IOSTANDARD LVDS_25 [get_ports ADC_A0_P]
set_property PACKAGE_PIN U2 [get_ports ADC_A1_P]
set_property IOSTANDARD LVDS_25 [get_ports ADC_A1_P]
set_property PACKAGE_PIN W1 [get_ports ADC_B0_P]
set_property IOSTANDARD LVDS_25 [get_ports ADC_B0_P]
set_property PACKAGE_PIN AA1 [get_ports ADC_B1_P]
set_property IOSTANDARD LVDS_25 [get_ports ADC_B1_P]
set_property PACKAGE_PIN AB3 [get_ports ADC_C0_P]
set_property IOSTANDARD LVDS_25 [get_ports ADC_C0_P]
set_property PACKAGE_PIN AB7 [get_ports ADC_C1_P]
set_property IOSTANDARD LVDS_25 [get_ports ADC_C1_P]
set_property PACKAGE_PIN V4 [get_ports ADC_CLKBIT_P]
set_property IOSTANDARD LVDS_25 [get_ports ADC_CLKBIT_P]
set_property PACKAGE_PIN AA5 [get_ports ADC_D0_P]
set_property IOSTANDARD LVDS_25 [get_ports ADC_D0_P]
set_property PACKAGE_PIN W2 [get_ports ADC_D1_P]
set_property IOSTANDARD LVDS_25 [get_ports ADC_D1_P]
set_property PACKAGE_PIN T20 [get_ports SPIDAC_CS]
set_property IOSTANDARD LVCMOS33 [get_ports SPIDAC_CS]
set_property PACKAGE_PIN U20 [get_ports SPIDAC_DATA]
set_property IOSTANDARD LVCMOS33 [get_ports SPIDAC_DATA]
set_property PACKAGE_PIN W21 [get_ports STEPPER_CLK]
set_property IOSTANDARD LVCMOS33 [get_ports STEPPER_CLK]
set_property PACKAGE_PIN AA21 [get_ports STEPPER_CS_N]
set_property IOSTANDARD LVCMOS33 [get_ports STEPPER_CS_N]
set_property PACKAGE_PIN AB18 [get_ports STEPPER_LIM_DN_N]
set_property IOSTANDARD LVCMOS33 [get_ports STEPPER_LIM_DN_N]
set_property PACKAGE_PIN AA18 [get_ports STEPPER_LIM_UP_N]
set_property IOSTANDARD LVCMOS33 [get_ports STEPPER_LIM_UP_N]
set_property PACKAGE_PIN U21 [get_ports STEPPER_RST_N]
set_property IOSTANDARD LVCMOS33 [get_ports STEPPER_RST_N]
set_property PACKAGE_PIN W22 [get_ports STEPPER_SW_N]
set_property IOSTANDARD LVCMOS33 [get_ports STEPPER_SW_N]
set_property PACKAGE_PIN Y21 [get_ports STEPPER_MOSI]
set_property IOSTANDARD LVCMOS33 [get_ports STEPPER_MOSI]
set_property PACKAGE_PIN Y22 [get_ports STEPPER_MISO]
set_property IOSTANDARD LVCMOS33 [get_ports STEPPER_MISO]
set_property PACKAGE_PIN V17 [get_ports CSB]
set_property IOSTANDARD LVCMOS33 [get_ports CSB]
set_property PACKAGE_PIN V18 [get_ports SDIO]
set_property IOSTANDARD LVCMOS33 [get_ports SDIO]
set_property PACKAGE_PIN U17 [get_ports SCLK]
set_property IOSTANDARD LVCMOS33 [get_ports SCLK]
set_property PACKAGE_PIN Y18 [get_ports clock_rtl]
set_property IOSTANDARD LVCMOS33 [get_ports clock_rtl]
set_property PACKAGE_PIN J22 [get_ports ATTEN_DATA]
set_property IOSTANDARD LVCMOS33 [get_ports ATTEN_DATA]
set_property PACKAGE_PIN K22 [get_ports ATTEN_LE]
set_property IOSTANDARD LVCMOS33 [get_ports ATTEN_LE]
set_property PACKAGE_PIN K21 [get_ports RF_ON_MON]
set_property IOSTANDARD LVCMOS33 [get_ports RF_ON_MON]
set_property PACKAGE_PIN H22 [get_ports DAC_SLEEP]
set_property IOSTANDARD LVCMOS33 [get_ports DAC_SLEEP]
set_property PACKAGE_PIN J19 [get_ports LEDS_DATA]
set_property IOSTANDARD LVCMOS33 [get_ports LEDS_DATA]
set_property PACKAGE_PIN H19 [get_ports LEDS_LATCH]
set_property IOSTANDARD LVCMOS33 [get_ports LEDS_LATCH]
set_property PACKAGE_PIN H4 [get_ports {DAC_P[0]}]
set_property IOSTANDARD LVDS_25 [get_ports {DAC_P[0]}]
set_property PACKAGE_PIN R1 [get_ports {DAC_P[13]}]
set_property IOSTANDARD LVDS_25 [get_ports {DAC_P[13]}]
set_property PACKAGE_PIN M1 [get_ports {DAC_P[12]}]
set_property IOSTANDARD LVDS_25 [get_ports {DAC_P[12]}]
set_property PACKAGE_PIN K1 [get_ports {DAC_P[11]}]
set_property IOSTANDARD LVDS_25 [get_ports {DAC_P[11]}]
set_property PACKAGE_PIN G1 [get_ports {DAC_P[10]}]
set_property IOSTANDARD LVDS_25 [get_ports {DAC_P[10]}]
set_property PACKAGE_PIN E1 [get_ports {DAC_P[9]}]
set_property IOSTANDARD LVDS_25 [get_ports {DAC_P[9]}]
set_property PACKAGE_PIN B1 [get_ports {DAC_P[8]}]
set_property IOSTANDARD LVDS_25 [get_ports {DAC_P[8]}]
set_property PACKAGE_PIN P2 [get_ports {DAC_P[7]}]
set_property IOSTANDARD LVDS_25 [get_ports {DAC_P[7]}]
set_property PACKAGE_PIN M3 [get_ports {DAC_P[6]}]
set_property IOSTANDARD LVDS_25 [get_ports {DAC_P[6]}]
set_property PACKAGE_PIN K2 [get_ports {DAC_P[5]}]
set_property IOSTANDARD LVDS_25 [get_ports {DAC_P[5]}]
set_property PACKAGE_PIN H2 [get_ports {DAC_P[4]}]
set_property IOSTANDARD LVDS_25 [get_ports {DAC_P[4]}]
set_property PACKAGE_PIN H3 [get_ports {DAC_P[3]}]
set_property IOSTANDARD LVDS_25 [get_ports {DAC_P[3]}]
set_property PACKAGE_PIN E2 [get_ports {DAC_P[2]}]
set_property IOSTANDARD LVDS_25 [get_ports {DAC_P[2]}]
set_property PACKAGE_PIN C2 [get_ports {DAC_P[1]}]
set_property IOSTANDARD LVDS_25 [get_ports {DAC_P[1]}]
set_property PACKAGE_PIN V20 [get_ports SPIDAC_CLK]
set_property IOSTANDARD LVCMOS33 [get_ports SPIDAC_CLK]
set_property PACKAGE_PIN L3 [get_ports CLK_DAC_P]
set_property IOSTANDARD LVDS_25 [get_ports CLK_DAC_P]
set_property PACKAGE_PIN W10 [get_ports SPI_CLK]
set_property IOSTANDARD LVCMOS33 [get_ports SPI_CLK]
set_property PACKAGE_PIN U15 [get_ports PCBtemp_clk]
set_property IOSTANDARD LVCMOS33 [get_ports PCBtemp_clk]
set_property PACKAGE_PIN J20 [get_ports LEDS_CLK]
set_property IOSTANDARD LVCMOS33 [get_ports LEDS_CLK]
set_property PACKAGE_PIN J21 [get_ports ATTEN_CLK]
set_property IOSTANDARD LVCMOS33 [get_ports ATTEN_CLK]
set_property IOSTANDARD LVCMOS33 [get_ports MODBUStx]
set_property PACKAGE_PIN Y11 [get_ports OUTPUT_EN]
set_property IOSTANDARD LVCMOS33 [get_ports OUTPUT_EN]
set_property PACKAGE_PIN U16 [get_ports PCBtemp_cs]
set_property IOSTANDARD LVCMOS33 [get_ports PCBtemp_cs]
set_property PACKAGE_PIN T15 [get_ports PCBtemp_data]
set_property IOSTANDARD LVCMOS33 [get_ports PCBtemp_data]
set_property PACKAGE_PIN Y13 [get_ports PLC_RFLOCKED]
set_property IOSTANDARD LVCMOS33 [get_ports PLC_RFLOCKED]
set_property PACKAGE_PIN V13 [get_ports MODBUSrx]
set_property IOSTANDARD LVCMOS33 [get_ports MODBUSrx]
set_property PACKAGE_PIN Y12 [get_ports PLC_RFON]
set_property IOSTANDARD LVCMOS33 [get_ports PLC_RFON]
set_property PACKAGE_PIN AA9 [get_ports pll0locked]
set_property IOSTANDARD LVCMOS33 [get_ports pll0locked]
set_property PACKAGE_PIN AA10 [get_ports pll1locked]
set_property IOSTANDARD LVCMOS33 [get_ports pll1locked]
set_property PACKAGE_PIN AB10 [get_ports pllSync_N]
set_property IOSTANDARD LVCMOS33 [get_ports pllSync_N]
set_property PACKAGE_PIN AB11 [get_ports SPI_ADCen]
set_property IOSTANDARD LVCMOS33 [get_ports SPI_ADCen]
set_property PACKAGE_PIN V10 [get_ports SPI_DATA]
set_property IOSTANDARD LVCMOS33 [get_ports SPI_DATA]
set_property PACKAGE_PIN AA11 [get_ports SPI_PLL0en]
set_property IOSTANDARD LVCMOS33 [get_ports SPI_PLL0en]
set_property PACKAGE_PIN AB12 [get_ports SPI_PLL1en]
set_property IOSTANDARD LVCMOS33 [get_ports SPI_PLL1en]
set_property PACKAGE_PIN AA15 [get_ports STEPPER_LOCAL_UP]
set_property IOSTANDARD LVCMOS33 [get_ports STEPPER_LOCAL_UP]
set_property PACKAGE_PIN AB15 [get_ports STEPPER_LOCAL_DN]
set_property IOSTANDARD LVCMOS33 [get_ports STEPPER_LOCAL_DN]
set_property PACKAGE_PIN AB16 [get_ports STEPPER_LOCAL]
set_property IOSTANDARD LVCMOS33 [get_ports STEPPER_LOCAL]
set_property PACKAGE_PIN W12 [get_ports SW_LOOPBACK_N]
set_property PACKAGE_PIN W11 [get_ports SW_LOOPBACK_P]
set_property IOSTANDARD LVCMOS33 [get_ports SW_LOOPBACK_N]
set_property IOSTANDARD LVCMOS33 [get_ports SW_LOOPBACK_P]
set_property PACKAGE_PIN M22 [get_ports cs_n]
set_property IOSTANDARD LVCMOS33 [get_ports cs_n]
set_property PACKAGE_PIN K19 [get_ports hold_n]
set_property IOSTANDARD LVCMOS33 [get_ports hold_n]
set_property PACKAGE_PIN L20 [get_ports mosi]
set_property PACKAGE_PIN N22 [get_ports miso]
set_property IOSTANDARD LVCMOS33 [get_ports miso]
set_property PACKAGE_PIN L19 [get_ports clk]
set_property IOSTANDARD LVCMOS33 [get_ports mosi]
set_property IOSTANDARD LVCMOS33 [get_ports clk]
set_property PACKAGE_PIN M18 [get_ports wp_n]
set_property IOSTANDARD LVCMOS33 [get_ports wp_n]
set_property PACKAGE_PIN Y16 [get_ports USBrx]
set_property IOSTANDARD LVCMOS33 [get_ports USBrx]
set_property IOSTANDARD LVCMOS33 [get_ports USBtx]
set_property PACKAGE_PIN L15 [get_ports reset_rtl]
set_property IOSTANDARD LVCMOS33 [get_ports reset_rtl]



set_property PULLDOWN true [get_ports reset_rtl]


set_property MARK_DEBUG false [get_nets design_1_i/clk_wiz_1/inst/clk_in1_design_1_clk_wiz_1_0]
set_property MARK_DEBUG false [get_nets design_1_i/clk_wiz_1/inst/clk_out50_design_1_clk_wiz_1_0]
set_property MARK_DEBUG false [get_nets design_1_i/clk_wiz_1/inst/clk_out125_design_1_clk_wiz_1_0]
set_property MARK_DEBUG false [get_nets design_1_i/clk_wiz_1/inst/clk_out200_design_1_clk_wiz_1_0]
set_property MARK_DEBUG false [get_nets design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/clk_i_1_n_0]
set_property MARK_DEBUG false [get_nets ATTEN_CLK_OBUF]
set_property MARK_DEBUG false [get_nets design_1_i/rst_clk_wiz_1_100M_mb_reset]
set_property MARK_DEBUG false [get_nets design_1_i/mig_7series_0/u_design_1_mig_7series_0_0_mig/u_ddr3_infrastructure/clk_div2_bufg_in]
set_property MARK_DEBUG false [get_nets design_1_i/mig_7series_0/u_design_1_mig_7series_0_0_mig/freq_refclk]



set_property PACKAGE_PIN W15 [get_ports MODBUStx]




set_property PACKAGE_PIN AA16 [get_ports USBtx]

set_property BITSTREAM.CONFIG.SPI_32BIT_ADDR YES [current_design]
set_property BITSTREAM.CONFIG.SPI_BUSWIDTH 1 [current_design]
set_property BITSTREAM.CONFIG.CONFIGFALLBACK DISABLE [current_design]
set_property CONFIG_MODE SPIx1 [current_design]













set_property CONFIG_VOLTAGE 3.3 [current_design]
set_property CFGBVS VCCO [current_design]




connect_debug_port dbg_hub/clk [get_nets u_ila_0_rfClk]

create_debug_core u_ila_0 ila
set_property ALL_PROBE_SAME_MU true [get_debug_cores u_ila_0]
set_property ALL_PROBE_SAME_MU_CNT 4 [get_debug_cores u_ila_0]
set_property C_ADV_TRIGGER true [get_debug_cores u_ila_0]
set_property C_DATA_DEPTH 1024 [get_debug_cores u_ila_0]
set_property C_EN_STRG_QUAL true [get_debug_cores u_ila_0]
set_property C_INPUT_PIPE_STAGES 1 [get_debug_cores u_ila_0]
set_property C_TRIGIN_EN false [get_debug_cores u_ila_0]
set_property C_TRIGOUT_EN false [get_debug_cores u_ila_0]
set_property port_width 1 [get_debug_ports u_ila_0/clk]
connect_debug_port u_ila_0/clk [get_nets [list design_1_i/clk_wiz_1/inst/clk_out50]]
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe0]
set_property port_width 32 [get_debug_ports u_ila_0/probe0]
connect_debug_port u_ila_0/probe0 [get_nets [list {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/readData[0]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/readData[1]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/readData[2]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/readData[3]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/readData[4]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/readData[5]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/readData[6]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/readData[7]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/readData[8]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/readData[9]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/readData[10]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/readData[11]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/readData[12]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/readData[13]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/readData[14]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/readData[15]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/readData[16]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/readData[17]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/readData[18]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/readData[19]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/readData[20]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/readData[21]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/readData[22]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/readData[23]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/readData[24]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/readData[25]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/readData[26]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/readData[27]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/readData[28]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/readData[29]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/readData[30]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/readData[31]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe1]
set_property port_width 2 [get_debug_ports u_ila_0/probe1]
connect_debug_port u_ila_0/probe1 [get_nets [list {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/s00_axi_araddr[0]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/s00_axi_araddr[1]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe2]
set_property port_width 2 [get_debug_ports u_ila_0/probe2]
connect_debug_port u_ila_0/probe2 [get_nets [list {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/s00_axi_awaddr[0]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/s00_axi_awaddr[1]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe3]
set_property port_width 4 [get_debug_ports u_ila_0/probe3]
connect_debug_port u_ila_0/probe3 [get_nets [list {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/s00_axi_wstrb[0]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/s00_axi_wstrb[1]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/s00_axi_wstrb[2]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/s00_axi_wstrb[3]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe4]
set_property port_width 4 [get_debug_ports u_ila_0/probe4]
connect_debug_port u_ila_0/probe4 [get_nets [list {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/state[0]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/state[1]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/state[2]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/state[3]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe5]
set_property port_width 32 [get_debug_ports u_ila_0/probe5]
connect_debug_port u_ila_0/probe5 [get_nets [list {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/writeData[0]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/writeData[1]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/writeData[2]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/writeData[3]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/writeData[4]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/writeData[5]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/writeData[6]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/writeData[7]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/writeData[8]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/writeData[9]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/writeData[10]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/writeData[11]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/writeData[12]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/writeData[13]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/writeData[14]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/writeData[15]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/writeData[16]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/writeData[17]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/writeData[18]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/writeData[19]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/writeData[20]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/writeData[21]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/writeData[22]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/writeData[23]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/writeData[24]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/writeData[25]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/writeData[26]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/writeData[27]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/writeData[28]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/writeData[29]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/writeData[30]} {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/writeData[31]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe6]
set_property port_width 1 [get_debug_ports u_ila_0/probe6]
connect_debug_port u_ila_0/probe6 [get_nets [list design_1_i/cs_n]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe7]
set_property port_width 1 [get_debug_ports u_ila_0/probe7]
connect_debug_port u_ila_0/probe7 [get_nets [list design_1_i/miso]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe8]
set_property port_width 1 [get_debug_ports u_ila_0/probe8]
connect_debug_port u_ila_0/probe8 [get_nets [list design_1_i/mosi]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe9]
set_property port_width 1 [get_debug_ports u_ila_0/probe9]
connect_debug_port u_ila_0/probe9 [get_nets [list design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/readFifoEmpty]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe10]
set_property port_width 1 [get_debug_ports u_ila_0/probe10]
connect_debug_port u_ila_0/probe10 [get_nets [list design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/readFifoFull]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe11]
set_property port_width 1 [get_debug_ports u_ila_0/probe11]
connect_debug_port u_ila_0/probe11 [get_nets [list design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/readFifoValid]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe12]
set_property port_width 1 [get_debug_ports u_ila_0/probe12]
connect_debug_port u_ila_0/probe12 [get_nets [list design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/writeFifoEmpty]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe13]
set_property port_width 1 [get_debug_ports u_ila_0/probe13]
connect_debug_port u_ila_0/probe13 [get_nets [list design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/writeFifoFull]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe14]
set_property port_width 1 [get_debug_ports u_ila_0/probe14]
connect_debug_port u_ila_0/probe14 [get_nets [list design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/writeFifoValid]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe15]
set_property port_width 1 [get_debug_ports u_ila_0/probe15]
connect_debug_port u_ila_0/probe15 [get_nets [list design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/axi_wready_i_1_n_0]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe16]
set_property port_width 1 [get_debug_ports u_ila_0/probe16]
connect_debug_port u_ila_0/probe16 [get_nets [list {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/axi_awaddr[2]_i_1_n_0}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe17]
set_property port_width 1 [get_debug_ports u_ila_0/probe17]
connect_debug_port u_ila_0/probe17 [get_nets [list {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/axi_awaddr[3]_i_1_n_0}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe18]
set_property port_width 1 [get_debug_ports u_ila_0/probe18]
connect_debug_port u_ila_0/probe18 [get_nets [list {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/axi_araddr[3]_i_1_n_0}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe19]
set_property port_width 1 [get_debug_ports u_ila_0/probe19]
connect_debug_port u_ila_0/probe19 [get_nets [list {design_1_i/Flash_axi_peripheral_0/inst/Flash_axi_peripheral_v1_0_S00_AXI_inst/axi_araddr[2]_i_1_n_0}]]
set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]
set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
connect_debug_port dbg_hub/clk [get_nets clk_1]
