//Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2016.4 (win64) Build 1733598 Wed Dec 14 22:35:39 MST 2016
//Date        : Thu Apr 05 08:59:17 2018
//Host        : SHRIRAJKUNJA80D running 64-bit Service Pack 1  (build 7601)
//Command     : generate_target design_1_wrapper.bd
//Design      : design_1_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_1_wrapper
   (ADC_A0_N,
    ADC_A0_P,
    ADC_A1_N,
    ADC_A1_P,
    ADC_B0_N,
    ADC_B0_P,
    ADC_B1_N,
    ADC_B1_P,
    ADC_C0_N,
    ADC_C0_P,
    ADC_C1_N,
    ADC_C1_P,
    ADC_CLKBIT_N,
    ADC_CLKBIT_P,
    ADC_D0_N,
    ADC_D0_P,
    ADC_D1_N,
    ADC_D1_P,
    ATTEN_CLK,
    ATTEN_DATA,
    ATTEN_LE,
    CLK_DAC_N,
    CLK_DAC_P,
    CSB,
    DAC_N,
    DAC_P,
    DAC_SLEEP,
    DDS_IO_UPDATE,
    LEDS_CLK,
    LEDS_DATA,
    LEDS_LATCH,
    LED_BLANK,
    MODBUSrx,
    MODBUStx,
    OUTPUT_EN,
    PCBtemp_clk,
    PCBtemp_cs,
    PCBtemp_data,
    PLC_RFLOCKED,
    PLC_RFON,
    RF_ON_MON,
    SCLK,
    SDIO,
    SPIDAC_CLK,
    SPIDAC_CS,
    SPIDAC_DATA,
    SPI_ADCen,
    SPI_CLK,
    SPI_DATA,
    SPI_PLL0en,
    SPI_PLL1en,
    STEPPER_CLK,
    STEPPER_CS_N,
    STEPPER_LIM_DN_N,
    STEPPER_LIM_UP_N,
    STEPPER_LOCAL,
    STEPPER_LOCAL_DN,
    STEPPER_LOCAL_UP,
    STEPPER_MISO,
    STEPPER_MOSI,
    STEPPER_RST_N,
    STEPPER_SW_N,
    SW_LOOPBACK_N,
    SW_LOOPBACK_P,
    USBrx,
    USBtx,
    clk,
    clock_rtl,
    cs_n,
    hold_n,
    mdio_rtl_mdc,
    mdio_rtl_mdio_io,
    miso,
    mosi,
    phy_col,
    phy_crs,
    phy_dv,
    phy_rst_n,
    phy_rx_data,
    phy_rx_er,
    phy_tx_data,
    phy_tx_en,
    pll0locked,
    pll1locked,
    pllSync_N,
    reset_rtl,
    wp_n);
  input ADC_A0_N;
  input ADC_A0_P;
  input ADC_A1_N;
  input ADC_A1_P;
  input ADC_B0_N;
  input ADC_B0_P;
  input ADC_B1_N;
  input ADC_B1_P;
  input ADC_C0_N;
  input ADC_C0_P;
  input ADC_C1_N;
  input ADC_C1_P;
  input ADC_CLKBIT_N;
  input ADC_CLKBIT_P;
  input ADC_D0_N;
  input ADC_D0_P;
  input ADC_D1_N;
  input ADC_D1_P;
  output ATTEN_CLK;
  output ATTEN_DATA;
  output ATTEN_LE;
  input CLK_DAC_N;
  input CLK_DAC_P;
  output CSB;
  output [13:0]DAC_N;
  output [13:0]DAC_P;
  output DAC_SLEEP;
  output DDS_IO_UPDATE;
  output LEDS_CLK;
  output LEDS_DATA;
  output LEDS_LATCH;
  output LED_BLANK;
  input MODBUSrx;
  output MODBUStx;
  input OUTPUT_EN;
  output PCBtemp_clk;
  output PCBtemp_cs;
  input PCBtemp_data;
  output PLC_RFLOCKED;
  output PLC_RFON;
  output RF_ON_MON;
  output SCLK;
  output SDIO;
  output SPIDAC_CLK;
  output SPIDAC_CS;
  output SPIDAC_DATA;
  output SPI_ADCen;
  output SPI_CLK;
  output SPI_DATA;
  output SPI_PLL0en;
  output SPI_PLL1en;
  output STEPPER_CLK;
  output STEPPER_CS_N;
  input STEPPER_LIM_DN_N;
  input STEPPER_LIM_UP_N;
  input STEPPER_LOCAL;
  input STEPPER_LOCAL_DN;
  input STEPPER_LOCAL_UP;
  input STEPPER_MISO;
  output STEPPER_MOSI;
  output STEPPER_RST_N;
  output STEPPER_SW_N;
  output SW_LOOPBACK_N;
  output SW_LOOPBACK_P;
  input USBrx;
  output USBtx;
  output clk;
  input clock_rtl;
  output cs_n;
  output hold_n;
  output mdio_rtl_mdc;
  inout mdio_rtl_mdio_io;
  input miso;
  output mosi;
  input phy_col;
  input phy_crs;
  input phy_dv;
  output phy_rst_n;
  input [3:0]phy_rx_data;
  input phy_rx_er;
  output [3:0]phy_tx_data;
  output phy_tx_en;
  input pll0locked;
  input pll1locked;
  output pllSync_N;
  input reset_rtl;
  output wp_n;

  wire ADC_A0_N;
  wire ADC_A0_P;
  wire ADC_A1_N;
  wire ADC_A1_P;
  wire ADC_B0_N;
  wire ADC_B0_P;
  wire ADC_B1_N;
  wire ADC_B1_P;
  wire ADC_C0_N;
  wire ADC_C0_P;
  wire ADC_C1_N;
  wire ADC_C1_P;
  wire ADC_CLKBIT_N;
  wire ADC_CLKBIT_P;
  wire ADC_D0_N;
  wire ADC_D0_P;
  wire ADC_D1_N;
  wire ADC_D1_P;
  wire ATTEN_CLK;
  wire ATTEN_DATA;
  wire ATTEN_LE;
  wire CLK_DAC_N;
  wire CLK_DAC_P;
  wire CSB;
  wire [13:0]DAC_N;
  wire [13:0]DAC_P;
  wire DAC_SLEEP;
  wire DDS_IO_UPDATE;
  wire LEDS_CLK;
  wire LEDS_DATA;
  wire LEDS_LATCH;
  wire LED_BLANK;
  wire MODBUSrx;
  wire MODBUStx;
  wire OUTPUT_EN;
  wire PCBtemp_clk;
  wire PCBtemp_cs;
  wire PCBtemp_data;
  wire PLC_RFLOCKED;
  wire PLC_RFON;
  wire RF_ON_MON;
  wire SCLK;
  wire SDIO;
  wire SPIDAC_CLK;
  wire SPIDAC_CS;
  wire SPIDAC_DATA;
  wire SPI_ADCen;
  wire SPI_CLK;
  wire SPI_DATA;
  wire SPI_PLL0en;
  wire SPI_PLL1en;
  wire STEPPER_CLK;
  wire STEPPER_CS_N;
  wire STEPPER_LIM_DN_N;
  wire STEPPER_LIM_UP_N;
  wire STEPPER_LOCAL;
  wire STEPPER_LOCAL_DN;
  wire STEPPER_LOCAL_UP;
  wire STEPPER_MISO;
  wire STEPPER_MOSI;
  wire STEPPER_RST_N;
  wire STEPPER_SW_N;
  wire SW_LOOPBACK_N;
  wire SW_LOOPBACK_P;
  wire USBrx;
  wire USBtx;
  wire clk;
  wire clock_rtl;
  wire cs_n;
  wire hold_n;
  wire mdio_rtl_mdc;
  wire mdio_rtl_mdio_i;
  wire mdio_rtl_mdio_io;
  wire mdio_rtl_mdio_o;
  wire mdio_rtl_mdio_t;
  wire miso;
  wire mosi;
  wire phy_col;
  wire phy_crs;
  wire phy_dv;
  wire phy_rst_n;
  wire [3:0]phy_rx_data;
  wire phy_rx_er;
  wire [3:0]phy_tx_data;
  wire phy_tx_en;
  wire pll0locked;
  wire pll1locked;
  wire pllSync_N;
  wire reset_rtl;
  wire wp_n;

  design_1 design_1_i
       (.ADC_A0_N(ADC_A0_N),
        .ADC_A0_P(ADC_A0_P),
        .ADC_A1_N(ADC_A1_N),
        .ADC_A1_P(ADC_A1_P),
        .ADC_B0_N(ADC_B0_N),
        .ADC_B0_P(ADC_B0_P),
        .ADC_B1_N(ADC_B1_N),
        .ADC_B1_P(ADC_B1_P),
        .ADC_C0_N(ADC_C0_N),
        .ADC_C0_P(ADC_C0_P),
        .ADC_C1_N(ADC_C1_N),
        .ADC_C1_P(ADC_C1_P),
        .ADC_CLKBIT_N(ADC_CLKBIT_N),
        .ADC_CLKBIT_P(ADC_CLKBIT_P),
        .ADC_D0_N(ADC_D0_N),
        .ADC_D0_P(ADC_D0_P),
        .ADC_D1_N(ADC_D1_N),
        .ADC_D1_P(ADC_D1_P),
        .ATTEN_CLK(ATTEN_CLK),
        .ATTEN_DATA(ATTEN_DATA),
        .ATTEN_LE(ATTEN_LE),
        .CLK_DAC_N(CLK_DAC_N),
        .CLK_DAC_P(CLK_DAC_P),
        .CSB(CSB),
        .DAC_N(DAC_N),
        .DAC_P(DAC_P),
        .DAC_SLEEP(DAC_SLEEP),
        .DDS_IO_UPDATE(DDS_IO_UPDATE),
        .LEDS_CLK(LEDS_CLK),
        .LEDS_DATA(LEDS_DATA),
        .LEDS_LATCH(LEDS_LATCH),
        .LED_BLANK(LED_BLANK),
        .MODBUSrx(MODBUSrx),
        .MODBUStx(MODBUStx),
        .OUTPUT_EN(OUTPUT_EN),
        .PCBtemp_clk(PCBtemp_clk),
        .PCBtemp_cs(PCBtemp_cs),
        .PCBtemp_data(PCBtemp_data),
        .PLC_RFLOCKED(PLC_RFLOCKED),
        .PLC_RFON(PLC_RFON),
        .RF_ON_MON(RF_ON_MON),
        .SCLK(SCLK),
        .SDIO(SDIO),
        .SPIDAC_CLK(SPIDAC_CLK),
        .SPIDAC_CS(SPIDAC_CS),
        .SPIDAC_DATA(SPIDAC_DATA),
        .SPI_ADCen(SPI_ADCen),
        .SPI_CLK(SPI_CLK),
        .SPI_DATA(SPI_DATA),
        .SPI_PLL0en(SPI_PLL0en),
        .SPI_PLL1en(SPI_PLL1en),
        .STEPPER_CLK(STEPPER_CLK),
        .STEPPER_CS_N(STEPPER_CS_N),
        .STEPPER_LIM_DN_N(STEPPER_LIM_DN_N),
        .STEPPER_LIM_UP_N(STEPPER_LIM_UP_N),
        .STEPPER_LOCAL(STEPPER_LOCAL),
        .STEPPER_LOCAL_DN(STEPPER_LOCAL_DN),
        .STEPPER_LOCAL_UP(STEPPER_LOCAL_UP),
        .STEPPER_MISO(STEPPER_MISO),
        .STEPPER_MOSI(STEPPER_MOSI),
        .STEPPER_RST_N(STEPPER_RST_N),
        .STEPPER_SW_N(STEPPER_SW_N),
        .SW_LOOPBACK_N(SW_LOOPBACK_N),
        .SW_LOOPBACK_P(SW_LOOPBACK_P),
        .USBrx(USBrx),
        .USBtx(USBtx),
        .clk(clk),
        .clock_rtl(clock_rtl),
        .cs_n(cs_n),
        .hold_n(hold_n),
        .mdio_rtl_mdc(mdio_rtl_mdc),
        .mdio_rtl_mdio_i(mdio_rtl_mdio_i),
        .mdio_rtl_mdio_o(mdio_rtl_mdio_o),
        .mdio_rtl_mdio_t(mdio_rtl_mdio_t),
        .miso(miso),
        .mosi(mosi),
        .phy_col(phy_col),
        .phy_crs(phy_crs),
        .phy_dv(phy_dv),
        .phy_rst_n(phy_rst_n),
        .phy_rx_data(phy_rx_data),
        .phy_rx_er(phy_rx_er),
        .phy_tx_data(phy_tx_data),
        .phy_tx_en(phy_tx_en),
        .pll0locked(pll0locked),
        .pll1locked(pll1locked),
        .pllSync_N(pllSync_N),
        .reset_rtl(reset_rtl),
        .wp_n(wp_n));
  IOBUF mdio_rtl_mdio_iobuf
       (.I(mdio_rtl_mdio_o),
        .IO(mdio_rtl_mdio_io),
        .O(mdio_rtl_mdio_i),
        .T(mdio_rtl_mdio_t));
endmodule
