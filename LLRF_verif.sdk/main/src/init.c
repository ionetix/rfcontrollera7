
#include <stdio.h>
#include "xil_io.h"
#include "llrf.h"

int stepper_busy() {
	return Xil_In32(STPR_BUSY);
}
void sync_pll_ctrs() {
	// syncs PLLs and sample counters
	Xil_Out32(BITSLIPSYNC, 2);
	// if a write to BITSLIPSYNC is not followed with a write of 0, the lines set high by the first write
	// will remain high indefinitely
	Xil_Out32(BITSLIPSYNC, 0);
}
void sync_adc_ctrs() {
	// syncs ADC sample counter in frame clock domain based on ADC sample counter in PLL clock domain
	Xil_Out32(BITSLIPSYNC, 3);
	// if a write to BITSLIPSYNC is not followed with a write of 0, the lines set high by the first write
	// will remain high indefinitely
	Xil_Out32(BITSLIPSYNC, 0);
}

// all init functions take the argument systemType, which defines what type of RF system the LLRF is installed in
// so far, the valid system types are as follows
// 0:	Ionetix 68 MHz cyclotron
//		61.625 MHz ADC sample clock (ref * 29/32)
//		29 input samples per period
//		CIC lookup table step size = 3
//		181 1/3 MHz DAC sample clock
//		8 output samples per period, RF output is 3rd harmonic
// 1:	Proposed Niowave linac (350 and 700 MHz cavities, but mixed externally to 50 MHz)
//		45.3125 MHz ADC sample clock (ref * 29/32)
//		29 input samples per period
//		CIC lookup table step size = 3
//		200 MHz DAC sample clock
//		4 output samples per period, RF output is 1st harmonic
// 2:	Proposed Pronova system (90 MHz)
//		40.78125 MHz ADC sample clock (ref * 29/64)
//		29 input samples per period
//		CIC lookup table step size = 6
//		120 MHz DAC sample clock
//		4 output samples per period, RF output is 3rd harmonic
// 3:	Ionetix cyclotron @ 66 MHz
//		59.8125 MHz ADC sample clock (ref * 29/32)
//		29 input samples per period
//		CIC lookup table step size = 3
//		176 MHz DAC sample clock
//		8 output samples per period, RF output is 3rd harmonic
// 4:	Ionetix cyclotron @ 65 MHz
//		29.453125 MHz ADC sample clock (ref * 29/64)
//		29 input samples per period
//		CIC lookup table step size = 6
//		173 1/3 MHz DAC sample clock
//		8 output samples per period, RF output is 3rd harmonic
// 5:	Ionetix cyclotron @ 67 MHz
//		60.71875 MHz ADC sample clock (ref * 29/32)
//		29 input samples per period
//		CIC lookup table step size = 3
//		178 2/3 MHz DAC sample clock
//		8 output samples per period, RF output is 3rd harmonic
void init_stepper(u32 systemType) {
//	if (systemType < 6)
//	{
		xil_printf("\r\nConfiguring stepper driver\r\n");
		Xil_Out32(STPR_RDY, 0);
		while (stepper_busy());
		Xil_Out32(STPR_DATA, 0xD0000000);	// read status register to initialize controller
		while (stepper_busy());
		Xil_Out32(STPR_DATA, 0xD0000000);	// read status register to initialize controller
		while (stepper_busy());
		Xil_Out32(STPR_DATA, 0xD0000000);	// read status register to initialize controller
		while (stepper_busy());
		Xil_Out32(STPR_DATA, 0x07000400);	// lower maximum speed to prevent stall
		while (stepper_busy());
		Xil_Out32(STPR_DATA, 0x0AFF0000);	// set running current to maximum
		while (stepper_busy());
		Xil_Out32(STPR_DATA, 0x0BFF0000);	// set acceleration current to maximum
		while (stepper_busy());
		Xil_Out32(STPR_DATA, 0x0CFF0000);	// set deceleration current to maximum
		while (stepper_busy());
		Xil_Out32(STPR_RDY, 1);
		return;
//	}
//	else
//		xil_printf("\r\nInvalid system type, stepper driver not configured\r\n");
}
// Initializes master oscillator (AD9912A device)
void init_dds(u32 systemType) {
	if (systemType < 6)
	{
		xil_printf("\r\nConfiguring master oscillator\r\n");
		switch (systemType) {
		case 0:
			xil_printf("min frequency:   67.5 MHz\r\n");
			xil_printf("max frequency:   68.5 MHz\r\n");
			Xil_Out32(MINFREQ, 67500000);
			Xil_Out32(MAXFREQ, 68500000);
			break;
		case 1:
			xil_printf("min frequency:   49.9 MHz\r\n");
			xil_printf("max frequency:   50.1 MHz\r\n");
			Xil_Out32(MINFREQ, 49900000);
			Xil_Out32(MAXFREQ, 50100000);
			break;
		case 2:
			xil_printf("min frequency:   89.5 MHz\r\n");
			xil_printf("max frequency:   90.5 MHz\r\n");
			Xil_Out32(MINFREQ, 89500000);
			Xil_Out32(MAXFREQ, 90500000);
			break;
		case 3:
			xil_printf("min frequency:   65.5 MHz\r\n");
			xil_printf("max frequency:   66.5 MHz\r\n");
			Xil_Out32(MINFREQ, 65500000);
			Xil_Out32(MAXFREQ, 66500000);
			break;
		case 4:
			xil_printf("min frequency:   64.5 MHz\r\n");
			xil_printf("max frequency:   65.5 MHz\r\n");
			Xil_Out32(MINFREQ, 64500000);
			Xil_Out32(MAXFREQ, 65500000);
			break;
		case 5:
			xil_printf("min frequency:   66.5 MHz\r\n");
			xil_printf("max frequency:   67.5 MHz\r\n");
			Xil_Out32(MINFREQ, 66500000);
			Xil_Out32(MAXFREQ, 67500000);
			break;
		}
		Xil_Out32(RESETDDS, 1);
	}
	else
		xil_printf("\r\nInvalid system type, master oscillator not configured\r\n");
}
// Initializes PLL that produces ADC sample clock (LMK03000C)
int init_pll0(u32 systemType) {
	u32 r3, r11, r13, r14, r15;
	int timeout = 0;
	if (systemType < 6)
	{
		switch (systemType) {
		case 0:
			xil_printf("\r\nConfiguring PLL0 for 68 MHz ref, 61.625 MHz ADC sample clock\r\n");
			Xil_Out32(ADCCLOCKRATE, 61625000);
			Xil_Out32(RFSTEP, 3);
			r3 = 0x00030203;
			r11 = 0x0082000B;
			r13 = 0x0291000D;
			r14 = 0x0830080E;
			r15 = 0xD4001D0F;
			break;
		case 1:
			xil_printf("\r\nConfiguring PLL0 for 50 MHz ref, 45.3125 MHz ADC sample clock\r\n");
			Xil_Out32(ADCCLOCKRATE, 45312500);
			Xil_Out32(RFSTEP, 3);
			r3 = 0x00030203;
			r11 = 0x0082000B;
			r13 = 0x028C800D;
			r14 = 0x0830080E;
			r15 = 0xDC001D0F;
			break;
		case 2:
			xil_printf("\r\nConfiguring PLL0 for 90 MHz ref, 40.78125 MHz ADC sample clock\r\n");
			Xil_Out32(ADCCLOCKRATE, 40781250);
			Xil_Out32(RFSTEP, 6);
			r3 = 0x00030503;
			r11 = 0x0082000B;
			r13 = 0x0296800D;
			r14 = 0x0830200E;
			r15 = 0xCC00910F;
			break;
		case 3:
			xil_printf("\r\nConfiguring PLL0 for 66 MHz ref, 59.8125 MHz ADC sample clock\r\n");
			Xil_Out32(ADCCLOCKRATE, 59812500);
			Xil_Out32(RFSTEP, 3);
			r3 = 0x00030203;
			r11 = 0x0082000B;
			r13 = 0x0290800D;
			r14 = 0x0830080E;
			r15 = 0xD4001D0F;
			break;
		case 4:
			xil_printf("\r\nConfiguring PLL0 for 65 MHz ref, 29.453125 MHz ADC sample clock\r\n");
			Xil_Out32(ADCCLOCKRATE, 29453125);
			Xil_Out32(RFSTEP, 6);
			r3 = 0x00030703;
			r11 = 0x0082000B;
			r13 = 0x0290400D;
			r14 = 0x0830200E;
			r15 = 0xCC00CB0F;
			break;
		case 5:
			xil_printf("\r\nConfiguring PLL0 for 67 MHz ref, 60.71875 MHz ADC sample clock\r\n");
			Xil_Out32(ADCCLOCKRATE, 60718750);
			Xil_Out32(RFSTEP, 3);
			r3 = 0x00030203;
			r11 = 0x0082000B;
			r13 = 0x0290C00D;
			r14 = 0x0830080E;
			r15 = 0xD4001D0F;
			break;
		}
		Xil_Out32(PLL0WORD, 0x80000000);
		Xil_Out32(PLL0WORD, r3);
		Xil_Out32(PLL0WORD, 0x10000908);
		Xil_Out32(PLL0WORD, r11);
		Xil_Out32(PLL0WORD, r13);
		Xil_Out32(PLL0WORD, r14);
		Xil_Out32(PLL0WORD, r15);
		xil_printf("Waiting for lock\r\n");
		while (!(Xil_In32(PLLLOCKED) & 0x00000001)) {
			if (timeout++ > 5000000)
				return -1;
		}
		xil_printf("PLL0 locked");
	}
	else
		xil_printf("\r\nInvalid system type, PLL0 not configured\r\n");
	return 0;
}

int init_pll1(u32 systemType) {
	u32 r0, r3, r11, r13, r14, r15;
	int timeout = 0;
	if (systemType < 6)
	{
		switch (systemType) {
		case 0:
			xil_printf("\r\nConfiguring PLL1 for 68 MHz ref, 181 1/3 MHz DAC sample clock\r\n");
			Xil_Out32(DAC8SAMPLES, 1);
			r0 = 0x00010000;
			r3 = 0x00010003;
			r11 = 0x0082800B;
			r13 = 0x0291000D;
			r14 = 0x0830030E;
			r15 = 0xDC00080F;
			break;
		case 1:
			xil_printf("\r\nConfiguring PLL1 for 50 MHz ref, 200 MHz DAC sample clock\r\n");
			Xil_Out32(DAC8SAMPLES, 0);
			r0 = 0x00030100;
			r3 = 0x00030103;
			r11 = 0x0082800B;
			r13 = 0x028C800D;
			r14 = 0x0830020E;
			r15 = 0xCC00100F;
			break;
		case 2:
			xil_printf("\r\nConfiguring PLL1 for 90 MHz ref, 120 MHz DAC sample clock\r\n");
			Xil_Out32(DAC8SAMPLES, 0);
			r0 = 0x00030100;
			r3 = 0x00030103;
			r11 = 0x0082800B;
			r13 = 0x0296800D;
			r14 = 0x0830030E;
			r15 = 0xD400080F;
			break;
		case 3:
			xil_printf("\r\nConfiguring PLL1 for 66 MHz ref, 176 MHz DAC sample clock\r\n");
			Xil_Out32(DAC8SAMPLES, 1);
			r0 = 0x00010000;
			r3 = 0x00010003;
			r11 = 0x0082800B;
			r13 = 0x0290800D;
			r14 = 0x0830030E;
			r15 = 0xDC00080F;
			break;
		case 4:
			xil_printf("\r\nConfiguring PLL1 for 65 MHz ref, 173 1/3 MHz DAC sample clock\r\n");
			Xil_Out32(DAC8SAMPLES, 1);
			r0 = 0x00010000;
			r3 = 0x00010003;
			r11 = 0x0082800B;
			r13 = 0x0290400D;
			r14 = 0x0830030E;
			r15 = 0xDC00080F;
			break;
		case 5:
			xil_printf("\r\nConfiguring PLL1 for 67 MHz ref, 178 + 2/3 MHz DAC sample clock\r\n");
			Xil_Out32(DAC8SAMPLES, 1);
			r0 = 0x00010000;
			r3 = 0x00010003;
			r11 = 0x0082800B;
			r13 = 0x0290C00D;
			r14 = 0x0830030E;
			r15 = 0xDC00080F;
			break;
		}
		Xil_Out32(PLL1WORD, 0x80000000);
		Xil_Out32(PLL1WORD, r0);
		Xil_Out32(PLL1WORD, r3);
		Xil_Out32(PLL1WORD, 0x10000908);
		Xil_Out32(PLL1WORD, r11);
		Xil_Out32(PLL1WORD, r13);
		Xil_Out32(PLL1WORD, r14);
		Xil_Out32(PLL1WORD, r15);
		xil_printf("Waiting for lock\r\n");
		while (!(Xil_In32(PLLLOCKED) & 0x00000002)) {
			if (timeout++ > 5000000)
				return -1;
		}
		xil_printf("PLL1 locked");
	}
	else
		xil_printf("\r\nInvalid system type, PLL1 not configured\r\n");
	return 0;
}

void init_adc() {
	u32 delay;
	xil_printf("\r\nConfiguring ADC and deserializers\r\n");
	Xil_Out32(ADCWORD, 0x04000000);
	Xil_Out32(ADCWORD, 0x50E00000);
	Xil_Out32(ADCWORD, 0x5B260000);
	Xil_Out32(ADCWORD, 0x60020000);
	Xil_Out32(ADCWORD, 0x6C530000);
	for (delay=0; delay<5000000; delay++);
	Xil_Out32(BITSLIPSYNC, 1);
	for (delay=0; delay<5000000; delay++);
	Xil_Out32(ADCWORD, 0x50000000);
	xil_printf("Configuration complete\r\n");
}

void sync(u32 systemType) {
	u32 delay;
	if (systemType < 6) {
		xil_printf("\r\nSyncing sample counters\r\n");
/*		while (Xil_In32(PREFRAW) > 0x08D3DCB0) {
			Xil_Out32(STALL, 1);
			for (delay=0; delay<50000; delay++);
		}*/
		xil_printf("Sample counters synced\r\n");
	}
	else
		xil_printf("\r\nInvalid system type, save valid systemtype to flash and reboot\r\n");
}

void calc_feedFwd() {
	u32 delay;
	xil_printf("\r\nMeasuring LLRF internal feedforward phase\r\n");
	Xil_Out32(RFLOOPBACK, 1);
	for (delay=0; delay<50000; delay++);
	Xil_Out32(STARTUPPHASE, Xil_In32(PRFLRAW));
	Xil_Out32(RFLOOPBACK, 0);
	xil_printf("Internal feedforward phase calibration complete\r\n");
}

