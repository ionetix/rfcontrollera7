/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "xil_io.h"
#include "llrf.h"

// Constants
#define CORDICGAIN			1.6467602581
#define ADCMAXRAWCIC		29691.0
#define MODBUSTIMEOUT		50000		// 1 ms
//#define MODBUSTIMEOUT		651
//#define MODBUSSTARTTIME		1519

#define BACKSPACE			0x08
#define SPACE				0x20
#define CARRIAGE_RETURN		0x0D
char str[100];		// only used in main function, but declared here to prevent dynamically allocated variables from overwriting the string

// Modbus register map
const int numCoils = 25, numInputs = 11, numInputRegs = 112, numHoldingRegs = 110;
const u32 coils[25] = {				EN_RF,				DIS_RF,				EN_TUNER,			DIS_TUNER,			EN_CTRL,			DIS_CTRL,			ZERO,				ZERO,
									INCINC,				DECINC,				INCFREQ,			DECFREQ,			ILKRESET,			ILKCTRRESET,		SETTUNERHOME,		SAVEDEVCAL,
									SAVESYSCAL,			SAVESETTINGS,		ZERO,				ZERO,				SELFRESET,			LOADDEVCAL,			LOADSYSCAL,			LOADSETTINGS,
									STPR_START_SWEEP};
const u32 inputs[11] = {			RFEN,				TUNEREN,			CTRLEN,				ZERO,				RFON,				RFRAMP,				RFINITIAL,			ILKD,
									ZERO,				FINDINGHOME,		MODBUS_LOCAL};
const u32 inputRegs[112] = {		PFWDSYS,			PFWDSYS_HI,			PRFLSYS,			PRFLSYS_HI,			PCAVSYS,			PCAVSYS_HI,			PREFSYS,			PREFSYS_HI,
									PFWDDEV,			PFWDDEV_HI,			PRFLDEV,			PRFLDEV_HI,			PCAVDEV,			PCAVDEV_HI,			PREFDEV,			PREFDEV_HI,
									PFWDRAW,			PFWDRAW_HI,			PRFLRAW,			PRFLRAW_HI,			PCAVRAW,			PCAVRAW_HI,			PREFRAW,			PREFRAW_HI,
									PCTRL,				PCTRL_HI,			VFWDSYS,			VFWDSYS_HI,			VRFLSYS,			VRFLSYS_HI,			VCAVSYS,			VCAVSYS_HI,
									VREFSYS,			VREFSYS_HI,			MAXFWDSYS,			MAXFWDSYS_HI,		MAXRFLSYS,			MAXRFLSYS_HI,		MAXCAVSYS,			MAXCAVSYS_HI,
									MAXREFSYS,			MAXREFSYS_HI,		VCTRL,				VCTRL_HI,			INSTTEMP,			INSTTEMP_HI,		AVGTEMP,			AVGTEMP_HI,
									ILKCTR0,			ILKCTR0_HI,			ILKCTR1,			ILKCTR1_HI,			ILKCTR2,			ILKCTR2_HI,			ILKCTR3,			ILKCTR3_HI,
									ILKCTR4,			ILKCTR4_HI,			ILKCTR5,			ILKCTR5_HI,			ILKCTR6,			ILKCTR6_HI,			ILKCTR7,			ILKCTR7_HI,
									VFWDDEV,			VFWDDEV_HI,			VRFLDEV,			VRFLDEV_HI,			VCAVDEV,			VCAVDEV_HI,			VREFDEV,			VREFDEV_HI,
									MAXFWDDEV,			MAXFWDDEV_HI,		MAXRFLDEV,			MAXRFLDEV_HI,		MAXCAVDEV,			MAXCAVDEV_HI,		MAXREFDEV,			MAXREFDEV_HI,
									VFWDRAW,			VFWDRAW_HI,			VRFLRAW,			VRFLRAW_HI,			VCAVRAW,			VCAVRAW_HI,			VREFRAW,			VREFRAW_HI,
									FREQ,				FREQ_HI,			STEP,				STEP_HI,			STPR_POS,			STPR_POS_HI,		DUMMY,				DUMMY,
									MINFREQ,			MINFREQ_HI,			MAXFREQ,			MAXFREQ_HI,			DUMMY,				DUMMY,				ADCCLOCKRATE,		ADCCLOCKRATE_HI,
									FIRSTILK,			FIRSTILK_HI, 		ILKLATCH,			ILKLATCH_HI,		ILKSTATUS,			ILKSTATUS_HI,		STPR_LIM_COUNT,		STPR_LIM_COUNT_HI};
const u32 inputRegsScale[56] = {	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,						// input regs 0-12 are phase values
									1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,	// input regs 13-31 are unsigned 16.16 fixed point values
									2, 2, 2, 2, 2, 2, 2, 2,										// input regs 32-39 are unsigned 8.24 fixed point values
									3, 3, 3, 3,													// input regs 40-43 are unsigned integers
									4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4};						// input regs 44-54 are signed integers
const u32 holdingRegs[110] = 	{	SYSOFSTFWD,			SYSOFSTFWD_HI,		SYSOFSTRFL,			SYSOFSTRFL_HI,		SYSOFSTCAV,			SYSOFSTCAV_HI,		DEVOFSTFWD,			DEVOFSTFWD_HI,
									DEVOFSTRFL,			DEVOFSTRFL_HI,		DEVOFSTCAV,			DEVOFSTCAV_HI,		FEEDFWDPHASE,		FEEDFWDPHASE_HI,	PSET, 				PSET_HI,
									PFWDCAV,			PFWDCAV_HI,			DEADZONE,			DEADZONE_HI,		SYSCALFWD,			SYSCALFWD_HI,		SYSCALRFL,			SYSCALRFL_HI,
									SYSCALCAV,			SYSCALCAV_HI,		SYSCALREF,			SYSCALREF_HI,		DEVCALFWD,			DEVCALFWD_HI,		DEVCALRFL,			DEVCALRFL_HI,
									DEVCALCAV,			DEVCALCAV_HI,		DEVCALREF,			DEVCALREF_HI,		FEEDFWDGAIN, 		FEEDFWDGAIN_HI,		INITVSET,			INITVSET_HI,
									VSET,				VSET_HI,			FINDHOMEAMPL,		FINDHOMEAMPL_HI,	ZERO,/*unused*/		ZERO,/*unused*/		CAVLOWAMPL,			CAVLOWAMPL_HI,
									FWDRFLMINRFL,		FWDRFLMINRFL_HI,	INITFWDRFLRATIO,	INITFWDRFLRATIO_HI,	FWDRFLRATIO1,		FWDRFLRATIO1_HI,	FWDRFLRATIO2,		FWDRFLRATIO2_HI,
									CAVLOWMINFWD,		CAVLOWMINFWD_HI, 	ATTEN,				ATTEN_HI,			ILKRATESHIFT,		ILKRATESHIFT_HI,	ILKRATELIMIT,		ILKRATELIMIT_HI,
									INITTIME,			INITTIME_HI,		HOMEDELAY,			HOMEDELAY_HI,		FASTRESETTIME,		FASTRESETTIME_HI,	SLOWRESETTIME,		SLOWRESETTIME_HI,
									CAVLOWTIME,			CAVLOWTIME_HI,		INITFWDRFLTIME,		INITFWDRFLTIME_HI,	FWDRFLTIME1,		FWDRFLTIME1_HI,		FWDRFLTIME2,		FWDRFLTIME2_HI,
									RAMPRATE,			RAMPRATE_HI,		DVDTRATE,			DVDTRATE_HI,		STPR_SPEED,			STPR_SPEED_HI,		SYSTEMTYPE,			SYSTEMTYPE_HI,
									ILKRATEMASK,		ILKRATEMASK_HI,		ILKCFG0,			ILKCFG0_HI,			ILKCFG1,			ILKCFG1_HI,			ILKCFG2,			ILKCFG2_HI,
									ILKCFG3,			ILKCFG3_HI,			ILKCFG4,			ILKCFG4_HI,			ILKCFG5,			ILKCFG5_HI,			ILKCFG6,			ILKCFG6_HI,
									ILKCFG7,			ILKCFG7_HI,			FREQ,				FREQ_HI,			STPR_SWEEP_CNT,		STPR_SWEEP_CNT_HI};
const u32 holdingRegsScale[55] = {	0, 0, 0, 0, 0, 0, 0, 0, 0, 0,										// holding regs 0-9 are phase values
									1, 1, 1, 1, 4, 4, 4, 4, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,			// holding regs 10-31 are unsigned 16.16 fixed point values
									4, 4,																// holding regs 30-31 are signed integers (bitfields)
									5, 5, 5, 5, 5, 5, 5, 5,												// holding regs 32-39 are time values (will be scaled by the sample clock rate)
									6,																	// holding reg 40 is a 8.24 fixed point ramp rate (will be scaled by the sample clock rate)
									8,																	// holding reg 41 is a 16.16 fixed point ramp rate (will be scaled by sample clock rate)
									7,																	// holding reg 42 is a raw stepper speed (will be scaled to steps / s)
									4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4};								// holding regs 43-52 are signed integers (bitfields)

// Flash register map
const int numDeviceCalRegs = 8, numSystemCalRegs = 11, numSettings = 39;
const u32 deviceCal[8] = {			DEVCALFWD,			DEVCALRFL,			DEVCALCAV,			DEVCALREF,		DEVOFSTFWD,			DEVOFSTRFL,			DEVOFSTCAV,			SYSTEMTYPE};
const u32 systemCal[11] = {			SYSCALFWD,			SYSCALRFL,			SYSCALCAV,			SYSCALREF,		SYSOFSTFWD,			SYSOFSTRFL,			SYSOFSTCAV,			FEEDFWDGAIN,
									FEEDFWDPHASE,		PFWDCAV,			ATTEN};
const u32 settings[39] = {			INITVSET,			INITTIME,			RAMPRATE,			VSET,			PSET,				DEADZONE,			HOMEDELAY,			FINDHOMEAMPL,
									ZERO,/*unused*/		FASTRESETTIME,		SLOWRESETTIME,		CAVLOWAMPL,		CAVLOWTIME,			FWDRFLMINRFL,		INITFWDRFLRATIO,	FWDRFLRATIO1,
									FWDRFLRATIO2,		INITFWDRFLTIME,		FWDRFLTIME1,		FWDRFLTIME2,	DVDTRATE,			ILKRATEMASK,		ILKRATESHIFT,		ILKRATELIMIT,
									CAVLOWMINFWD,		ILKCFG0,			ILKCFG1,			ILKCFG2,		ILKCFG3,			ILKCFG4,			ILKCFG5,			ILKCFG6,
									ILKCFG7,			TUNEREN,			CTRLEN,				ZERO,			STPR_SPEED,			MODBUS_ADDR,		FREQ};

// scales value and converts to floating point, but returns as a u32 type for easier Modbus transmission
u32 scaleToPLC(u32 value, u32 type)
{
	float temp;
	if (type > 8)	return 0xFFFFFFFF;
	else if (type == 0)	temp = 8.38190317154e-8 * (float) ((signed) value);					// phase value
	else if (type == 1)	temp = 1.52587890625e-5 * (float) ((signed) value);					// u16.16 fixed point
	else if (type == 2)	temp = 5.96046447754e-8 * (float) ((signed) value);					// u8.24 fixed point
	else if (type == 3)	temp = (float) ((unsigned) value);									// u32.0 fixed point
	else if (type == 4)	temp = (float) ((signed) value);									// s32.0 fixed point
	else if (type == 5) temp = ((float) ((unsigned) value)) / Xil_In32(ADCCLOCKRATE);		// raw time value (scaled by sample clock rate)
	else if (type == 6) temp = 3.619509548e-8 * (float) value * Xil_In32(ADCCLOCKRATE);		// u8.24 ramp rate (scaled by sample clock rate and CORDIC gain)
	else if (type == 7) temp = 0.014901161194 * (float) value;								// raw stepper speed
	else if (type == 8) temp = 5.26165140086207e-7 * (float) value * Xil_In32(ADCCLOCKRATE);// u16.16 dV/dt rate (scaled by sample clock rate and samples per cycle)
	else temp = -1.0;
	return *((u32 *) &temp);
}
// scales value and converts to fixed point
u32 scaleFromPLC(u32 value, u32 type)
{
	float tempF;
	u32 tempI;
	int tempSI;
	tempF = *((float *) &value);
	if (type > 8)	return 0xFFFFFFFF;
	else if (type == 0) tempI = tempF * 11930464.7111;										// phase value
	else if (type == 1) tempI = tempF * 65536.0;											// u16.16 fixed point
	else if (type == 2) tempI = tempF * 16777216.0;											// u8.24 fixed point
	else if (type == 3) tempI = tempF;														// u32.0 fixed point
	else if (type == 4) {																	// s32.0 fixed point
		tempSI = tempF;
		tempI = *((u32 *) &tempSI);
	}
	else if (type == 5) tempI = tempF * Xil_In32(ADCCLOCKRATE);								// raw time value (scaled by sample clock rate)
	else if (type == 6) tempI = tempF * 27628052.5507 / Xil_In32(ADCCLOCKRATE);				// u8.24 ramp rate (scaled by sample clock rate and CORDIC gain)
	else if (type == 7) tempI = tempF * 67.108864;											// raw stepper speed
	else if (type == 8) tempI = tempF * 1900544.0 / Xil_In32(ADCCLOCKRATE);					// u16.16 dV/dt rate (scaled by sample clock rate and samples per cycle)
	else tempI = 0xFFFFFFFF;
	return tempI;
}
int flashBusy()
{
	return Xil_In32(FLASH_STATUS) & 0x00000001;				// check busy flag
}
void flashWriteEnable()
{
	xil_printf("\r\nEntering flash write enable\r\n");
	Xil_Out32(FLASH_STATUS, 0x6);
	xil_printf("\r\nFlash_status 0x%08X ", FLASH_STATUS);// reset FIFOs
	// wait for previous command to finish
	while (flashBusy());
	xil_printf("\r\nFlash_write fifo 0x%08X ", FLASH_WRITEFIFO);
	Xil_Out32(FLASH_WRITEFIFO, 0x00010001);
//	xil_printf("\r\nFlash_write fifo 0x%08X ", FLASH_WRITEFIFO);
	Xil_Out32(FLASH_WRITEFIFO, 0x06000000);
//	xil_printf("\r\nFlash_write fifo 0x%08X ", FLASH_WRITEFIFO);
	Xil_Out32(FLASH_START, 1);
	xil_printf("\r\nFlash_start 0x%08X ", FLASH_START);
//	xil_printf("flash write bit set\r\n");
}
void flashEraseSector(u32 address)
{
	xil_printf("\r\nEntering flash erase sector\r\n");
	Xil_Out32(FLASH_STATUS, 0x6);							// reset FIFOs
	xil_printf("\r\nFlash_status 0x%08X ", FLASH_STATUS);
	address &= 0x00FFFFFC;
	while (flashBusy());
	xil_printf("\r\nFlash_write fifo 0x%08X ", FLASH_WRITEFIFO);
	Xil_Out32(FLASH_WRITEFIFO, 0x00040004);
	xil_printf("\r\nFlash_write fifo 0x%08X ", FLASH_WRITEFIFO);
	Xil_Out32(FLASH_WRITEFIFO, 0x20000000 | address);
	xil_printf("\r\nFlash_write fifo 0x%08X ", FLASH_WRITEFIFO);
	Xil_Out32(FLASH_START, 1);
//	xil_printf("flash sector erased\r\n");
}
void flashEraseBlock(u32 address)
{
	xil_printf("\r\nEntering flash erase block\r\n");
	Xil_Out32(FLASH_STATUS, 0x6);							// reset FIFOs
	xil_printf("\r\nFlash_status 0x%08X ", FLASH_STATUS);
	address &= 0x00FFFFFC;
	while (flashBusy());
	xil_printf("\r\nFlash_write fifo 0x%08X ", FLASH_WRITEFIFO);
	Xil_Out32(FLASH_WRITEFIFO, 0x00040004);
	xil_printf("\r\nFlash_write fifo 0x%08X ", FLASH_WRITEFIFO);
	Xil_Out32(FLASH_WRITEFIFO, 0xD8000000 | address);
	xil_printf("\r\nFlash_write fifo 0x%08X ", FLASH_WRITEFIFO);
	Xil_Out32(FLASH_START, 1);
//	xil_printf("flash sector erased\r\n");
}
// assumes any pages being programmed have already been erased (no error checking done here)
void flashProgramPages(u32 address, u32 regCount, const u32* regArray)
{
	xil_printf("\r\nEntering flash program pages\r\n");
	u32 byteCount = 0;
	u32 x = 0;
	u32 limit = regCount;
	Xil_Out32(FLASH_STATUS, 0x6);
	xil_printf("\r\nFlash_status 0x%08X ", FLASH_STATUS);// reset FIFOs
	address &= 0x00FFFFC0;

	while (x < regCount)
	{
		if (regCount > 64)			limit = 64;				// max of 64 words per page (256B)
		else						limit = regCount;
		byteCount = (limit << 2) + 4;
		while (flashBusy());
		xil_printf("\r\nflash write fifo 0x%08X\r\nbytecount 0x%08X",FLASH_WRITEFIFO, byteCount);
		Xil_Out32(FLASH_WRITEFIFO, (byteCount << 16) | byteCount);
		xil_printf("\r\nflash write fifo 0x%08X\r\naddress 0x%08X",FLASH_WRITEFIFO, address);
		Xil_Out32(FLASH_WRITEFIFO, 0x02000000 | address);
		xil_printf("\r\nFlash_write fifo 0x%08X ", FLASH_WRITEFIFO);
		for (x=0; x<limit; x++)		Xil_Out32(FLASH_WRITEFIFO, Xil_In32(*regArray++));
		Xil_Out32(FLASH_START, 1);
		if (regCount > 64)
		{
			regCount -= 64;
			x = 0;
		}
	}
}
void flashRead(u32 address, u32 regCount)
{
	xil_printf("\r\nEntering flash read\r\n");
	u32 byteCount;
	Xil_Out32(FLASH_STATUS, 0x6);							// reset FIFOs
	xil_printf("\r\nFlash_status 0x%08X ", FLASH_STATUS);
	byteCount = regCount << 2;
	address &= 0x00FFFFFC;
	while(flashBusy());
	Xil_Out32(FLASH_WRITEFIFO, 0x00040004 + byteCount);
	xil_printf("\r\nFlash_write fifo 0x%08X ", FLASH_WRITEFIFO);
	Xil_Out32(FLASH_WRITEFIFO, 0x03000000 | address);
	xil_printf("\r\nFlash_write fifo 0x%08X ", FLASH_WRITEFIFO);
	Xil_Out32(FLASH_START, 1);
}
void load_devcal() {
	xil_printf("\r\nEntering load devcal\r\n");
	const u32* regPtr;
	u32 x;
	u32 flashValue;
	u32 regAddress;

	Xil_Out32(UNLOCKCAL, 1);
	regPtr = deviceCal;
	flashRead(DEVCAL_ADDR, numDeviceCalRegs);
	xil_printf("\r\ndevcal addr 0x%08X \r\n numDeviceCalRegs 0x%08X \r\n", DEVCAL_ADDR, numDeviceCalRegs);
	xil_printf("\r\nLoading device calibrations from flash\r\n");
	for (x=0; x<numDeviceCalRegs; x++)
	{
		while (flashBusy());

		flashValue = Xil_In32(FLASH_READFIFO);
		xil_printf("\r\n value stored in flash value, 0x%08X ", flashValue);
		if ((x == 0) && (flashValue == 0xFFFFFFFF))
		{
			x = numDeviceCalRegs;
			xil_printf("\r\nDevice cal page of flash empty\r\n");
		}
		else
		{
			regAddress = *regPtr;
			if (flashValue == 0xFFFFFFFF)
			{
				regPtr++;
				xil_printf("\r\nInvalid value stored in flash, 0x%08X set to default\r\n", regAddress);
			}
			else
			{
				Xil_Out32(*regPtr++, flashValue);
				xil_printf("0x%08X written to address 0x%08X\r\n", flashValue, regAddress);
			}
		}
	}
	Xil_Out32(UNLOCKCAL, 0);
}
void load_syscal() {
	xil_printf("\r\nEntering load syscal\r\n");
	const u32* regPtr;
	u32 x;
	u32 flashValue;
	u32 regAddress;

	regPtr = systemCal;
	flashRead(SYSCAL_ADDR, numSystemCalRegs);
	xil_printf("\r\nLoading system calibrations from flash\r\n");
	for (x=0; x<numSystemCalRegs; x++)
	{
		while (flashBusy());
		flashValue = Xil_In32(FLASH_READFIFO);
		xil_printf("\r\nFLASH_READFIFO 0x%08X\r\n", FLASH_READFIFO);
		if ((x == 0) && (flashValue == 0xFFFFFFFF))
		{
			x = numSystemCalRegs;
			xil_printf("\r\nSystem cal page of flash empty\r\n");
		}
		else
		{
			regAddress = *regPtr;
			if (flashValue == 0xFFFFFFFF)
			{
				regPtr++;
				xil_printf("\r\nInvalid value stored in flash, 0x%08X set to default\r\n", regAddress);
			}
			else
			{
				Xil_Out32(*regPtr++, flashValue);
				xil_printf("0x%08X written to address 0x%08X\r\n", flashValue, regAddress);
			}
		}
	}
}
void load_settings() {
	xil_printf("\r\nEntering load settings\r\n");
	const u32* regPtr;
	u32 x;
	u32 flashValue;
	u32 regAddress;

	regPtr = settings;
	flashRead(SETTING_ADDR, numSettings);
	xil_printf("\r\nLoading settings from flash\r\n");
	for (x=0; x<numSettings; x++)
	{
		while (flashBusy());
		flashValue = Xil_In32(FLASH_READFIFO);
		xil_printf("\r\nFLASH_READFIFO 0x%08X\r\n", FLASH_READFIFO);
		if ((x == 0) && (flashValue == 0xFFFFFFFF))
		{
			x = numSettings;
			xil_printf("\r\nSettings page of flash empty\r\n");
		}
		else
		{
			regAddress = *regPtr;
			if (flashValue == 0xFFFFFFFF)
			{
				regPtr++;
				xil_printf("\r\nInvalid value stored in flash, 0x%08X set to default\r\n", regAddress);
			}
			else
			{
				Xil_Out32(*regPtr++, flashValue);
				xil_printf("0x%08X written to address 0x%08X\r\n", flashValue, regAddress);
			}
		}
	}
}
void executeCommand(char * Str);
void respond(unsigned char * message);
int debug_rcvd()
{
	//xil_printf("\r\nEntering debug recvd\r\n");
	return Xil_In32(DEBUG_STS) & 0x00000001;
}
char debug_getchar()
{
	//xil_printf("\r\nEntering debug getchar\r\n");
	return Xil_In32(DEBUG_RX);
}
void debug_putchar(char c)
{
	//xil_printf("\r\nEntering debug putchar\r\n");
	Xil_Out32(DEBUG_TX, c);
}
int modbus_rcvd()
{
	//xil_printf("\r\nEntering modbus recvd\r\n");
	return Xil_In32(MODBUS_STS) & 0x00000001;
}
unsigned char modbus_getchar()
{
	//xil_printf("\r\nEntering MODBUS GETCHAR\r\n");
	return Xil_In32(MODBUS_RX);
}
void modbus_putchar(unsigned char c)
{
	// if TX FIFO is full, wait to send byte
	//xil_printf("\r\nEntering modbus putchar\r\n");
	while (Xil_In32(MODBUS_STS) & 0x00000008);
	Xil_Out32(MODBUS_TX, c);
}
void init_crcin()
{
	//xil_printf("\r\nEntering init crc in\r\n");
	Xil_Out32(INIT_CRCIN, 1);
}
void update_crcin(unsigned char val)
{
	//xil_printf("\r\nEntering update crc\r\n");
	Xil_Out32(UPDATE_CRCIN, (u32) val);
}
u16 get_crcin()
{
	return Xil_In32(READ_CRCIN);
}
void init_crcout()
{
	//xil_printf("\r\nEntering init crc out\r\n");
	Xil_Out32(INIT_CRCOUT, 1);
}
void update_crcout(unsigned char val)
{
	//xil_printf("\r\nEntering update crc out\r\n");
	Xil_Out32(UPDATE_CRCOUT, (u32) val);
}
u16 get_crcout()
{
	return Xil_In32(READ_CRCOUT);
}
void reset_modbustimer()
{
	//xil_printf("\r\nEntering reset modbus timer\r\n");
	Xil_Out32(CLR_TIMER, 1);
}
u32 read_modbustimer()
{
	return Xil_In32(READ_TIMER);
}
void respondModbus(unsigned char * message);
int main()
{
	//xil_printf("\r\nEntering respond modbus\r\n");
	int configFlag;

	// modbus state register
	// 0 = waiting for transmission
	// 1 = waiting for next byte
	int modbus_count = 0;
	int modbus_messagesize = 8;
	unsigned char modbus_packet[8];
	u16 crc = 0;

	char ch;
	u32 x;
	// read values stored in flash: device calibrations, system calibrations, and settings
	// do not restore from flash on an automatic reboot caused by reference loss
	if (Xil_In32(CONFIGSTS) & 0x8)
		Xil_Out32(CONFIGSTS, 0x9);
	else {
		load_devcal();
		load_syscal();
		load_settings();
		init_dds(Xil_In32(SYSTEMTYPE));
	}
	init_stepper(Xil_In32(SYSTEMTYPE));
	configFlag = -1;
	while (configFlag)
	{
		configFlag = init_pll0(Xil_In32(SYSTEMTYPE));
		configFlag |= init_pll1(Xil_In32(SYSTEMTYPE));
		sync_pll_ctrs();
		init_adc(Xil_In32(SYSTEMTYPE));
		sync(Xil_In32(SYSTEMTYPE));
		calc_feedFwd();
	}

	Xil_Out32(CONFIGSTS, 1);
	Xil_Out32(ILKCFG5, 0x7);		// PLL interlock configuration is overridden here so it can not be disabled unintentionally
	Xil_Out32(ILKRESET, 1);			// Clear latched interlocks (PLL)

	Xil_Out32(MODBUS_LOCAL, 0);

	xil_printf("\r\n>");
	x = 0;
	while(1) {
		// reference has been disconnected and reconnected -> reboot software to reinitialize PLLs
		if ((Xil_In32(ILKLATCH) & 0x20) && (Xil_In32(PLLLOCKED) == 0x3)) {
			xil_printf("\r\nReference loss detected, rebooting to reinitialize PLLs\r\n");
			Xil_Out32(CONFIGSTS, 0x9);
			Xil_Out32(SELFRESET, 1);
		}
		// if previous Modbus packet included "load_llrfcal" command, load device calibrations from flash
		if (Xil_In32(LOADDEVCAL)) {
			xil_printf("\r\nSwitching to local mode to load device calibrations\r\n");
			Xil_Out32(MODBUS_LOCALTIMER, Xil_In32(ADCCLOCKRATE) << 1);
			load_devcal();
			xil_printf("Exiting Modbus local mode in 2 seconds\r\n");
			Xil_Out32(LOADDEVCAL, 0);
		}
		// if previous Modbus packet included "load_llrfcal" command, load device calibrations from flash
		if (Xil_In32(LOADSYSCAL)) {
			xil_printf("\r\nSwitching to local mode to load system calibrations\r\n");
			Xil_Out32(MODBUS_LOCALTIMER, Xil_In32(ADCCLOCKRATE) << 1);
			load_syscal();
			xil_printf("Exiting Modbus local mode in 2 seconds\r\n");
			Xil_Out32(LOADSYSCAL, 0);
		}
		// if previous Modbus packet included "load_llrfcal" command, load device calibrations from flash
		if (Xil_In32(LOADSETTINGS)) {
			xil_printf("\r\nSwitching to local mode to load settings\r\n");
			Xil_Out32(MODBUS_LOCALTIMER, Xil_In32(ADCCLOCKRATE) << 1);
			load_settings();
			xil_printf("Exiting Modbus local mode in 2 seconds\r\n");
			Xil_Out32(LOADSETTINGS, 0);
		}
		// if previous Modbus packet included "save_llrfcal" command, save device calibrations to flash
		if (Xil_In32(SAVEDEVCAL)) {
			flashWriteEnable();
			flashEraseBlock(DEVCAL_ADDR);
			flashWriteEnable();
			xil_printf("\r\nBEFORE\r\n0x%08X\r\n0x%08X\r\n0x%08X\r\n",DEVCAL_ADDR, numDeviceCalRegs, deviceCal);
			flashProgramPages(DEVCAL_ADDR, numDeviceCalRegs, deviceCal);
			xil_printf("\r\nAFTER 0x%08X\r\n0x%08X\r\n0x%08X\r\n",DEVCAL_ADDR, numDeviceCalRegs, deviceCal);
			Xil_Out32(SAVEDEVCAL, 0);
			xil_printf("\r\nLLRF device calibrations saved to flash\r\n");
		}
		// if previous Modbus packet included "save_systemcal" command, save system calibrations to flash
		if (Xil_In32(SAVESYSCAL)) {
			flashWriteEnable();
			flashEraseBlock(SYSCAL_ADDR);
			flashWriteEnable();
			flashProgramPages(SYSCAL_ADDR, numSystemCalRegs,systemCal);
			Xil_Out32(SAVESYSCAL, 0);
			xil_printf("\r\nSystem calibrations saved to flash\r\n");
		}
		// if previous Modbus packet included "save_settings" command, save settings to flash
		if (Xil_In32(SAVESETTINGS)) {
			flashWriteEnable();
			flashEraseBlock(SETTING_ADDR);
			flashWriteEnable();
			flashProgramPages(SETTING_ADDR, numSettings, settings);
			Xil_Out32(SAVESETTINGS, 0);
			xil_printf("\r\nSettings saved to flash\r\n");
		}

		// if character is received via debug terminal, process it
		if (debug_rcvd())
		{
			ch = debug_getchar();
			if (ch != '\0') {
				if (x==0 && isspace((int) ch)) {
					if (ch == '\r') xil_printf(">");
				}
				else if (ch == BACKSPACE) {
					if (x!=0) x--;
					xil_printf("%c", SPACE);
					xil_printf("%c", BACKSPACE);
				}
				else if (ch == CARRIAGE_RETURN) { //interpret and execute command;
					executeCommand(&str[0]);
					x = 0;
				}
				else {
					if (ch == ' ' || ch == '\t') {
						if (x == 0) {} //do nothing
						else if (str[x-1] == ' ') {} //do nothing
						else str[x++] = ' ';
					}
					else str[x++] = ch;
				}
				str[x] = '\0';
			}
		}
		// byte received via Modbus link
		if (modbus_rcvd())
		{
			reset_modbustimer();
			if (modbus_count < modbus_messagesize)
			{
				modbus_packet[modbus_count] = modbus_getchar();
				if ((modbus_count == 6) && ((modbus_packet[1] == 15) || (modbus_packet[1] == 16)))
					modbus_messagesize = modbus_packet[6] + 9;
				if (modbus_count < modbus_messagesize - 2)
					update_crcin(modbus_packet[modbus_count]);
				modbus_count++;
			}
		}
		// If end of message, process it
		if (modbus_count == modbus_messagesize)
		{
			// check crc, if valid respond to message
			crc = (((u16) modbus_packet[modbus_messagesize-1]) << 8) | modbus_packet[modbus_messagesize-2];
			// only commands 1-6 and 15-16 are implemented so far
			// support for Modbus command 6 (write single holding register) has been removed
			if ((modbus_packet[0] == (unsigned char) Xil_In32(MODBUS_ADDR)) && (get_crcin() == crc) && (((modbus_packet[1] > 0) && (modbus_packet[1] < 6)) || (modbus_packet[1] == 15) || (modbus_packet[1] == 16)))
				respondModbus(modbus_packet);
			// prepare for next message
			modbus_count = 0;
			modbus_messagesize = 8;
			init_crcin();
		}
		// If receiving message and delay is too long between bytes, abort message
		else if ((modbus_count != modbus_messagesize) && modbus_count && (read_modbustimer() > MODBUSTIMEOUT))
		{
			modbus_count = 0;
			modbus_messagesize = 8;
			init_crcin();
		}
	}
}
// Returns the integer portion of fixed point value for printing
u32 fixed2Int(u32 fixed, u32 shift)
{
	return fixed >> shift;
}
// Returns the fractional portion (3 significant figures) of fixed point value for printing
u32 fixed2Fraction(u32 fixed, u32 shift)
{
	if (shift > 20)
		return (((fixed & (0xFFFFFFFF >> (32 - shift))) >> 10) * 1000) >> (shift - 10);
	else
		return ((fixed & (0xFFFFFFFF >> (32 - shift))) * 1000) >> shift;
}
void printPhase(u32 phase)
{
	u32 fixedVal, fixedInt, fixedFraction;
	fixedVal = ((float) phase) * 360.0 / 65536.0;
	fixedInt = fixed2Int(fixedVal, 16);
	fixedFraction = fixed2Fraction(fixedVal, 16);
	xil_printf("%D.%03D degrees", fixedInt, fixedFraction);
}
void printFixed(u32 fixed, u32 shift)
{
	xil_printf("%D.%03D", fixed2Int(fixed, shift), fixed2Fraction(fixed, shift));
}
void printTime(u32 time)
{
	u32 fixedInt, fixedFraction;
	fixedInt = time / Xil_In32(ADCCLOCKRATE);
	fixedFraction = (time - (float) fixedInt * Xil_In32(ADCCLOCKRATE)) * 1000.0 / Xil_In32(ADCCLOCKRATE);
	xil_printf("%D.%03D s", fixedInt, fixedFraction);
}
int getFixed(float val)
{
	u32 fixedInt, fixedFraction;
	if (val >= 65535)	return 0xFFFF0000;
	if (val < 0)		return 0x00000000;
	fixedInt = val;
	fixedFraction = (val - fixedInt) * 65536.0;
	return (fixedInt << 16) + fixedFraction;
}
void respondModbus(unsigned char * message)
{
	unsigned char i;
	unsigned char tempChar;
	u16 tempU16;
	u32 tempU32;
	int index;
	int count;
	u32 pointer;
	Xil_Out32(FREEZE, 1);									// freeze all read-only regs before processing any command
	// slave address and function
	init_crcout();
	update_crcout((unsigned char) Xil_In32(MODBUS_ADDR));
	modbus_putchar((unsigned char) Xil_In32(MODBUS_ADDR));
	update_crcout(message[1]);
	modbus_putchar(message[1]);
	index = (((int) message[2]) << 8) + message[3];
	count = (((int) message[4]) << 8) + message[5];
	switch (message[1])
	{
	// read coil status
	// all coils are implemented as 1-bit wide in firmware (so this code assumes the 31 upper bits are all 0)
	case 1:
		// send byte count byte first
		tempU16 = message[4];
		tempU16 = (tempU16 << 8) + message[5];
		tempChar = (tempU16 & 0x0007) ? ((tempU16 >> 3) + 1) : (tempU16 >> 3);
		update_crcout(tempChar);
		modbus_putchar(tempChar);
		// add data bits
		tempChar = 0;
		for (i=0; i<count; i++)
		{
			tempChar >>= 1;
			if (index < numCoils)
			{
				pointer = coils[index++];
				tempChar |= Xil_In32(pointer) << 7;
			}
			else
				tempChar |= 0x80;
			if ((i & 7) == 7)
			{
				update_crcout(tempChar);
				modbus_putchar(tempChar);
				tempChar = 0;
			}
		}
		i &= 7;
		if (i)
		{
			tempChar >>= 8 - i;
			update_crcout(tempChar);
			modbus_putchar(tempChar);
		}
		break;
	// read input status
	// all inputs are implemented as 1-bit wide in firmware (so this code assumes the 31 upper bits are all 0)
	case 2:
		// send byte count byte first
		tempU16 = message[4];
		tempU16 = (tempU16 << 8) + message[5];
		tempChar = (tempU16 & 0x0007) ? ((tempU16 >> 3) + 1) : (tempU16 >> 3);
		update_crcout(tempChar);
		modbus_putchar(tempChar);
		// add data bits
		tempChar = 0;
		for (i=0; i<count; i++)
		{
			tempChar >>= 1;
			if (index < numInputs)
			{
				pointer = inputs[index++];
				tempChar |= Xil_In32(pointer) << 7;
			}
			else
				tempChar |= 0x80;
			if ((i & 7) == 7)
			{
				update_crcout(tempChar);
				modbus_putchar(tempChar);
				tempChar = 0;
			}
		}
		i &= 7;
		if (i)
		{
			tempChar >>= 8 - i;
			update_crcout(tempChar);
			modbus_putchar(tempChar);
		}
		break;
	// read holding registers
	// 32-bit registers have the most significant bit of their address set to 1:
	// when accessing these registers, that bit must be set back to 0 and the data shifted right by 16 bits
	case 3:
		// send byte count byte first
		tempU16 = message[4];
		tempU16 = (tempU16 << 8) + message[5];
		tempU16 <<= 1;
		tempChar = (tempU16 > 254) ? 254 : tempU16;
		update_crcout(tempChar);
		modbus_putchar(tempChar);
		// add data bytes
		for (i=0; i<count; i++)
		{
			if (index < numHoldingRegs)
			{
				pointer = holdingRegs[index];
				if ((i == 0) || (pointer != (holdingRegs[index-1] & 0x7FFFFFFF)))
					tempU32 = scaleToPLC(Xil_In32(pointer & 0x7FFFFFFF), holdingRegsScale[index >> 1]);
			}
			else
				tempU32 = 0xFFFFFFFF;
			if (pointer & 0x80000000)
				tempU16 = tempU32 >> 16;
			else
				tempU16 = tempU32;
			index++;
			tempChar = tempU16 >> 8;
			update_crcout(tempChar);
			modbus_putchar(tempChar);
			tempChar = tempU16;
			update_crcout(tempChar);
			modbus_putchar(tempChar);
		}
		break;
	// read input registers
	// 32-bit registers have the most significant bit of their address set to 1 for the upper half-word:
	// when accessing these registers, that bit must be set back to 0 and the data shifted right by 16 bits
	case 4:
		// send byte count byte first
		tempU16 = message[4];
		tempU16 = (tempU16 << 8) + message[5];
		tempU16 <<= 1;
		tempChar = (tempU16 > 254) ? 254 : tempU16;
		update_crcout(tempChar);
		modbus_putchar(tempChar);
		// add data bytes
		for (i=0; i<count; i++)
		{
			if (index < numInputRegs)
			{
				pointer = inputRegs[index];
				if ((i == 0) || (pointer != (inputRegs[index-1] & 0x7FFFFFFF)))
					tempU32 = scaleToPLC(Xil_In32(pointer & 0x7FFFFFFF), inputRegsScale[index >> 1]);
			}
			else
				tempU32 = 0xFFFFFFFF;
			if (pointer & 0x80000000)
				tempU16 = tempU32 >> 16;
			else
				tempU16 = tempU32;
			index++;
			tempChar = tempU16 >> 8;
			update_crcout(tempChar);
			modbus_putchar(tempChar);
			tempChar = tempU16;
			update_crcout(tempChar);
			modbus_putchar(tempChar);
		}
		break;
	// force single coil
	// all coils are implemented as 1-bit wide in firmware, so read-modify-write is not necessary
	case 5:
		pointer = coils[index];
		tempU16 = (((int) message[4]) << 8) + message[5];
		// if command is valid, set the coil
		if ((index < numCoils) && !Xil_In32(MODBUS_LOCAL))
		{
			if (tempU16 == 0xFF00)
				Xil_Out32(pointer, 1);
			else if (tempU16 == 0x0000)
				Xil_Out32(pointer, 0);
		}
		for (i=2; i<6; i++)
		{
			update_crcout(message[(int) i]);
			modbus_putchar(message[(int) i]);
		}
		break;
	// all registers are 32-bit and most are scaled, so 16-bit writes are disabled
/*	// preset single register
	// 32-bit registers have the most significant bit of their address set to 1 for the upper half-word:
	// when accessing these registers, that bit must be set back to 0 and the data shifted left by 16 bits
	// read-modify-write is always used for all registers
	case 6:
		pointer = holdingRegs[index];
		tempU16 = (((int) message[4]) << 8) + message[5];
		if (index < numHoldingRegs)
		{
			if (pointer & 0x80000000)
				Xil_Out32(pointer & 0x7FFFFFFF, (Xil_In32(pointer & 0x7FFFFFFF) & 0x0000FFFF) | (((u32) tempU16) << 16));
			else
				Xil_Out32(pointer, (Xil_In32(pointer) & 0xFFFF0000) | tempU16);
		}
		for (i=2; i<6; i++)
		{
			update_crcout(message[(int) i]);
			modbus_putchar(message[(int) i]);
		}
		break;*/
	// force multiple coils
	// all coils are implemented as 1-bit wide in firmware, so read-modify-write is not necessary
	case 15:
		for (i=0; i<count; i++)
		{
			if ((index < numCoils) && !Xil_In32(MODBUS_LOCAL))
			{
				if ((i & 7) == 0)
					tempChar = message[7 + (i >> 3)];
				pointer = coils[index++];
				Xil_Out32(pointer, tempChar);
				tempChar >>= 1;
			}
		}
		for (i=2; i<6; i++)
		{
			update_crcout(message[(int) i]);
			modbus_putchar(message[(int) i]);
		}
		break;
	// preset multiple registers
	// 32-bit registers have the most significant bit of their address set to 1 for the upper half-word:
	// when accessing these registers, that bit must be set back to 0 and the data shifted left by 16 bits
	// read-modify-write is always used for all registers
	case 16:
		// this only supports writing full 32-bit registers, if the index points to the middle of a 32-bit value, skip the first register
		i=0;
		if (index & 0x00000001)
		{
			index++;
			i++;
		}
		// build the 32-bit value from 4 chars and scale before writing to the FPGA register
		for (; i+1<count; i++)
		{
			if ((index < numHoldingRegs) && !Xil_In32(MODBUS_LOCAL))
			{
				pointer = holdingRegs[index];
				index += 2;
				tempU32 = (((int) message[9 + (i << 1)]) << 24) + (((int) message[10 + (i << 1)]) << 16) + (((int) message[7 + (i << 1)]) << 8)  + message[8 + (i << 1)];
				tempU32 = scaleFromPLC(tempU32, holdingRegsScale[(index-1) >> 1]);
				if (i + 1 < count)
				{
					i++;
					Xil_Out32(pointer, tempU32);
				}
			}
		}
		for (i=2; i<6; i++)
		{
			update_crcout(message[(int) i]);
			modbus_putchar(message[(int) i]);
		}
		break;
	}
	// add crc to end of message
	tempU16 = get_crcout();
	modbus_putchar(tempU16);
	modbus_putchar(tempU16 >> 8);
	Xil_Out32(FREEZE, 0);									// unfreeze all read-only regs after processing any command
}
void executeCommand(char * Str)
{
	u32 i = 0;
	u32 range = 0;
	u32 fixedVal = 0;
	char * ptr[5];
	u32 arg1;
	u32 arg2;
	float setpointf;

	Xil_Out32(FREEZE, 1);									// freeze all read-only regs before processing any command
	xil_printf("%s\r\n", Str);
	ptr[i] = (char *)strtok(Str, " ");
	while ((ptr[i] != NULL) && (i<5)) {
		i = i + 1;
		ptr[i] = (char *) strtok(NULL, " ");
	}

	arg1 = strtoul(ptr[1], NULL, 0);
	setpointf = strtof(ptr[1], NULL);

	if (ptr[2] != NULL) arg2 = strtoul(ptr[2], NULL, 0);

	if (strcmp(ptr[0],"r")==0) {
		if (ptr[1] != NULL) {
			if (ptr[2] == NULL)
				range = 1;
			else
				range = arg2;
			for (i=0; i<range; i++) {
				arg2 = Xil_In32(arg1);
				xil_printf("\r\n0x%08X: 0x%08X %11D", arg1, arg2, arg2);
				arg1 += 4;
			}
		}
	}
	else if (strcmp(ptr[0],"w")==0) {
		if (ptr[1] != NULL && ptr[2] != NULL) {
			Xil_Out32(arg1,arg2);
			arg2 = Xil_In32(arg1);
			xil_printf("\r\n0x%08X: 0x%08X %11D", arg1, arg2, arg2);
		}
	}
	else if (strcmp(ptr[0], "sweeptuner")==0) {
		if (ptr[1] != NULL) {
			if (arg1 > 127)
				arg1 = 127;
			Xil_Out32(STPR_SWEEP_CNT, arg1);
			xil_printf("Sweeping tuner %D times", arg1);
		}
		else {
			Xil_Out32(STPR_SWEEP_CNT, 1);
			xil_printf("Sweeping tuner 1 time");
		}
		Xil_Out32(STPR_START_SWEEP, 1);
	}
	else if (strcmp(ptr[0], "rfon")==0) {
		Xil_Out32(EN_RF, 1);
		xil_printf("RF On");
	}
	else if (strcmp(ptr[0], "rfoff")==0) {
		Xil_Out32(DIS_RF, 1);
		xil_printf("RF Off");
	}
	else if (strcmp(ptr[0], "close")==0) {
		Xil_Out32(EN_CTRL, 1);
		xil_printf("Voltage closed loop");
	}
	else if (strcmp(ptr[0], "open")==0) {
		Xil_Out32(DIS_CTRL, 1);
		xil_printf("Voltage open loop");
	}
	else if (strcmp(ptr[0], "tuneron")==0) {
		Xil_Out32(EN_TUNER, 1);
		xil_printf("Tuner enabled");
	}
	else if (strcmp(ptr[0], "tuneroff")==0) {
		Xil_Out32(DIS_TUNER, 1);
		xil_printf("Tuner disabled");
	}
	else if (strcmp(ptr[0], "atten")==0) {
		if (ptr[1] != NULL) Xil_Out32(ATTEN, getFixed(setpointf));
		printFixed(Xil_In32(ATTEN), 16);
		xil_printf(" dB");
	}
	else if (strcmp(ptr[0], "freq")==0) {
		if (ptr[1] != NULL) Xil_Out32(FREQ, arg1);
		xil_printf("%D Hz", Xil_In32(FREQ));
	}
	else if (strcmp(ptr[0], "devcalfwd")==0) {
		if (ptr[1] != NULL) {
			Xil_Out32(UNLOCKCAL, 1);
			Xil_Out32(DEVCALFWD, arg1);
			Xil_Out32(UNLOCKCAL, 0);
		}
		xil_printf("%D", Xil_In32(DEVCALFWD));
	}
	else if (strcmp(ptr[0], "devcalrfl")==0) {
		if (ptr[1] != NULL) {
			Xil_Out32(UNLOCKCAL, 1);
			Xil_Out32(DEVCALRFL, arg1);
			Xil_Out32(UNLOCKCAL, 0);
		}
		xil_printf("%D", Xil_In32(DEVCALRFL));
	}
	else if (strcmp(ptr[0], "devcalcav")==0) {
		if (ptr[1] != NULL) {
			Xil_Out32(UNLOCKCAL, 1);
			Xil_Out32(DEVCALCAV, arg1);
			Xil_Out32(UNLOCKCAL, 0);
		}
		xil_printf("%D", Xil_In32(DEVCALCAV));
	}
	else if (strcmp(ptr[0], "devcalref")==0) {
		if (ptr[1] != NULL) {
			Xil_Out32(UNLOCKCAL, 1);
			Xil_Out32(DEVCALREF, arg1);
			Xil_Out32(UNLOCKCAL, 0);
		}
		xil_printf("%D", Xil_In32(DEVCALREF));
	}
	else if (strcmp(ptr[0], "devoffsetfwd")==0) {
		if (ptr[1] != NULL) {
			Xil_Out32(UNLOCKCAL, 1);
			Xil_Out32(DEVOFSTFWD, setpointf * 4294967296.0 / 360.0);
			Xil_Out32(UNLOCKCAL, 0);
		}
		printPhase(Xil_In32(DEVOFSTFWD));
	}
	else if (strcmp(ptr[0], "devoffsetrfl")==0) {
		if (ptr[1] != NULL) {
			Xil_Out32(UNLOCKCAL, 1);
			Xil_Out32(DEVOFSTRFL, setpointf * 4294967296.0 / 360.0);
			Xil_Out32(UNLOCKCAL, 0);
		}
		printPhase(Xil_In32(DEVOFSTRFL));
	}
	else if (strcmp(ptr[0], "devoffsetcav")==0) {
		if (ptr[1] != NULL) {
			Xil_Out32(UNLOCKCAL, 1);
			Xil_Out32(DEVOFSTCAV, setpointf * 4294967296.0 / 360.0);
			Xil_Out32(UNLOCKCAL, 0);
		}
		printPhase(Xil_In32(DEVOFSTCAV));
	}
	else if (strcmp(ptr[0], "calibratefwd")==0) {
		Xil_Out32(FREEZE, 0);
		xil_printf("Previous gain: %D   Amplitude: ", Xil_In32(DEVCALFWD));
		printFixed(Xil_In32(VFWDDEV), 24);
		xil_printf("\r\nPrevious offset: ");
		printPhase(Xil_In32(DEVOFSTFWD));
		xil_printf("   Phase: ");
		printPhase(Xil_In32(PFWDDEV));
		Xil_Out32(UNLOCKCAL, 1);
		Xil_Out32(DEVCALFWD, 1 + (u32) (16777216.0 * (float) Xil_In32(DEVCALFWD) / (float) Xil_In32(VFWDDEV)));
		Xil_Out32(DEVOFSTFWD, Xil_In32(DEVOFSTFWD) + Xil_In32(PSET) - Xil_In32(PFWDDEV));
		Xil_Out32(UNLOCKCAL, 0);
		xil_printf("\r\nNew gain: %D   Amplitude: ", Xil_In32(DEVCALFWD));
		printFixed(Xil_In32(VFWDDEV), 24);
		xil_printf("\r\nNew offset: ");
		printPhase(Xil_In32(DEVOFSTFWD));
		xil_printf("   Phase: ");
		printPhase(Xil_In32(PFWDDEV));
		Xil_Out32(FREEZE, 1);
	}
	else if (strcmp(ptr[0], "calibraterfl")==0) {
		Xil_Out32(FREEZE, 0);
		xil_printf("Previous gain: %D   Amplitude: ", Xil_In32(DEVCALRFL));
		printFixed(Xil_In32(VRFLDEV), 24);
		Xil_Out32(UNLOCKCAL, 1);
		Xil_Out32(DEVCALRFL, 1 + (u32) (16777216.0 * (float) Xil_In32(DEVCALRFL) / (float) Xil_In32(VRFLDEV)));
		Xil_Out32(UNLOCKCAL, 0);
		xil_printf("\r\nNew gain: %D   Amplitude: ", Xil_In32(DEVCALRFL));
		printFixed(Xil_In32(VRFLDEV), 24);
		Xil_Out32(FREEZE, 1);
	}
	else if (strcmp(ptr[0], "calibratecav")==0) {
		Xil_Out32(FREEZE, 0);
		xil_printf("Previous gain: %D   Amplitude: ", Xil_In32(DEVCALCAV));
		printFixed(Xil_In32(VCAVDEV), 24);
		xil_printf("\r\nPrevious offset: ");
		printPhase(Xil_In32(DEVOFSTCAV));
		xil_printf("   Phase: ");
		printPhase(Xil_In32(PCAVDEV));
		Xil_Out32(UNLOCKCAL, 1);
		Xil_Out32(DEVCALCAV, 1 + (u32) (16777216.0 * (float) Xil_In32(DEVCALCAV) / (float) Xil_In32(VCAVDEV)));
		Xil_Out32(DEVOFSTCAV, Xil_In32(DEVOFSTCAV) + Xil_In32(PSET) - Xil_In32(PCAVDEV));
		Xil_Out32(UNLOCKCAL, 0);
		xil_printf("\r\nNew gain: %D   Amplitude: ", Xil_In32(DEVCALCAV));
		printFixed(Xil_In32(VCAVDEV), 24);
		xil_printf("\r\nNew offset: ");
		printPhase(Xil_In32(DEVOFSTCAV));
		xil_printf("   Phase: ");
		printPhase(Xil_In32(PCAVDEV));
		Xil_Out32(FREEZE, 1);
	}
	else if (strcmp(ptr[0], "syscalfwd")==0) {
		if (ptr[1] != NULL) Xil_Out32(SYSCALFWD, getFixed(setpointf));
		printFixed(Xil_In32(SYSCALFWD), 16);
	}
	else if (strcmp(ptr[0], "syscalrfl")==0) {
		if (ptr[1] != NULL) Xil_Out32(SYSCALRFL, getFixed(setpointf));
		printFixed(Xil_In32(SYSCALRFL), 16);
	}
	else if (strcmp(ptr[0], "syscalcav")==0) {
		if (ptr[1] != NULL) Xil_Out32(SYSCALCAV, getFixed(setpointf));
		printFixed(Xil_In32(SYSCALCAV), 16);
	}
	else if (strcmp(ptr[0], "sysoffsetfwd")==0) {
		if (ptr[1] != NULL) Xil_Out32(SYSOFSTFWD, setpointf * 4294967296.0 / 360.0);
		printPhase(Xil_In32(SYSOFSTFWD));
	}
	else if (strcmp(ptr[0], "sysoffsetrfl")==0) {
		if (ptr[1] != NULL) Xil_Out32(SYSOFSTRFL, setpointf * 4294967296.0 / 360.0);
		printPhase(Xil_In32(SYSOFSTRFL));
	}
	else if (strcmp(ptr[0], "sysoffsetcav")==0) {
		if (ptr[1] != NULL) Xil_Out32(SYSOFSTCAV, setpointf * 4294967296.0 / 360.0);
		printPhase(Xil_In32(SYSOFSTCAV));
	}
	else if (strcmp(ptr[0], "fwdphase")==0) printPhase(Xil_In32(PFWDSYS));
	else if (strcmp(ptr[0], "rflphase")==0) printPhase(Xil_In32(PRFLSYS));
	else if (strcmp(ptr[0], "cavphase")==0) printPhase(Xil_In32(PCAVSYS));
	else if (strcmp(ptr[0], "refphase")==0) printPhase(Xil_In32(PREFSYS));
	else if (strcmp(ptr[0], "fwdphasedev")==0) printPhase(Xil_In32(PFWDDEV));
	else if (strcmp(ptr[0], "rflphasedev")==0) printPhase(Xil_In32(PRFLDEV));
	else if (strcmp(ptr[0], "cavphasedev")==0) printPhase(Xil_In32(PCAVDEV));
	else if (strcmp(ptr[0], "refphasedev")==0) printPhase(Xil_In32(PREFDEV));
	else if (strcmp(ptr[0], "fwdampl")==0) {
		printFixed(Xil_In32(VFWDSYS), 16);
		xil_printf(" Vp");
	}
	else if (strcmp(ptr[0], "rflampl")==0) {
		printFixed(Xil_In32(VRFLSYS), 16);
		xil_printf(" Vp");
	}
	else if (strcmp(ptr[0], "cavampl")==0) {
		printFixed(Xil_In32(VCAVSYS), 16);
		xil_printf(" Vp");
	}
	else if (strcmp(ptr[0], "refampl")==0) {
		printFixed(Xil_In32(VREFSYS), 16);
		xil_printf(" Vp");
	}
	else if (strcmp(ptr[0], "fwdampldev")==0) {
		printFixed(Xil_In32(VFWDDEV), 24);
		xil_printf(" Vp");
	}
	else if (strcmp(ptr[0], "rflampldev")==0) {
		printFixed(Xil_In32(VRFLDEV), 24);
		xil_printf(" Vp");
	}
	else if (strcmp(ptr[0], "cavampldev")==0) {
		printFixed(Xil_In32(VCAVDEV), 24);
		xil_printf(" Vp");
	}
	else if (strcmp(ptr[0], "setphase")==0) {
		if (ptr[1] != NULL) Xil_Out32(PSET, setpointf * 4294967296.0 / 360.0);
		printPhase(Xil_In32(PSET));
	}
	else if (strcmp(ptr[0], "setampl")==0) {
		if (ptr[1] != NULL) Xil_Out32(VSET, getFixed(setpointf));
		printFixed(Xil_In32(VSET), 16);
		xil_printf(" Vp");
	}
	else if (strcmp(ptr[0], "initampl")==0) {
		if (ptr[1] != NULL) Xil_Out32(INITVSET, getFixed(setpointf));
		printFixed(Xil_In32(INITVSET), 16);
		xil_printf(" Vp");
	}
	else if (strcmp(ptr[0], "ilkratemask")==0) {
		if (ptr[1] != NULL) Xil_Out32(ILKRATEMASK, arg1);
		xil_printf("%D", Xil_In32(ILKRATEMASK));
	}
	else if (strcmp(ptr[0], "ilkrateshift")==0) {
		if (ptr[1] != NULL) Xil_Out32(ILKRATESHIFT, arg1);
		xil_printf("%D", Xil_In32(ILKRATESHIFT));
	}
	else if (strcmp(ptr[0], "ilkratelimit")==0) {
		if (ptr[1] != NULL) Xil_Out32(ILKRATELIMIT, arg1);
		xil_printf("%D", Xil_In32(ILKRATELIMIT));
	}
	else if (strcmp(ptr[0], "cavlowampl")==0) {
		if (ptr[1] != NULL) Xil_Out32(CAVLOWAMPL, getFixed(setpointf));
		printFixed(Xil_In32(CAVLOWAMPL), 16);
		xil_printf(" Vp");
	}
	else if (strcmp(ptr[0], "cavlowtime")==0) {
		if (ptr[1] != NULL) Xil_Out32(CAVLOWTIME, setpointf * Xil_In32(ADCCLOCKRATE));
		printTime(Xil_In32(CAVLOWTIME));
	}
	else if (strcmp(ptr[0], "inittime")==0) {
		if (ptr[1] != NULL) Xil_Out32(INITTIME, setpointf * Xil_In32(ADCCLOCKRATE));
		printTime(Xil_In32(INITTIME));
	}
	else if (strcmp(ptr[0], "ramprate")==0) {
		if (ptr[1] != NULL) Xil_Out32(RAMPRATE, setpointf * 16777216.0 / Xil_In32(ADCCLOCKRATE));
		setpointf = (float) Xil_In32(RAMPRATE) * Xil_In32(ADCCLOCKRATE) / 16777216.0;
		arg2 = setpointf;
		xil_printf("%D.", arg2);
		if (arg2 == 0xFFFFFFFF)		xil_printf("000 V/s");
		else						xil_printf("%03D V/s", (int) ((setpointf - arg2) * 1000));
	}
	else if (strcmp(ptr[0], "fastresettime")==0) {
		if (ptr[1] != NULL) Xil_Out32(FASTRESETTIME, setpointf * Xil_In32(ADCCLOCKRATE));
		printTime(Xil_In32(FASTRESETTIME));
	}
	else if (strcmp(ptr[0], "slowresettime")==0) {
		if (ptr[1] != NULL) Xil_Out32(SLOWRESETTIME, setpointf * Xil_In32(ADCCLOCKRATE));
		printTime(Xil_In32(SLOWRESETTIME));
	}
	else if (strcmp(ptr[0], "feedfwdgain")==0) {
		if (ptr[1] != NULL) Xil_Out32(FEEDFWDGAIN, getFixed(setpointf));
		printFixed(Xil_In32(FEEDFWDGAIN), 16);
	}
	else if (strcmp(ptr[0], "feedfwdphase")==0) {
		if (ptr[1] != NULL) Xil_Out32(FEEDFWDPHASE, setpointf * 4294967296.0 / 360.0);
		printPhase(Xil_In32(FEEDFWDPHASE));
	}
	else if (strcmp(ptr[0], "fwdrflratio1")==0) {
		if (ptr[1] != NULL) Xil_Out32(FWDRFLRATIO1, getFixed(setpointf));
		printFixed(Xil_In32(FWDRFLRATIO1), 16);
		xil_printf(":1");
	}
	else if (strcmp(ptr[0], "fwdrflratio2")==0) {
		if (ptr[1] != NULL) Xil_Out32(FWDRFLRATIO2, getFixed(setpointf));
		printFixed(Xil_In32(FWDRFLRATIO2), 16);
		xil_printf(":1");
	}
	else if (strcmp(ptr[0], "initfwdrflratio")==0) {
		if (ptr[1] != NULL) Xil_Out32(INITFWDRFLRATIO, getFixed(setpointf));
		printFixed(Xil_In32(INITFWDRFLRATIO), 16);
		xil_printf(":1");
	}
	else if (strcmp(ptr[0], "dvdtrate")==0) {
		if (ptr[1] != NULL) Xil_Out32(DVDTRATE, setpointf *1900544.0 / Xil_In32(ADCCLOCKRATE));
		setpointf = (float) Xil_In32(DVDTRATE) * Xil_In32(ADCCLOCKRATE) * 5.26165140086207E-7;
		arg2 = setpointf;
		xil_printf("%D.", arg2);
		if (arg2 == 0xFFFFFFFF)		xil_printf("000 V/s");
		else						xil_printf("%03D V/s", (int) ((setpointf - arg2) * 1000));
	}
	else if (strcmp(ptr[0], "fwdrfltime1")==0) {
		if (ptr[1] != NULL) Xil_Out32(FWDRFLTIME1, setpointf * Xil_In32(ADCCLOCKRATE));
		printTime(Xil_In32(FWDRFLTIME1));
	}
	else if (strcmp(ptr[0], "fwdrfltime2")==0) {
		if (ptr[1] != NULL) Xil_Out32(FWDRFLTIME2, setpointf * Xil_In32(ADCCLOCKRATE));
		printTime(Xil_In32(FWDRFLTIME2));
	}
	else if (strcmp(ptr[0], "initfwdrfltime")==0) {
		if (ptr[1] != NULL) Xil_Out32(INITFWDRFLTIME, setpointf * Xil_In32(ADCCLOCKRATE));
		printTime(Xil_In32(INITFWDRFLTIME));
	}
	else if (strcmp(ptr[0], "fwdrflminrfl")==0) {
		if (ptr[1] != NULL) Xil_Out32(FWDRFLMINRFL, getFixed(setpointf));
		printFixed(Xil_In32(FWDRFLMINRFL), 16);
		xil_printf(" Vp");
	}
	else if (strcmp(ptr[0], "cavlowminfwd")==0) {
		if (ptr[1] != NULL) Xil_Out32(CAVLOWMINFWD, getFixed(setpointf));
		printFixed(Xil_In32(CAVLOWMINFWD), 16);
		xil_printf(" Vp");
	}
	else if (strcmp(ptr[0], "ilkconfig0")==0) {
		if (ptr[1] != NULL) Xil_Out32(ILKCFG0, arg1);
		xil_printf("%D", Xil_In32(ILKCFG0));
	}
	else if (strcmp(ptr[0], "ilkconfig1")==0) {
		if (ptr[1] != NULL) Xil_Out32(ILKCFG1, arg1);
		xil_printf("%D", Xil_In32(ILKCFG1));
	}
	else if (strcmp(ptr[0], "ilkconfig2")==0) {
		if (ptr[1] != NULL) Xil_Out32(ILKCFG2, arg1);
		xil_printf("%D", Xil_In32(ILKCFG2));
	}
	else if (strcmp(ptr[0], "ilkconfig3")==0) {
		if (ptr[1] != NULL) Xil_Out32(ILKCFG3, arg1);
		xil_printf("%D", Xil_In32(ILKCFG3));
	}
	else if (strcmp(ptr[0], "ilkconfig4")==0) {
		if (ptr[1] != NULL) Xil_Out32(ILKCFG4, arg1);
		xil_printf("%D", Xil_In32(ILKCFG4));
	}
	else if (strcmp(ptr[0], "ilkconfig5")==0) {
		if (ptr[1] != NULL) Xil_Out32(ILKCFG5, arg1);
		xil_printf("%D", Xil_In32(ILKCFG5));
	}
	else if (strcmp(ptr[0], "ilkconfig6")==0) {
		if (ptr[1] != NULL) Xil_Out32(ILKCFG6, arg1);
		xil_printf("%D", Xil_In32(ILKCFG6));
	}
	else if (strcmp(ptr[0], "ilkconfig7")==0) {
		if (ptr[1] != NULL) Xil_Out32(ILKCFG7, arg1);
		xil_printf("%D", Xil_In32(ILKCFG7));
	}
	else if (strcmp(ptr[0], "fwdcavphase")==0) {
		if (ptr[1] != NULL) Xil_Out32(PFWDCAV, setpointf * 4294967296.0 / 360.0);
		printPhase(Xil_In32(PFWDCAV));
	}
	else if (strcmp(ptr[0], "tunerdeadzone")==0) {
		if (ptr[1] != NULL) Xil_Out32(DEADZONE, setpointf * 4294967296.0 / 360.0);
		printPhase(Xil_In32(DEADZONE));
	}
	else if (strcmp(ptr[0], "tunerdelay")==0) {
		if (ptr[1] != NULL) Xil_Out32(HOMEDELAY, setpointf * Xil_In32(ADCCLOCKRATE));
		printTime(Xil_In32(HOMEDELAY));
	}
	else if (strcmp(ptr[0], "findhomeampl")==0) {
		if (ptr[1] != NULL) Xil_Out32(FINDHOMEAMPL, getFixed(setpointf));
		printFixed(Xil_In32(FINDHOMEAMPL), 16);
		xil_printf(" Vp");
	}
	else if (strcmp(ptr[0], "modbusaddr")==0) {
		if (ptr[1] != NULL) Xil_Out32(MODBUS_ADDR, arg1);
		xil_printf("Modbus address set to %D", Xil_In32(MODBUS_ADDR));
	}
	else if (strcmp(ptr[0], "loaddevcal")==0) {
		xil_printf("\r\nSwitching to local mode to load device calibrations");
		Xil_Out32(MODBUS_LOCALTIMER, Xil_In32(ADCCLOCKRATE) << 1);
		load_devcal();
		xil_printf("Exiting Modbus local mode in 2 seconds\r\n");
	}
	else if (strcmp(ptr[0], "loadsyscal")==0) {
		xil_printf("\r\nSwitching to local mode to load system calibrations");
		Xil_Out32(MODBUS_LOCALTIMER, Xil_In32(ADCCLOCKRATE) << 1);
		load_syscal();
		xil_printf("Exiting Modbus local mode in 2 seconds\r\n");
	}
	else if (strcmp(ptr[0], "loadsettings")==0) {
		xil_printf("\r\nSwitching to local mode to load settings");
		Xil_Out32(MODBUS_LOCALTIMER, Xil_In32(ADCCLOCKRATE) << 1);
		load_settings();
		xil_printf("Exiting Modbus local mode in 2 seconds\r\n");
	}
	else if (strcmp(ptr[0], "savedevcal")==0) {
		flashWriteEnable();
		flashEraseBlock(DEVCAL_ADDR);
		flashWriteEnable();
		flashProgramPages(DEVCAL_ADDR, numDeviceCalRegs, deviceCal);
		xil_printf("\r\nLLRF device calibrations saved to flash");
	}
	else if (strcmp(ptr[0], "savesyscal")==0) {
		flashWriteEnable();
		flashEraseBlock(SYSCAL_ADDR);
		flashWriteEnable();
		flashProgramPages(SYSCAL_ADDR, numSystemCalRegs,systemCal);
		xil_printf("\r\nSystem calibrations saved to flash");
	}
	else if (strcmp(ptr[0], "savesettings")==0) {
		flashWriteEnable();
		flashEraseBlock(SETTING_ADDR);
		flashWriteEnable();
		flashProgramPages(SETTING_ADDR, numSettings, settings);
		xil_printf("\r\nSettings saved to flash");
	}
	else if ((strcmp(ptr[0], "status")==0) || (strcmp(ptr[0], "s")==0)) {
		xil_printf("Vcav=");
		printFixed(Xil_In32(VCAVSYS), 16);
		xil_printf(" Vp   Vfwd=");
		printFixed(Xil_In32(VFWDSYS), 16);
		xil_printf(" Vp   Vrfl=");
		printFixed(Xil_In32(VRFLSYS), 16);
		xil_printf(" Vp   V+/V-=");
		fixedVal = 65536.0 * (float) Xil_In32(VFWDSYS) / (float) Xil_In32(VRFLSYS);
		printFixed(fixedVal, 16);
		xil_printf(":1\r\nV+/V-(1) counter=%D   V+/V-(2) counter=%D   Cav low counter=%D\r\nPfwd=", Xil_In32(ILKCTR1) >> 16,Xil_In32(ILKCTR2) >> 16, Xil_In32(ILKCTR0) >> 16);
		printFixed((u32) ((float) Xil_In32(VFWDSYS) * (float) Xil_In32(VFWDSYS) / 6553600.0), 16);
		xil_printf(" W   Prfl=");
		printFixed((u32) ((float) Xil_In32(VRFLSYS) * (float) Xil_In32(VRFLSYS) / 6553600.0), 16);
		xil_printf(" W   Tuner pos=%D", Xil_In32(STPR_POS));
		if (Xil_In32(FINDINGHOME))
			xil_printf("\r\nLLRF is finding tuner home position");
	}
	else if (strcmp(ptr[0], "max")==0) {
		xil_printf("Maximum safe fwd channel input: ");
		printFixed(Xil_In32(MAXFWDSYS), 16);
		xil_printf(" Vp\r\nMaximum safe rfl channel input: ");
		printFixed(Xil_In32(MAXRFLSYS), 16);
		xil_printf(" Vp\r\nMaximum safe cav channel input: ");
		printFixed(Xil_In32(MAXCAVSYS), 16);
		xil_printf(" Vp");
	}
	else if (strcmp(ptr[0], "systemtype")==0) {
		if (ptr[1] != NULL) Xil_Out32(SYSTEMTYPE, arg1);
		xil_printf("%D", Xil_In32(SYSTEMTYPE));
	}
	else if (strcmp(ptr[0], "reboot")==0) {
		Xil_Out32(FREEZE, 0);									// unfreeze all read-only regs after processing any command
		xil_printf("\r\nRebooting\r\n");
		Xil_Out32(SELFRESET, 1);
	}
	else if (strcmp(ptr[0], "local")==0) {
		Xil_Out32(MODBUS_LOCAL, 1);
		xil_printf("\r\nModbus connection is in local mode\r\n");
	}
	else if (strcmp(ptr[0], "remote")==0) {
		Xil_Out32(MODBUS_LOCAL, 0);
		xil_printf("\r\nModbus connection is in remote mode\r\n");
	}
	else xil_printf("Invalid command");
	xil_printf("\r\n>");
	Xil_Out32(FREEZE, 0);									// unfreeze all read-only regs after processing any command
}
