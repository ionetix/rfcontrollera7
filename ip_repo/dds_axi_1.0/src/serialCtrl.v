`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Engineer: 		Nathan Usher
// Create Date:    17:51:05 02/19/2013 
// Module Name:    serialCtrl 
// Description: 	Serial port driver for AD9912A DDS
// Additional Comments: 
//		Currently implemented as write-only, msb-first
//////////////////////////////////////////////////////////////////////////////////

module serialCtrl(
	input clk,						// FPGA logic clock - 50 MHz
	input cmdValid,				// high when data and size are valid
	input [47:0] cmd,				// command: upper 8 bits = size of data in bits, next 16 bits = address, remainder = data
	output reg done = 0,			// pulses high when transfer is complete
	output reg sclk=0,			// serial clock to AD9912
	output reg csb = 1,			// active-low chip enable to AD9912
	output sdio);					// data to AD9912

	reg [1:0] state = 0;
	reg [5:0] ctr = 0;
	reg [39:0] shifter = 0;
	reg div = 1;
	assign sdio = shifter[39];
	
	always @ (posedge clk)
	begin
	done<=div && (state==2);
	div <= ~div;
	   if (div)
	   begin
		case (state)
			// idle, waiting for next command to AD9912A
			0:
			begin
				
				ctr <= 0;
				sclk <= 0;
				if (cmdValid)
				begin
					csb <= 0;
					shifter <= cmd[39:0];
					state <= state + 1;
				end
				else
				begin
					csb <= 1;
					shifter <= 0;
				end
			end
			// clocking data out to AD9912A
			1:
			begin
				
				sclk <= ~sclk;
				if (sclk)
				begin
					ctr <= ctr + 1;
					shifter <= {shifter[38:0], 1'b0};
					if (ctr == cmd[47:40] - 1)
					begin
						state <= state + 1;
						csb <= 1;
					end
					else
						csb <= 0;
				end
			end
			// remove the command from the FIFO by pulsing done
			2:
			begin
				
				ctr <= 0;
				sclk <= 0;
				csb <= 1;
				shifter <= 0;
				state <= state + 1;
			end
			// return to idle state
			3:
			begin
				
				ctr <= 0;
				sclk <= 0;
				csb <= 1;
				shifter <= 0;
				state <= state + 1;
			end
		endcase
	   end
	end
	
        	 
	
endmodule
