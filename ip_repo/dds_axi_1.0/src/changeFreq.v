`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Engineer: 		Nathan Usher
// Create Date:    18:13:52 02/20/2013 
// Module Name:    changeFreq 
// Description: 	updates freqency and tuning word after increment command
// Additional Comments: 
//		default frequency after power on is 90 MHz
//////////////////////////////////////////////////////////////////////////////////
module changeFreq(
	input clk,
	input incInc,									// increase increment size
	input decInc,									// decrease increment size
	input inc,										// increment frequency
	input dec,										// decrement frequency
	input [28:0] min,								// minimum frequency
	input [28:0] max,								// maximum frequency
	input [28:0] start,							// start frequency
	input reset,									// reset 'freq' to 'start'
	output reg [28:0] freq = 68000000,		// frequency in Hz
	output reg [19:0] step,						// increment size in Hz / step
	output [47:0] ftw								// frequency tuning word for DDS
	);

	reg [2:0] stepIndex = 3;
	always @ (posedge clk)
	begin
		case (stepIndex)
			0:			step <= 1;
			1:			step <= 10;
			2:			step <= 100;
			3:			step <= 1000;
			4:			step <= 10000;
			default:	step <= 100000;
		endcase
		// incInc increases the increment size by factor of 10
		if (incInc & ~&stepIndex[2:1])
			stepIndex <= stepIndex + 1;
		// decInc decreases the increment size by factor of 10
		else if (decInc & |stepIndex)
			stepIndex <= stepIndex - 1;
		
		// reset sets current frequency back to the start frequency
		if (reset)
			freq <= start;
		else
		begin
			// inc increases frequency by increment size
			if (inc)
			begin
				if ((freq + step) < max)			freq <= freq + step;
				else										freq <= max;
			end
			// dec decreases frequency by increment size
			else if (dec)
			begin
				if ((freq - step) > min)			freq <= freq - step;
				else										freq <= min;
			end
		end
	end
	
	ftwMultiplier calcFtw (.clk(clk), .a(freq), .p(ftw));
endmodule
