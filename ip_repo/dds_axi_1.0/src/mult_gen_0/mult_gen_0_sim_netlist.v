// Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2016.4 (win64) Build 1733598 Wed Dec 14 22:35:39 MST 2016
// Date        : Tue Dec 12 12:48:44 2017
// Host        : SHRIRAJKUNJA80D running 64-bit Service Pack 1  (build 7601)
// Command     : write_verilog -force -mode funcsim
//               c:/Users/shrirajkunjir/ip_repo/dds_axi_1.0/src/mult_gen_0/mult_gen_0_sim_netlist.v
// Design      : mult_gen_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a200tfbg484-2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "mult_gen_0,mult_gen_v12_0_12,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "mult_gen_v12_0_12,Vivado 2016.4" *) 
(* NotValidForBitStream *)
module mult_gen_0
   (CLK,
    A,
    P);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 a_intf DATA" *) input [28:0]A;
  (* x_interface_info = "xilinx.com:signal:data:1.0 p_intf DATA" *) output [47:0]P;

  wire [28:0]A;
  wire CLK;
  wire [47:0]P;
  wire [47:0]NLW_U0_PCASC_UNCONNECTED;
  wire [1:0]NLW_U0_ZERO_DETECT_UNCONNECTED;

  (* C_A_TYPE = "0" *) 
  (* C_A_WIDTH = "29" *) 
  (* C_B_TYPE = "1" *) 
  (* C_B_VALUE = "10000001" *) 
  (* C_B_WIDTH = "8" *) 
  (* C_CCM_IMP = "0" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_ZERO_DETECT = "0" *) 
  (* C_LATENCY = "1" *) 
  (* C_MODEL_TYPE = "0" *) 
  (* C_MULT_TYPE = "2" *) 
  (* C_OPTIMIZE_GOAL = "1" *) 
  (* C_OUT_HIGH = "47" *) 
  (* C_OUT_LOW = "0" *) 
  (* C_ROUND_OUTPUT = "0" *) 
  (* C_ROUND_PT = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  mult_gen_0_mult_gen_v12_0_12 U0
       (.A(A),
        .B({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .P(P),
        .PCASC(NLW_U0_PCASC_UNCONNECTED[47:0]),
        .SCLR(1'b0),
        .ZERO_DETECT(NLW_U0_ZERO_DETECT_UNCONNECTED[1:0]));
endmodule

(* C_A_TYPE = "0" *) (* C_A_WIDTH = "29" *) (* C_B_TYPE = "1" *) 
(* C_B_VALUE = "10000001" *) (* C_B_WIDTH = "8" *) (* C_CCM_IMP = "0" *) 
(* C_CE_OVERRIDES_SCLR = "0" *) (* C_HAS_CE = "0" *) (* C_HAS_SCLR = "0" *) 
(* C_HAS_ZERO_DETECT = "0" *) (* C_LATENCY = "1" *) (* C_MODEL_TYPE = "0" *) 
(* C_MULT_TYPE = "2" *) (* C_OPTIMIZE_GOAL = "1" *) (* C_OUT_HIGH = "47" *) 
(* C_OUT_LOW = "0" *) (* C_ROUND_OUTPUT = "0" *) (* C_ROUND_PT = "0" *) 
(* C_VERBOSITY = "0" *) (* C_XDEVICEFAMILY = "artix7" *) (* ORIG_REF_NAME = "mult_gen_v12_0_12" *) 
(* downgradeipidentifiedwarnings = "yes" *) 
module mult_gen_0_mult_gen_v12_0_12
   (CLK,
    A,
    B,
    CE,
    SCLR,
    ZERO_DETECT,
    P,
    PCASC);
  input CLK;
  input [28:0]A;
  input [7:0]B;
  input CE;
  input SCLR;
  output [1:0]ZERO_DETECT;
  output [47:0]P;
  output [47:0]PCASC;

  wire \<const0> ;
  wire [28:0]A;
  wire CLK;
  wire [36:0]\^P ;
  wire [46:36]NLW_i_mult_P_UNCONNECTED;
  wire [47:0]NLW_i_mult_PCASC_UNCONNECTED;
  wire [1:0]NLW_i_mult_ZERO_DETECT_UNCONNECTED;

  assign P[47] = \^P [36];
  assign P[46] = \^P [36];
  assign P[45] = \^P [36];
  assign P[44] = \^P [36];
  assign P[43] = \^P [36];
  assign P[42] = \^P [36];
  assign P[41] = \^P [36];
  assign P[40] = \^P [36];
  assign P[39] = \^P [36];
  assign P[38] = \^P [36];
  assign P[37] = \^P [36];
  assign P[36:0] = \^P [36:0];
  assign PCASC[47] = \<const0> ;
  assign PCASC[46] = \<const0> ;
  assign PCASC[45] = \<const0> ;
  assign PCASC[44] = \<const0> ;
  assign PCASC[43] = \<const0> ;
  assign PCASC[42] = \<const0> ;
  assign PCASC[41] = \<const0> ;
  assign PCASC[40] = \<const0> ;
  assign PCASC[39] = \<const0> ;
  assign PCASC[38] = \<const0> ;
  assign PCASC[37] = \<const0> ;
  assign PCASC[36] = \<const0> ;
  assign PCASC[35] = \<const0> ;
  assign PCASC[34] = \<const0> ;
  assign PCASC[33] = \<const0> ;
  assign PCASC[32] = \<const0> ;
  assign PCASC[31] = \<const0> ;
  assign PCASC[30] = \<const0> ;
  assign PCASC[29] = \<const0> ;
  assign PCASC[28] = \<const0> ;
  assign PCASC[27] = \<const0> ;
  assign PCASC[26] = \<const0> ;
  assign PCASC[25] = \<const0> ;
  assign PCASC[24] = \<const0> ;
  assign PCASC[23] = \<const0> ;
  assign PCASC[22] = \<const0> ;
  assign PCASC[21] = \<const0> ;
  assign PCASC[20] = \<const0> ;
  assign PCASC[19] = \<const0> ;
  assign PCASC[18] = \<const0> ;
  assign PCASC[17] = \<const0> ;
  assign PCASC[16] = \<const0> ;
  assign PCASC[15] = \<const0> ;
  assign PCASC[14] = \<const0> ;
  assign PCASC[13] = \<const0> ;
  assign PCASC[12] = \<const0> ;
  assign PCASC[11] = \<const0> ;
  assign PCASC[10] = \<const0> ;
  assign PCASC[9] = \<const0> ;
  assign PCASC[8] = \<const0> ;
  assign PCASC[7] = \<const0> ;
  assign PCASC[6] = \<const0> ;
  assign PCASC[5] = \<const0> ;
  assign PCASC[4] = \<const0> ;
  assign PCASC[3] = \<const0> ;
  assign PCASC[2] = \<const0> ;
  assign PCASC[1] = \<const0> ;
  assign PCASC[0] = \<const0> ;
  assign ZERO_DETECT[1] = \<const0> ;
  assign ZERO_DETECT[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  (* C_A_TYPE = "0" *) 
  (* C_A_WIDTH = "29" *) 
  (* C_B_TYPE = "1" *) 
  (* C_B_VALUE = "10000001" *) 
  (* C_B_WIDTH = "8" *) 
  (* C_CCM_IMP = "0" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_ZERO_DETECT = "0" *) 
  (* C_LATENCY = "1" *) 
  (* C_MODEL_TYPE = "0" *) 
  (* C_MULT_TYPE = "2" *) 
  (* C_OPTIMIZE_GOAL = "1" *) 
  (* C_OUT_HIGH = "47" *) 
  (* C_OUT_LOW = "0" *) 
  (* C_ROUND_OUTPUT = "0" *) 
  (* C_ROUND_PT = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  mult_gen_0_mult_gen_v12_0_12_viv i_mult
       (.A(A),
        .B({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .P({\^P [36],NLW_i_mult_P_UNCONNECTED[46:36],\^P [35:0]}),
        .PCASC(NLW_i_mult_PCASC_UNCONNECTED[47:0]),
        .SCLR(1'b0),
        .ZERO_DETECT(NLW_i_mult_ZERO_DETECT_UNCONNECTED[1:0]));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2015"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
fPF16TcpNgM9dNC6nyb4WjUK+7bY8P+I62AEEiiM/KOMhIKuPOHBoWeWL2UjxSNO68WLeYIZp8lA
I7rHN/CieA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
E6OKJxjnDRUVVFwAhrQMAtoyRVVpuMKsXlca4m9CcIt6QI8vnYN0tf7gH3uVuxZ90322B7kUeFw5
Pu0UeqAoBaSyysHuDqXazxHy7oyk4BIWChvcrp7LULlVLcL76obtSwsXi1ORVmpdTi5b+AcD+WUo
OP1PSFj5jpodG+LwXm4=

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
x+agogSsgbiI6PGyBpMY8RQCDzLctIr3EaG23mH5kJHlNmNKNolnP54yJ8Y7nIFi6yl6tlyOLMoF
/kxU0pyFmIj8QM0/MArMxPTiemXbDLS2VKtonyK9dDH7VbjFnRWwzK0Ngkas0+nbW3TqGPAY98x3
251QPjQoZCw3A7W9PDc=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
KNs7hA49BKKrboRSEkqIGldOa3ndCnhjRkSn8lL1xFfKUn+p+Wbc09ogKV6YYnPU/RaF1LbzyoE4
udPSNea4bST+08IjO5GAxXqUugcig44J+hzpGKmh7oO0TuyNbYq1CnYcsZXaD9vsmNYz8fBDoW2S
VK/mYa21mBKTOuTdQ1yp3wi73aJ1G9N6Ngt7ovDUrjyd5oNxxNlvWU8JkJDinbEnci0qjZ3Wu9Wg
y44pHUXf6xqwFYJpZ1ZcGRKl83P8p74+pLzt19lw9TPlTfKI++IowVjb6wo36ztNDJS0QjQE5Riv
hwbPU/Bt3S82MVCY5NAA6bKC/8NnoWMbmX8Wiw==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
QaRubtGbYrmCghuFdQuTgTEtoVYYLcPnD5z0C7mo18fwCG17qy0y8mj8xWiwE6bo49IP1/JXSIw7
rTBwHFOVrmbm926sWNrF1r3IHB83C5cstprQ1om7vnkw9XX87SjkscphhkrHmi08jjzW4qX96m61
/ymclz5TlAocMQJGz/jwscvIMOrrbuH4SkWQOLQnRfx9GIOv5Y7PM+w/wuDSeFXsAXz7Ahq3/qmU
cylNfSufW7/zfN4RZB4u+d28AXsuFe03aSF1dpW+uBK1xtNZccvj9h9NMN0cuwxt8ZUlLJw8l6e2
hqRfTTZl1F4qnnrJu6w8h8uEGrmgnQG1AW0epg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinx_2016_05", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XXj6Nc59BeA5Kznlx14IKravf7ohERw7h0fbO7pT7/HsiPDCWh2DlTGpFUcnbNZslPN2RfE0nJNX
WMzLQtaHK4Bm6kxY71OsXEKm7MAIjEdLwOMtJTtlZrbm7chBbSxcW6sjWvI36jk5De3Yct9Ao1py
DpQ9NICUtRTwGG8SAiRkAXRh2Jv3rKvnookQrlVxIkNRSBMSgbwuTbq1ze/KMUZebBWwJNUVIC9r
RV/i9wjYXBOeCCUk+cGDC5uSpwdLXYV9ZxhQUU6C1ufAaK2m4OIUeBqPc2ski2O0qQYQ67c35k50
ynO8H9PTEROPEOn5c37S7feU+36OcOOAsVBTBA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="base64", line_length=76, bytes=256)
`pragma protect key_block
UvDe227NQRCZLgBusyH7BKjHQEnC383UBBTaWFiRUBHxvHTOwLDrhtymc5Bd4OCBUdAd/iBmzfxo
kx98ZSVCS0ETXU9wU64dyOKWMjp+yp1Er0pZvVjSxfA4Sjs3Huf7CgqgYNhcNUuNB3PXYcbbbQ4i
YclOdJ9jbrOrxInSUeAPpFELVlbZ33WsLe0fe2RM2yEaPvs4A5Yu/v+6k37jipsGJsWAYo4PbHKt
6NxEL648ULYDuaTga7gCcaZ5JKCuJDqmC44BPSMNsUwaXoA+7frfK6GeuQLwnDpXelmt+JfS8lCx
mOj0IcejE3Fqw4v3iU0aieI+xM6hWNhIEtUFRA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP05_001", key_method="rsa"
`pragma protect encoding = (enctype="base64", line_length=76, bytes=256)
`pragma protect key_block
vLpP6Eq5RvABlkwlTSTPShxLK+vMt8gR7V9YLwCqkyJINzs8Z+vZN1kP8ZCDqhnKVNTjrXbQp+O4
XO1Kj2Olwa1H+fSQGM/J3hlJdphYtyyE0DirQUOqG5yTneyM1IEoVIt9QURWA1CfTyA0pu8MLkHs
PHJ4G0786mwXzINgFgJ1CDJpOKIXnmleNS9QKpRbgo3kEUyaAVnTewPdtLSn3VUBb36TkJEOkBhJ
tOT8UVHZxPFRKiOj77kuwKs1pAcNBO872ENM00Ok6KYVp5G8bi7s1W/GLBuMNTmu8xr3QSteA6IY
OlIWtITM9LAQaR4jNAomYZFVMYct+TFkDQDGBQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 34592)
`pragma protect data_block
ZO4vqziXC/u8Oq3HCMaZqp+yqtajqagLYdyf84M7fhBYp+L7yL4jhgSlLVTn0NQnnxFkqdQaDmp+
HZY2VeKHSCG37tRdaVI8NeNFkIeq6YfwUbFTEQdiLXuXlHQl0iO0FRtASTjZnDbpc7vJYNt/JBp7
q+nIKTQ4kN1Aji4kVySaWkVg6OkqfsBdb5mJ9x5Q6+uObF78X5YLrBnCb5gul4jFGm+FlCOhfP0k
4O2yhnVrQKmI2GF7XsWziLsazeDoVuEW8QvSDo7nvUVgjryX6QAalX/scIXF5cbsKN7E0QRRwcn8
MPNLEze18nbdHo1SJB4UoT+WEKKJtVmUspc6Ps4HHsfFt1o3ptai85OrdiALW0oLNw0ksVwxl4mi
XmOo72dxXu57Pxg1o2DtEzNPwXk0Ce+hb65jQds9SLccu53GQ5riTOhuhJZBx33ctXoY0TmnxdJW
eV4Er0n0MB3RVohxRqwzYXRHve2TQgBhDAC4NFxHS3UOatir/HMK80L3shwa1TDFSkk5spZ3B8j1
JhrcOpohRNZKl9HQjVTXTs9jpSSnhAtcQFELkvSjJ461+tI7ytZoKhAoqQmIOU02tfH3/O8WDCzQ
vTItAhZXQuF6rXSiOA9FRdUD6DkRvLkvvS1WbPHqVdh2K3ITKsraPq/bOwbYTr6eBuP7TeTi/68Z
dwZpzqcAvpx2JH0ILHbPv8I4j1X6Op97c1femIQCx7MXLGTTuvXayzaymDT2GKCeRYDVSI41EczB
2tfIjNwMhcOUge9O7pZ98Oh25ECwT3eQW7qdQSzyiorculUuYdWU37uoOv4I+sSMq0k1VoW3xomE
p2l45j7jjn6es1PoljBB9gOUuNKFw5aua4EmZzXTdq4gtSe2Ax8uLJlytx7prKvRc1d7GStqlIpi
zkVnA0XBcPnmTZh4Ej5BISrOu2qqk9UIXbiBoc5Q8edmHxwi6OtBj8/ELyxnyfNACiJfCw2PNlAe
BdWAFC1mO7Bm13caoId+e1ubG/7rubZx3CFe48iJNbq+eLC8oe33LZlACx/NFEMnyVtHDLSYO6cy
9JiPwupJlj3HH4ZG1OlJFK6E+RbqWeZlWNUc3HNfRnWSWa2m47/LoEaublVCxwK4DTC3e0uO5fpU
THdHzIIyfmJVIlRWbonf0kjd82DfsfChXQudkaNjd4sGI+k1WJ4291a8XQsXmsrB8VvUIyMNCJ+7
Ncc7Ael68oaJ1XEPIiTEKqOZo7l+stZmZNxspQza9leNYv8fc/g7mHW9ps6a0lb60dwg5nOTK9Lv
gPYcl+mYt2BGBhV5TF/jkt8A2obgH1hMLHUSR4qAkhadCigwJfW4cihJwVzt76ZZj/TawK0tvzt3
w0dDvpGSl3IsSkKX9+dVwhP+xZig95/x/XyLWmGipQG8gxheIrPpY+GuMKgVcAvamGyn34gZLvOC
kP9qqNmwtuu9h/Vup32Lv2L36sV+MwM4so5NLe54eETNQ8OGqY/tjih7jmm/o64tdCz4wbONyrfJ
RHmQveXhhT+cDDEvTV88os9MUr8HOJkXgOqoPnDUDADq2ZzT3+KYPwatt5TGNcaTs+bNlFeV6fQW
zHxsxf4HCDLuLZGtn5xzg80pqNDGwe7BqOPSDTW7wwItYFjpKCyLauQN6+ozQXEU17rhhmU7n7c3
uSZ6xBJ/J5A2giKG9tDe4UADeDbjntAS/49c5H873HjtJtraWj6Nr/PNtsY2VOwzluvpVYYg2/kr
mjo/S2SEKL/kJiyI+5sRNwz70zVFxgrWoDJ4fzglyxNJ+/5mKh9dwgkNZSUsER1BqKDUmKhszNbD
xn4NW66yaiQDolFrvQX34jeqzkzHAgRruKOVR11PV2r+MWjDZnUDlUFTz/NPNjsnvveQYad7P0OS
eXuqtDvckBIJU/zNXGU+CCLswTZC2/IjrFWmey6EWZQudf+Y5KV6cKOgTJoraTnmIKNuydlUjwp5
OU0U6/N9vfQ6yh642pPgWuN3Vmd59I2QTrGapVOin8THSA5MiwwvtLmWCxCvrsDppXP81AO5xMET
y3SYRpk5OYixN5K1XZWGo+n5Yid2Xb5QzFrlg5/Lxe9U1CoiasWMP/41+cwxFNQdO5+KgHakekcF
bRVWS6EczKJi9QIGR/Fmj0XrBI55wo9qWaEGDE7g9W9UPEHDl6oTZxqcJD6PiUmS7LhlL+L7Z9lG
AioqghbnYSjNXRpNpETOv90EJQeWTvVV3x9cd65/n4/I921dxC237UxR5o+nVdVntf8y7BRTgbJO
iSEKuTzqkh0HADxrQsUEh3F6lXnaQTEv9Enyn6EQ0MwA0wWbclPI1QkDjASFA7yHFAtHDsXQa4FP
NF0a3J4OgLeb2fKEBg1LiAuZRQo9p++mBKWVfLzdKuVJSm+6+hlE+HQSDXTA0t9zwKQePQpimPl7
6BidC60z/YCgPnnU4R1YWeM+VqHorigWojTPG9iYOT+t+LC3hfNQNbdHaNhEzshxEe+OsRHjmJBY
uzcw/kIegUFq7PGFicp6r/atKos9abJJKhHjrN/5gNEI1DdF8su/f9y9IwTf37orrmWwXHOD42yh
cbyXo2Qtksi0QlIY5u8eN1dOz/u3gobyZxF2Zytua9zJETF2baFjihmaS+yGUl9GSIrsNzRAr+pL
CtT1qyPxkS+phyrI9+7lO6v88hzKDJaJWRWNuaJGxaJr0ehS/1js+ec6cJ0dkFv2QUbNPBcOjKWC
sFhUuBY91JD/WDfCRxHHtBBFMO19jkEdvKzvd0eydO2F05DJGvQo7kFJVNcIlsXVfIk599XJ3anK
1IUvH+uwE3Q1EvDH+9zuFKMj5rOGewVRS3RYBQQBXf45XE530hGUEmRyOR489q3lafV+ctimZk98
fynYR3t/pvPH4rv33j8gAPumJAQHoLHxIl4wgDEv3JcVartsVv0rBDHhDfZBAGQ7eonPUxWk6f/z
logn4nu0stW9EIweDD87zS8iz8o0zRDiEdXRgZITK778Nr2zu6qCc7ME1cKNPQGxaYAUqyhRdkgZ
4jCK8119x4w0FG6X3oqAgFAX1T9ePLVlFXb+wM86yepvQ1Uo1YvOorFcttYX+foPRoJCowS24mZ8
KFnBW2ecmbLMnLu7DbVsLuQ7hpPaOOVRKqhhArzwqY+BelkzwBOtr/0JrZTIlysorGosLb6/5xO8
x8FI5EO4dvEtOpuY0nfc52+luzj4bWNKWxLkysvmgsdYWQQQGFGtdjDqdqfr9XGQSk54UExA8iSe
hZM0uxW/1iI1zZBWdZdHO9IhTMmBXLAF33jIK96Co9hT4v+aK33QIbmszWf2pDawnHEk5rFWrQCQ
IJnGzwVQM2lzCV04strW7FfskZukiPCCtQKLA025QAHh9TguXXpokjD9UGf5cjTVcO7CpTASGlmE
ecbuIV2Jbrqbv//LU8/TIdKn9tDmXI6f9INZ3xH7diOi3Q5vM8xcNxdhSECt1CXLStrCWYZBUZuA
NAYSloCqYKYhMvG4MHFnHDu9CvRYzhgZIClat4nLwZ2SKT5v6RDP5BgtvH7el6TlztS2DLa6XXBp
tuq19h6EUFFiOYr+8V49mKjn24MB35z6GdYAXNEry7DWQttAxNer+ECcDHXda4ZRrqIBBXL8Qm0z
rEI6pohMRtQhzQrvbwFxn1yRBoQUCyc04wkmMj97J99Jkxx8pXRvF51GfukpcGvIkGF1rq04dwds
ZTw6Dk6bLnvbW1RKT/RY+xl+x2hFJe9ILo/6KQiQp5JqP82ZpcHvwTDAc1+xo5TFnVv0yrUDfUwr
R/He0e4fCNqC3Unmmkd/4DaO9U4DDrdAtZLamgGR3X8t88C/eGSXD+FZqxSL0+EksNjEsKsaIweI
KGumqk36E4VbhMbx7gzazeGjWWFJGWiXGlCRpA8PBqyPuQrf52Xeq2ckrMKHEEvhKHXdvkBDkGkW
wTQ8NHHLPcwm3LpJ/BXzxFoyVizZ+FzCe5sh9YUhMKnExvFmqcaJm/ZvzcOBPGyXPDY3DmCB1qAE
7URb3a/R1JvSGhCLkJYRMMnFu0u3cRcB2MaoghLG0lUyXT1Q/LMmrTRqkHb8gE040El1/cLy7UbM
+7UIV/L2CAubiJUenLol5kJJCqOcQmwLE+zBSUUBE73NoHeU7ReHoxylttmg+vvBZIQhHpS1ZQqR
iWFo8jCKkOM9ql0czkmHaAslslhHtsp0a/TR5tW3Fb1raR4uMjDEo2ykYhge44yoDBErU6ZYuwpO
62wSWuMjqz0uKK1HSAAM03qOWXseIwqsb4PXvHQTr5rFVwVpRoEakj319Df64/6le4yPteIdRQV/
s0KwKsV+Adf6eS5pRofVRaxqBWZYL29wfT8R45aKQqBYBCZRIfyaWd+1Ku4FN6ed5bq1U2E3GDl+
2VqN55X+0MVFaFltw/AIIgWYsWR2TQPHOkDkyRGbJBNeCfUlwmWUODCw3JMzfHhcFgUOujZZMOCV
+FpZGNDLpgUX4xc1irle9whMw6c4qV6zqp72kh5jOu/acQ+9VqnEzLfQDvIe9a6uOWNl1Jgf3o2l
0xzRCv4Ji0VVh9oFQRVkPgzEFRLcCHgUT6E5nS1ENLfblUG/hczLzk/z9ceXUEgDFen/5lYS4pOj
Nz39yAfTFEVpLej2OyfgA7OUcJABWMTxpF5ayLAB51wcFjBDFrECfk1QAW+g4/mDcbu4YI/Hd/Vu
KQRvqDU7wLUItppquBssJG6aeqr62evyuDeZZVyzbpj/s9LZZK9SknDnPIY/sWEgjusk5lJw+Sl0
nDouhEH3JQFf1F26024tmN8Fqrc6TvR2MP0D4NYBLNWxbvzhPeH51UBKzch3ipT8dO6F3NCw1KN/
duaNra9fiymPQBZlDLQuJ/YgkpmnkSE5HkcdO7ua5xSxP1CgGNg564/3m0hO/XYOIGFN8bwvetHE
FrYBu3dVYZqX65pNhky8P0eWP8u2g6dMP0p3VnuvJGeqE2fVMDbrHOG017AygLaDhobkt6jIK+tH
WEjDOvzvbn60owOi4APzEmI3RTx1+dn8j/KAjMya8vY0luuVJwUDHRJU143Cwgvkms4zFP4bZ7Qa
4bRN+2nykb/qjfQEoviG7XCmFYauqQHgm2dzMtM8Z54R9sw0XhrS7dZwHCUE6nPTn4dvOt4m7Gb8
YohzXfWZU4ZYZH8XpOrJDoUu6Ea3aEk/NnvjsNFWWfNRt/fTQRWKHge14v5OuMwsBkzjS5u6L4CP
TSH6Fl2f/timgNxtq/jDRY6qcmhzpIr3GTZDsqpcaLJAElFMed2E07YroxslbFeEE6fibVQWt/D6
/+btO2mQdeub0IDo9Sb+QsW8/oIlqs+wLIOHEJJu3U03BwrLLk4eEDykSFpmZ88HpNZ13uTsW+RS
bMqk5roM/VC/ZPP4tcCKTPl80TX1FQXbHW5uFFlqE0lLrc/zhIolRtUXUvIpnOWjTGGDjZv2bEL5
kf0OK9QumxHyS7U34CdoL6s5xugu4vFEqfGMWTtreu2Nn/6Sm4Sl+UEQb6Hr4Gns3CcVb1d9rxmI
DO6xCwf8Fxp5jHNrFS5CVWDrRd6uXEzI/+iFFE/nAFrpyi6z4Bbi6JlPEpySsliPZwwWG2i5oicx
Q/wpr2/P/LRC8ek++ZZQ29N2sYvbxwUABMCvhtxphNxQz0EN626c11BJ8zoqxltjNNPmgTS8IftJ
NRGTuXg6nw1M8rJDk9ajWR6kwBMSCEBviAddaT7DQiZAdy9CB/HMejtRUS/W0xLYYYI/XCe276Ha
5eIjomHnVx8sQp8lpoZW6Y42JavfL82Q0QRQ5PufNrD3eBGFjiNdRNW67ZbnpttoWoyzI8aq+RCn
KCVPeRwKqsY8aE7/KiVYBuyC1PIsoJf2Z7Fa8LUqbXercOyHkMw/VTRHG7/Yj6oKkuMndXLAyHoQ
NIR3Dhz44BU2V9R8e6LUdkHqKEJH8QC6ejbXj/UU3cZC64Y9KCeWjcO1/49oJZo3utVidNVQUOUF
KK+v7rsFWmzhfviMRENj9jDPhtkKCezIbJABb8QdKZxW4UgSYVGIQDtGNQnYGJFB3Ycp+AoZ4Ft2
MCncdxgoGtfvX9wTeSpmjiyzD8rLJt8iqg+5W4d6JTHLF0yBeUrf6vct+zUrf6jOYb7zDw+9P6s9
kuGqBfBhpgfUut4XyLAIWMzMHmLxG7uIHlvVKaA002+ED6oNTolw+Y1rY5m9G2RKPeWqTEmK9Yzq
bBOWXKxHnwRssl95w+eSwIhp/LgqM7n4m440NPQk6BH0kQlOgAyXiBIGKSbpwLF5TmX9IbJGmEis
HIWSyZx/mRsVmlImIc3OdZeOR8VoO2wvjHoUFOrxU/ujyynxs2Y4h7xAiMkvzZjKD6WxdZDqkl+g
tunnJU4xzIMcGqCYNM+3DsYfRQXz4m3xbGqipaF5/sBHFbxqg2wKNlqHP/QiUkGATntnbm+Er8Qj
GG+SKloI17iMisC2gP8on0lUhGEkJPa7fUmqeBW8tm4XIOA9aJfyi9zrjFQ9NP31LvQnDoO20HuM
p4zbDxI+1Iz4hgR4fpRY5jO6G8LSS+2bY0VHnZF1pozS4GBah/3ecMoDIX4NHG/oaINi0zC+4Dgl
dh0iRFp/JGprv1iSphx+t/mUfZf3wSlSMl9CX5bNh01rUzlrN5sSs7Q9AwsOvMSpVZOs1iAnP0Ne
mReL0SLS+X2cVMa8iEF4yUrFOqrbNO+3eAMcHZadfnvCxH/evYoBS9g7YTKNsk96+ZLRNBUWav1n
BHUPt+AImjV+9UH28PwCNxL3J9pxA1hI0uiJJ00ksdwQ6Ok8iRDX9AJD15FFeI8JVh4vKYUoI0z8
FmfrHzGCg5qPrEkppABTjlDZ7zFGN4mM4c2qqOvVFwCStKqB5Ehh7j9NusUn26bYTrD3pVA/4z2K
QtEmrbZxthUJbiUemZyVhwW/+zUIRwj1i4joF1yrmhQ/9nQgLFSi0tkt8V6V5l79tDFmopxD+SXx
bOt0AY5Kb01k+vtXq/7ReO2h9b0EYZiUBfRZn151QcD3CPWbG12eTMvcuNnieWQDin2a5I6E1lKj
ZpUIr9YVWJVtbcQ6EGilwRTmZWSlye4F2jlf6pz8e7sYc3j9gftj3eP7t4JFlQx2dsPJ6U176u2m
5/IOh+8i5eBHhhWpaKAobt/JMr1bWwP5oEW+6sDd4qoTwpdsOj32xwzeVIZtzbB+5gcOWEYBrEVs
q3/ur6Qz5l2QNvoSSFbjPCmrH8DMytpM3a02HPWrSUGH1P0GxRvAZrvndX9+t9ZPaq1ItI99CgpP
jdce1scU0IvEEygWrTs4nJwZqDTAaepftMr6zISPn89viQDhhTPe6se+74+FWixTALpBXfuBEv0C
RwefiZbVT8kPVJkfMxayoDH12tP7Xy6i/j95NShRzsY4XjYJT9DK3fpqbBxXUpsAQN/38BIom1YU
MHKr4nFLXFy5Y0XzKR/BcHBlSe22E+dhlsInvv51vAspVtL9wOQeE/dRLLiF8BtmPXDCmQYTN9YP
rNpAFNO008wLJMTAkQMmI8HeEqvFbLcacB0iCqb/wyXMY2EcnINYF8nXNYwTBRg9lJui1VKYhZaW
bchsZHdapskVoIbcx7OvREN609kaIOkOcU4Y7wPP6FxPCJR89CTDsA6yXtygm8h+O72nm7oXIxrw
sJnumYATrnUB1EfRoucv4DoQgnqYHAtjy/BL8kWE+Zq24lYjHu6IPzY6/ABAYKaJi3vNW4dFZwQl
oN8Uxvdhl3/iUjhyze89Jx9P2f/ulJz4gCRG92cwEzYI/w3JaNYIduEtQuEDtxInoeMLA2farGdc
tQKkY9W/dzVER54SDDi0I6bdcbk5QHz9lePIgXk/ZGxlFAFh31Et3falDpkrIzn3iqKAxhieGpZ6
IC+ZtQgJ7xBeM3BpPlUKieoffbzGaKRZZGpB1IiT+O77JI7nqf+xDUU0Jbt3n8KQgICbB+Y5hXYM
lnxxLDxwBxqv67HymcET6f8aIQggEBxO70L4TEDxcZLH4ZdG9pcc4+ZqEVvIRgOCnYqP7MRE44tr
Hymj3lUIyPp3kgNZHFytkZO+EzrrpHm1V0g0CqVJ/4tmxSjyZr1lW+R/fXhO4tS33a3AgHTNFTyu
WQF7MjfHJ3hVKHiduAx7/CcNqUH1KwMKz+gYlGLJWdHzRemnU5M+VOEWCMMR7N28DsVJjnsZKxvb
tamamhysEBM8+9mlZa1zF9YQy1VEJTwp2/Yslj6gySzCzDo55qQVLKevX5gpjyUXNLmg/HsyU4BE
1d0HsZDgFGNL3ruayfH2/ydCYJnlD8TW2i50BizLzKHoQHQIqFAlT+UiCQsTDyubgj22ZnSI+vtU
Ms4rahQY5tPEz3Nq13yy5pDcTKD2kLM9f/1MCfxfkJ7Hmh3KEqog1VhrXmVY/KXjJtyFs649VmcM
rgaq4Bx8wfpZ+TCXvavwiD3ojQkSC+ETeoro49mvnRnJBrvC8+dDavcajFoL4vW4dqrq8nk6Fv7p
wpcmS51J/Pt3qNzZlHqRY1Ayqekr9Ibgj44h8QDSam7HJfRMsa7ZDJRF9r/vxC3iXiSywJPUbnVQ
GrgcC0qBe14tNhrgO4JyEDuXPOdLMS8gMnLbetD80IfxyUy9DBPoERIbZg5iITGv3oDfugrMgw15
MU+YPvIEk9IdUiPPb2GsfIa/fnWqvzRinochxwpMnX6vcXBORnAl74RVvO/qHyI9eF5GcAzAjYxZ
L6LbWXPE3lI6xcLlpiKLbcoMWnzaaZAO5TALYrid+K7g/hAnXVbxTcnAWIHO8zNWL/eabLl7xh+3
JvRb4zO7Wamlewf8yl5DF7gj27T43OgwxyKOnvs6p/n/Hhc6e0N2WMWTp8Z4Xe+iD6IUaTkhh1tl
2JUGKcGAjas7SyNVIs139KhEOMRkbepIFd4w5kSM2b6HK6UWJJ7RGBIr7S5kMU4kFncNRKk1ygJ1
jt+3qY7V55dWdah+IsbudYnnb1n0kEMZkskbzU2dcG72u/tAa3egFWMAPzNpsfvBxR/l+SDlVL2Z
SwvLJe5rrzjpnwCb3THJ0kLsJJxe6PUmJ/7r5VsxZS2NItudMhO8XBw+ujfQ+zF9HExuRg4o4aLl
8kHUKdnutaVLrNHl+/NR/Jb7/1c5VlgyemyOqZWmnOPTAgbtz/5L313hQsz1TScZyysmB/AiaUZP
e3p0G/A7jSNsuO8N0LgTJ2h1aVSszVo2M3v99OivEm9BO0Bd2/5v01qBF82BbMgPeEJFfCn/Etc+
xMfcei/RNqc7FxkNkk3LHOSZgxyq8UfUNHF7gxK547mTGpywt+Ct9pVVB1SVjsf2ZyPM7iCK8u3P
z3td42xkFyKIMaYb+UMngoC/wFyRCvgCeTXFwIbNQO+dUvh3Nx8Fi59X6DBvKaOVcLN4jyfHlDdb
bWNPPCFvKHDhi0HLkDVHcUnfrhWfYRtSp3kkxW8/cqKK3ok13TswKCpRZKUNmhVyL94QAtixAx+C
gwl91Q3kwUcEy/xWyNrqSIZfhZlYRcsAGEXYUzZ4vJ1LBsu7RV3qdxFBGRWrYTPSNEENkd/fAE8q
jeKpz2Ki0BbN3QSNZDUfuqpD4vg/f3vSn6liYFttkfaLnGA8J1q6ff4ScHeDkDGoSAQgpa78b2cL
6GpeF9e71zQ35WRTBLKHXJd2cbREvaf7LQka/lvlnbo7tN8Dc69YWPQQJdsuJv1WGEz8RT7RzBci
LVb0mBA2Nbk0E5X0+yxqsW/rszNL8VUxlBV/oOZdjWdSv+1WfjGXK5vZ6otruNO04d1fvEux9GOh
YQ1I3Pe5No5DqGetIw30kkBeyi6hjXScqNCHsRZkT1yXwmpvtyy+SERDePrE50qpCZhkanR3Fnoh
NHOiD/JrR3ICTq/XX2UkEGc4I/P1ubUhzZdTChsw1Mu2Cy0KpQ/1jWEyB3zynZKxpAU0D+7C8w6u
DP/t5QG345DIlxtY+E8WD9JGFgVCY/6tSGCHnnP4Zffr6xL+VZYunxInKNpSsTFXKdmvqDC+iXDK
9T5Dkanfjq495iPZ+TdiUq2SSfDqMpm4ARyr9XaJy5h4zjJgzbxWlYJOFwB5t1FIRAU3k1yamy8D
b8+oA6owTUbOw8Wlif+FeOPOkBQkEVA0M/HmrWgjJDjDC25JdaCGXviwRzJopgg1M/bL6JoXe/6J
4MkeUr7Rw4OVIVafeKJqwU8UQs7r/as5tOOUfC5KSiC3Eu13VEGoGzD+1RcflNMBrnXJndEOV2ZH
5SHMgG/e2rBA01HNDuNARnm8B22ja0SE7TR2FjUWPfmB1klSB0YiZgUrplFJ7tiBFa43eU9YQQd9
jqbeZEdPM+4L6p3yCEp2h2SM2RAd+F8GaKvSv2CsvnM8h5iiq6bhzawYh9MPEM7cFU7KbJsZtb2O
5rIPPyEKi8nUNdrEBxCUoSd0/dcw6TTAMPn4XHO+J+cr0J2KRlSLl5Urjdh+/n/i9r8qg+j8+Hx1
uICW6Rb8Ywz8Yslxkv5ViE+7KVyDKX1FRf1cmhGid6kQ2WBNqpp9ytTgevu7gpNnI6ItM5j3XkOM
lFjGzMQaFEG55+hrYdtWK3G6Jir143LV8brVLpka2gHs8XxJBGLX3yM6H4m6zBowoDSFDwMsnoaH
1Py/VbE71yNcBfOLVr8B8Clj2lSnxlxcDFMhGSCd17xpLsDeGm2F5DG5Q39vr1CbvebcIfqbyb/l
QxGZVIsmP3+RyN4Rm3+6dDrndcuxZMpOKACTj62yUUeJcktlNHpepP2WFZTTk+7RqkCgfjpVlQ4O
aZWCh7xPzvC26ZIWM8tXC3BKHBU2uEKKEFWm6mfWaZHKkQWvyvrSB21eBztgM3zpvigi0SPCKgSN
ZsEBs1PM+QpyLVIWs5LtevPHHH+Q2j8bLSUUeUVheOqGrZfcpQZ2yMI3mTOf9UCuhyBWlSR+kqPm
VVnCJNRxF6upWoVidDjMlABG1y7a+ArHTKC7BtodKZxBDFc7wzGBmpWFPuCgYPdOnRH0GpbGe9tG
6RC71j4qtbH9a7zrbnMNEAStrwTl6h6/ByK93eNkNRhFPWZzriUJb/XYF4v7X4iQv0gniurYrtKS
f1FA4vldbh5Gp9L1rbXibsf6uKz2evGzMszGc9feei4SKjTFqaCcM4xRfBvYFxFJ5j6jOfovh2jI
0CAVvd5MXsSMRqk5NeuvAV4N+nbLVhP6AFBQgfOetZ18yKsQbB8xlUkKdW+GGeloTdLzukqcemQN
g+1p9/1jrckq+3gmtHxbhd9n2B4dr57EowbNe3A6ExNqUwoQNdVPX3NWr0oDGj3QXgeBDElqPqUM
XYGzu+rl3MO00fFXUQKBAYhAd3Wh5yX+mK2sCumsEOaJZ8xFoyJ7rNFstQDJynNZPZcduSHJQY0p
jnhI9k8pp3yc471klTQJCb6wf4HVo7LzW5iqrMbS1gXUXdcMtT/3+K2or7NTmlKQPPQpwfCqoQ+8
pZI8RMQ+uNZVqM058a2SmKFj+hobt+Y2PDuzKuI4LH/EYVxNRCrsFPS+n30u23cNy9v0+AccSs/c
3SvoY42ktMB/b1yq8zmnHOJDmSxO8mNC+s13RgfApUNxyL/HF37oVPuN7Z8ryORVcxfLdT4hSItH
/iod9OEk+aY6kFDJJajZq6McZtAfwA6oAxY7Gbmi3NxZi+qYSy3J+k/q52VAcTiW+bs1LlUcLXhU
xd3MKr4eLdUyM9+drtdKRzYlkh34IBkiu1Cd4lrUsKWXVVaM/V6VN5cLVSUkFrLGSuu8QTaidw6U
HLAk0xP3Ei1Q2ZXkZj4SEeKmL5ZUBogPqvDs2XXEBL3V2yAYI8Q0gQHR5/4IlowhxpCIvb5AHDvM
QBXxfVMuVpSDHQ8LaLiGC2WwCqK59ExOgqlp0WdrIqiWIqi2MLOFdF7DPcRNguzi19LdvHrdhSJi
qzhVmsp8CXYuZP8AqDJN3zdm6ZaCmrAlKJ8qUOGf0+ODV1iRQTYR7e9fbo0tsJfcxh2fuZHhidOg
ppZiGvk3jc70ChAVLbD9zx4HExJMqB+AiM1DDiJ4Xq/4BzIzSOEKJ6BxyBOL/zooNIO7qUHJSwC/
3To3ggtnQcbz3BQxQh/foPjFtHY3HTFP5D1OgJ4J38ulCeN2FChRyd/boZVy8UGT1IOnH2RaACIy
ECv+JpcYC1C39aZvMJrmeIAVviIY/SVQvYSRsOG24sj4FRQluU+5W7x6QDSilQL8YI7g97Ej6MUj
2hEOLjpxtooejx9MtB0XYCNPqI/Y+LT8JchEqYvJyMcCnOBfVqhMbNA7QM1ZtKrmwOn4ceFAc26P
ZXKuR3Myx5c8gPA933ZXJ57gOKTKlqiEeezZQTxqH3TG8+V8b2JvcJghNwBIYMcI9P/gekmPmODf
XH2t0b3IB8tCJPsin7YB4dM/lwBQZ0rl8AVXNG+4AIwqHEjwirLGFs0rctni9idOvrU8txScQD0J
ObLta3zJ4SgTednTplin5CxLZ898k42tUtS8m81211ACDLQJnGriMV262ItRd+8TAOrb+yPjqong
CGdxQUiXSJQX7CB7upEtxiRBoOo/CtvKfGkhw83bdchnBS9tNbvO9vsiMxFv49ctHUPRTK67rUgb
D6es6lnFsTPBLKG0P3Av8nS/0T/iJdxs9exEmuHcOSi7mbtudW5dnjgtYjJIU+qDoamEqjlMTvRJ
ZOyoR5P3m0HX8brPY/FPMet+nhqN18y/wgy19K4AA2TAjOBfkXvleYFhNF9DVvKdnQWuQz+sgC/y
HkIX5pRTRyhS24tUGjPGyobq295OVfbmn7MVCHgjETwzFwD8zic0LzYPMwo4bih1XRa2ck1YczfZ
vzworeaQFYyrOwnei9/AmknhkNOO7Z1pUztiqyFbXZ4/f3LrxjSlXRBzOhCs7XaOjuLpNfltFOOI
GUQYnyOSY3U0XgWH3O/Rsv3C2xwfgXQ+iDsThsJEx8rskRzNRfR3EpOXFms6KSnvf03iSRIoLe1m
y0uKFxeewXFSADQj4kJtwI8htWgp1lh6jZoZWiI07Ew4vklBn8zyR+Qcwbxqh7QTntMkeOAueiSQ
Q/YCZp91N8W1FFJBeBFp0pCQHCks3TmTyBI1ah7vXMLqs5MBJrLN3RmhQ3Acq9iPI+YYtRGueZJe
E9gPEKSsD4YotiSiBt+o+GLFXc37mz2COnnn1BrCtF+0EFLJNaWyCXSkQfX87pO2A5fX6WkZDSBJ
/ed/fJW3RO4ArrctdEcYU7MC6y/aCQerZzZJVrFj99RsSq+GsMxWpRJcGUv6uGEfK0jDxOCDDscX
XDTe28ckWP41iQoGazt9EIM95mteSS2pP0mFkW2IrCJwQB/i00oswzCgf8Z8jrnT9m90R9GLI0v9
U6mLBTGCP5e3/LtIRvZONE+d1Go4iaJR0v3sQwnZMUMGFAMvBd1ifdB21lO+FBE8eCiUSYwaZSCA
2j2ZW0Jkt7YK/q31f+rWxxOIv94wvKsWnhjkIbfThkqgLrDi4m0VbTi0wimKQbtQowZZNKhfCAI4
+Pd1v5I59PkKuiCm6u2tt3C+gHeAphl25CFm+UPeQO9yrdxP3cCX8OHq+uBlUplhqs/s//30QKhU
p5Vu2FH1CITLojN1JX197abTUKFCb5xDBibCAF+eh8Om6CamKwz8riHHjbA7RDIVdvXmQXkn04eR
OFDWnIwDlQCECaPvUSPM+RCFRtbPnbMQhj2l02vOZ/qogeWzNP8qXbdfkCeJ86jSkF35QRX5uidg
C7F/aYbayiFrlq3dq+2nYLH4WSuKIpa4ZePOva4Ys1mxHHTBkb5M28ItNfqKxcfFHPtoXwyIElFN
I7Zxn97aCftDgfjNfwIFg1P+32d7/kt81EZv/cOy8nkYTMBUVQuxgkD1bN7/K6nE/3z5IbRrD3PG
AmJ1E9GV5eAZ6+LiFzl6a7b6q5cxQAjZJ83FDbqJZ/CFqJZ11mzUleSQ9InWdkXpLOZghqvJCoRF
CDH0tn/ILPm8l98iWl7BfrgEvzF+iEpHsNEd6P+5mQyCj74EWs0aJWv/N98HVlOszRY4uJbGvRa6
x03vWtt6gOMGmgRnwiBHaQv9AoH/OY5Uj6ipfZFhQ5v3/S1M9npvZOnC6zO0GubNiALInjMS5EYM
6d6VB0dgH7FoseMZbUJyk+E0afmQzliGPs2pKQRQEkEix8JnxyqAY244jNdsY+UGKQ1cfNejR0lX
xtNssWvM5CN1PHkSI7t5bcUeQ+3WxPXZA+NdqI6kc5keFcSKiDh1OMlvYfosZDMaZh+v/Fy1q7DM
Fi1bnovDGFMV6xc14g6CaHNT9vkYBggg1p7CmmihwYJsetwyn4Oin0taaOHbQxUWOWcvX+pXV73U
mQQf1OEBuP03aX2wf4/nJvJG6IjrLnCS0b2Huozax99YkCs0N3+IyuTS4EW974VP9aFQxdZ4g5uK
YrEOjl82+Z3qzzV0Z/8yHl/peBn0sH55nGgLn1Hy+fdUzbNwUZepCwNcXun+gB7xIl+JTbtbOidV
U/DcgfoYiqWF4XogWoeKKqyXUGgVovvCG/0f7p07UZX8s9pxOsjWBq5LMG4/2dK09+CMf02Qb+Gs
k7GPVqrPe9F6+ieg+kMoX+iCIH85oHLOIb+G4+AmCSgQZJvsarGapmxqWhotNadqkedl6meUK5dG
YkSMabPOnG1tJYljkLeCaBkHiU3hjeQCq6TC8ngXimG4YDn9iTrbiom4TzeiRzauR6XfZ/+saCPS
CjWTYCyeDhBhwKgatdMkiU2Sfi4CqKar1hdRT3nI+gzQ6I+I+mM7UYnpyix52ZlUk0kqI77P3PUM
QePjjsqQvBLfasJX2/SDY2RGjbVqRufDQI29YvFUspGWjeAMZb/Ux4lwF3vCyDjg9GJOck4QkczC
gCzT5sVxLbnRMwLYp0VARCdZJTMiERgZDlqqEtbjOtxsNo4ZI5C+GyBPWpPR11KD4+gpJS1T/lR0
HlCAYvM4Dd+cbb2JffaEl9Wa1jnPSlWUUdMGvyHXIV0fxYAzTA/mFGg+6IJT4SitGTkbX8IoPSnX
Np8BlmhwRLoPQxZ1awR00pQTWN0uaVE/QzXyUYNv/SPjwMY7/j8irQNeL4CuAa5obvM/Nabs3912
7Xxx+IeL+NuJJH68v+FniZ8C/XUcoLbxeTWR44f+SmLwxUuh7Nrgs02JnsQHgwpQwFDJlaTdP3YX
lnVu0vbwA4FFFZXH41WK+zTjMqkVy4UIXMVAuoeZSF5MHmur2cjQXbgN+IYpYD336AtsEihshMU8
WEy1l+rTkx9KByKTgS1fKWyO/D+SDrXUGH8RWGPwT2HKDWJO00gWrH8u1EAvpz+qkpjGvxr8wXP+
fXXih++dMnVTmKI73gXsWgx0vR+6gxU4I5O7tmgNegza1rrh1Jd/C8Q29+A5bwwaXAmY6fTBPylZ
2zlZYfpBbBCT3CLFP3EwJQ80bCUWQmkH4XO7AzOx6EHLZFWoxivor3dVMdocmLpScUEJFt3Rvrcj
745xb0daZMV9K0VknPVXtsDps+9DG2kffUhI1OfDdSxkgfSEvmf7Q0w9wS0Be5W9dsRXUD2Br4SV
2myG7FgBNp/KrWCBfh16MZY1M+xdR6zsBJlgA11BPLCegeyQ/6tMEoIt8K7qPqkHYtyxU5FAhKd1
kksN81/j8vPtGZQac1LG3YbUJmYxV0zg4KD4rv/V4pzazXMJCwn5SWHuWzfGGanmvaP/ZN8ezRXY
VokJoFV6QkCIqiTfnHVapU9R09PQNT4xMWMLC0gA5dJcVP9IOarL5GtgX17+zSk0kHVfcewnOQTL
Hs8qtW49Zckb+HKZmxM1w9YTOfmrB//7o//yVNMafJGWan565Yg9Dp3+M3WOxPvkSrVC0Mx4jyDH
HUajMqWmlWga7JRxmK5STMG6Wo26oi2oL4ABRB+CC2MwdkpNvwzTQdAPAr+ElhdEGuNmmzNh5Flc
LesAAmRi9jKnjLBxdHw6KbH9w23JJI6N0bFpgdBcgnbTadkumtCJ+zz3adkUgCa4A55I6niN1//9
wiyzIZh0GWtyxEnIw7qr86g08sbTMwGHBKDwOsvFa5q5bNL+2qohJ1dUAzrupV2WIWjIfedeIqBP
NlJURXOYqui5X+tVPT1QMQGcXX9Xwpl7/c7OUMh8W2kf7ir5H+66RexCp1/R1YpmeMU0zri9m4MP
OtMyPYHRnckDniU0Z6jghxKCKj+pRKtWSFfl2YgMmYvd0BHmJRTfPB3dMUmJmgxTOYETRxevsjrD
eUrfScX2xAsyAzyH7MK7fzsuzP/ft4XjS31Ojhus4gzfWCPv1PbK6t+baC1Ib1GhsyIRsM1eow2+
nWgyZS3QhGW923Tg+t3bVQxtArRkvwq0H6WWQ2TvXAp01SHWRV5TLgSIi/oiyXu4+wHFngr7PoKG
k8BviqR1gF3FXA99M0TmKQ1tOhMGTztj90awlav1/4NqHHCr3POrGWOuYmue0fAml2Ph3nf5I6QS
BnEBwKyHVl2q4sjnCZ0rJQ7I0EgtTUZlWxRvxHhNI0LZNtomRTJWpZ+sTsm8oTkGp1Z1Jj8NZ6RI
n28h98RJaSMvT50QMQKhFHVzSLZNya2rqUVeMKaaemUrxF8Fhlj3v64GZ33QauiyOZ3peRnZ+v3V
PgCTDNDetYoIB+RGHMA7I1xVk7E5OKidka8hpdMQGpxfBWLFXEHtw8K5JDFo8/clspShxQiN5obW
53sXh9wllnoTuBTjlhV+6OeeGcc8OYFlHI/e0K0zhEwM5tOKynmQp4FYxoQx4+u+y5QqJn1skYjt
peWxR5v2Wg91HnJLe6u+/9vU2cR5vpvJ7Genk3n6TWZxbyHJ2dG7oAI6mTSZH5y6DukMsZaaFlWZ
WdsieK3KBOePrMbQ9xpO0tNYBNq7R0o0afsMf//eeIsQFciqbkPnFXmc07oZeYLz4wmNDRbX/WCm
z5RT6VYNLOI+N/RcXPbrDkCZIj/EP3cJ2kB/o7GmWirSS6R20A/qJaZWZr2zyIZe0cuctfqtYCyo
sperztsii1nG8ZMyCMNnlOBxBbB5AdkZocMbjjnHj7xbqXWRNK9uCFdUtHUp0Uma28fWmhj5gUPJ
nbAJbtK97Kq3hIgfu0gfjfkaQGcpSMlSUbr2iaWpft8NAcdmHNLAhOd2DcQf4EEXhOb6rXrZXXev
l/8Q4O/xV5/ixHkyG+KaM00tg5sDptRdDW5kCUJW2J5SIvJOIiNFx8YFrV/GURG5JGPns7bcBvjw
TMcEebwe98MX17A114zYD6RCnR27V3WQgciScjVkQoGfYQFLoDjRRb1U54mgnRn8mT/nzI1S/Lre
7wtUbHlbr00BCkF4OyPsNPDCGe1ZBMe9XwaKgtQQ5fcdP9TEv6pMqdaVKJG6IjBM0Tx4PT3zftvj
bpxVr3shUiCeABZ+qIEn6/N01+o7CbWgXBBl7bCvAvCWZgXcwdyShYsqRPgzI6ewVZBbIBWD+VQg
JQeqnyTPQiOBSPOhMLGsLZOqzyAumbnh0gph0haT/eLegbb7RJos8uPq/aTaXe0lCYX/KO+5pnX5
/TstNzwH/XxRUCNq5vZmJl1wv+a0a3Rr/C7WyIZdk/FnxUcUIT+MY4Su1ejKhSAbJUw8FchPjW4w
VnHAJYDfk+rCxy5FeYkS+qopXnlPasfo5oJIhbeE3WzS2TK23CsIgF+f6M9GFyGuv2AF8dqWw+sd
+HX4CCE7p1ZBeKukyjKQk7AoiXxv/hKD/4WoOXPLX8i5rYY+aF+PSvRKdzN57m68DKhTqVcq3g0T
kxHoWaobKpIsTjY/l8i7IUuA2bbENh6O8hdZj3F/TnIkogaBVy+PlMa0kcYc279KEgXj9lZHW2Bz
NwKx7G9+9Dk21gQArf4ARTcFwl/Hfp6xoSfadPFqiC575nn11hbCu/O5ftcZPanH+WsWicDTHcAc
CScapeUqR6TMd8PT8S1ZQN0+uOhDpRdJnP6LkLpqhhhQCxanpYZRhVF4OdoANxpHxEg+LEcDOhfX
QkcnLzjQpQhfSQSQZCId9ZiuDMqEqmyCGk7YIuXaD7zkwjhDxqul4m7r3c2t+/KrOY/veBxgFK37
3X8J/xrmU/Bs/+zTlOb+ueIKjiLxEi/hpdzirudHYq8qj+2NP4i7fVQ6rT7FYciZjGLxmZBurcCM
01+wqGOYYM6GGSTnGEHWZfHZOCsz006xj7O9AI1pSTlmLCrLQaAunMt0gPRZU75yf/On6z5yOteO
my+FJArNM5wDuVFCwsDYI5bIDilQF9DNR9K1YlhDtPXuOEVFKh5P5DsKmibcNV8mFWfKBMgAmUEX
zKetKhKwMdMyLQZZCDBIEiXoAxtzuq5jbPwJGdRh3VCvS0oF1UcB5jp2PzR+x+QJPOgkNnvmHxNe
fg5wLvPAOhqu/luR8A7lQYxWcBKCbKjfgI64iz8UtynTqWS+Q52eBFqgUg2MKRVu7yJ94UeAr1wA
aTu2OJf1sgVYYXWTnCxvI8NqBXZ+WTQ2h+wmQT/YEOX5oacvyhSX/Qft0auk1WfuNURc5Giyjfsg
f3/8B0TehCLEsSq0PMzbpwWB0GePl3Sq0c5n0TDKbgtZRCx0QLzk04aGFJSP6JbDv4lAZ+luGjgm
ii/WLT9MCAs7jK5O3PJQmDUzrLPbW+gj7oGVUCqMw0dzDtG5L4nnRRo7/CJ2n3++41DMrLFs9lo6
Ri2zUlpyeGSrApyzwFijpgsLQ2ihO7PGxm1LrRmBrgFu6+xnxhaXvbaaiMaclRC80YLc1gZyK4Fu
/lyoJVv/hqvcDfe8XCfsjhp40TqMUH144FJPCp5aBOY6OgGpFe+fZ+Z2B2Vp7m4O4cuYUJWH7Y7y
X0fUGaGPAKUsjlatSUHjx1+x67iDNxvsJHIJvsITa+asD7qmLtDH81lo34jMjFe9Xpw+sDkOgmOn
j+/y9KIlslMeRYsIwv+Yw515FJa4G5umsM1X1N44JgcR3ITuPuGVvD5xBYz49Sg2lB5NY3GEPwDy
NfDuuxvUm/9qoEo5WrFpIgVtGqx5ej+hItUAF/OZQUbPVKR83lPm897wc+Eg24PuvG2DzNXVDIYa
e2o/SbDVgY+CZy8eebhmzH1ewx61m54M2whVO7ymedBHnA8bd4+/1nPDv15r5ZofCkumKPd6XQEo
3pwfdxhNqPRXZLW308eoirBRPhv1agdjpBZB4YBC76YTgEFWIWgKZoTsne/TaQ9Wc5tWM9/9lL2u
ZXqc7rqYZ0hy+hg1mDKTB8XQILlcWRvztOXjAZHKQ5pRol2iEgwK/KH/84O3614Kxmn+DdnExQCR
80KsWj0bkWbod+fMKYluZ3To1nOkG9yz2pjZlRAJi8Vmqx4EPSPHTuB6JO5meCNbsk5OgxjpgGJP
cMMOUinJEuwAEFUmhvvUg9LIoeG2CpuJdh7v9evBA73gk9fYDleooEUe8J7jkesX2iJ0tgPM3z9O
oXYaC0zOMzV003p4tOK31ZLdMlTEcJ2cJtrbLsJYjas6Yi6OXMOsHg7gSTXBjLQtXybdEiD2HdAA
cxKEiahLvYlaC0d5AE3ShWosvJ683QBnUzO0ko5Qxlcd0zV0j+jHu6x3Zdl9f4SsCJY6W5FzoboD
pOX7sufeZLWL+NggAD+n/S6/D2VUn7tFLhAvOw324E0YmIz7ywNmTc37m2bbic/sG0H1ghxm9cMq
ubcGTv5rc45d/eLS2gTe+3Zsw4LItZeH3xi4yFD9Ez6enryCpTwEJQI4TWjoUJxkvwU0fO4MHlol
Fz19ZoyjaSxrMLFbexWJnj5HkRlsnY1gHVtddwawCtsHnI8e5ej/bpA8FMQ83AagXaQynWa8OwqE
5XRZkv68hr3e7B610S0DEZDRr0tprywiuU44ppVRH+gFmBzng4pjzuVDstETGI8TNjIAk6bnFATZ
Q6KhkxG0AU8zrRLUhFEQcediD9WPwBlTH8KQX1U7JrjyXJU3U/YW79udiwFNpqkGaV8HptLBOELS
VsiSfDYCI6orajr+HiZPlCF32yGMsr4TpSOTnlSMLNAfCLw4SP7orVJxDLzcCU2C5suqFtwipM8d
ou1lQpeB/bhbysHhWHYXPwoqUUZRMD6F6yLi7a4xeJqE48glk52/s3NrEAV3DmFpyvf9flhEhLvj
XxUD4cZ4ByyJDl6jmfjqdHE3x2Yv+e12PkQkNit0SXS3N9gcmbkljh3l4Y9g3TKPTqyYW+Y0LCt9
tFnD6aK5qkZvgip4LaS+UxVV2dYcu3YTQjnTi8gnFs9WURbKmrFQjq0aHZNteirCWvvZYNZZs+X1
Gd2f08MP7hAhBt9/bTM34I3eO4rqDYqkNdpMswVL+8w6ztI11RXtLOHOBrj2CJgjj7KpwhgGJm77
MqJPdCHubvOI8PK6FChDv0Tg86upBajvk5WHiu+85XZGnsv+X4V1gqBMrj4wD19QbxCsTS2PSC/b
eFPHwspJ2WFED4HZYlWqARuQndX9H0ZlpN3c8Dp/Tvkw0mF4FiaepYgzi1rAzkLSNhKFcTL5wXg/
ZS+35+dkD3cMDdmsRaUFTz/qZ6JJ4Md0yyqngD7JVQLl/UN4G+qGlpy/leLgMtp0konS1YXTk+xi
z4HxnBeqCzs/TviyPugocwH6GiWSuywDs7r0l0TNXiM+6KcfDoY9GJgVxfiTStzDI0qqK7VYspTC
BtaHTCmcM9IZsBBXGm04FYUWHV9wzVGCGqz4ECqmETKSWIZLtk1aNIzc0AW7ezNnwMS6dkciOV+Y
Oj7tyYO96acOgXrTcStqHHmXdt3vQLKMN970VqJVUFCfvru99mUZ2GqjYCM/Go5VKbfaAUeDsjU2
ROrpqW35w3Ggeu77J0Asw2XyX047n8ncslH6Xr6yzThQ/9jNt6C06f7ulxBPMLeB3orON/v+odtd
ENaqQ83pgiea6Kx3cf4zUDtXc1j9eCapFkOqNmjzBiZ2TmI3bDu28nDmLffIr8Mo7XDYqVT83Fgp
s8G1MWe+08Q/3b9Yf5mXud98QW1Oe+xyOqzr7K0P8aNCS11cE/O2U1YwXOyeBD9LemCsZ6o0SUIC
X86t7BQhslux+wG70F9a2ZkVecG5tJwnDbEfkbAavm0zMdinVl/eaZmwn+zincK41exzfUKT9AgZ
ABbU7Y7fdEr9KxC8oD6wsI3QBBsvVdkOJAaVZsEy6k/p8Acn5PzfiqB92h0+RkQLPO1hA17dA4Id
HeGDXZ/abuK76c3P0+8CQLhk7Xr8P4I5ymV9iODUgiWKyhx339ek4bbZrtds4/JlWTC2MboynCHt
kis2uE+6guCVAI24Zg3EE6+yjuIrwMRGFjMLBjODSeMDOlZqHCqUlpeNADIolzFca42Ft5v50ssc
G6sGC/LPgbobmAe/GRzoxLZ5OdQSsg4UcZ4P2TLFGDNqMaqur+XXS3btImws6tBms/IDNORDrVZJ
/qV5JwAawlLX27QujxJeD+b+MrjXujp7ua1Nn/asWr54aYalEwo0knlWdU7ZmRWWQTde8rKXAoKX
B44CyvhfFNM88iUzsUgJwdEJ5PiOZKnPVCP7RxLFNauSJLEGyZmWkjuRecQsvkxHS3hEqFms0gY8
afljEVJLSaDdf5dqmIiQv+hE4LJSfOZTAclMZxrLfphMiJ/lRWqvfsgcS97wG28kZN0QIk29VwYM
2pWj+CaMY7Bq3xd6ctJPPkOPRlisvw7MjJawLOtmAN7lWj5qjdECSSGszu8Jr19kE2FWJIWHO/gR
eWmvDEgiKlul/+vUAX7VAbBMbBDYGfJKn2uUeYqFgEd3y8ChujRqZfJPn25b5Tl7O6Fd54o2EFoz
EcsYYBS5GVVfK2OuvojNzLCgFi8cdTY9W5imEz6fzfRVmVh0V+WruqAieUCE8CSmMagOW4dDJql0
BpQ+dNB3DTQa6h6tqc2RG7qpk36H1TGBxdb7MPC5TOfqnDZh/hqtJMdKot3lA2iX3vBe4tNNocCj
bNuCN+fOHUN4gjlIKVpm2/NcoKxZaLk/gAIM6C8Bi2Z9V9MWbWaBU7Itmt7ZzReQD1nVK0OdzAwg
UTnJJp3mt2tMHF7t5bRixfsN1FMq6mtqlxdaSDNvp9BD3r4hbdab1W1AKp7//K2TUkjtq7USaQca
9bNbXphm5LPpfAddmPcaiC4qUWL5QNRAmN6bVgU4wMNRGRY6bWZ8Kdrn0nZIB1wI+cL82F1MhbND
eGQbKSX0Peaq1IaoCXwRSppv7P3GZ91u41ucW3HMkSXQKBE8DNsD/HvTOE17PqpMBmarHOPIOejR
QfqNy5NwQ/VHvdGyfrHV3Tsx+y+p3QkLLAy9ZGseDjs+eP+ua1AVZwMwtUZwk2chRu5k919WQZqt
FYujseHC+GmclWVhpZsob7yoMHJ7iBV0+rbErvdUpuHrMpXdyIJd7WcnAQEK78Cxa0E8etaeE4S7
YCt3ivWQMKQKUpGJ3PziqEDHhyrzjSycWxmCR46m61mrLkuOgDGRFbNRKc34vuhrENMSkZQTHGUN
1qoFm+KNEqfKWMwQhN2229+X2DH5rjHRVaMHIoziww0T8OQciDOCHSpmbcl3oVKy5iEQHoA9nbfi
3jmoIMKU5QI/Idt3ntoZjO09btw/a0Hr81pIyyXh73D7J8Rb4vcQJ14arqTFyVvR3c4G1IasMq04
oQaVJv9xEX6Lsil/ncKNUreLW62BY5ehSplzpTlchLqBUKqc7RqeoPvzfUzZ+aN+t6LQi7xxtJio
IoyPk626m3tPQ+1mY69zlJswGIkRbGC/50U9x/u449l8U+LQUW6MRTnoVvJjKyOamz8FTf78aCD6
PYnn1r+UyGw1iiOBStx6dZuKr///3ayARdjG2S6yJtjHL90eEb8ykj5g7kOnfi/RuKLiXwXwotE5
BPgb6/t1R5NJqtoqQOuFeyRrNuFWoSDHjPT9RYx3X1wzBXCaFCPaUksEtyLP24bxxyM7kNv2dMtj
pq6oxlr0+3LQSDsEZ+V7pDJNQNkuxLjKxB1wo04J23sKNpcscr6p4WxkZMHfungmD+A7QRhN95Pg
BV/TQDO2DTpSECzVxzEeCjEqtn0ijHQP7Q0pOfyDwHvIUOEuFyIBUV8ea61DO+88qQvK/gkbctEX
e8XHj4Sd37EsRdzGKM/v99HBFwyJib+EAcpdpJYQFwyknubqx7onjOALmhGOyJzC5bBRHeMp8uTE
hJbSKqsDNNNI+t3OzfhW4Vuzm2tY613itmGPK0EPGNkhhDCgXeHGVEAcISw19KaAtREFU/NeHpR4
K73L3oalCjIH6/pkeSI7sbkEW9buaonRlAPO/kK7DN723DsqdS/FTAMmqnPOfYqkryB2dgmuSoXj
dFz5qqUggATLCrkYoyaZVBmvDXWQbGQVsC3FqUTJcO6q2sPtxysSPKA3ATDbNWCCNzfuZFaL4aUR
tvyprifHZFpRYOzkv7vV8P8c0Dt3OgKKdaoBJL5bFCE2yJZICkb5FGWDnKXPGqlolA2axk30r8Gh
rMhmaiRFHZO/Or+r5GKydo5XUDqElg7r9vLwhQ3bJBDFDGgb33+WQhBkALnS9TP/KIwZ0plFJiA0
A1/RtHSeheAcdvH5AUDgaPJ0b6zA8m/MDKwoOcNIHg88w7IsCN8wkbxxfHAKBry9vo8qCiEZi7jp
gWECviXQsO/SrW3NBeNn4vF6sDmwGzC1Ed1FY8KvU15uGCdqNCCcMer1mT7FK6nbQ00vqu3b1HUN
baALvDVoaR9/2FwfRtEgkADGijx8MIBYpk+4v6hTZ23fGsv0GBKERBM5WjiO8zqZQy9pXUtC3CNC
TDWuJPeFWbijXYQFX8f1fV7fB0qe1wn1baiEiybK0qNu/91n5KacuLkvGJ6GRDVd+4mu7XNZgUJ8
ApKbV7BaguhLtFALFy91BYH+7HYKjBx4sJpzGECMBQXQTM9safdgrMMbpUmU8LmHb+unYZiwmUv3
get+L4nXNxWX3VA6zmLMVByGFmeBIXNRzwvWqmE2ZsFbV60Re/DOJYRoQod53mGD6FPKr76NQexo
PkfMziHZA6AXzMWIINnVsMlSnfbDaUXCVx4Ns3EpG69qfG6gS3X9WqB4l+8ygB1+DoV+c+8PsJBF
Ng6vW2bXI48fv48uaY+W0cfZtMfp5o1iTlnsIND3dM4HCdgQ6wMUbeSJ1C+K6jut9xR+dDkjf2I1
3JziqV6ajWbuOVi+nakvbOFbRYSuwTxYhHtjCuqXOaPAhcmV6ox4lTFWABK+DlnHPgIfxHrutIO/
rwOiXwEJR6ozPfONkRuooM2YPJEK2aH6pKJfgrHFqd7Nt2RSRF+J0hu+JgHXLsht8L58RDKpMyiV
MuIvJH7d8f7ge+DVVK+lm21tLne/2deK1HRVQ+L7MYVaTOHgqmfL6xUWf+QZ0XFOWeVMqytOfPbe
K9m55TkSwgp05xr5F3Zl4NV7Z6co9mA1XIBG5wuxxI2ErrhPqo6/rnb6NlTWVyzbupnIdYW21C9z
gHWDUyYNHC4aZGRCXaAwPT2ToKFKlTU4Hf78J85pwccifogVXio4eiKrsTctbaB4Thnmbu71+YmM
daN4jtrV2n2HldFy7iEFcoK1WmeQ4TQ0eiGY2wmB879/nnJRdx5U5rWGU32IWoIVDymGqEfBAbEw
vhDZh9RwGudFXK0YV++j8IpzHlXW1buo+FPzZmhRDxpRsIcPNKKHBWbEQw5XYAPEYhzlGUJzdjXd
OoMqEHyktM4IVSNSLkB9c9ubfiZXeLMWx2M/5izz2+uqlYXlrKnBo8BzvpRHXwrTcdqlubjUrcdd
pDF56f9eCdI/kWShVqXvuGjdiBq7iWe5NyAcpHK4Fq7Eifge6XYif76PIIalnmecmQQXCNhY0TyK
9uMJUah+mu0q3GVb3+RsIZm4HaV2m7btmxkkcI3f9utuZ305Lc7YZS6l0PoJ0aIcqAJ7ddwCgCdS
iroeVGs4bNTJJMbcZ5UxoNTsKDxPFcfMywNdRokU1PdUu3Wn3rU6qJc1Mn9A+Eu1kE30t62sJjfQ
MuySxQ/Lp+oRtI+j04aifTHFyPc665Jc4T5qmwT03eF1UUF1AuI7LFOErHbR0zQxmrs5WxGW72Vi
kQw1fSMu35VXz9ZgvhROQTjsYnmILxyrOXnjSgBrI10pCLOB4xnbVEyFHEGDAzEDFl0Rg8e13MnF
oF7bu42k+MosiSmGTt5+0hsDlnatojIMggBPQfbzZ3gIA4fMOlB854QHoVVVIKcd0nnchprM2+QQ
gl94wEiWgorOqH332dxPnjXPFPBqawIXz91Ra7o6hVEM/Enn2fBpzzcsxx8mBvDTfi2SgoMr4pUI
U+LvZJsDTsVhIsdFg22rq1G0IpGmrxQo9quujhTP4qL3SkqSmTbczps55Wo4+Yo0zcgvyrTHQ46B
ukK2nQIUkVX6BMS+SLOpPKl+gudjT80bTkU83uAUYOp55n5w3iqFvuE4zSLx3PDCB6rV0Cgq4DEn
yK8+hzwQgPgBvDny9Yv2ALvi/QgyamggBniUQrtMZfeo8u2GE5zBS/a3xY3sOEPXYQq9EnedfXoJ
iUNSlnoncbZ3QgzyUFtEimh/1rKp5IfHl1WrUL9fyvFUHrHiEyzy3v46L17af164hW2bpcTUyu9u
bf4en0927vssp1jkjZsybpsnIipYJby+iuGw/gS4IFzVgwRqHFE0YMGLQ8hGorWMRkst3ujd8YuT
mBCfzGCTYVCj4149Rk7ZmBl7z3CRlKIojtDIbSQIMacuc6fK34X0HAJvGK/m8hRNkbNvu4LBFuru
Zdfl4oRZEkUtAwpgF0IK2TpZsoV3fSvh16C/4BRxExyxQFj8Zt8ZlSXvsdra2/WPYU9PlwaOoLR3
biya4TTJLHMP0GS1NY5tTw3bh0bQsl0t0CecyE5PcYNcOnF6D+Pq4O/BD6sk3e01teN1xt4fA5A6
v5MSUxt1ozW7k33UgcKT5LNKxSIgjTz/1rtHayAJF0I1KApxKXCdRklzQ8tAUgtd0doZtvxVulhl
PMHuIabSRmupcNtm0KvyyDtH8wPGG6ZHiux+Jk4RRO0reltH5S9unkpp4uTYPBLzWJuTVXQTxoQZ
gjrb8I81tM1mk/Ieqdp4Fz5c9/Qf2y/VobyOJr8EGOwKy8Ge4F5SLzJMOZkomY3Omu4gOZ9uEi/o
VSLoQHtJHZ9VdMsLqMNdFQCBgAiBFCpKIqNJCg4pUS9Ag7RCEpJdzDrDaUVOT9WcbE+W2dOU4HW1
Phg6YXZ1sd04DrSF1zR7mFAaUUorWpUikA6qRWHfx/jlF5EMsM5D3Kku2/WKHuqT4A2I8oT7WsnU
Ca1jT+ZOKcU6g2Q/NX7k7mt2entAgvqlae5sxDzQ6GlcPbgdzfY/O6DPhzp/HDZEwR/q09wXM7Ga
B/zNesNEQMZFNKqb0MJhReSFzBDuwywOQSXpX+/5YP3C0G4a+LoTPw3yz3lk77Mvkbdhkb77wn4H
GXL9Zl0EzMlR/EsxQdrfMb+YvpKOGZqy1sscpBPbdNSgculjSP1dm9KLjwYsUhWgfxCcBR0nVi9h
W7eluJqJDGyc5lOSfMYioQ/wy2/IbS9k0Xy+noZQgL+P6IlVg9X5vobexQkMJcHDzIKUxAHFuHkK
qy3kJV0903k4XwMkz2qmszXRMFElnIJzXq+Wa1MudQH2xub+2aLZv2sO9okI7qGqWSLJFS9PQ+Ds
40+PwTe7nl84H/LPG3mD3/LuhX/k9kS3QFqzW4hlVfO64nEudGLsviCkwwLo79AnwQwnlvEB3K5y
HnuMyeF8DZAXJZ4HNwxj6gOJJP0pBbQuQ1ZPRuC/d8sfBsAlwRXZfrKqgjCIuleWob19BL67ceOI
Rr6rGrmfBqd3kjZHknb5sO2c8ZHlQPwtJP3aC6UZp52dsn32QCEHMX60L1byTocDavjeTqd1bKrA
pwfSvjXbhY7Ic1rOYVqfjjvzNhgRCzeITuKqezTfvdsgjAL406fek1Y6KQGHCQEC99MhgTfFDNzm
5XAdSGodUdHZ4PB69yzAUnhCLfcKFCxLIcWF0vaCl9KicHY0tiPNJFHrcRTVm453S9PRHIYtFEwO
ws2HpMya6p+Jl5NHhqYyTfjpns8qWF8fjJE3tfR4T9xufELfbM4Aw17Y5qYA9uofN0Q1is/2AkcQ
s1+bUAdC3d+7wsFQ3D9CoHR3uA4xLKpmlt6olKnM0uGwv2xetyLttXwgYJG/8d7OOiQrNUSDGDUR
kvCNRJyQDi3PWJ5BigzDAr1uL/rpVR+Jx2WhFS537szhGcbijZyd8EW11Nwhs+onirrfCcdgdoYN
CVYgJY/pMXExLMpauu/gmQsaOqrIfjgCrkbEUhx0yNzXhwQHvLWmXXG52dixvpJ82nQxT4lQnHQj
sxHErFiQo3aktP2kB/RmT9W/5fVHSeLsgCGk839aCo4tWeUzOQa3Frc5UMSRVHLCHH8ZHdXQYdfK
7fsAsOProJUy5T7jcOSPFn8gr5VPQTFGCsl2lj4Fp0y53V13SpqNY6JEBTd4H7Cen9HPg/oyK++q
o3gxetDKPlqpFbFPiM4qWt9rFlMeXb/YwaQDpSus6he/BSKM62pK0GdsyyDpdEp+wp1KzEBJ485o
40jGEHIxJ97mi4qVuOObddAKDV0SFI4ooqPah1rcBSOZZUsAAr9vZNTE7XZS+zluojZiFGMBDsLT
VpNtLRjgQPkc7Gi+0VA7SDmlyBGZedjgYB5lDoOZhMejnnwpqBJKuFVxUemuqj8vKVuLD2dlqvj1
r8rBJpR+ygMqn94X0fCzqKG95n2J6ikWtE7LwXhC3pb6VkAJ6mTc0wIQDJYzPBW6Sgl0IjcRqBco
kn34QqIRIvpzt8WL88G5A4wIkK1GudKJEVEAvCUQ24/4ZzAh4niUGP5RIRqz5pqUaH6AsDaWPoJ1
P0auQbGghP6yAqfiyWLzwxwhbGTcZI3XJGzzOyxTjggI48XLDBHle1U/knIdNNEmN6rcIFB4Vcp2
O5+RJhnQdwI6FyWOcB1kSYcXEuAhrh0bohNuCPHSdozeP0FUZb8BO7jaQ4cpKBc54ky+KEcasLtn
FerVE1hLAqTnK2ZsHrdQih9yL82ylK3ORKamhpWcezHos1q/FnGUPWqqNk/IIFpkCKSweTWXLDxk
053vqRAfznsHWUAmrgcVA2/n0FBZs6JgvaefD08ezzbHiyt3VsdZGh/6e/sCdDwLBUrmmytHHiEf
IuQA+YmTaSz9bIKll0uxh4CVaKnH+RMl/2oUlkgFc8L9pqCR7cNyUgoujr/Zk67o+WclSoIs1bDa
BC+/3VWsglnCkpuhGda/FjqVGQUdoMCRYZWbZauH/IXcPWRufqkmaFk4S+bitoZ8uPZRRQWErNd6
ymehA22RFb7LrxkSqj5qdz+ScX+VwZ1Cc0PT2nxJnNUdXO6yR6yHhgXWNE3IR9dSAqtv3JQxqcer
dnP1qndtfJeDxHIk9ghAI8ypqU66ANxWn03v+A86DSHQw9G5hDckU/NKI1zvDrDTVb3fc+iQgAqq
082Olg/+NstIB1f1OGodKunHjOElkoYX9sbZL/OdcX7cZESlfboIy7BFaeC++cMgykh3fnz/IleC
EDTutDNwhvF9MASCv5+i6vzawL/t7RvTL1vXeRbgisaKMyxzpIw6oBkHvUGArmPKk+tKxZT5gaRh
9ih3GZgFtTsXqm039RAxI9sthocy7nc2+Xu92CLByucx/xWn4tY96EGzvVJHqld4oQ2iIVuGEjTx
1SMoYjkOLbu/QVNv0kfrNpKHIyeVa+LLEZLRtGeo2p/EgWPcVxYt4vfyeXJGD2m23PCrWOVoQj1a
Jio7iBykaicb7Jv5GsfNS9LMqQr/xgMqetOm19n1ljHJVjqfHdSXAS1Hy0jdi/xOxPybGM5yHUrr
dMn8J6W1Th0NDIYqRH4/gD2ERezHW/0i//6kAO0r46/bHex1yEj6oqS7SVWM55NVQMu4wbpIuoT2
IvOWEWlkYg9HzQeKk08EBc6sZbk+/4i+QBv2PNpGke7Zr3kcAbneaHerw75huvbkXMFC/Gaec9EV
E/eaGpz4HLfqaFWFYENIKwqOxigrOSG39lNqi1Jgdu4AQq7Uz851tegWRuim0idbWXCjSkihOhsi
43mM+t5covJZmvelv6KfqTptAMToecXYgvV+IbUS+epdLUjOa/IPU6xPX7+6AV7LHKNyfMFjLfRq
BTuHv/k6Po3cYIdv8jQjSDu+O0TqmdDNnE4aoleX/5AqjyciqmavH8cK41bSDU+8s0ABJ/pvI83e
IinDOAYNi4kTiVkxWnRnFXFmYKsnxgghZfbgla11Xov3rkNc9LGiQD4i6Qd6fRQLLCnucfsFlSUO
rB6mj3JBqf8IMhbzkH4krVYiizewKq13oEMeHw8BOT+zerbwpFOple0LReFdPQePSO6iVBNVv9nf
FljSvovxqTDdKLT31OmYAtEl+WNcf1q4xiIQgP8glN/jSay70OB3Ao3TdLueuLK88b0/RwmHByAD
jIErJvAtsKXvuKjtFFVvOVNWZJ3FhvPq58QhLtFBbnVHJcNE43nToI6I7dDfAC90T8dMfrXG1CiA
w9v2i5+ccYdEBc1Nlmb3apqmbBvrTFtYMPXm9Gf8/nPEwaBMzDqHqdY30buWZ/lP6cC6/jjovGoI
6BM+cd+WtfbQG4o9MNj1/5e8LMT+S6sU+n8o+YbRMtyXVzCF8dz51gMqja9qMKwK4Zc6K/ozO9Jz
4lcSGir5IkvMxsy1bPSqSrhfJwWQNjJt6nE1N2HiKkxVP1CMKRA/nM7iGtPiC0jZmCzHW9pZIZRo
Xo/Xqhq7yOiOMev6LMw1uJxMIPb3ktQ27H1S+Vo/nEyD2tqUgD/53nvso+H0Ws1sNNEdVvTuBaCD
g9IVneIsqLU1zW1X1UPRyqryLrNzRXk2Kg8Nwfdq8GY+AArOUqiMZGAWPopL+oioLFOY7VZbTMq0
Erz787R94pIiFz0lGFAd6JfA92URvxo7noIRTFBL/wePo9AvneAlPk3U8P38GJr2K+7w4BJJDkaS
GVTb4WIV/XPqC+N/WRzg/nQmBntC6osElQmOAGHnY95EBwR0thFMSUn3f+J6VlpQmPAEwi/ztCL7
eZUMoOrvihBUKUC59VnDEJDpUcgDUv/ZEWzuuP3EmeFTWMMU0mUHmOGuK/Eh7+4v9opAxVMU9Rlz
lGZJm/dRdzOXndtfIpPUrVU+46J2BiYCHcC75g1DoEk/s9z9pPYBydbleqLYbRc8W8MWbuCdAQjC
IGEICsmRhPqCVcpMCoqbuwJQzZC0po8W/RHm485Gp8t4dGVqpXZnPsvy9fALMts9DicFiVi1T6+Q
B69eL6GWDnlPJcLnOl6/jKwQdP5Ah+1Lbl4gcfOEZa76z4zPkPjf14OF89fbxLGpQw5nQmOt8AUW
5rWrjnLZN8iOVMsdzHLC9E8fCYqhxmp9kOyOvCIGOGmpBLQiGn0hUZe7+3z4R2qtTy4kRg5XE6m+
bsGm2lB07t6Snl/Vlyl6avXWVFMwrKcpC6INIwLZB4d4evlA+Dxa8dpbvstUGKgDL4f5+6/jgbES
0mL3Wys1HlTaLhsC8hK8sYuBaZnxXjCRBmUJ/a4yh3m+NwGdyIZHR/aKuO4AJO4y/U3c7vvnQGjR
H9VRKIypDTNo08ZNExroX4brXveEpY6zkWYbHdSyCjQxUWsiJ/9H32mWg8Rku84rAbAFgJpQUHeW
Xq5DpgxUQlJu7xoE0CyhcScCTFTnNbSRai7++3ejkI0LUbs0kr6KsujVU4HHZgwCaBceWdRW+ze/
y8uSGbSC0ZXZ+MgjC03IZ8JTHN1ucbYSo2TsRa6w5s6ckPYM8gzTyQiNE/9Y0vmqUKAIdwiGUscw
b7rpB/sTwsmvp5rFI1xjhbZpsofX/Emg9GAxuf5GGgZYl1yeDn3PoEiQOoNPx8zNWJ6tq4bJ1Aq0
U7KITTk8zb0kaX82DiqmaftwYnlxkmjqVI6PYPJRfd5NroQ3w/tAIYNwOiCrzVXoPYUQKKqJbynl
gCaP1UQIKyJm5rVp9S88gD9gYbx75pIXy0ZgR46biQ0nbzia1Ag04k6/2mZVvQNSoo2TUGu/aUXZ
sd5QpqJt+/u2lE2fg8NcLasDQ5ZMBkFj89NnKverMeyXeBWf8n7tCL2HmtjG44ZLdfUQzsg64CiD
GtfS1dCZW5ev2qd0MQ99wNaadW5hGJFjH4g1Q3M2EMjXojLi4uyjqek016MsE/HONHjPvVl1E+he
WjMFCcma+UXekapTx8nGLdsXM7Fy7xTI1OZ3mYRlTDoWwHz7eYBNOd4CD3+ukB/KviwSK+vpaQ9/
gRN4NV2uY839UngaZWg3TTpd7XFn39zv2MW1pXCaC7sBnqyKpSQUn7IvMJuz25hJH6aYQnqF8JH3
JJ+OOXNnXSfCK5qstdTYe+4z2zXKoTf6suZ1ce7MK1XOh7xtGqjk7UBvUa7cOJumv2keJcdZdzxl
GMgOPANj2j+wMDgwEFp3+ExqW2jPscvHK4EnDvsDQn0LMyNv/5xHN0B+O4R+V3hnWBR+z0Tad9I/
t7uen3stet19Z6V0AMNVcj03P0RM3FSXru+QKzH6fCyKnI438N5foa10rKQEPD6aL+um8rVVRqmX
o+uGUkD+ge0xTu7dm1xtR4dZEQfEbejRACA7Io+env1kaRG6BLP8OqLlx90s4+VIetQwSMQm2g5c
CXFC+8chRmGS4DPGa93mgsyV7lzlGUEd9Cmtxjv9gfqn7pKbx7QoRPBph09fMizTbL08MojM05DO
3asKi5Sttx9fmZ83hUFXfDBAyx01fk/jmnRxMmBz27g+0dqdKkGI5uKQUr0aQS06B51nsy8KsRr+
0tuStduUZnJGpIhs0IzGsjiqH4XMGpeXAyjLn8dDzsMfh/OUiEXZezMMypdleIzxsTKz1qiP8EZ5
BEfTsUYkTq0ARWNwTACABsnNTml/JWSjTp9JrE9litD3j7NVOZh2fXlx/SyIcOcFIYWItW6GVUE4
cIYdRDb8PBmBlKKNtmaVuXR+JOt4QH9I4EacWdUKSjqwG05WDc+LR1YFacILS5hSQUy+tQThTFqi
pIrvh3dHKKF63Y9sGc/g1ra7zkAEE9mZLUoAK+QKCxSd4mh1JlQQF9VXCgJ3sSSlknTeQMdjV4Yr
CbVwL3WdGOe2h5Dg8hlMr3owJ6BPkpA4fjYakfWDe+zZkhRKJXE2iZavmO74fGSkQBNqJbSm/SXv
9ZQIunxCyvo82UEMnfZ6ady1K64vQiBH/8aCgy67zY7Lp57C+uAaAXBizc+P3dJA70KGEAAgOFCa
r8IL2zauosKWhATYYkON6tJrdB9eSKx2El00zqW1ZrRi929gI8+96YldVFB1TPYKwLGNSpLUiP1Q
cIlocj528bnWgjDwPlg2tpOpurHfvrvu6TjzdYyOK7WM/UCADG2IEwrBlhV5smb8Rxj087ecZus1
Mup+37MILuGK/ep67sRJOEAv7va9GneFV69kEnFD1v/CeLMi8rV8iJHLBNRqzH89cgMh0F1gT5YO
hhlZzQTDpabXLFyntRYi9ZS/525SldZyrChutA5HszLHq5DkjS5emYN8oWVcdjMiaPaeuv3BPT7B
D4+jVSDBGDRfsQV2NoUymySANGlMIsG7dtJI1nYjdmQdMNcc+Wph44CAMArV0URTDUbn1rZ6hh6K
1RAvGChGNeODWSZnaImMMl9kBRgW9caWRSEIWfXUpdBsR6X5FUi2xOaXRf8+hU5xSe7YnIfo8Aqp
XbHN6DkQRaWlBQ5XxfIZuvLmEa/0l89ekS9P2zJsegA6pLFYxoVSOvJ6s5sM20sK/TEIvDXl4AJk
YhfkpcYOXi4COi39aq/PlFwi3Qlh9eAae3JKwyhrAfMLIsnWfKBtM6+Ts9ik6k+hV+tAmmzjJxbd
8uIfr7sUggHkQTOKkf9nadQYJFSca9g1Sv4BWfHBP+X2ne0tY0uOMm2MbezoZpGPcSj0jSCMBqM8
u3Dw08JGE2kpDRu8nWKpVfFzf5iwTJ1Zn42/uvkeLJ2wkExDwC2Cap+Gd5RAarOIzpjiDwgbEdp+
aLpOlp7FVlJXCkJFDjxp78BVGx2P4XWk+sYWb0dzWrXhnmSX8AyWn7gHL2lPGAGjzJWRx13OZvhW
+KoYX29SmOjzP6BlYtMmjJnU5wvXt7uSvVJHBCFK4CYXrqlY9yy98LLhkLKviteWNy3s+Qx3D5z8
6G0+m8lBHFaRhv7GqbQALTJUTzCSSlng9HrpSs7OLrKhsUzR1TSNgPlOfrL3Cf84PNXtuOeXyj8c
fP2QJBBnPx0vW59di9PWX8cKhD/x259cGYjEBe2gvwN56EigUKSDlMqj249DdwcZlHFLYqXOltFU
x73pWM7B4KES6ghsn7yRighdRqsWLq6CFli++IdIOa1i8yHyoVEJxdEYhZ0bB904bfsQpuLdeqyN
AoA6XQP34SW8qzzWXh8xb45g73gxJHw9Svz/W4l24AP0w6vpCEhtFe1pgRIBs/zZ4iF8fuBDRCGf
zEEbZylg+ER4QdTmvs9ps350M5Sy9SR5LXDoADJqejOGxScM11tVRmXh14MXe43JvYP7XL4aQels
ApTgTjuA8mHjkeOwEQIEiL+4UVogZ5v69CWCgu6TSGh38a6Io+gTzKJ2cJN+UoGIDIKHf9Z6kbgL
EQwOZ/r5I7pvLWyhdf8+JvwTyuYEcy89rSSqdnhZSpOApx0y90Aq/s+K3eqYDnFpuTD/vWDNywc8
ewy9bneYgl9hSo/3VDoC3yLRQv+ofTUoyjio7tYcYLRgQPckyKSeoojd8vcj8Ho9dga2ZKwWB+5z
Bkm7EfyR3sdloHubM/86/K1Mw340zIOfT38G7YbQNE3HJyPVrRL1iQE669eJHmuU+YP9Wwp0JvY0
zyZGfesksFgYyF9c1xJl6yhdLHad3AayPlAS+F5/dXVWOKCV8wQvO2ujxjlPEIlhizmD7AVcIYhZ
SM8LkAn/xvZ/aztxqrXpL9fn+E+scG1rah9t76d4x65iNYkbbmStuHRNrUvDsOU21j25w8ovXqpM
SKwjmyn47VyigL/1HkQCendGzAiyKQQ8xy5/+F+L9cdIz+92GTZ+pUyFl34wNdvVo96kjkOX0DsA
jan9kDpweI/rYUNj9hpAmc0DAJ1ai768wkZyeBfBZRBD7V56DSyPsusYRWuA8tTrNboKBlLt3kwD
5X74ozeUpnjkSq2ZmJwU758y8WKH6ui7GoNwdIfkNQYEywVYliWGgMdireCsnUfCeBv+mGASRw1/
fMkBAgt22JIjWMVCiHYtxRRzijourHZWXZ8034ASbZbsKjC4OOmVkJ4njY7DTk5F/v2F1L19I2qL
wqLuaZbHu7c0y/5e6XwUz3Ng/vdRUqiLa9XBX/yIzjcamCY38A2LCwdKqbYJbYtswZh4KC31JNib
89aSggZ5nP1z2v1uUs5f5F2ZRioKZqJIz0YB5OnemdDvPyCHrTH8NIh1Pj0e1IIb1HaIvaLsHwTL
01Jfm/IQj1CxGMSqWZc79CHAiF+ES+Ok4UkU6I5dQtf47AMCtbGUtjnDU7hk9NronBV+O//vCsoE
0U/DChqhwPcJt7HhRVHDt+rBFzsVa/l1h2+cbkDTrLx+xA6gu0Q7SGZgjava6+L8l3YqzaD0PK56
bHcN/Ijjq/eG5AkQx1GYGyrG0ZOufIG8m3868m5qSQff41qYWCEkIgySj7LID1X3vzVOiEsh7DrG
SM3lLVo5A/zyazKiMuFP3wmgy9uq+hke5pqaO392hTRU8X0LB/id5gozRP/LbESg+GqoM4JT76hZ
xvZWIcybtL00B7BMN3kMlJIsti4qq6CO4+utoMge4rF/gPzQHyCvhelVsETy3lnfBSiUogTF6XY1
AGdpnQ9eCcl+kGXV3BKwGdlZKCv2eUaXoHpyaCx8ISfe8v4VfRCGvCKqOGvuCb3j3wL2mEtCmxeO
yb30gWeoV/N0vNrA/p3YUskwk3n9+bscf9hDfgh0WXkahfCGgejEZ6GcbiE6QLYVamxrf84Clp4m
1+db2hu9P1ILwotn5sikAJvcD0Xb6kBoEC2YljfK7wjXoS6xtWBv/hDoJrz0NkmcBXoshRSysgyC
e36ylVw8dX1xjBBWCAgvAtsiIoJbvwq51ybxf07xB4/lZk6X0Y+VoZUCIhOeeQfxjjbqc6QU0sz2
TDEFZooj8KGkARQ7A25g2wGtBoddIutRE2GhHjQZ6ZBRHRyKELDaFeL+2WrIama4/l5+pzDKyy9j
05azIJwcGR78vcsoOcEQ2PI1Nu7jrxvkPfecV2GzwTjIqryDRqywd/YRx04my12RM5OmA2sFcWwd
z2hDf359KbssYLttpgoqXOYv1sMccwJ7cLj7n/fa6UDmLnbKn/BpmJKRQary5a7moyMxwpR8XfSf
AA/h8rok/STba3wmdYGcrTplfDtD+/D0tEPdvEaxrIxZGglpDT3F83nFp6sMwMI0J1YekFSFSvWT
aMl9IbhtiWzxkTcmyykbeO3q7ysVJy9hxij/ggG8+TvmnIROIzpfIKxVCBD/LbNy/3zfDtfqGy81
sCo0jllgvXrvDCY6+vKREyUtkDvCmbmI53fAQxUTDAp+5QRebpnynBa7jF772r/a6O2qLh/yztxx
K8/5dSSJZnWqpASznLukHta9t7DqNninOOw7GLFEOKCqx+WYDdKZsoegUdAHYqrJZc6HJH6vo5M6
VJ2epKDlU6SB6NVf0oIIEzZbw8Fb0L0jem17RvMoVKqrBf9L7qhwO20shCHFwhEB7EJKVq5ezGJ6
7s54rI+OzLQ/QY/gYCjRmuD0RHX5qrlpDwgTTParTCqXOndgJ5cWiZvArJeX/outUlYWA6gJF3da
zoRpPW4GF7oTYfyJ3H+caqGSbjURwH3gulwxXUwB3weQb+E7rJG7SjP9XTHlnWowia4lOkRpcDwX
JpmCOibKqb4H671Av8Hfno/WDjS/0SMxaOgVVdhqx+dpCuK42pnsftAM62VKAQHZYUP7BRXcXGCC
PUCdJcsBGfhXlNhuy3b7OjWessox2WkA4gt6xi+46POknnGwC/YvZwUf/96mUqSm0lHSN6N0vYhZ
1sNajirbNlgrHuQ+5Sephf+cn7tA/I+UtJ5/dy22GgKAHROG1+sSMeuB+Ht2ieownnFpTaoHOWYP
228sNCX+3UKR14GRdV1QXcXkgeC6BcWqQVOlM2NMhMGIx+uNh9aYW/PWYmw/GcmcpztYU9HRpg0r
RwLVBf27+dwy/5+Bdpsb0WAvZF8vh/toVoF/mU2rx6g5AsePBS0z/1HcUvOaupVnwEtDYrstvfUF
bLkNGqFpiSHHe4KtM4qY/WNyTQeNhFkjIIiMI6KdhkcKdwTeGCbt2tQsu8gtwRzFtZ55MDr+hzYl
qj0bQo9QeorS4SaCfB6uMLJH4lRDlZhlsfjSQj7WAfoDRS0W6kYwA8On+Cvx622Vj8MK28PjwuT3
PhbquZlmm4AsknBqiU2JaJZqQ+5E5Fpr5MZQF8bih0fGUJPeX4PDHhuzmJhHAYgUvI5fvyR7iIlP
dndwla05xWYUqHO90ZvSDNfmoPdNCaFWiDwjSvdEtvoAgPNJWBc6ezAa+A5CyBMHiYjVivAhA7Xg
Z1IsBQGojcx4AVd9+xtzW2cfuZXwJysmx+JhTP5qw2X2iVqPqp4hL/tsDDGViwRx8+zJ4C/FZVFX
WlqZ8VkUsUI7gJ7bQatg3w+EKcMBZWtSiZlOEIji1UlIDMbeJcbECeDSElZeE3XB0coTtpaEHNQV
sSxKLnJazd+EvuAvktpP2mXmJSQNRivNBolaDl1m4yzkwLoUKIN7uDUrPBBInx/AURTP+vZEpgDE
Lnhvvip+C1EAIwmiUbPpRtCDJFSzXruuZGSWhlyrfjisGegU4BUlx46cU5oNIHddIZ00iLDqyqHF
uAPnAO1KoeEmjrJZ12XMc+CHNIY/I9FCRaQNQUtfwmB2ezLTxm1oivxH1JvTo9advvNflmrhtL13
3xTWXkhlV+3+1+En1rSN3Ydx04pma1EtQ+Fr7AMnFh0sAL3kboLxBRAeSIdQm/wrLHPpw0rYC8Lq
FzLIEAB6QaDEZWyyb/25kO/ld7H812q2CvtS6HPmAgSEiCIqWVdXzg0u3IUb1DM+Vis4z/77n+0M
+U38SK7VqIOOYlV2vHrsdooX2IGlzvEbWprFzwYRaQZ9e0Xx9b4QgX8UU7Dn92VZeAWy/HdRkzxi
scqvGS2Vqe1ca2Fgy+jXyYBWBp1H9fq5AGhYzmfLoQ9La6veZ7b5s/DjzcXZAS3cB0auPBMsA/Do
nZAvbGoG632zYpjPD9C4IpWLv0PMN0THwY1Q1sNfkJA9ZzFu/4f2BXea6FCOtq7bHuGkkXHRxqjr
TNr1B9IBNUzTTj3G8XhyKK5edcFR62529J44okUK1qZK5TrAtlWZXQtudri6mh3Bxg69y+5TEtpw
BT5ha3yKOP2JQ+HG3vp2boaBtXVH/A9kKh++e0GoW2y9DxSA3xtvDFDxTML8yqPV+dli7GbR74GG
R4n8PBc3i8OqWWzoCiQ65s0QMHEya8pV+dqdDeHy44AZ1As2uB+O94ITznTe19/QtVKpv5WkSfi2
pmSUrRedSc9WpPyx4B+ioUWwpOY7XOlZ/oAsD5ZPwtIRqp8K3xQbMi7C5HMiNvFwJnv+sFOrnsoP
2yAgtxmhzxPbyzc+t6oHZx7z6aUnHdcaIffCH06VdYUPfXf6R2CzPRRDmWO0J5hglx7s/Eq+dFJa
3WljHsvR0QoGnejqDT9JmMPMSB902f1UIwmWPY87BtI97a+H5RrYnRQ4GtnsKwGqNctI7U6/gE9P
OV1FHx5E+1xPsBLQQP1xtt3olm4I3iBV5l+bZHWfUTlXFst2aCXL+4JSatHCq4/fXV8+0aqJ3SMq
e+mYb9XcRzOW0GgMqokS5e2TC8/wtO/AFZN0Kqj8AkSbb+tXViN4QtnDjj7AYsfIZJXx/UC9coEm
PnMPF07ZDBiExESWaMYrjCCKOELvTvq0pOirG4JEiSkQtmo/X4BmLqopUaUl4AXxThMxKOGr94Zk
ENlylH775aCmoZZGyKXT+77H6IwXXe4vR1JYwpGWInyFsg5my8umN2u0z1gGO4zmfL2gqBsoiokU
KDVaEwty5q5CvVf47sCl8rudVmXBVn/mhcBSBOwCekyKkgLG/nhostRPiD8XpSoNDnZrxNxrqxaj
GkZiJXbkAyvtXw15zx23EQIxzHcjfRY1dcs9SINKPvVCj9CWf/ygYlxqveojFweP9jCCNWA4TuC7
Vv3lyi85D4KAMnFbfICKvxYMBzimu28DwT9by2GPYpH59EuchaIHNu/6WVKTevJrWquLM+Qu2/4a
HJpsRnZBXYi5ltXXlI6wsOwuQjyfpEvjS8mj1OIkKv//d1/vZxYPU8RU1g2rAUGiLKIQQXUQk/iM
WItrX5rNHcbcbcUTC8AzQxT0VPO02snL7cLR/9a8BA2fgyMgvgnNfFyfBXYjbusBvZJC7ybIc/Bc
Y199k/xeHaT4RJnue4ore84BGi23AwGcKmTl6NQN3rjK/HlsUJlf4Q4jmQkeT0WyaW0O9sGMiMks
nd4P9v7Llqs6I6xSJCZ7CCz8uiVPX7HRkpy+1sNaRT45o/PprVLz5o+HsXqDIkxkLrQsRS8b5dso
kTzLvIqJC0746CSFo/trahoqjR9Ab7SQ44qk/EHe606GrEBaQobhmvZlks5f2UDxN0DPvZ9UVwd9
GnPj08mm0k8rtV9yV5L55sL8VITgdq5E7y6tTHVYoopICN4K1gX8ONS8Q+VKL4ncH6pEvDR5BtyF
1rM3azom2U0Hiz9PthUKQQf/f3ESuyVr9PigaOufzzmJXUT884mcxb61kl12mt2qs38pLR1KqxF8
snS2eiNRT02oCZWocZ5JjoojjypW5x3BemUMgVQPpb477XsDDw/AYp6AiNWZvPce06fCVqsUahzG
fcf2iY834DKVkFRmZ/1zjA1SJATvLHDz4yDfYhZ9ReSIKjJj1pDzCQAKsUSAMvHWLmSZV9sWkGRo
UQkpUZolA/RN9PP12HaYsWYyQRNmeU856zIyViaW7sKK0aianX18817+LUPkODuHAs0HWZODewIW
vIwCJZjNfrPPHp3d1EeAFUzrTt66b+LRGXyfArDAX20QBcoSrUZqm2b/VftuIRj5jviT4547kuSY
f222PGLu5bcxowaUJN6oSjkf15Wj6xskxLHs5BpJfbyT238y5VV8eg1XUHdMm6x3T9mcMgtVAW26
seJ3Zr85oJwUCOegWn2ePk1V/FLv23Ujt37H7M3qyjdon0DFQR10uvdX+fMjp5VFxr8YMLxgZBSr
wfhKc2fnKxvlnx6dAq7xlw2iPtHhD9BvPlHWaCSkjajfRZU+FuW4alb73Tp1DaOd6KCdgVSumsrB
KHtV0kztswqBa+/OycK3PMm2cU4O5mrs5ui5fIuFCpxUmzUALuyTiHafZjWZkSfZvC6IDf2YxoYI
/q3wl92xUi8Pae2KUu4x1ktWsVFW1hfp5JCyI0udBtz7YU7cgEyvVRuFHbwgX6qWrBR0L04NMzEH
hzC4Wd+m9SAg4fTDyFHEDOCk16FI4MjEZrPSKU9NbDpsVUr7bT58c5fUlKU4Dzwv/J0IbeKuUuCZ
g3iJNTIFGyurgj7zC7BMIc/z0cdOOX5NjFdeJTgoRFqwkx1PLLLn+iWoEuml7dgghNgUruEvnrno
/XSIhg/K4+37T0QY4KaNTVw/2t8I+q0QmkYNS3b/sZ4gRaD20ErZmd3gFMo1R7h08la02dSEuyFz
UXbJixzdmCCkGiaIjynikIV8Adl1JoNZaONVGoceDz6ukhfhh+gn5G5GMha1M3c3RaVHqnfgl+gq
wOTCJTMdtJ0TVrfvx3l6mt37bOTHhacYzmvftEqgU1ZVBm1tWYliJW2w6DfsLUGK433vWkPnw7/7
ycOfajmx1L4R3OjJ+1rYiff5EipgGYvpg8YyRVA5E54KV+twXre0lG1qrG+HyUB/f23/TLbv0Iv1
HMXh5Yjp03d9IQ0p50nl2JsQbOfgIqYDzOT5uz5OEjqfTKmbAcNCyBW8d/1fGYIkGQiKvtVo/DdY
pWcORXtn8JhQNgGfR2a9l9K3lhLqN1PgcPNmD280NUeKoKkvzvFSqDmKyByO2RksCM182LuwMFrH
vbiy2kVmxJULmid5CYYZk3cfekPoC0INvCx9fMLNizPqcqonl6txu5R4FQ8sNylj6VWsGdvtGwoK
dvWcYTc3C2mAeKM9swJGk+9KtJCj2JI4vz1L9bTFQh/b+G7t7o1qxZMXtdKYefO894qoi1Rkf2cd
TuPH98Av5Ugx6cxD7uSzjyoxt/r/CTg4Srnaiw5TjR58wLXI9EhmQV7qSRhNgf38Ky+Dlo5FM8yf
T3/jWw5X1+GIRsrDa16Fl8ywuwkImfunAQKsI/Dq8h4pCz34BYxM2JzZa7eCMcW2zFvyWgaCI573
iZ9Gyp+T28/Rm94okE9MMykJqFJ6Qy2CyS9pA9WZGKsVthuWNJvNAen8fD9Rk08vralNiLGUJGeI
so1W9aEUlFDtM1Hi+DUt0BD5hla1slBHbjftguCHbRe4J1R9+lwjMhxGpDxVW5JSOU4hahvLYpnR
OWJOm1M/i+78qNdejaU1tHi0GuXF9qgZCYdpIZjeyHgMTYx3S6ytaMQHqkC8Z4Pps+lK0qP9iknX
PR4uYeitHF/ethrBcHghFJ6DWsj1o5De4i0VaQCRpVxp/AufW0NxEBdkVt1grd3XI5jlSH9xarul
bTQ8y5v9wzUczLE28N67bDWxZ+V1acg8lDFtYBSB30Qycz8WeaIc24TOHAToZPE43xQQkzGj1uNn
9fmX7bZSF+EnyGqSKeRr+msOROrHluSuFBmu7SPEBDnQ5m5LZuhhzlQeQtdYfKFJZNW1ouh67BPr
vlceuavt/+y5hg07dq5AS8n0J0DbetKjhs0kQ5C+eJv1XwRbs3kAPfIdWs4g9FkBzR9rBF2dq/JA
4fwYWxoOKlSn4oKK+dBbg1Ft4anE5/iloGVi6zQuBCtQg9xMEFdQO4MeUCpoXG0ZhsGl27j0g1Am
56utVfLPqSPoABtHAKxuBktE3L8PjsDVIscFGwVcSEJhwzrZffqnhXylDurBk+/a6m4O7Pzp4/jw
ml2dVkcogs1cx4u9vwTwmqnEiPiVBGSmFgQ57AqvTstzrgmszinpR4mHeTbImKLqvyXZ1Nd7W2lL
mxyWhDqbir4CVfoGLXu9gKDJwIFDWnhiUZPEbKF15DQTToPheYe4wuTZKEl0Iwd489S/m+qAevog
Axh39nDijkuRYC2EgjTe8rloAfvJXhdQYYFmCHV2P8beOQC8xlGVWh+ZAEwWL2Vrpe+/XX3MMddw
s9S//jLiRBXKx0AuserZHEdet0tz8rQc9cHdvgBFsoPXeGjOWqqczHEleMgt82rttcnQdoTwLga7
1JFNCjFZz9z13Eeub2XOiZwcdXoXcplJ3qiW5/ggmLPn/52vS1ZcI7ekFRyQPsAfvSobeWe4INj/
joUPjjs1i2A954FgKtlvFTMXmUfoR5zWgdpOLPf+WDfu/Q2C0u2XeyMmrUaTJFxIUd9SoYC6b7P1
9txJIlR+elI7AufehOt10utLI/380DHcOFIr+HcXo0FsJji7rVWdCAu3iuKPLGzH8SNQEhnTZlTY
/CXrCV6Rv42q42Ybj18BXqF6NGXV00YHQswkrPqPUE/O/tAODm2ZBHSz3sQCyDApaRupmqlduwUt
1xCu35hX2EIgKyKv1KzMZk7yYp38mmiaMPufx4nPnQavR51b5Qp+s1YuUvBO5Dt4fKLT+lOQyOLe
BZqv8r2GdkFv1XQrQZJDME5lhx5nUF88CtypRnh2d/pq1kEjgmLJqCZnCzY1VneInwTB7PgzW7HA
cpJD02PgnpbLxi6YDw5AA95kffWKUgExM6rCvtiMf2EKN03e059mWoicAfwqeerPLyh2zHc7A8Pk
+gQb8wpDQZ6Nve01OPKx588DkDqXnKGsdlFll5IGxi/pDobtDOwLrfMgIardDJJCBXv09aNhJJeX
xnUBFLYxn81wdOQ9yHIDWY3/22obLXBpLWfNxENDk2kNEaKJ4UfkkvO6f/cwQNDv9ASw2NoqDwps
8fiLXVRqmhhMjK5cXVcqE9/tJSNmpLyLfGmmBlSEVkNg4w7TEQO17Z5yxH4Uk85twlpYPzIV4gW2
v/taoPOjic1XQ2EKejeHTYCxk0dY041MjnX6G+X9RpL+5SxKfgiF1m/J5d+0WUcUr6EQY4zouw5m
k5Z/hJWc4s+zeeDYH0fwhJWbklRYXiOrY428XsMXOwcBSf2KNP/5Mh8K4I3BfxaRV9n1/VkADWUq
Nbt+OOJjl4eOTW5tqv5XZl0TMfl+KDlgGIt4sWpWQgRw8KH8KH69aVqDJqdvspn2WcjlpZnmqgkR
VPC7FHlfzrpV4r9Fh2s241UxnKgPKq3K//TozPnaBiL1PdIjbeoBnTKlgma4cvfvjPj1CEEuKAKb
XBxDH2rKGUMMoDoLAt8jB5HVtFG8vM1J5rviQsNl5F4SLJpKXjqc43UH3HjZPx3I9E962DCm7kTk
rYwDfl/BYRJtFZbgK1nKKNZZCmlu4d/HPVSauQrvq7TI58SByo4vxccUXN9qMH/i2q4oh4V1QhCg
0DdBhh2hMYqBmWx2FJEzxkXE4ukgV56pAcQ046+zmk0ADWT3+TOZnvTlFNjX/52HwkqIVsMmcEdw
pb8/JBhPpZuuzjJcJ61PNZErw9UgXXX/kOaS+81UOehMOaZbZp8ppnU4W73kWNAT4kcwfz6QEWTG
CX6lNra6oh95PKGP588qgVKubMhxkXlTBBGxPEM33ENcOc8+TJOB9TCW+V7whrwkSpyfjymSUGb8
7dT41DJfsIebaJxlNi/ZFiuYCFsn1cKTsQoQ0M2aGpNDqAL3C6/OUW/xY/fbL10YL/+do8CcYvc8
Yev7uXylCE5IeyCtf1AFktpNZsJRktUIw3m8GgkQ4svqw570p7m8OqYUdWKXU1j8mFgOjM0jb6wu
6JUCs9LUbaP6eTbZl8Y1p0OxKyEetyeUOf8tNtSUZpJihCE8A4MxBDPsbr3qiJwxMt/rIIbLm74U
7q2gT721Syg9TQlAPbJY9iVrXB/C0d0Muv1SnbbDA4yqE/7XnUOprCJffzCKpX6uQ/g927xAzyS4
pB8mNdgfv94iyle2nuk/V0U4XU09timEaTaunVlbJ/qGw2/hHP+4cTB+68cWuyRIZQOtzi8FIvek
6ZHKSgo3ONXfAnU3f6A01c/qutbwtw04+ZvVW0StBuITdETKSGaO67Ein1875/CBujP55LZsBjNh
3Id11XBEI2v2r5vKAbF5lSAt4apApKr3AiPkkGO5fUNjGiikNJ6srUa5kmFFQh5+AuHtqAUOokDQ
1Cx9+2txyWmscOaXkhPiERWsk/w88d2Lo/bPA34LH04Ydceym35WwOOcXYrnDKZ8wz+/0I+i1/5G
PA9cnH57VoVkckwHovkmaNf4kwVujT0fu8N60J1JzCTM5hzb7qn3t//FHaBBoydfw2+ABxLa+PmK
KLvQyCMC3jJ7rQJh2K5XhYCu3TKTxbfSJygzDE/rqCHW57ielys9gozzhNyzz6Ka4MZnIRssntoB
Wu+vgMkz1wmGpRwgOrMQojSHNyzuLa68TO8eUIm0+yd5DO1SIT5oonX43Dc9kxLuISMbW0YBdsHI
iNhF5XtJ9rxLsNYuJMauCzMEfugDhyvEtcm/wF00zf2ZH/vyse0tfrcgUTUvE8LAAliTzDv2I0e0
41GRuTzYEEuKLY+EWiHc0ExZ2utpbWgaK+U9cjN+PwyLYGLaoHAaHg/vmTIp4645XFXZ2NROXGV2
xDJsclTIPYt5dvUOsNANsuHlMBO/LmWukeOPSNMarQEvX6mXziQ908Cpb7oGKE/UHkH9Z2TrkQjb
dt3seekz9E8nx5WzLzfbR8BT6fxOtop9qTy3+kHX77nKq2gt9twXGvF3pnPBKrpg+J5ByeL1ytlE
wnCm8naYZsVku3T2ocozq8LTQDXVb23qNxhfzL74bIkq/4QKlKDeHnOJhqIkzXh3qdZVO6Wdbqaq
xAi/f6OYz0ev+g4LD5bXKVrDZAVkHbGkk0ZifvHwLAQ525WWwiJ7M0UEqG+qzrvELDQQqSKtPYQd
hHI2az+DPGls5WwFAzkJpJug+vDR4ZjfXw3eeIlmwmftZi0aewptwL+0FdsarnSn7XB2MxukUfWQ
21z7owqUQq9rOzYXzdGQDn6Rcb9Y9cM7Xf65p0M/yKACbZdcK9TWxOYHyKjmyLR+C2hFR2Mfx9Jg
JhbSw8bN8CuzhCNrS8MK69lecM+WaIFpLOZHykUIk/J0a2BTltZ2kpjXS9+JOMwYJ2I1awSjtwKw
ZhgiV03Yr7naEXnIdKJhRT3IZioW8Ri0FW4CBBFKBYmve12Qx1bVgjSHvtCU12zdn62Y/AfhCRjy
j0Jz3/3tiCl9F1pQRrSssEuD6BlGqRB+T33Hr/mu4manjy+PjzVotdY0Eo/J5BkKTMNT4OYHZkg0
MmHIA09CQVz8VYXE6cinxpXtSm8X9c4Sec0wGnYkRMGsUnJ7cjBvgDxo+zx+cV8fw61v/2eE+Ovd
4NZcFq+zRuBaWr9OsiI9PKiDbpLxUmbdBjQGFYej6SkAQo9z5xKqKjiWPlSTykuYExHST1wN6KEt
BWIxP8I29cYvrUWysZ7DobmSe51HmUETbNECjoGBEiBaphlIfYIjsTQpzyFYtqd/7EBmEhtm2eS4
DEwY5sIIRhaKsLw2eD6/LRFchbeyubVl2itJd8+pKJjtr2N9MubrFqca8c+fbq/F95a9y7XoQR25
wCC8eTG/+vSyndZf5V9SMT3DGjB7YPOUfuSv6c8SR86hfmAwf9PtRZvdXGY/kakrr6WA7XoZWlX7
DVVGi4E64YWZOHnxuPxLBlyD3WObCeE90u4s06E+NpHauUw9o/huKS3kQ5x/AsUsXjsvYGu//jcM
LTvcBDrMRvnO8yGu1+W2EGUXDlPWxd3cY4Una5XzbXfiKGJgoxpEUbe6URKT6UbO2LfHcMjXJAhp
AO3KbMq9U0yL9DEFJhTUJD+aLnhGo2JK/8BU6Ink0Xq2O+VP55nYRxDPviK+qQ7CzAFt35kzkawe
NtGs9C5McgEU+396/SBslQ9HFvKgvJvonLAiEY+uWWKCO79zMX/AkjmzBQI9rOnOBNmKu4kTBQ1g
MOktZ9K3ZztBwo4KClAuHo0Wx7YdR2l4x7GZYMZpI92kGyxVy4qzR6l/xs4RI6uu6CNf7R+bSFDO
kPuHbNL1GwJtF5ihwAi0OFQIO6SkMgScCXryHXLrUYAvJ/nru8HaZu0BRBvQAp7yBBS1tKVIbSvV
2fUimoE4A2mrZqsoZHc7bYAmgdyNttDaY34wU8/uJfvm0+eGvJr3vRqRfkTbRrhbVHPlR0XqWxSt
qJXUFWH7bzGbbBGQ7tgjw66jsjLxjQHRrqC0KilsT0bX6/IVxUzHowBBTvoO0dGgdyFVPPs6Yx05
G9mf0WUjn/rUKzxwuYN4M98IPlQgXwRy6i0NOpV72FzaVVJaonxAJfzpM/29eVNNJm4j/ZggZmvJ
q0Bw50Ddv1kE+DN1SZxa+qRMribVN0bQoRy/bR9v9vd/ZmVR+4ojSGF1s0HLSa4w1ze8dN5CpmyQ
/bQApA3XMB1BKN3xOoICG0dM1H6yJyCgzXe59S59g9NEoihJPRaSn99PvcLU80ZQiv3RiYuwXnET
7HYrEoZOjx+oKjH0kdgHattH1qK2Ju9/BPFLFboFEqr+stJHbhOnEynP1/1HFIJ/UayT+VmBkyc1
8eKbov6tilSw0EybfPq+OY7F3xLV+pc5MohQToWbxuc4g8xfjXdhVKqIr092G1BAueJBxCszUoXU
Ysfb/nfHNxfoui0OJTcAN0FbVuv+E4GV16g7f+iFCQjuauIKUtltDanZi4bWuhVf/E3xqvlXMupD
38qNvjPsNEEONyKAeT2jBKd4/M4NMligQwNeznOz6A8aRF4EmTILlCGVCGOg7zkiPOk=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (weak1, weak0) GSR = GSR_int;
    assign (weak1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
