
`timescale 1 ns / 1 ps

	module dds_axi_v1_0_S00_AXI #
	(
		// Users to add parameters here

		// User parameters ends
		// Do not modify the parameters beyond this line

		// Width of S_AXI data bus
		parameter integer C_S_AXI_DATA_WIDTH	= 32,
		// Width of S_AXI address bus
		parameter integer C_S_AXI_ADDR_WIDTH	= 6
	)
	(
		// Users to add ports here
        output                                    CSB,
        output                                    SCLK,
        output                                    SDIO,
		// User ports ends
		// Do not modify the ports beyond this line

		// Global Clock Signal
		input wire  S_AXI_ACLK,
		// Global Reset Signal. This Signal is Active LOW
		input wire  S_AXI_ARESETN,
		// Write address (issued by master, acceped by Slave)
		input wire [C_S_AXI_ADDR_WIDTH-1 : 0] S_AXI_AWADDR,
		// Write channel Protection type. This signal indicates the
    		// privilege and security level of the transaction, and whether
    		// the transaction is a data access or an instruction access.
		input wire [2 : 0] S_AXI_AWPROT,
		// Write address valid. This signal indicates that the master signaling
    		// valid write address and control information.
		input wire  S_AXI_AWVALID,
		// Write address ready. This signal indicates that the slave is ready
    		// to accept an address and associated control signals.
		output wire  S_AXI_AWREADY,
		// Write data (issued by master, acceped by Slave) 
		input wire [C_S_AXI_DATA_WIDTH-1 : 0] S_AXI_WDATA,
		// Write strobes. This signal indicates which byte lanes hold
    		// valid data. There is one write strobe bit for each eight
    		// bits of the write data bus.    
		input wire [(C_S_AXI_DATA_WIDTH/8)-1 : 0] S_AXI_WSTRB,
		// Write valid. This signal indicates that valid write
    		// data and strobes are available.
		input wire  S_AXI_WVALID,
		// Write ready. This signal indicates that the slave
    		// can accept the write data.
		output wire  S_AXI_WREADY,
		// Write response. This signal indicates the status
    		// of the write transaction.
		output wire [1 : 0] S_AXI_BRESP,
		// Write response valid. This signal indicates that the channel
    		// is signaling a valid write response.
		output wire  S_AXI_BVALID,
		// Response ready. This signal indicates that the master
    		// can accept a write response.
		input wire  S_AXI_BREADY,
		// Read address (issued by master, acceped by Slave)
		input wire [C_S_AXI_ADDR_WIDTH-1 : 0] S_AXI_ARADDR,
		// Protection type. This signal indicates the privilege
    		// and security level of the transaction, and whether the
    		// transaction is a data access or an instruction access.
		input wire [2 : 0] S_AXI_ARPROT,
		// Read address valid. This signal indicates that the channel
    		// is signaling valid read address and control information.
		input wire  S_AXI_ARVALID,
		// Read address ready. This signal indicates that the slave is
    		// ready to accept an address and associated control signals.
		output wire  S_AXI_ARREADY,
		// Read data (issued by slave)
		output wire [C_S_AXI_DATA_WIDTH-1 : 0] S_AXI_RDATA,
		// Read response. This signal indicates the status of the
    		// read transfer.
		output wire [1 : 0] S_AXI_RRESP,
		// Read valid. This signal indicates that the channel is
    		// signaling the required read data.
		output wire  S_AXI_RVALID,
		// Read ready. This signal indicates that the master can
    		// accept the read data and response information.
		input wire  S_AXI_RREADY
	);

	// AXI4LITE signals
	reg [C_S_AXI_ADDR_WIDTH-1 : 0] 	axi_awaddr;
	reg  	axi_awready;
	reg  	axi_wready;
	reg [1 : 0] 	axi_bresp;
	reg  	axi_bvalid;
	reg [C_S_AXI_ADDR_WIDTH-1 : 0] 	axi_araddr;
	reg  	axi_arready;
	reg [C_S_AXI_DATA_WIDTH-1 : 0] 	axi_rdata;
	reg [1 : 0] 	axi_rresp;
	reg  	axi_rvalid;

	// Example-specific design signals
	// local parameter for addressing 32 bit / 64 bit C_S_AXI_DATA_WIDTH
	// ADDR_LSB is used for addressing 32/64 bit registers/memories
	// ADDR_LSB = 2 for 32 bits (n downto 2)
	// ADDR_LSB = 3 for 64 bits (n downto 3)
	localparam integer ADDR_LSB = (C_S_AXI_DATA_WIDTH/32) + 1;
	localparam integer OPT_MEM_ADDR_BITS = 3;
	//----------------------------------------------
	//-- Signals for user logic register space example
	//------------------------------------------------
	//-- Number of Slave Registers 4
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg0;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg1;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg2;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg3;
	wire	 slv_reg_rden;
	wire	 slv_reg_wren;
	reg [C_S_AXI_DATA_WIDTH-1:0]	 reg_data_out;
	integer	 byte_index;

	// I/O Connections assignments

	assign S_AXI_AWREADY	= axi_awready;
	assign S_AXI_WREADY	= axi_wready;
	assign S_AXI_BRESP	= axi_bresp;
	assign S_AXI_BVALID	= axi_bvalid;
	assign S_AXI_ARREADY	= axi_arready;
	assign S_AXI_RDATA	= axi_rdata;
	assign S_AXI_RRESP	= axi_rresp;
	assign S_AXI_RVALID	= axi_rvalid;
	// Implement axi_awready generation
	// axi_awready is asserted for one S_AXI_ACLK clock cycle when both
	// S_AXI_AWVALID and S_AXI_WVALID are asserted. axi_awready is
	// de-asserted when reset is low.

	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_awready <= 1'b0;
	    end 
	  else
	    begin    
	      if (~axi_awready && S_AXI_AWVALID && S_AXI_WVALID)
	        begin
	          // slave is ready to accept write address when 
	          // there is a valid write address and write data
	          // on the write address and data bus. This design 
	          // expects no outstanding transactions. 
	          axi_awready <= 1'b1;
	        end
	      else           
	        begin
	          axi_awready <= 1'b0;
	        end
	    end 
	end       

	// Implement axi_awaddr latching
	// This process is used to latch the address when both 
	// S_AXI_AWVALID and S_AXI_WVALID are valid. 

	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_awaddr <= 0;
	    end 
	  else
	    begin    
	      if (~axi_awready && S_AXI_AWVALID && S_AXI_WVALID)
	        begin
	          // Write Address latching 
	          axi_awaddr <= S_AXI_AWADDR;
	        end
	    end 
	end       

	// Implement axi_wready generation
	// axi_wready is asserted for one S_AXI_ACLK clock cycle when both
	// S_AXI_AWVALID and S_AXI_WVALID are asserted. axi_wready is 
	// de-asserted when reset is low. 

	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_wready <= 1'b0;
	    end 
	  else
	    begin    
	      if (~axi_wready && S_AXI_WVALID && S_AXI_AWVALID)
	        begin
	          // slave is ready to accept write data when 
	          // there is a valid write address and write data
	          // on the write address and data bus. This design 
	          // expects no outstanding transactions. 
	          axi_wready <= 1'b1;
	        end
	      else
	        begin
	          axi_wready <= 1'b0;
	        end
	    end 
	end       

	// Implement memory mapped register select and write logic generation
	// The write data is accepted and written to memory mapped registers when
	// axi_awready, S_AXI_WVALID, axi_wready and S_AXI_WVALID are asserted. Write strobes are used to
	// select byte enables of slave registers while writing.
	// These registers are cleared when reset (active low) is applied.
	// Slave register write enable is asserted when valid address and data are available
	// and the slave is ready to accept the write address and write data.
	assign slv_reg_wren = axi_wready && S_AXI_WVALID && axi_awready && S_AXI_AWVALID;

/*
	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      slv_reg0 <= 0;
	      slv_reg1 <= 0;
	      slv_reg2 <= 0;
	      slv_reg3 <= 0;
	    end 
	  else begin
	    if (slv_reg_wren)
	      begin
	        case ( axi_awaddr[ADDR_LSB+OPT_MEM_ADDR_BITS:ADDR_LSB] )
	          2'h0:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 0
	                slv_reg0[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          2'h1:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 1
	                slv_reg1[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          2'h2:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 2
	                slv_reg2[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          2'h3:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 3
	                slv_reg3[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          default : begin
	                      slv_reg0 <= slv_reg0;
	                      slv_reg1 <= slv_reg1;
	                      slv_reg2 <= slv_reg2;
	                      slv_reg3 <= slv_reg3;
	                    end
	        endcase
	      end
	  end
	end    
*/
	// Implement write response logic generation
	// The write response and response valid signals are asserted by the slave 
	// when axi_wready, S_AXI_WVALID, axi_wready and S_AXI_WVALID are asserted.  
	// This marks the acceptance of address and indicates the status of 
	// write transaction.

	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_bvalid  <= 0;
	      axi_bresp   <= 2'b0;
	    end 
	  else
	    begin    
	      if (axi_awready && S_AXI_AWVALID && ~axi_bvalid && axi_wready && S_AXI_WVALID)
	        begin
	          // indicates a valid write response is available
	          axi_bvalid <= 1'b1;
	          axi_bresp  <= 2'b0; // 'OKAY' response 
	        end                   // work error responses in future
	      else
	        begin
	          if (S_AXI_BREADY && axi_bvalid) 
	            //check if bready is asserted while bvalid is high) 
	            //(there is a possibility that bready is always asserted high)   
	            begin
	              axi_bvalid <= 1'b0; 
	            end  
	        end
	    end
	end   

	// Implement axi_arready generation
	// axi_arready is asserted for one S_AXI_ACLK clock cycle when
	// S_AXI_ARVALID is asserted. axi_awready is 
	// de-asserted when reset (active low) is asserted. 
	// The read address is also latched when S_AXI_ARVALID is 
	// asserted. axi_araddr is reset to zero on reset assertion.

	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_arready <= 1'b0;
	      axi_araddr  <= 32'b0;
	    end 
	  else
	    begin    
	      if (~axi_arready && S_AXI_ARVALID)
	        begin
	          // indicates that the slave has acceped the valid read address
	          axi_arready <= 1'b1;
	          // Read address latching
	          axi_araddr  <= S_AXI_ARADDR;
	        end
	      else
	        begin
	          axi_arready <= 1'b0;
	        end
	    end 
	end       

	// Implement axi_arvalid generation
	// axi_rvalid is asserted for one S_AXI_ACLK clock cycle when both 
	// S_AXI_ARVALID and axi_arready are asserted. The slave registers 
	// data are available on the axi_rdata bus at this instance. The 
	// assertion of axi_rvalid marks the validity of read data on the 
	// bus and axi_rresp indicates the status of read transaction.axi_rvalid 
	// is deasserted on reset (active low). axi_rresp and axi_rdata are 
	// cleared to zero on reset (active low).  
	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_rvalid <= 0;
	      axi_rresp  <= 0;
	    end 
	  else
	    begin    
	      if (axi_arready && S_AXI_ARVALID && ~axi_rvalid)
	        begin
	          // Valid read data is available at the read data bus
	          axi_rvalid <= 1'b1;
	          axi_rresp  <= 2'b0; // 'OKAY' response
	        end   
	      else if (axi_rvalid && S_AXI_RREADY)
	        begin
	          // Read data is accepted by the master
	          axi_rvalid <= 1'b0;
	        end                
	    end
	end    

	// Implement memory mapped register select and read logic generation
	// Slave register read enable is asserted when valid address is available
	// and the slave is ready to accept the read address.
	assign slv_reg_rden = axi_arready & S_AXI_ARVALID & ~axi_rvalid;
	always @(*)
	begin
	      // Address decoding for reading registers
/*
	      case ( axi_araddr[ADDR_LSB+OPT_MEM_ADDR_BITS:ADDR_LSB] )
	        2'h0   : reg_data_out <= slv_reg0;
	        2'h1   : reg_data_out <= slv_reg1;
	        2'h2   : reg_data_out <= slv_reg2;
	        2'h3   : reg_data_out <= slv_reg3;
	        default : reg_data_out <= 0;
*/

		case (axi_araddr[5:2])
              4'h0:        dataToBus <= {3'h0, freq};
              4'h1:        dataToBus <= {8'h0, step};
              4'h2:        dataToBus <= 0;
              4'h3:        dataToBus <= 0;
              4'h4:        dataToBus <= 0;
              4'h5:        dataToBus <= 0;
              4'h6:        dataToBus <= 0;
              4'h7:        dataToBus <= {31'h0, configDone};
              4'h8:        dataToBus <= {3'h0, minFreq};
              4'h9:        dataToBus <= {3'h0, maxFreq};
              default:    dataToBus <= 32'hFFFFFFFF;
	      endcase
	end

	// Output register or memory read data
	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_rdata  <= 0;
	    end 
	  else
	    begin    
	      // When there is a valid read address (S_AXI_ARVALID) with 
	      // acceptance of read address by the slave (axi_arready), 
	      // output the read dada 
	      if (slv_reg_rden)
	        begin
	          axi_rdata <= dataToBus;     // register read data
	        end   
	    end
	end    

	// Add user logic here
/*
dds_top inst1 (
               .CSB          (CSB),
               .SCLK         (SCLK), 
               .SDIO         (SDIO),
               .S_AXI_ACLK     (S_AXI_ACLK),
               .S_AXI_ARESETN  (S_AXI_ARESETN),
               .S_AXI_AWADDR   (S_AXI_AWADDR),
               .S_AXI_WDATA    (S_AXI_WDATA),
               .S_AXI_RDATA    (S_AXI_RDATA),
               .S_AXI_ARADDR   (S_AXI_ARADDR),
               .S_AXI_AWREADY  (S_AXI_AWREADY),
               .S_AXI_WREADY   (S_AXI_WREADY),
               .S_AXI_RREADY   (S_AXI_RREADY),
               .S_AXI_AWVALID  (S_AXI_AWVALID),
               .S_AXI_WSTRB    (S_AXI_WSTRB),
               .S_AXI_WVALID   (S_AXI_WVALID),
               .S_AXI_ARVALID  (S_AXI_ARVALID),
               .S_AXI_ARREADY  (S_AXI_ARREADY)
               );
*/
	// User logic ends

	reg incInc = 0, decInc = 0;			// increment/decrement step size command
	reg incFreq = 0, decFreq = 0;			// increment/decrement frequency command
	reg [31:0] minFreqSet = 0;				// minimum frequency that can be set by decFreq command
	reg [31:0] maxFreqSet = 400000000;	// maximum frequency that can be set by incFreq command
	reg [31:0] freqSet = 67000000;		// start frequency when DDS is first configured
	reg [28:0] minFreq = 0;					// minimum frequency (force <= 400 MHz)
	reg [28:0] maxFreq = 400000000;		// maximum frequency (forced >= freq and <= 400 MHz)
	reg [28:0] freq = 67000000;			// frequency (in Hz)
	reg [2:0] stepIndex = 3;				// index into possible step sizes
	reg [23:0] step;							// frequency increment/decrement step size (in Hz)
	reg [47:0] ftw = 48'h170A3D909F80;	// current frequency tuning word stored in DDS
	reg resetDDS = 0;							// reset DDS state machine
	reg [3:0] resetCtr = 0;					// holds resetDDS for 16 clock cycles
	reg configDone = 0;						// configuration status

	// AXI read command
	reg [31:0] dataToBus;



	always @ (posedge S_AXI_ACLK)
	begin
		// set frequency step size based on index
		case (stepIndex)
			0:			step <= 1;
			1:			step <= 10;
			2:			step <= 100;
			3:			step <= 1000;
			4:			step <= 10000;
			default:	step <= 100000;
		endcase
		if (axi_wready & S_AXI_WSTRB[0] & S_AXI_WDATA[0] & (axi_awaddr[5:2] == 4'h6))
			resetCtr <= 1;
		else if (|resetCtr)
			resetCtr <= resetCtr + 1;
		resetDDS <= |resetCtr;
		
		// Regular registers
		if (minFreqSet > 400000000)			minFreq <= 400000000;
		else											minFreq <= minFreqSet[28:0];
		if (maxFreqSet < minFreq)				maxFreq <= minFreq;
		else if (maxFreqSet > 400000000)		maxFreq <= 400000000;
		else											maxFreq <= maxFreqSet[28:0];
		if (freqSet < minFreq)					freq <= minFreq;
		else if (freqSet > maxFreq)			freq <= maxFreq;
		else											freq <= freqSet[28:0];

		if (axi_wready & S_AXI_WSTRB[0])
		begin
			case (axi_awaddr[5:2])
				4'h0: 													freqSet[7:0]		<= S_AXI_WDATA[7:0];
				4'h2: if (S_AXI_WDATA[0] & ~&stepIndex[2:1])	stepIndex			<= stepIndex + 1;
				4'h3: if (S_AXI_WDATA[0] & |stepIndex)			stepIndex			<= stepIndex - 1;
				4'h4: if (S_AXI_WDATA[0])							freqSet				<= freq + step;
				4'h5: if (S_AXI_WDATA[0])							freqSet				<= freq - step;
				4'h8:														minFreqSet[7:0]	<= S_AXI_WDATA[7:0];
				4'h9:														maxFreqSet[7:0]	<= S_AXI_WDATA[7:0];
			endcase
		end
		if (axi_wready & S_AXI_WSTRB[1])
		begin
			case (axi_awaddr[5:2])
				4'h0: freqSet[15:8]			<= S_AXI_WDATA[15:8];
				4'h8:	minFreqSet[15:8]		<= S_AXI_WDATA[15:8];
				4'h9:	maxFreqSet[15:8]		<= S_AXI_WDATA[15:8];
			endcase
		end
		if (axi_wready & S_AXI_WSTRB[2])
		begin
			case (axi_awaddr[5:2])
				4'h0: freqSet[23:16]			<= S_AXI_WDATA[23:16];
				4'h8:	minFreqSet[23:16]		<= S_AXI_WDATA[23:16];
				4'h9:	maxFreqSet[23:16]		<= S_AXI_WDATA[23:16];
			endcase
		end
		if (axi_wready & S_AXI_WSTRB[3])
		begin
			case (axi_awaddr[5:2])
				4'h0: freqSet[31:24]			<= S_AXI_WDATA[31:24];
				4'h8:	minFreqSet[31:24]		<= S_AXI_WDATA[31:24];
				4'h9:	maxFreqSet[31:24]		<= S_AXI_WDATA[31:24];
			endcase
		end
	end
	
	// Output signals to AXI bus
//	assign S_AXI_RDATA = S_AXI_RREADY ? dataToBus : 32'h0;
//	assign IP2Bus_AddrAck = S_AXI_AWVALID[0];
//	assign S_AXI_WREADY = ~S_AXI_WREADY & S_AXI_AWVALID & S_AXI_WVALID;
//	assign S_AXI_RREADY = S_AXI_AWVALID[0] & S_AXI_RVALID;
//	assign IP2Bus_Error = 0;

	// calculates the new frequency tuning word when the frequency is changed
	wire [47:0] newFtw;
/*
	ftwMultiplier625 calcFtw (
	                          .clk(S_AXI_ACLK), 
	                          .a(freq), 
	                          .p(newFtw)
	                          );
*/

ftwMultiplier625 calcFtw (
  .CLK(S_AXI_ACLK),  // input wire CLK
  .A(freq),      // input wire [27 : 0] A
  .P(newFtw)      // output wire [47 : 0] P
);
	// initializes the DDS and sends commands when the frequency tuning word changes
	wire valid;						// command is ready to send to DDS
	wire [47:0] cmd;				// command to send to DDS
	wire cmdDone;					// command sent to DDS, ready for next command

	serialCtrl ddsCtrl(
	                   .clk(S_AXI_ACLK), 
	                   .cmdValid(valid), 
	                   .cmd(cmd), 
	                   .done(cmdDone),
		               .sclk(SCLK), 
		               .csb(CSB),	
		               .sdio(SDIO)
		               );

	// FIFO that buffers the data bits to the AD9912
	reg [7:0] bits = 0;		
	reg [15:0] addr = 0;
	reg [23:0] data = 0;
	reg wrEn = 0;

/*
	serialCommandFifo cmdBuffer(
	                            .clk(S_AXI_ACLK), 
	                            .din({bits, addr, data}), 
	                            .wr_en(wrEn),
		                        .rd_en(cmdDone), 
		                        .dout(cmd), 
		                        .valid(valid)
		                        );
	*/
	serialCommandFifo cmdBuffer (
                                  .clk(S_AXI_ACLK),      // input wire clk
                                  .din({bits, addr, data}),      // input wire [47 : 0] din
                                  .wr_en(wrEn),  // input wire wr_en
                                  .rd_en(cmdDone),  // input wire rd_en
                                  .dout(cmd),    // output wire [47 : 0] dout
                                  .full(full),    // output wire full
                                  .empty(empty),
                                  .valid(valid)  // output wire empty
                                );	
                                
                             
	// state machine that generates commands for the DDS
	reg [3:0] state = 0;
	reg configuring = 0;
	reg [9:0] configDelay = 0;

	always @ (posedge S_AXI_ACLK)
	begin
		if (resetDDS)
		begin
			bits <= 0;
			addr <= 0;
			data <= 0;
			wrEn <= 0;
			state <= 1;
			configuring <= 0;
			configDelay <= 0;
		end
		else
		begin
			configuring <= valid;
			if (|configDelay | (configuring & ~valid))
				configDelay <= configDelay + 1'b1;
			configDone <= configDone | &configDelay;
			case (state)
				// Wait state (wait until software is ready to configure DDS)
				0:
				begin
					bits <= 0;
					addr <= 0;
					data <= 0;
					wrEn <= 0;
					configuring <= 0;
					configDelay <= 0;
					if (resetDDS)
						state <= state + 1;
				end
				// Reset state (return here if software resets DDS)
				1:
				begin
					bits <= 0;
					addr <= 0;
					data <= 0;
					wrEn <= 0;
					configuring <= 0;
					configDelay <= 0;
					state <= state + 1;
				end
				// Reset DDS
				2:
				begin
					bits <= 8'd24;								// total of 24 bits, including address
					addr <= {1'b0, 2'b00, 13'h0000};		// write, 1 byte of data, address 0x0000
					data <= {8'h3C, 16'h0};					// data 0x3C
					wrEn <= 1'b1;
					state <= state + 1;
				end
				// Reset bit set in previous state must be manually cleared
				3:
				begin
					bits <= 8'd24;								// total of 24 bits, including address
					addr <= {1'b0, 2'b00, 13'h0000};		// write, 1 byte of data, address 0x0000
					data <= {8'h18, 16'h0};					// data 0x18
					wrEn <= 1'b1;
					state <= state + 1;
				end
				// Reset the DDS
				4:
				begin
					bits <= 8'd24;								// total of 24 bits, including address
					addr <= {1'b0, 2'b00, 13'h0012};		// write, 1 byte of data, address 0x0012
					data <= {8'h01, 16'h0};					// data 0x01
					wrEn <= 1'b0;								// disabled - full system reset is done in previous states
					state <= state + 1;
				end
				// Set the PLL parameters register
				5:
				begin
					bits <= 8'd24;								// total of 24 bits, including address
					addr <= {1'b0, 2'b00, 13'h0022};		// write, 1 byte of data, address 0x0022
					data <= {8'h04, 16'h0};					// data 0x04
					wrEn <= 1'b0;								// disabled - should not be change unless hardware is changed
					state <= state + 1;
				end
				// Power off CMOS output (on by default)
				7:
				begin
					bits <= 8'd24;								// total of 24 bits, including address
					addr <= {1'b0, 2'b00, 13'h0010};		// write, 1 byte of data, address 0x0010
					data <= {8'h90, 16'h0};					// data 0x90 - disable PLL
					wrEn <= 1'b1;
					state <= state + 1;
				end
				// Increase output level
				8:
				begin
					bits <= 8'd24;								// total of 24 bits, including address
					addr <= {1'b0, 2'b00, 13'h040C};		// write, 1 byte of data, address 0x040C
					data <= {8'h03, 16'h0};					// data 0x03 is aligned in msb, lsb filled with 0s
					wrEn <= 1'b1;
					ftw <= newFtw;
					state <= state + 1;
				end
				// Set upper 3 bytes of tuning word
				9:
				begin
					bits <= 8'd40;								// total of 40 bits, including address
					addr <= {1'b0, 2'b10, 13'h01AB};		// write, 3 bytes of data, start in high address of 0x01AB
					data <= ftw[47:24];						// data: frequency tuning word upper half
					wrEn <= 1'b1;
					state <= state + 1;
				end
				// Set lower 3 bytes of tuning word
				10:
				begin
					bits <= 8'd40;								// total of 40 bits, including address
					addr <= {1'b0, 2'b10, 13'h01A8};		// write, 3 bytes of data, start at addres 0x01A8
					data <= ftw[23:0];						// data: frequency tuning word lower half
					wrEn <= 1'b1;
					state <= state + 1;
				end
				// do an IO update using the serial bus
				11:
				begin
					bits <= 8'd24;								// total of 24 bits, including address
					addr <= {1'b0, 2'b00, 13'h0005};		// write, 1 byte of data, address 0x0005
					data <= {8'h01, 16'h0};					// write 1 to lsb of register, fill lsb of shifter with 0s
					wrEn <= 1'b1;
					state <= state + 1;
				end
				// idle
				12:
				begin
					bits <= 0;
					addr <= 0;
					data <= 0;
					wrEn <= 0;
					if (newFtw != ftw)
						state <= state + 1;
				end
				// frequency has changed, update registers
				13:
				begin
					ftw <= newFtw;
					bits <= 0;
					addr <= 0;
					data <= 0;
					wrEn <= 0;
					state <= state + 1;
				end
				// send new frequency to DDS
				14:
				begin
					bits <= 0;
					addr <= 0;
					data <= 0;
					wrEn <= 0;
					state <= 9;
				end
				default:
				begin
					bits <= 0;
					addr <= 0;
					data <= 0;
					wrEn <= 0;
					state <= state + 1;
				end
			endcase
		end
	end


	endmodule
