// Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2016.4 (win64) Build 1733598 Wed Dec 14 22:35:39 MST 2016
// Date        : Wed Jan 24 10:50:56 2018
// Host        : SHRIRAJKUNJA80D running 64-bit Service Pack 1  (build 7601)
// Command     : write_verilog -force -mode synth_stub
//               c:/Users/shrirajkunjir/ip_repo/spi_axi_1.0/src/FIFO36bit/FIFO36bit_stub.v
// Design      : FIFO36bit
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a200tfbg484-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "fifo_generator_v13_1_3,Vivado 2016.4" *)
module FIFO36bit(wr_clk, rd_clk, din, wr_en, rd_en, dout, full, empty, 
  valid)
/* synthesis syn_black_box black_box_pad_pin="wr_clk,rd_clk,din[35:0],wr_en,rd_en,dout[17:0],full,empty,valid" */;
  input wr_clk;
  input rd_clk;
  input [35:0]din;
  input wr_en;
  input rd_en;
  output [17:0]dout;
  output full;
  output empty;
  output valid;
endmodule
