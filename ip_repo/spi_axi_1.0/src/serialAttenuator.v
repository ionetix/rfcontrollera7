`timescale 1ns / 1ps
module serialAttenuator(
	input clk,
	input [5:0] atten,
	output reg sclk = 0,
	output reg sdata = 0,
	output reg le = 0
	);

	reg [5:0] attenNext = 6'h00;
	reg [5:0] attenPrev = 6'h3F;
	reg [4:0] shiftCtr = 0;
	reg [5:0] shifter = 0;
	reg [1:0] state = 0;

	always @ (posedge clk)
	begin
		attenNext <= atten;
		case (state)
		// Check whether there is a new setting to send to the attenuator
			0:
			begin
				shiftCtr <= 0;
				if (attenPrev != attenNext)
				begin
					attenPrev <= attenNext;
					shifter <= attenNext;
					state <= state + 1;
				end
				sclk <= 0;
				sdata <= 0;
				le <= 0;
			end
			// shift out the data bits
			1:
			begin
				if (~|shiftCtr[1:0])
				begin
					shifter <= {shifter[4:0], 1'b0};
					sdata <= shifter[5];
				end
				shiftCtr <= shiftCtr + 1;
				sclk <= ^shiftCtr[1:0];
				le <= 0;
				if (&shiftCtr[4:3])
					state <= state + 1;
			end
			// set latch enable high
			2:
			begin
				shiftCtr <= 0;
				shifter <= {shifter[4:0], 1'b0};
				sclk <= 0;
				sdata <= 0;
				le <= 1;
				state <= state + 1;
			end
			// return to idle state
			3:
			begin
				shiftCtr <= 0;
				shifter <= {shifter[4:0], 1'b0};
				sclk <= 0;
				sdata <= 0;
				le <= 0;
				state <= state + 1;
			end
		endcase
	end
endmodule
