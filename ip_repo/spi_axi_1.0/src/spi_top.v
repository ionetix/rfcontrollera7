//----------------------------------------------------------------------------
// user_logic.v - module
//----------------------------------------------------------------------------
//
// ***************************************************************************
// ** Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.            **
// **                                                                       **
// ** Xilinx, Inc.                                                          **
// ** XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"         **
// ** AS A COURTESY TO YOU, SOLELY FOR USE IN DEVELOPING PROGRAMS AND       **
// ** SOLUTIONS FOR XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE,        **
// ** OR INFORMATION AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE,        **
// ** APPLICATION OR STANDARD, XILINX IS MAKING NO REPRESENTATION           **
// ** THAT THIS IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,     **
// ** AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE      **
// ** FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY              **
// ** WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE               **
// ** IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR        **
// ** REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF       **
// ** INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS       **
// ** FOR A PARTICULAR PURPOSE.                                             **
// **                                                                       **
// ***************************************************************************
//
//----------------------------------------------------------------------------
// Filename:          user_logic.v
// Version:           1.00.a
// Description:       User logic module.
// Date:              Tue Mar 24 14:03:38 2015 (by Create and Import Peripheral Wizard)
// Verilog Standard:  Verilog-2001
//----------------------------------------------------------------------------
// Naming Conventions:
//   active low signals:                    "*_n"
//   clock signals:                         "clk", "clk_div#", "clk_#x"
//   reset signals:                         "rst", "rst_n"
//   generics:                              "C_*"
//   user defined types:                    "*_TYPE"
//   state machine next state:              "*_ns"
//   state machine current state:           "*_cs"
//   combinatorial signals:                 "*_com"
//   pipelined or register delay signals:   "*_d#"
//   counter signals:                       "*cnt*"
//   clock enable signals:                  "*_ce"
//   internal version of output port:       "*_i"
//   device pins:                           "*_pin"
//   ports:                                 "- Names begin with Uppercase"
//   processes:                             "*_PROCESS"
//   component instantiations:              "<ENTITY_>I_<#|FUNC>"
//----------------------------------------------------------------------------

module spi_top
(
  // -- ADD USER PORTS BELOW THIS LINE ---------------
  CLK12_5,
  cpuSelfReset,
  ATTEN_CLK,
  ATTEN_DATA,
  ATTEN_LE,
  SPI_ADCen,
  SPI_CLK,
  SPI_DATA,
  SPI_PLL0en,
  SPI_PLL1en,
  bitSlipCal,
  pll0locked,
  pll1locked,
  configDone,
  pllSync_N,
  serdesCalDone,
  atten,
  PCBtemp_clk,
  PCBtemp_cs,
  PCBtemp_data,
  S_AXI_ACLK,
  S_AXI_ARESETN,
  S_AXI_AWADDR,
  S_AXI_WDATA,
  S_AXI_RDATA,
  S_AXI_AWREADY,
  S_AXI_RREADY,
  S_AXI_AWVALID,
  S_AXI_WSTRB,
  S_AXI_WVALID,
  S_AXI_ARVALID,
  S_AXI_ARREADY,
  S_AXI_WREADY
  
  // -- ADD USER PORTS ABOVE THIS LINE ---------------

  // -- DO NOT EDIT BELOW THIS LINE ------------------
  // -- Bus protocol ports, do not add to or delete 
/*
  S_AXI_ACLK,                     // Bus to IP clock
  Bus2IP_Resetn,                  // Bus to IP reset
  S_AXI_AWADDR,                    // Bus to IP address bus
  S_AXI_AWVALID,                      // Bus to IP chip select for user logic memory selection
  Bus2IP_RNW,                     // Bus to IP read/not write
  S_AXI_WDATA,                    // Bus to IP data bus
  S_AXI_WSTRB,                      // Bus to IP byte enables
  S_AXI_ARVALID,                    // Bus to IP read chip enable
  S_AXI_WVALID,                    // Bus to IP write chip enable
  Bus2IP_Burst,                   // Bus to IP burst-mode qualifier
  Bus2IP_BurstLength,             // Bus to IP burst length
  Bus2IP_RdReq,                   // Bus to IP read request
  Bus2IP_WrReq,                   // Bus to IP write request
  Type_of_xfer,                   // Transfer Type
  S_AXI_AWREADY,                 // IP to Bus address acknowledgement
  S_AXI_RDATA,                    // IP to Bus data bus
  S_AXI_RREADY,                   // IP to Bus read transfer acknowledgement
  S_AXI_WREADY,                   // IP to Bus write transfer acknowledgement
  IP2Bus_Error                    // IP to Bus error response
  // -- DO NOT EDIT ABOVE THIS LINE ------------------
*/
); // user_logic

// -- ADD USER PARAMETERS BELOW THIS LINE ------------
// --USER parameters added here 
// -- ADD USER PARAMETERS ABOVE THIS LINE ------------

// -- DO NOT EDIT BELOW THIS LINE --------------------
// -- Bus protocol parameters, do not add to or delete
		// Width of S_AXI data bus
parameter integer C_S_AXI_DATA_WIDTH    = 32;
// Width of S_AXI address bus
parameter integer C_S_AXI_ADDR_WIDTH    = 6;
// -- DO NOT EDIT ABOVE THIS LINE --------------------

// -- ADD USER PORTS BELOW THIS LINE -----------------
input                                     CLK12_5;
output reg                                cpuSelfReset = 1;
output                                    ATTEN_CLK;
output                                    ATTEN_DATA;
output                                    ATTEN_LE;
output                                    SPI_ADCen;
output                                    SPI_CLK;
output                                    SPI_DATA;
output                                    SPI_PLL0en;
output                                    SPI_PLL1en;
output                                    bitSlipCal;
input                                     pll0locked;
input                                     pll1locked;
output                                    configDone;
output                                    pllSync_N;
input                                     serdesCalDone;
output [5:0]                              atten;
output                                    PCBtemp_clk;
output                                    PCBtemp_cs;
input                                     PCBtemp_data;
// -- ADD USER PORTS ABOVE THIS LINE -----------------

// -- DO NOT EDIT BELOW THIS LINE --------------------
// -- Bus protocol ports, do not add to or delete

		// Global Clock Signal
		input wire  S_AXI_ACLK;
		// Global Reset Signal. This Signal is Active LOW
		input wire  S_AXI_ARESETN;
		// Write address (issued by master, acceped by Slave)
		input wire [C_S_AXI_ADDR_WIDTH-1 : 0] S_AXI_AWADDR;
		// Write address valid. This signal indicates that the master signaling
    		// valid write address and control information.
		input wire  S_AXI_AWVALID;
		// Write address ready. This signal indicates that the slave is ready
    		// to accept an address and associated control signals.
		output wire  S_AXI_AWREADY;
		// Write data (issued by master, acceped by Slave) 
		input wire [C_S_AXI_DATA_WIDTH-1 : 0] S_AXI_WDATA;
		// Write strobes. This signal indicates which byte lanes hold
    		// valid data. There is one write strobe bit for each eight
    		// bits of the write data bus.    
		input wire [(C_S_AXI_DATA_WIDTH/8)-1 : 0] S_AXI_WSTRB;
		// Write valid. This signal indicates that valid write
    		// data and strobes are available.
		input wire  S_AXI_WVALID;
		// Write ready. This signal indicates that the slave
    		// can accept the write data.
		output wire  S_AXI_WREADY;
		// Read address (issued by master, acceped by Slave)
//		input wire [C_S_AXI_ADDR_WIDTH-1 : 0] S_AXI_ARADDR;
		// Read address valid. This signal indicates that the channel
    		// is signaling valid read address and control information.
		input wire  S_AXI_ARVALID;
		// Read address ready. This signal indicates that the slave is
    		// ready to accept an address and associated control signals.
		output wire  S_AXI_ARREADY;
		// Read data (issued by slave)
		output reg [C_S_AXI_DATA_WIDTH-1 : 0] S_AXI_RDATA;
		// Read valid. This signal indicates that the channel is
    		// signaling the required read data.
//		output wire  S_AXI_RVALID;
		// Read ready. This signal indicates that the master can
    		// accept the read data and response information.
		input wire  S_AXI_RREADY;



/*
input                                     S_AXI_ACLK;
input                                     S_AXI_ARESETN;
input      [C_SLV_AWIDTH-1 : 0]           S_AXI_AWADDR;
input      [C_NUM_MEM-1 : 0]              S_AXI_AWVALID;
input                                     Bus2IP_RNW;
input      [C_SLV_DWIDTH-1 : 0]           S_AXI_WDATA;
input      [C_SLV_DWIDTH/8-1 : 0]         S_AXI_WSTRB;
input      [C_NUM_MEM-1 : 0]              S_AXI_ARVALID;
input      [C_NUM_MEM-1 : 0]              S_AXI_WVALID;
input                                     Bus2IP_Burst;
input      [7 : 0]                        Bus2IP_BurstLength;
input                                     Bus2IP_RdReq;
input                                     Bus2IP_WrReq;
input                                     Type_of_xfer;
output                                    S_AXI_AWREADY;
output reg [C_SLV_DWIDTH-1 : 0]           S_AXI_RDATA;
output                                    S_AXI_RREADY;
output                                    S_AXI_WREADY;
output                                    IP2Bus_Error;
*/
// -- DO NOT EDIT ABOVE THIS LINE --------------------

//----------------------------------------------------------------------------
// Implementation
//----------------------------------------------------------------------------

	// additional upper bits are stored so that value can be saturated if it is set too high
	reg [16:0] attenUnsat = 17'h0003F;		// initial value 31.5 dB (until updated from system calibration)
	assign atten = |attenUnsat[16:6] ? 6'h3F : attenUnsat[5:0];
	reg softConfigDone = 0, pllUnlockReboot = 0;
	reg [15:0] systemType = 16'h0005;
	reg [7:0] selfResetCtr = 0;
	wire [31:0] temp_inst;						// most recent temperature sensor reading
	wire [31:0] temp_avg;						// averaged temperature sensor reading
	
//	assign S_AXI_WREADY = |S_AXI_WVALID;
//	assign S_AXI_RREADY = |S_AXI_ARVALID;
//	assign IP2Bus_Error = 0;

	always @*
	begin
		case (S_AXI_AWADDR[5:2])
			4'h0:		S_AXI_RDATA <= {11'h0, atten, 15'h0};
			4'h1:		S_AXI_RDATA <= {30'h0, pll1locked, pll0locked};
			4'h2:		S_AXI_RDATA <= {31'h0, serdesCalDone};
			4'h3:		S_AXI_RDATA <= {28'h0, pllUnlockReboot, configDone, ~valid, softConfigDone};
			// 4'h4-4'h7 are write-only
			4'h8:		S_AXI_RDATA <= {16'h0, systemType};
			4'h9:		S_AXI_RDATA <= 0;		// 4'h9 -> write to this register to reset the CPU
			4'hA:		S_AXI_RDATA <= temp_inst;
			4'hB:		S_AXI_RDATA <= temp_avg;
			default:	S_AXI_RDATA <= 32'hFFFFFFFF;
		endcase
	end
	
	always @ (posedge S_AXI_ACLK)
	begin
		if (|selfResetCtr | (S_AXI_WREADY & S_AXI_WSTRB[0] & (S_AXI_AWADDR[5:2] == 4'h9) & S_AXI_WDATA[0]))
			selfResetCtr <= selfResetCtr + 1;
		cpuSelfReset <= |selfResetCtr;
		if (S_AXI_WREADY & S_AXI_WSTRB[0])
		begin
			case (S_AXI_AWADDR[5:2])
				4'h3:	{pllUnlockReboot, softConfigDone}		<= {S_AXI_WDATA[3], S_AXI_WDATA[0]};
				4'h8:	systemType[7:0]	<= S_AXI_WDATA[7:0];
			endcase
		end
		if (S_AXI_WREADY & S_AXI_WSTRB[1])
		begin
			case (S_AXI_AWADDR[5:2])
				4'h0:	attenUnsat[0]		<= S_AXI_WDATA[15];
				4'h8:	systemType[15:8]	<= S_AXI_WDATA[15:8];
			endcase
		end
		if (S_AXI_WREADY & S_AXI_WSTRB[2])
		begin
			case (S_AXI_AWADDR[5:2])
				4'h0:	attenUnsat[8:1]	<= S_AXI_WDATA[23:16];
			endcase
		end
		if (S_AXI_WREADY & S_AXI_WSTRB[3])
		begin
			case (S_AXI_AWADDR[5:2])
				4'h0:	attenUnsat[16:9]	<= S_AXI_WDATA[31:24];
			endcase
		end
	end
	
	wire read;
	wire [35:0] instruction;
	wire valid;
	wire [3:0] FIFO_prefix;
	assign FIFO_prefix[0] = (S_AXI_AWADDR[5:2] == 4'h4);
	assign FIFO_prefix[1] = (S_AXI_AWADDR[5:2] == 4'h5);
	assign FIFO_prefix[2] = (S_AXI_AWADDR[5:2] == 4'h6);
	assign FIFO_prefix[3] = (S_AXI_AWADDR[5:2] == 4'h7);
	assign configDone = softConfigDone & ~valid;
	
FIFO36bit fifoinst (
              
      .wr_clk(S_AXI_ACLK),  // input wire wr_clk
      .rd_clk(CLK12_5),  // input wire rd_clk
      .din({FIFO_prefix, S_AXI_WDATA}),        // input wire [17 : 0] din
      .wr_en((S_AXI_AWADDR[5:4] == 2'b01) & S_AXI_WREADY),    // input wire wr_en
      .rd_en(read),    // input wire rd_en
      .dout(instruction),      // output wire [17 : 0] dout
      .full(full),      // output wire full
      .empty(empty),
      .valid(valid)    // output wire empty
    );
    
 /*   
	FIFO36bit configFIFO (
	                      .wr_clk(S_AXI_ACLK), 
	                      .rd_clk(CLK12_5), 
	                      .din({FIFO_prefix, S_AXI_WDATA}),
		                  .wr_en((S_AXI_AWADDR[5:4] == 2'b01) & S_AXI_WREADY), 
		                  .rd_en(read), 
		                  .dout(instruction), 
		                  .valid(valid)
		                  );
	*/	                  
	serialAttenuator outAtten(
	                          .clk(CLK12_5), 
	                          .atten(atten), 
	                          .sclk(ATTEN_CLK),
		                      .sdata(ATTEN_DATA), 
		                      .le(ATTEN_LE)
		                      );
		                      
	rfConfig adc_pll_config(
	                        .clk(CLK12_5), 
	                        .instruction(instruction), 
	                        .valid(valid), 
	                        .read(read),
		                    .spiClk(SPI_CLK), 
		                    .spiMosi(SPI_DATA), 
		                    .spiAdcEn_N(SPI_ADCen), 
		                    .spiPll0En_N(SPI_PLL0en),
		                    .spiPll1En_N(SPI_PLL1en), 
		                    .bitSlipCal(bitSlipCal), 
		                    .pllSync_N(pllSync_N)
		                    );
		                    
	tempSensor pcb_temp(
	                    .clk(CLK12_5), 
	                    .PCBtemp_clk(PCBtemp_clk), 
	                    .PCBtemp_cs(PCBtemp_cs), 
	                    .PCBtemp_data(PCBtemp_data),
		                .temp_inst(temp_inst), 
		                .temp_avg(temp_avg)
		                );
		                
endmodule
