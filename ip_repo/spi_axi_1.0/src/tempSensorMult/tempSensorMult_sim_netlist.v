// Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2016.4 (win64) Build 1733598 Wed Dec 14 22:35:39 MST 2016
// Date        : Wed Jan 24 10:46:35 2018
// Host        : SHRIRAJKUNJA80D running 64-bit Service Pack 1  (build 7601)
// Command     : write_verilog -force -mode funcsim
//               c:/Users/shrirajkunjir/ip_repo/spi_axi_1.0/src/tempSensorMult/tempSensorMult_sim_netlist.v
// Design      : tempSensorMult
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a200tfbg484-2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "tempSensorMult,mult_gen_v12_0_12,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "mult_gen_v12_0_12,Vivado 2016.4" *) 
(* NotValidForBitStream *)
module tempSensorMult
   (CLK,
    A,
    P);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 a_intf DATA" *) input [15:0]A;
  (* x_interface_info = "xilinx.com:signal:data:1.0 p_intf DATA" *) output [31:0]P;

  wire [15:0]A;
  wire CLK;
  wire [31:0]P;
  wire [47:0]NLW_U0_PCASC_UNCONNECTED;
  wire [1:0]NLW_U0_ZERO_DETECT_UNCONNECTED;

  (* C_A_TYPE = "1" *) 
  (* C_A_WIDTH = "16" *) 
  (* C_B_TYPE = "1" *) 
  (* C_B_VALUE = "101001010" *) 
  (* C_B_WIDTH = "9" *) 
  (* C_CCM_IMP = "2" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_ZERO_DETECT = "0" *) 
  (* C_LATENCY = "2" *) 
  (* C_MODEL_TYPE = "0" *) 
  (* C_MULT_TYPE = "2" *) 
  (* C_OPTIMIZE_GOAL = "1" *) 
  (* C_OUT_HIGH = "31" *) 
  (* C_OUT_LOW = "0" *) 
  (* C_ROUND_OUTPUT = "0" *) 
  (* C_ROUND_PT = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  tempSensorMult_mult_gen_v12_0_12 U0
       (.A(A),
        .B({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b1),
        .CLK(CLK),
        .P(P),
        .PCASC(NLW_U0_PCASC_UNCONNECTED[47:0]),
        .SCLR(1'b0),
        .ZERO_DETECT(NLW_U0_ZERO_DETECT_UNCONNECTED[1:0]));
endmodule

(* C_A_TYPE = "1" *) (* C_A_WIDTH = "16" *) (* C_B_TYPE = "1" *) 
(* C_B_VALUE = "101001010" *) (* C_B_WIDTH = "9" *) (* C_CCM_IMP = "2" *) 
(* C_CE_OVERRIDES_SCLR = "0" *) (* C_HAS_CE = "0" *) (* C_HAS_SCLR = "0" *) 
(* C_HAS_ZERO_DETECT = "0" *) (* C_LATENCY = "2" *) (* C_MODEL_TYPE = "0" *) 
(* C_MULT_TYPE = "2" *) (* C_OPTIMIZE_GOAL = "1" *) (* C_OUT_HIGH = "31" *) 
(* C_OUT_LOW = "0" *) (* C_ROUND_OUTPUT = "0" *) (* C_ROUND_PT = "0" *) 
(* C_VERBOSITY = "0" *) (* C_XDEVICEFAMILY = "artix7" *) (* ORIG_REF_NAME = "mult_gen_v12_0_12" *) 
(* downgradeipidentifiedwarnings = "yes" *) 
module tempSensorMult_mult_gen_v12_0_12
   (CLK,
    A,
    B,
    CE,
    SCLR,
    ZERO_DETECT,
    P,
    PCASC);
  input CLK;
  input [15:0]A;
  input [8:0]B;
  input CE;
  input SCLR;
  output [1:0]ZERO_DETECT;
  output [31:0]P;
  output [47:0]PCASC;

  wire \<const0> ;
  wire [15:0]A;
  wire CLK;
  wire [24:1]\^P ;
  wire [31:0]NLW_i_mult_P_UNCONNECTED;
  wire [47:0]NLW_i_mult_PCASC_UNCONNECTED;
  wire [1:0]NLW_i_mult_ZERO_DETECT_UNCONNECTED;

  assign P[31] = \<const0> ;
  assign P[30] = \<const0> ;
  assign P[29] = \<const0> ;
  assign P[28] = \<const0> ;
  assign P[27] = \<const0> ;
  assign P[26] = \<const0> ;
  assign P[25] = \<const0> ;
  assign P[24:1] = \^P [24:1];
  assign P[0] = \<const0> ;
  assign PCASC[47] = \<const0> ;
  assign PCASC[46] = \<const0> ;
  assign PCASC[45] = \<const0> ;
  assign PCASC[44] = \<const0> ;
  assign PCASC[43] = \<const0> ;
  assign PCASC[42] = \<const0> ;
  assign PCASC[41] = \<const0> ;
  assign PCASC[40] = \<const0> ;
  assign PCASC[39] = \<const0> ;
  assign PCASC[38] = \<const0> ;
  assign PCASC[37] = \<const0> ;
  assign PCASC[36] = \<const0> ;
  assign PCASC[35] = \<const0> ;
  assign PCASC[34] = \<const0> ;
  assign PCASC[33] = \<const0> ;
  assign PCASC[32] = \<const0> ;
  assign PCASC[31] = \<const0> ;
  assign PCASC[30] = \<const0> ;
  assign PCASC[29] = \<const0> ;
  assign PCASC[28] = \<const0> ;
  assign PCASC[27] = \<const0> ;
  assign PCASC[26] = \<const0> ;
  assign PCASC[25] = \<const0> ;
  assign PCASC[24] = \<const0> ;
  assign PCASC[23] = \<const0> ;
  assign PCASC[22] = \<const0> ;
  assign PCASC[21] = \<const0> ;
  assign PCASC[20] = \<const0> ;
  assign PCASC[19] = \<const0> ;
  assign PCASC[18] = \<const0> ;
  assign PCASC[17] = \<const0> ;
  assign PCASC[16] = \<const0> ;
  assign PCASC[15] = \<const0> ;
  assign PCASC[14] = \<const0> ;
  assign PCASC[13] = \<const0> ;
  assign PCASC[12] = \<const0> ;
  assign PCASC[11] = \<const0> ;
  assign PCASC[10] = \<const0> ;
  assign PCASC[9] = \<const0> ;
  assign PCASC[8] = \<const0> ;
  assign PCASC[7] = \<const0> ;
  assign PCASC[6] = \<const0> ;
  assign PCASC[5] = \<const0> ;
  assign PCASC[4] = \<const0> ;
  assign PCASC[3] = \<const0> ;
  assign PCASC[2] = \<const0> ;
  assign PCASC[1] = \<const0> ;
  assign PCASC[0] = \<const0> ;
  assign ZERO_DETECT[1] = \<const0> ;
  assign ZERO_DETECT[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  (* C_A_TYPE = "1" *) 
  (* C_A_WIDTH = "16" *) 
  (* C_B_TYPE = "1" *) 
  (* C_B_VALUE = "101001010" *) 
  (* C_B_WIDTH = "9" *) 
  (* C_CCM_IMP = "2" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_ZERO_DETECT = "0" *) 
  (* C_LATENCY = "2" *) 
  (* C_MODEL_TYPE = "0" *) 
  (* C_MULT_TYPE = "2" *) 
  (* C_OPTIMIZE_GOAL = "1" *) 
  (* C_OUT_HIGH = "31" *) 
  (* C_OUT_LOW = "0" *) 
  (* C_ROUND_OUTPUT = "0" *) 
  (* C_ROUND_PT = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  tempSensorMult_mult_gen_v12_0_12_viv i_mult
       (.A(A),
        .B({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CE(1'b0),
        .CLK(CLK),
        .P({NLW_i_mult_P_UNCONNECTED[31:25],\^P ,NLW_i_mult_P_UNCONNECTED[0]}),
        .PCASC(NLW_i_mult_PCASC_UNCONNECTED[47:0]),
        .SCLR(1'b0),
        .ZERO_DETECT(NLW_i_mult_ZERO_DETECT_UNCONNECTED[1:0]));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2015"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
fPF16TcpNgM9dNC6nyb4WjUK+7bY8P+I62AEEiiM/KOMhIKuPOHBoWeWL2UjxSNO68WLeYIZp8lA
I7rHN/CieA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
E6OKJxjnDRUVVFwAhrQMAtoyRVVpuMKsXlca4m9CcIt6QI8vnYN0tf7gH3uVuxZ90322B7kUeFw5
Pu0UeqAoBaSyysHuDqXazxHy7oyk4BIWChvcrp7LULlVLcL76obtSwsXi1ORVmpdTi5b+AcD+WUo
OP1PSFj5jpodG+LwXm4=

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
x+agogSsgbiI6PGyBpMY8RQCDzLctIr3EaG23mH5kJHlNmNKNolnP54yJ8Y7nIFi6yl6tlyOLMoF
/kxU0pyFmIj8QM0/MArMxPTiemXbDLS2VKtonyK9dDH7VbjFnRWwzK0Ngkas0+nbW3TqGPAY98x3
251QPjQoZCw3A7W9PDc=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
KNs7hA49BKKrboRSEkqIGldOa3ndCnhjRkSn8lL1xFfKUn+p+Wbc09ogKV6YYnPU/RaF1LbzyoE4
udPSNea4bST+08IjO5GAxXqUugcig44J+hzpGKmh7oO0TuyNbYq1CnYcsZXaD9vsmNYz8fBDoW2S
VK/mYa21mBKTOuTdQ1yp3wi73aJ1G9N6Ngt7ovDUrjyd5oNxxNlvWU8JkJDinbEnci0qjZ3Wu9Wg
y44pHUXf6xqwFYJpZ1ZcGRKl83P8p74+pLzt19lw9TPlTfKI++IowVjb6wo36ztNDJS0QjQE5Riv
hwbPU/Bt3S82MVCY5NAA6bKC/8NnoWMbmX8Wiw==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
QaRubtGbYrmCghuFdQuTgTEtoVYYLcPnD5z0C7mo18fwCG17qy0y8mj8xWiwE6bo49IP1/JXSIw7
rTBwHFOVrmbm926sWNrF1r3IHB83C5cstprQ1om7vnkw9XX87SjkscphhkrHmi08jjzW4qX96m61
/ymclz5TlAocMQJGz/jwscvIMOrrbuH4SkWQOLQnRfx9GIOv5Y7PM+w/wuDSeFXsAXz7Ahq3/qmU
cylNfSufW7/zfN4RZB4u+d28AXsuFe03aSF1dpW+uBK1xtNZccvj9h9NMN0cuwxt8ZUlLJw8l6e2
hqRfTTZl1F4qnnrJu6w8h8uEGrmgnQG1AW0epg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinx_2016_05", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XXj6Nc59BeA5Kznlx14IKravf7ohERw7h0fbO7pT7/HsiPDCWh2DlTGpFUcnbNZslPN2RfE0nJNX
WMzLQtaHK4Bm6kxY71OsXEKm7MAIjEdLwOMtJTtlZrbm7chBbSxcW6sjWvI36jk5De3Yct9Ao1py
DpQ9NICUtRTwGG8SAiRkAXRh2Jv3rKvnookQrlVxIkNRSBMSgbwuTbq1ze/KMUZebBWwJNUVIC9r
RV/i9wjYXBOeCCUk+cGDC5uSpwdLXYV9ZxhQUU6C1ufAaK2m4OIUeBqPc2ski2O0qQYQ67c35k50
ynO8H9PTEROPEOn5c37S7feU+36OcOOAsVBTBA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="base64", line_length=76, bytes=256)
`pragma protect key_block
QiAdp1gIpfCeNj7mqqjLbdTuhEKjJWlpfkkDvHj/9oeqEZqF+RvGMe13OaTxBi5xh+HpOZcyN7fY
sVKVCggkOx8Dwu1QHPtc/ZoGHYfgEPWrcXUef4Zv1RrHKN0vps0XC0paT9lbaImalRFagmaLBsND
EZXH2614cHO34XC0m3W0J65QW1cLMkwUQUJRpsB8znzBV6MnT3tPNJAEu3lBRWlNpORO/SZe/Ogy
pKHLaJcn+iGWKGxC6+ezdrAW9PcWM5U85m0DZ0lQU+kNn/0+eE8rVoTS/BtFt4WdVZj3esio0e8c
qo7cddVgw4TDnpsQHzLKF+Pv5FfG3pNBT6qsOA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP05_001", key_method="rsa"
`pragma protect encoding = (enctype="base64", line_length=76, bytes=256)
`pragma protect key_block
VYwLBzqE+ZFW8aN969EcPYPk/fPJedQAVW+q8ihDXvnrrLt5mLV0g+z9cqO/J80M5p4KumZYV3gk
DO7iircY7f3B6oItF64zPNv+bRldPyVG3txtPzAUNRDfDKXlj6toij3overdx+kuQwZyR+SYTvXp
KMnDwvq1pJS6/rf4+E+sPD3EHOqCfD92MygY4uAWxHCk7rJGbrMWgG9CkSUIY5m5eHA2YMQRuKY8
RgRcWA/Hnn9tazZeAZeowqDXY7ZeYaBqD5S+KoG+dyNo0XqqDC1UY5JcM+9Cj2nNhAsTFUXiNrwj
5PxfVnSRtPLOsj4XgbLcEi3XZJSgQKLYObPcyw==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 7728)
`pragma protect data_block
ixxeOuI5sUCSpsLezPNs9P4F2j78dF41NNDv91Ed5PGhjv7qgh2uAGuRXZebxJcAu4jgqeEgq52U
h6JhjPq63MIqJa0608UHwSNX4FaRQ0xxk+0vFsrTgB3XZkGm64yH0AnEDnYcMAsFDi1fxU+9ImAg
amgjMGFsj4a+5aeMH70NJEtd7eTf3q8ea26NNS5Bai3pTc7urUzkeBU1RyfICQOeCjbZWUTy+p6M
eV/++iWHi7TwPFSsAphSMvnJT+JeKGoj8rgIxo8xddVz7nRKhsTyyUTSLoAN8ZjdLZJtUT6SS6hf
t0U73MSmep2LW7ywcY4+4sLgApFlCCszqofEAGZx9aoNBwNRLmqwZp90AChyxc4QCJ8a5hbUvrhT
OkheiMpGC+fpPEbSMqwKEAYEXAeVy+orzXhqY8Y5QEGFu67ElHE7iLO367oUnCbrFQFC5iFRx54/
4wF2Gph9mSHp8X7XTf/Zv24mugtD4RieBSBtHl1jzfU3bZcLtANP1yX7fTgyj1/M1pQ5FCd+6DXO
KLBeCMTqq62lILi+U686ta3Oa9t5p6u8cgbBYQxl8mYyFXQpy3kQaMg/ak3QbjP+af+6NNT6EPpP
ga7TBHht1r5YyxCWE869faMpYdZYzJL7jAYfNcqRnA0lEwkEyUy3ET6TQ0YuGqmG5gl+96DhI6h/
miED2j6NOUxI1m/2a+aZXY37+Gxlvm4C9vyEmK56YezXhuyWM1MUWBp1RcQdhV+gcayj02imsqMR
Gd+nf8n7N1Rp+XViNFOu7kcaKht6ubH+kmi603TN9NHz0P/duTb16vykj5RMulMg6gB/1p0AbkHe
tXOJB7PgbiCkI8PsC90unMnstBrkZjBQVqAMzntAn+mJk7P7sYb0aPMcpP4UHPPYKfcdlvcxO7rx
D1ChzzYgfEbPLqFNcS/koCifkWTijGwBGzebSYGIar/CUhVpPdURg8/NcmbQFCVHRyrINYrvRb6r
zG63wEFGsggkH6OSlhuazRf2FGfJat3Do+vpckZCgYnhd4GqxKQCqJ4A/ZLf/IYKhhDj8jI9/VXH
Z7ZuwR4dabE9+MT6oxLgnOXtGdsInd0IBi6wqObkzai33XeYV1WyCsHrPU1+jYHEMQepnRYwipLA
wcfEJKy+viN+cr5A+VlLISj3KVL4+bbltN68HyfuOhueBt4CCCNACTQlZX5ka34XD8aAPwSM5C4Z
I25uL5N7vPpB2pOxcP7PBBWTTFhjrVW8sShZPFdddFELTd0EyIogMuNXLRsUljDuZ3ONnQQCZ6cc
ygj7x9mYRIf7jjuo9DOtNbacb9IR69Fin9ow0vbW1A3f9rr9Y+MX+jGGEEdyaJh1bUqu5FgV1dkM
mjchBgX8KRAarpm6AbxSVIiMuQFsGLhe7g4XNdF9jIuzgMYbXKM8xkIT5oxIZF7Lh42Oj+GqKaJl
k9DgpEmNB658C8s9b/hA8PdKK3sAhFgUOVGf3VQiOPlO4mce2OcNRlUNS4wP/xMsGG9W7XAF7Kkd
Ne6jMfcY0dk6eX0OMJ01l6ZtYe4jrkLLZFoXt8k6OUF3JAE4lS7/mebSkTb9mISxcjHUdzH17KWP
UnGwJr0lILhwVUg/j32XlEs11Nd01F6S2ufmNyAq4GBHseSZ1GdJiV2GyF9Qb1zZDW7ALzwSjNf2
fGq0QRHATi+iQpLco95q1Zo2nkv8lFZmH7wWlO940wMTHHfuHYPvqzvK9tXuIplSx7FVsLNsWmjL
A7fG7ylup27Rv0Kex2nZdueDeKx7FqyNHHp8mMhnwaDwbjGrsyQVMZlpA2XwR4I8NzqEkDKRnJ0C
r6fAZBZK/Xfgg2otxNoHkpztEyxUDX9sqgOLd8iBoJkLL3ub0V2VWUG0NDDGQIxRaydAdqRQ5NGU
un6L0zmWUn0lb582i2ey3xddSFirVBYXoeB6wLm1QyWaIlkd5ke4dtHm6H/PlO8L+qqs7pbIpXbr
oaBnFcCSAc7P7zW/hUYZnuuQkOrWzmC1KGaYbdoHCI/IjZhQPsjmHOQRDLf/Rl6mSm0LYMzCBjGg
NbTpDFkZ73693WaVBf82RYRsr5R6gTGsFDhmKAhRjoDvzZej+kkdxsWZ7Fr7Qx1LMmN/U3zisaSX
bRBpGuj29rMc/QAhKwPSxADBvesGxCk5uyzuyaHxH/pcM9aZuu4lDqAP6iUAcaQP7lgb+++81HXm
8POv30tOZcLAMAIHe8JKM6i7YLz1xvE5yyD2zuPzSw4ZGa7p8JiCwar0GImto+maWubPmv4MST/s
Re6o856bASJh4gLeVLf+rBgZyzQ8AHaKxoWCrVd7JU0affQXiP3v7kIKB3WUWNT7pFa28wskLoBx
oVC8Cl6kFhRjLXYNhCfR4pQEue6V8LSs6jyzt1qYfurk8THZ63RsM3ViDkYUkbStqvtTHGUWeqy6
UwsBg2IPp4uDQsbvWwLhD/AiPLidO1lAhN7VyLVv7SO2cxiYxGguuFdHHIM+UjuVJEAMq1eaH2ZA
CzkyEx94quyWBZFBHSQv+EsM4cGdzCB7olO/AxU55hSa2NeY4WPF9KvzQnpyLt0ExOSc4PpGrbkQ
TOP7grdguri8gNA60zFzGhvO9964u3Udb/CgY8gk7bLeNAM7wyZVCvkAVaiMW1gBqgnLQZmpQ/Sn
auMADMKr82lOU0IfkBoOgaMiLo4z+P8bORfmmx2aqc8G/U6hh3DCIJO3YWPoXhBfuSw432EApRpp
KDywvHi263TTVn+WHK5lPxDWeb/jCbGQ/f+MTqx4oj1EwVtZW6aHY8b2q9G/v6bAQXytezcchDp3
FotiLGKK4ZhXdhMnQzVeMyJfvlpwrr3rSsDzJpvqXABeRRhc0VEiguN7yNdaBD6vX0ozt1D0fKHU
ctuwRhEJXCPZtBLhbar4u3mZOxv7W7GcMln5OlM07iR1EwSmrXIpBPDfrQlTGLyHAdFw6u2a1AdO
z8pZGnHx2NbsNK0vqzW/x3IfcM+2LYSY/pw8Zf+cK5xYRuQ0GO2DZnTpBMD4dMfW61woy+3Stz10
1FVDVWqLv6Z34oDjrNqpfqr9QzKU/M/y03VZbSKysMyc5KFvt55IQXycvguCi2unhfBud2tvT+5t
seRsBqg7J0nrtGIsR5Pq9zBkCfpGa4K/v3RkvW7m+GktctpwYjCnXHazst1k+YmyGVqN8f8Uwyzr
5imuHtp0kURPSLmmP+CRR7t4GeVQc00Z/MrTq78Rk9ZF1KEXDNKK5KDkpjJKQ7ARSdi1mSV8TJGQ
2Dbm6druL7XEQP8iClOhhn9OTvc923nnbWxWwjkbniRSpKv+ohi/ATL9oaP/xmloJLQsT3S0qspn
sxzN0zQHC0mxRwQWJLfVETN5dMRx64qjZam1XIOfOQ2pPch2iL75eYYxz6/5gnQ4xLa97UHYOzFz
vijDFOvfPUrIJRwihi/qvPYXuckV/zxhcrjKv+2SrRFoDhQ10n7mBVv+E2b/sqG2/6vZKfoF+WHu
TLJLf/4wHMP/28Z6leVqa4TbnNoi80skGPXoSQy4cM0zAFKB/2+Dv2vfUD0TpmWZuQAMx3xuFrvl
dwywae0n7K9unED3KCQvDjN9ze98OtC/i/irCV6C3fR8GWW1C1YgBW/x95Nak2eWtXrKq+09ZYBG
OgqaugCRCrs0YcOEglIje2E0fGf3myAOSggrTIv5AU0f1Q3V1KBoK7tkIlKItHH9khkCwZyPQcAH
PhsF55Ix6M/NbBNfxBkQeLT7UabGzM1UyQlOV3/quQIiFV6Ms2IP9e0ikxQgNluV40nzZulu2rdQ
FWD3iWZHb4KiRLqsEMaqm4m5NiRt9Ud3klIQ+9z9ypN6QdFE95Y2+mRsDC8zucerYJj7Vf/K5S6D
WfNy5eS2O2NtZoGbZeE480/2jznxeKp2erXprzcOp2WOfVq7lNpFetNM++TKDW+E8hpQSKdSwSWI
6xNeIMjWf3KPfSLR/4H1YhudZz08uxtclIq7SneIDzylhEFjFEdtSyfy+d0ZxPecCUwP91UZhUHq
fhbiXhQRnw77IVt5aKo/a/qycuF4mNX0Gi6KAKqLeOTaJJfQX1Tq++ArlPIQDX+55Reu/Ruyou2C
BkEnP9QhyyciFIdzO1XvXtD9HCVM1XNqf11V9AoqpWFjrJPIVIOvnsucgFy27E0vHZgJRn+q6bH4
JbXr4kQgPS7K38hK7s9w0mcANHWh2YxVq5kln2MPIjSIfkjZwyLhQigkYxDFiBbLZw3tiKyBnBrq
X9g0M8yfYzzME5kbxP5/N52ITFV0x9JKsf3lUD8lgX0yuMCkS6ygJOzISzZSs1ScPXy3/TwLUMGt
ki1LH9oVeFMmQCTxocpO4ROirTiu8vTZk6GDgtKQiaB853vFGj8FJWbsG9VJWblO/au4/f3ZpA2T
kRJpM53kuvNV8LhNPnD8z4Y8AquyECIX8TSe3XW2hh43ejEA1ZnXqzBv2J+PBTcOEbXJ2xHwtlQ8
eatfs5Pwp9A7mzmHvZz3L0ab1/3M7IMrmzKZIwuE4Wwk6cArgGzLCzWwTd8MaTle2V59AN1stHYX
diy7jVpiIZj9whiPEpxfFV0n/Ep9deoztwlLCY+RsmU7vn8OfRFzlb6WuXaNwr8ATG+ILhcpvWTN
QxQEzwcoNNvaKioO4FbdQpsnkgqBBplDMyEs8KKOuRQ2tnqWFtzwO/ey3DMxBbNYJ8iQ459R9f/i
R4cLDIMqN/4bncmWoEPOdZUPHpQvkgtqepzKVOlftb89vKGs79S5qyI8GIGWGvkUB45Me6ccu4lh
CZXPSHpQtqiZ4wBoo5+vzSda/QXFxpZtVPYgPbNM859so4BEgHDfL3fxVhu2jAb5q+gVMD7boYHK
vkL2DBWHKfj7HwdK4yhV4/POc2NHgtsknTOoEty6jBUImI5NgWXa/gcONU01w7kqrV5rAOUAESRX
Hgf4OkUDJ4xM46c6Koo5dRzvy1o38PBzeaqmKOt0gtlGm4aAF04NT7gY54KYRDbm0mwnbsqRgKHW
GvOaJ/ENiodfuznNv1H6RodMKh+VkFM7itH/r/gwuaY65oiZL7kSH33Njj/yvhGf62OK3+Ke8sPW
7gIkyu6jSjSNYmyaeB4bKpZealSIN5uRaUFxB78UM2WIa6/FQHeEVEgIwK5JdvBwVRtcnwCWRKiJ
WlTctqLX7fKPs04tNEvHQVKeTyUJbyplf3H2g7T19b4ci1UpLL0czovgodGTyUUdl3lmBuhY6WaR
riYXfuUuMA2NNN67wzjR7Sfu9YDbz1YBrNYSkM0/46BUBsPtdpivGtuTd6oB7/p01cnfWyGWOwIm
6UTD+rwokicNn0ujcXTWeInPmI2bN5o9PmpGUuWQ+V6CLz74bXtwCQF8veYPCsGk1yI8CrOYbeI7
ND+78VVyMyCSbl0pbMbiudVlTE5EZHTMpG5mGTfu4PVu5oxfAFW/P7Kdisw+WS8IdxhMb0gX9H4C
oKAAvAAona6jEfxC8wFkEeEeN3pBuTz/o3umsTFNhgXcz//LWq1oR2ptv0J5YiqUoUC0nEvLfqOI
wFJWyZGT5D2KDPopgTjC76mr6Mx7VOORHQg3Cnn2JN5rJ4+WopZ4QYwMm2KQsWBGSjp/QpdlRh4A
+4J7DWqi5aLX7v6OWQm/P8OC8lGbMKLSBa7fXTmW/U5FK1OaEk7/QRG4Y83rS430gV6pp+8ZueP/
pIsTbGeBADH0ynXewsfYBo6HwMm9fXUxpBVnDPikRJV7WGEypmIDdX5w9kxdVPj7UicP8h8oc9so
5ikkDmFVKFaqGrAyvaSQ7o4Nk+DJ32s3Yu8gkPKmxT9UmH9eMbET4tQ+kwrtUuPdt/6UEj875Kll
zCZr9dOyuMKkDKCjAorG20ghEc4sP7yn3FqoHuvgYo9bJ6skzeVyQ3HOxBRap0oiSkC9biMYTXy3
CCO1aQBCYRmTFwfaIJjsSz567OWYJehqThSahNWFLXuatds+CKdAqAvjWwoiXI5EC+KHcbueqpps
g7pCK/cZP0O9kfuKUZ7SY068StIsG5wNTuVPDUixYMi563hZxeZ75O9fjUIu8DwO66nr/zpz1lIG
RKa8Kj+pdwTR6/4npfbrbwcwhWfw9qaM5bxKdH6a+Py5rs9BKazwxEZQpKHNNE+uvWmvudymazZf
eI3sGenPZApB7/ksqO8MjAW4pR6EZpZHbt3fsoGVX1P1f49wptyRzU26zBmXYxEA2bqlTaGdrVXH
fhuKs19rqs0Q1CUqGdhkPXtRtzCfGe+cIy0phmqKKLBlKtIEwLX0e0ZxH5yetPvDZXvOKvXxaVk1
ewJaBeq4dXpjQOn7bdN8Tx2ZN6Mz8Ui6FYXiT7z2vJqiNnjeFcbbcjxvaSPt+xRoUEU9QbOeh4s7
/QvAfmk+xzDuTo++I8hzNVxC7DYRWuaE75cN+JzWHLfiITo24Yv4VgeGyDVGfcC8gH0LkPXhxP4/
xIcX0420wqf1/2KM8wgbpso8x1r+XVdIK+lnFqiy0WgM4b1ecI8glc+jONiFtNcpIfd2nWwAlQpZ
BuQjBxrEPCVvqB3V1Upc8XVXlhYJml+R2Qih360U/iqSi3cb+ubiwwCRJrqnlvMo6iklc7Dsegif
cDpsg1K9wG8h4RPJiWW/vBp6z2srV8rZ+hp+4cxrhs55O/hHZDrjCjvyASIU8BFVBlVmtfNY0izj
33Xy8aR2ID22n+V05oyYppE47rgJ0ZRpnk+/TQuh6AN6WlVa+LLaFIfEcbC8CwCzZKUHE/bpkTW0
Y1JCnrHkDuwq5rWQppoLy8fcD1XOXhioBKmD3oWi7vXQk1WBhaL317NIcXfHL4HxSgZQxpf2Btpi
rA+KoDZ71tixbSaVFDLQ6PpPWYyeovE3AaK+JHP8vqZqdnFEoMvdWFsjdRyik6LMJj/QMXybtOxT
MDcOosprnfwXYt3JWqBcQQZGa/2rxw4pgqLw6oYV0hGlUUK7eGXz5amicTQGXSFkHNwI86te2Fek
p3oeN5wHBjNboIz7ubc7VXelVgA1z/T4g3D1dUZJiatDwMP+u/AOJKzBDaYZD+aMCa+ka+b74WmR
2OdLtSy307/GnA55scH+rIU1OaH1owuq9epFn3VcWVBym64p1oMsDxPGoHP+XULeLaJ6EBHD54eX
d2q2QrhaVGYPHHsDTbwZqq4dQesayy5OiiaFJRUCYQAlgVoL2q+TR+X+HgWAz1Z0txughRt81+7k
/D0J/WxVVZwWOffpm7FUvOaTGgHK7i3htxD56ti5euON9Dbrs0tl/vbnpmM5lGD0OUGOlMjYNiVW
7TggIsAc+hrrvJ+u0BGxxkzmf7fJQG55T3C2saViKUsCh6msYT8oiwoXnw0ntouxlT6s7KfL0bcz
8TMQgCQTCypqqbjiynUQ2DcZKbDXtMA75C/wiC1evM2Ebg1fRKRIxo8RLj/SqbtfTovB86VQ6VUd
EVIk9pyaWo6dp3Ibz9D7lQobWm1/DOfEh62DgMhqZy7lcCU4Axi4FdthPQqIlo1JDCysQ+te9L4m
gH6rVjDTpvl9w7H+lLmaFYrKlNgctZ71Jvvh4+tr5B0tvSCZfIG/gMpKC+RMhfFiTMQdOCnn+8tt
5/mYQAVId32U0IwIqqShXRw5MJVn+Nq9N3UK2WsmddlTKnIWl+8aVOcyyNEkoHZ2AGfCUMarSkJz
bFfLTMAYXMMj2kU2ocRukj6uwe4MIeNl1i+4RzlmTXlgqR0Gv3g8cciTCfivHLedtHDvcirYmiVF
E49Y0piaf3/k2hNlv0xyZjSlMW41PZWfP4nn/N2uWM603hLvSUqEv/nn6GGGAiwDT6bAdfW65jNb
DdUMgpJyktBEP0W3OFYYwxC+CDOaWCl3jouOIREzgCuUvw/VNd+0QdYE7LmEc/9Z/Is2vGpQEq90
pKftRx4f0VgM7v9FcXk7hEXlRphp4jBhMVuSMbO8IfDOgH/m9NBu9K0x6p2S5zRWSM2XaSDZFbLK
L3BMcgzLa1btZaHYzSCdOygeRVo10oXzY+F4uguHlZT/9g3nfDZtAtA/AnM02AYmulUYILQ2vEXV
DtakPGfxARvqu4RveCuwphWn4zP+c1BAPpBkSjOI1UdNnEYCER5JCBw2nafiNVhkUlLX/sBAPPBw
EG4QoIL0VClSqazMFimYNiGKQD3+ERevH7jpT+gGMEEQBQX0mgVDC3/lTcNbigOQ4mdel3mAW4ea
HTcj3QKdLNNVnuOhXawqYnvaiHIPXMCYPekC4h8vpdZyq6wP1fBKU6Kyfml9durPMEcjv88w+5HN
GmOPrWqWiP18UX+iyP1bNtxPANOnRqh1Sv1ceMx9cpAaeEpMVEn0tXYTbJHo/cUR4k/KC2W4p/EF
lPzj11TukjagQhZMR5beZFMD2mK6KHGttSHcCZtqrA6tytRkIwgNrsLhQuSnPHYBitCxpqKVY53G
W62D9Bq7okVprkhNNpgIBr8Rxp/0YwTfVscrtgblVCE4goRY7RN+pquwtntFLCPbpIR+fFF7rh7L
kINoHA3Obl4eVl42aPrG6ISGjwTbsZH672vqdjsYfLvYler4arkvKFi6+vPZMv640YUmKDrBd25H
gqEYm3VUscRWIrrCvpSYYZluDftMLzmnxZxdV681FF0SI96jwyGEFEBN1WEZLrwZkIL1khjuFnhT
wAkwLn6Svw1DpRiJtEdBiy1+sUUXyXXAhQYYlSaTgrOEedTbEkYn2PuOmOn1IGGSj4kDxepfCe2w
0aVEUzceNDwlM7f+cEtEOHcn5nnrqv9FzVnjeBIEx8ANKw0PGpvid/5x1WpU3Expoe6+qEBBShIK
ii5FR9zqapkw/ffPUJfq2gg31m/vTSv76EHb3BkMRNkgkIo1OlBYv0C3/nnngX8uJPRCQN/VUOWP
+qdiCJc7QgJXNaV+nU9pSTQL+r66VzbZQIEUHqZQM5UToF3R+D741tC54m9W3s0B16WRtAA1RV9g
Qw6OJZf0bbYBolsFNQTAogp38R/9eyu+RyO4IdFimzahxZMnyBbahj9tZRcQnDGK7czBSNGfTsLL
QfNJyVd2pOhOkGHM9lyxBXH0DgIdN7TFtb/uB0/GpGuF0wWwYnxvo3Zyj9D6FHh6S8IaFZHYvSiB
pFJhKfl+XSLo4v89/giehxknrEMr3hRdmAUof9GRBSxPZ+Ry07vGcnUtXrlLX2VyIrD6+wG2YXN9
OThEi2ZCyR34yNbykHg7G+nbceYmlaq4v+ZRt4vm4CuchDVBDZuTOYu3Bdl618KyPAKFNgxzrVcS
tqTgkihcxFoUZHG09E2U4Qn5jPQ2Glgbk57j+R3twTR/Ns0Rb5imH5iLXYX4buy+Be7retqQaCS1
TJMsIvzXRn5kndCCLBJ2wX44SQCMbUJ7HyxJDiCJHOUsu+0ZIlPVa9Bn1yg+92a0l2SQj0m6xhlr
i6x9f2XhFJRbGu6rtOsq8jW/KZlHy1ci0bIq1EbImNz3lDqLRQQb82OkN+JArHcPQ8DqE6XKL9ne
rRPUhsoJN6RtNeiBq0HszbPgkMsU1EAFAO/API9eyS0nkqfa7eu85ETg8ooIZj7EpJvtoIWD1sjf
/yDp6Z8L4ZvDQAVfYVO2QXNr5N8yoKRObAq7wRfDvQ2F0V8s1UIXnw6j8iY1gY0lCUQ+K5Z9enx+
kCjz0tPwSahzUx3pA2bzGRN4gsXxz7DnnCJQ57HofWC07gz1oLt+dOAlzp2W4Cx+kmZecNP1CTA0
vcM7D055yzDw/vxHVs/R/MB6fiXF5C9l1UkRCXluSFkQ8t6vez0bjLurjihvHx7chQUQe6FZTyZg
iGkcPRsRltsq4BdB1reXQXmWpc5z8zrr8lANymxTc7FXkhf/Y8+cGbLDTjjMtipzEWkvOsKzkUSi
Zq/Tyfc/89sbTKMKXuQnoPUVVFrtZE5HZns0o4nEsmH0p85G8jLGLb7BDEV7svkn6uATOXqa0hDH
IW01EO7UHMkiWgMPl6HxwvxTwLE49O//xV6yu5kIbNkxqUvGoXNENpQQHcIPSXGUjhKcaR/T8Ict
LEqloQSI5QyAiqreDa2UsO0DFUcIeOZiLIdm3I72BIzMJ+dElp8sk92HDC09IGHY0IBySXOPPM/E
rKMUv7mzKHucJfrK5XxBJg92z7+thLSr4XBVX95itqagiCOZ0Ybze3HpgLyd7fyMj8VmBihMQlJH
flul4VgJFGDejpTjZIVOHy5BjMDpzeTUc5qumrYXvD7XWD+mprkhvy3cGEPAJag+47nBGe80uCRn
j4NCZRn7Th2ZqLVumlqtpqRhNATJ7RvDzjzAlGCMrASy
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (weak1, weak0) GSR = GSR_int;
    assign (weak1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
