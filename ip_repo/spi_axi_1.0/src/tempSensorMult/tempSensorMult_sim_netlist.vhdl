-- Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2016.4 (win64) Build 1733598 Wed Dec 14 22:35:39 MST 2016
-- Date        : Wed Jan 24 10:46:35 2018
-- Host        : SHRIRAJKUNJA80D running 64-bit Service Pack 1  (build 7601)
-- Command     : write_vhdl -force -mode funcsim
--               c:/Users/shrirajkunjir/ip_repo/spi_axi_1.0/src/tempSensorMult/tempSensorMult_sim_netlist.vhdl
-- Design      : tempSensorMult
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a200tfbg484-2
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2015"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
fPF16TcpNgM9dNC6nyb4WjUK+7bY8P+I62AEEiiM/KOMhIKuPOHBoWeWL2UjxSNO68WLeYIZp8lA
I7rHN/CieA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
E6OKJxjnDRUVVFwAhrQMAtoyRVVpuMKsXlca4m9CcIt6QI8vnYN0tf7gH3uVuxZ90322B7kUeFw5
Pu0UeqAoBaSyysHuDqXazxHy7oyk4BIWChvcrp7LULlVLcL76obtSwsXi1ORVmpdTi5b+AcD+WUo
OP1PSFj5jpodG+LwXm4=

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
x+agogSsgbiI6PGyBpMY8RQCDzLctIr3EaG23mH5kJHlNmNKNolnP54yJ8Y7nIFi6yl6tlyOLMoF
/kxU0pyFmIj8QM0/MArMxPTiemXbDLS2VKtonyK9dDH7VbjFnRWwzK0Ngkas0+nbW3TqGPAY98x3
251QPjQoZCw3A7W9PDc=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
KNs7hA49BKKrboRSEkqIGldOa3ndCnhjRkSn8lL1xFfKUn+p+Wbc09ogKV6YYnPU/RaF1LbzyoE4
udPSNea4bST+08IjO5GAxXqUugcig44J+hzpGKmh7oO0TuyNbYq1CnYcsZXaD9vsmNYz8fBDoW2S
VK/mYa21mBKTOuTdQ1yp3wi73aJ1G9N6Ngt7ovDUrjyd5oNxxNlvWU8JkJDinbEnci0qjZ3Wu9Wg
y44pHUXf6xqwFYJpZ1ZcGRKl83P8p74+pLzt19lw9TPlTfKI++IowVjb6wo36ztNDJS0QjQE5Riv
hwbPU/Bt3S82MVCY5NAA6bKC/8NnoWMbmX8Wiw==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
QaRubtGbYrmCghuFdQuTgTEtoVYYLcPnD5z0C7mo18fwCG17qy0y8mj8xWiwE6bo49IP1/JXSIw7
rTBwHFOVrmbm926sWNrF1r3IHB83C5cstprQ1om7vnkw9XX87SjkscphhkrHmi08jjzW4qX96m61
/ymclz5TlAocMQJGz/jwscvIMOrrbuH4SkWQOLQnRfx9GIOv5Y7PM+w/wuDSeFXsAXz7Ahq3/qmU
cylNfSufW7/zfN4RZB4u+d28AXsuFe03aSF1dpW+uBK1xtNZccvj9h9NMN0cuwxt8ZUlLJw8l6e2
hqRfTTZl1F4qnnrJu6w8h8uEGrmgnQG1AW0epg==

`protect key_keyowner="Xilinx", key_keyname="xilinx_2016_05", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
XXj6Nc59BeA5Kznlx14IKravf7ohERw7h0fbO7pT7/HsiPDCWh2DlTGpFUcnbNZslPN2RfE0nJNX
WMzLQtaHK4Bm6kxY71OsXEKm7MAIjEdLwOMtJTtlZrbm7chBbSxcW6sjWvI36jk5De3Yct9Ao1py
DpQ9NICUtRTwGG8SAiRkAXRh2Jv3rKvnookQrlVxIkNRSBMSgbwuTbq1ze/KMUZebBWwJNUVIC9r
RV/i9wjYXBOeCCUk+cGDC5uSpwdLXYV9ZxhQUU6C1ufAaK2m4OIUeBqPc2ski2O0qQYQ67c35k50
ynO8H9PTEROPEOn5c37S7feU+36OcOOAsVBTBA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="base64", line_length=76, bytes=256)
`protect key_block
HFFUVogdE6YhEbeWkneuQsFxdx9m00ZI0UE6SYIXOgSz4gJ1F+zo4Q/HyHRVB/MAVeTok7m0ElrF
CEs6H1DGWY4a2IsWJfplIyiIk8p0tWtxzpEUjl63dCQmuE0X7+NsiLvyHnmfkMlkFCqr4rQ94xrf
zUQ8iYuSOcwf+TcoPlboeeyDvR+5FwvDw0Fd5To1u6d7Kon25ylyMCuVWr6m0YtdQM0arf2zWNiN
l+bguDvDku7IlmweT+fVChN3wAtneA6m9LI4/5UbpzeSbHkpPWvFwjBlowpijEh0LprCxLlrbbvd
AeV1DfV62lZifJ8iUejl1PWb7E4k4egmuUDscQ==

`protect key_keyowner="Synplicity", key_keyname="SYNP05_001", key_method="rsa"
`protect encoding = (enctype="base64", line_length=76, bytes=256)
`protect key_block
IdDPAMKagd6ITdmj6g//ox12eKJ+mInh6dcK/4cHawtEhxGg51oTAU1EmUo3CUHAx1JP1uavVJ8q
KMSmYrCmSbT6mL+krSb2mjI89RVrPluhYydGZVBvXo7c6Ibknt69Sac9cl2RM40o94y/FtJBYrNu
iprBP1PvJ4IyrCsfL0C5IxFRs2qy4KCsU79EvK4yEGoNRe5Bjta3LhTuUcLv5Dliw8L+yvyXJa3H
765ge5i6bkwR5a9TjXTGsQ2KrAJ8+f88ddkg5c3Hxmskbyvzb1fINp4DosZ1sgdumWdccdeFRJD0
F2eWzWghRwM9OiS00Mar2zT1tJhxIowGS9JKwg==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 10368)
`protect data_block
DW9/H1lLbNT4OeafI+UmtgaWPTXoz7LiW+8PMF6QqXYOXppYFNlHwqrdTjbKAh7MTSMql9CHjC/V
pxhKFiU5UenxnufdMcsFuY3yKHZFG+OzZGw2ul5MsjC4alGRRwKGWPKFK1wlB0Em+sWExaorkDFb
E9zUiF9N88S6VcXN822ijtSkOwAv8f9Lh0L1jvUwR9+d4QvNvcBGZriwwNmHhUvg9vL+wgE7IRFp
Sgr/hrbYtCyHCSmzR7vOl3F4nYrcNEOUWycAirbsC1vhr4rnUqS/G7eJ/cHuArWQf6Vq/bv/fINq
/G79QRphCkhOOtfwKY6Z48a4Ef17Xgie/I7gC8MldKbQCjr3fOkCXZnd8x+Nk+FrQ7XuxyIW5n6z
WJX4blg5Mch3oI9TlcNHQnM3cMVfjjpJJpmE0+Jpgrne/lRfPBmfMvgtFtxEnv497T8zR1maBZ/+
7+eRB5zRjGL4qyZcnvU2eFmkxcDGHZ+CD8rjYm3fQMkYlqH6Y+Y7m9AZvNYk1MEXUdeQIJoaPiB5
fLLnbRTjGdmqF0PvKiElRE+lXfCrm3kicqZHwlsJM2NACt7ZzQp9rgb5rZ1222owIAw9vf7ZSdxN
HAKWW8yisysG/HNwJsXxCBFF6XKWsYvORuGX1bcRwy8JpT+abrwCmeyRHDZIHpX6LuJhNZfLN2wp
kRd2YZPgjsB7TSXb8Umlv3uCuG71A+5cDU3QzbdTkvQZxxxq//TC99agn8LpxygrdK4AWGeTjhrw
28IJeo9HYhpt+F07TA99ppLo/2hrAbXnBsr2IZe8JyIxAX0Knv6iV3oyUk8o7Iv8mLgcAWUJS7kG
bFJVc7SlUaOFmXWV1HLyFK2fnqP3nBPoTbG3PAJpRzbQAZg+yu+FX+/6q5LsgBOMq/hr0h6zVyBS
xfc6F4jrmEkmUAjYdRPGDEer10K2Vicl1Huq5ZuYokFuYysmP9LXFSenJM2Rzc6Z5Q97qO5648FA
HFNLFnGZKMKAtDFoSf7BlEfY5U1rSl+xPRZI1z6BqMuvV3iGf3tir/um380omnaM2NKEgMGeZzcC
FuO1UUTgw6wUCQaXduxxrMHmg2wyqobWz5l/L2/TtXu2oXnwOvhvtisQAg9gaMWZ/7Fn26qHzUrl
lSJa13w1z0lagwsg6vw/s8qEtlZOQv+w72Tzjptv/iGZAfTTCCy9uxXBwDxhdMkoJXV30PnujXtO
2E8RkJNuv+UKcvr8daxdCE4ABEd1dySyGzL6mwUvpewBa/QK/rjquyD7fsbdI+RRDMlNa83F5nYY
5/ivSjIajWYiCHv60xwCIQDYa6zExplYTf1MkPZXeeGywK1BBYfbDmMd8wMpKLW26+4IIYW3ifqN
P7/UYHboqI33YOesudRQjyAUPV/NfiK1/DPbTzsbjPxn74p3mhXUbYi+YzQ5w4sYYHY7EJNiZcKU
LO/RhlXeHki1bwefnUX2pg3Hpl2lxkdW8untDBmIbyjsBJnCfdNBZYLbXdLeu9iV9dhDRzpycZCm
mUHCBahCPbzAKUjApxsoSNJPvUfP8P2q0LjS1P8DxsmbfU7NJYF+uuUtBmpXAYq4YnEPRxcSQjSN
yblCZW7vYRSmYpOsjmpedid+PwEYb6rTaHdc4BmiOs5YLEwAe3kDjEBXvzJZS9G+SwVedcwORJXq
CTsuvSzDEyt5uUwEMc/PaSPHvUF1rK4Q++HBbTs49L/bhbkyv6s1Ou3zAV+GBPNUNOfvZWAeWsZ5
tFXR/L3QP548+dmQdzl8T2/qn8GWVcVxegrd+3hPjeih4n2Alj8sK1Aq7my2v6FJrXZIPEFRq2mM
/p+jNjRECVp6zkI0gpVBd+HB55QN8njKj1kf2JLml5VjgRfSuode+mJpBfxGm5DeGBTM1jcb+c1j
bUJBgML1TiXPllXYiCEkJWUcTRp5OxRmAPps4HTuyFSDpFpaZ36HixsMC8rfjXMQosUs+6WuBn8P
2juJusbiSutibwtGsaX3LO6NmccKsCOcpcdDv4ckjvVaf4SmlU5M1Et33wAMPxyvrViFP211cuiz
AaOXT2HH9aiVAJtDnqGAF1bzRLSE8zQcL3cPaZcuXl3lV43m6fHlaaBsCPdub3Sn/qVmQR9TGVIl
F1KlnZbDBzNFqIA2WB9nma5CcKdGaDXQ5ZQbiX2D8EF9XkhY1HmmzWvsBCp+IhhMsjEEVxEk34I5
ik+2dyDejqkq3KYZghhB5h9Kuw9z7mMeoSFWyKHGO6C32IFs7rCqmuhyJDd3XotfriDQK8h4R0Qs
ARbM/cAcQ2h6DyHpBBnjoRmvmtp+j/w5FYCNoR72evKsNrFPeFbwX4Qv4tjt4yKydylQ3+UWtRJM
hSmwvAGEdq2mcua9eJpVoAlkR0BbLQOiQvATHb8ut2yrIK22ugg698n201Vj21GKdroVbA7aVrZd
HwnijTU/vo0++z3yPB71t9A4sNzanyqH8ekYEpJNH9zJ8Y4OM7Nv/AgWajsXkYFGxvE41XuWNrF/
hXpoW4PiP6BMZnTRdHLhncjohvtKwNMTMtWWy2oNy1dH1PwTe6/H3UFhThVeilaShMJb+8RMI7b3
U10qSUSdlsAkuKiPdP2Bfl0n+bBTM5q88wToPvMjwemDYCiWJrzgy7rzpTd0fmRAkZcE9WvuK0T1
3/dKz/9f6V9XubQB6K6hEdAnH0OuSLV+cpmT53nvgaAOc0uowVXd301RCJFLN8b3Bf64OmFgf1vV
3r7KGZXo5Oq1gyq8CH8aAmBLs7Qgr8iNhet/Rq5/VFz+23Rlr7xw2e2JfxvuuyWp/pszi0ZTLW0Y
ak2XxKMODqSqOvNQmxrBMN+QWqfoNIJI4OtD7WdSUtU1d4GrH8cNTwAmtkpTR4UQA3UFfxQsVgx9
cSqEBvbgcW3qNEgrK05IwgB3dyMF02jh2pkLUvYr+DR5r2gpaKtZkoF83mj/PkPWiOI5mtKWyf5C
JEQH2e2n9nLVec/78r9O+wDI/yxJ7Iey9oeS/DjzKROU8s1Pm17QHs+o/nWnJDkadmWvNBZdjm5l
2hWWkSvTP3ezEbn/9u/hYgCRiSSua7Z8jvUXKNiBPdQviN7xpgiP8Ce6yuJvTnfvEASo40obId+B
dGkaLDb1lyG7J2T4jyM3LR4Chwa9V1ExR0ClJ1dUxfKs0Qd6VW/VKhesaQ9k6OtZA6FNxfLImgD8
qDcIQMZ2CZwQfDa2xU6F+gvOgOYUD/SbeZLTI4dtzZ5rUP9/VFyzkLiqAbnplIgejdfRfndYK5Jz
dL7642c06S/ZYJIdwW4e6U3K0us2bgSA548x5eGlDsVCXxjhpi/pc/QrDUtYQDMetOIwehLcxa0Q
Wuf+LNhF1Evx0j63d7ik1IXtR62aF1mCmkT8CVo7CeNM10q/qIe8SfS1zaXIB77cCNcywAx2gCPI
/qwUYvLtxv2NAGLFtk3zaVmaNAYItt52wpIJkLkuWs8IuoNegH6hy6lwvJKoDDPrp1jlkciMZ6Os
XIO+g6VLFMsBv/Xj4i4aGbl9ONfmG9XWvSLkf7yDYRkuhKn3PbuOYIwo007i33aHU8Ru8S1tbsvq
Pn9yB6qOosOoeskssA4SPmkjowzgfqnV1ObT8vyiJ5xAeKOODFz9DZVsL0fRLl/vK+AkYl4OZqKO
iyij87Q5YfWWjTUHyucEQZmlVaooRmcozeAe8ezF4/9EuQ/K9MJHU9FJH6xTNEkJwAvK8MOrGXlw
XqEdjxZvaKdlaUlYJpbx8h1zAc217aDaotwqnegoLrAigjS1CSTeUUl5RGdgQAIqYm9k5k9czZ6Z
tLQvzIdESZywgTg5M5+Mxt7j8XcCqdg2FKMYJOswr8L4QESEvbwogq3CgbVj2iYBz1K+oEcsMh3U
21fgtE+zbFmPsM2emBKyJAV4ln8UNVRu5JAKgqTUA+sRGssI8m0ghpMFkJTTkIbTkI43NvsH1dHP
38fXZoUCHlVQBhy+v6U3CW/aP6hIuHoZoGaH1nx/jzcjyYJ5aQaA387+u2bf1pcQEp952HHHmjek
duOF8qXFD2HmTZnrktjUDBBFXrvyxSzKV7viPC9hhA5kse7GvqtEokDwqNOUxc55SYopCrxpr42L
JkRl9wE0+76Upd8VaWkuNBo4vrrx/152YmAi/ypcbS5gVXL7kn6Ba4lsqwInYquw32RNRqf4/0fz
naTETWHln2eywR4p+EmqUFmAdOk1Ymm4Pod0OjK8nCpSMBxKKy40MEOmxRmw+Ur9tKTlOqfwwpl6
ZgQdiVLaaKD/IwJeRJn6+/wSuRc1EHcgkv9EJlq/4sDJ4mFYxV/NSiRH0lmL1C1Tu6FFGN2zwCoE
YW00DVvQqH+FZaZyoxOGuul0ugD/6GusrzvsZict8+Tuqm48UAQKOXDxyZo8x2gnVC4OYmck5AG2
gY8qo3ZmocH15ceG5KOJq/g19b4z7011eE0tPS/wJkWyDmyLs5oYKIuwLyslyapf9S2WmkXAD0xK
bZD2usWF+KvJTD4E0upDy4AcESYrIQuLjTGmZZOT+gvBArBwi9eSNH88EOSDFsCCV63xKEDwzQHZ
XHZbp8QB1gy5hSocTz8U3ebVBpeF5/JQSQT8MP1yFEL46NEjYW+yrxBrcR/Vf4rmJF09JRDfIMiq
hr7xoUtPx5oZfExPyjGCYk7diBkQ40hgAzhY0tyFfUmGMCIrvYarXvY1x085BDHO9aX0XmeOfILT
xjntpH3lyeIF8Is8VY1QQ/yn12QIeLu5KWPOHaL7byybyFze6N1q5jzZdzY8X6Y6X6j6lchVLk6Z
vT2aim4qpQDhD/3Y5tfeZuFBd1M5C+1Eq9Zlw8Wn40baWLI7VmvcZyUf7o1siHCnPzqBt9chAE4S
np8HoiaVNgDoRnkPEULT87IQBz2jV6DkSjZjGM4V9TO8UH/QXYlA6aqvL1dFugu9e77PXooT8WyO
QyrCC63EtvMo3Mpyq5VV6CaC5m/LS01JWakgmVChI56xQL1wQMeFT+51BBQ+Ti8Hs/f3GU/Df1ml
yD5HTHiNQ3xJAtgOvw4LveqPSXQxNkWn23Z8P7U7R8Is5uoJqMZQauze7q+EGRlsMdGmZYDGcwNr
4D5dcPrO50ASa3+ZR8SOIgyaKjgYRu+rxoAYOLgXM687TjfkISREdF2mRJf2j5ys0b3m8T64Fs+5
SF4phDfljw69sUnXzLYPPOVv5c7RCKFnSHSTIBfRXE3y43L++93AGuDuFaN5VrTTr7bDnw+og+By
isTP0W9xUgLJ5PDtrYYs9TFo/vO6DlNMH2lYgPCaeJ0C1a8iGPFWBvAi3ivTuLUPmI+7guEckwFw
Y4R05JpYBjDcitUWP0J6f5ruFpYE8WZNLSRwYDEmDr2XvUo8R85W+7aZz5wGcFwhKTN2M37rSGrf
WtMP4voM9EWyXXwAYNRd2CNmgtE/PT5Cia6RfsR8JuEcHELtiQUz2s2IsVuzHkRQ3mbRZLVB5lMk
di4sWCxsB5KmX6hsraHa5SVGOpbp19uA9+uSoSpK9fw7jSnsDdkAWSAB0kNxbJFQJUOUhXzGVEYk
tc4c8IMS+kfjBGENQI2hHimDi8CmOqHFj1+Q4W3cZJ16ZBhDY1K0SUg658/7j5bA+YqqFgI397+c
ARmWjK6XpEJL/1WaU4uvZN+o/ak5rQeR70+pJUg2VzoRShTyvwBYmk+DeRGW5AoI/jkF/v3+3dzG
u7TpeJbh8UYIxab1sPYmIH54IONJB6Nh8wIlrYOFOO6hFMiCemULVurlHbfb6Kb3MRRliIoEmT9C
cd10mnWhRUzkh3bnvMMIJMH2lBBvqzpVXz9Sgb66P8N98I63W98IjahMUtshZUGBUG68b8DHNrI+
ThTAvfBknDC3ITqjnJxILpwQmNqYCdyag0g2/XEyk9ASEcajN7QPit2tB8u/yt5Gr9qvw6R/8WKl
qgs2uFlLfnLF90X/YPSpBpmi7IW0CKHRBvbtA5BzLEAiDJGHIFK4KLqxTq3qpiReUaXjNowwoGwU
u0QSFDvAY9hr/mT9jXPzGYO29ChNogiB0lR1qo+kXUk8+FxdXYTBSH+9EwDPUNQZ4aFmHW/ecVqg
z4N/CsFdPr1JgGHvd0sTvYQFf0TP/ubMZdZ6vSJ1dtjjs0+aswGRuVVhrnJZ86F/qTw/ocAgUOEM
fuyD+E+bnUb223jf9+uslBtBkLYS1vMjRANcLW1st4huS2soEpEJQ4DnG4quzCm165ht6ulbLeLV
Gl4XrRqtbpEm2NZPN3Q4a3nykj/NcF+CgPSDGLV3B8+RnH9Py1dS3GTuJw3n9xedR4vpKo0GY70f
dIwkvH0AEFdaVHg9TfuWgY56dHSyPsaSmBx33Hn+c+wAs9w47L6Xw0WwCbN+bscwCttw1ukrQPRB
ANsad9tjz1PPymVAotJ7XzHvOfBHJeeWcoQrWiLXivKqkRwijVPU1chqasLy4Fy00pepvrRTYgAk
NkLVUuB1cgbg2bmCrtpn8/k7RqyOSv6Vyh1oxe2EdZ1+a3JOY2Hw+vzP95XWnaD0bMTaL0ebWXbY
tMLM9ZOIED47F+WzDIeH9aBpKbASA51dl7v1BgB/l0lFfMNINeYzsEi+joyYGA1tC+SHtrdQOnuj
sEKLvDhO7ORN9g51BRKtAJNqq6sm9GjqC4FgT9Cp5/HQhDtquSIJmVMYD0zTGoijcDQ5WlN4kT0X
GgR4DzZCvAN8HfmpTg3WtG4X51jI+kgQVbRDQ9CakcaJ/Wx63QCNVWwOjPuJx15dSsMpAJ2s3YcJ
1UGK0jeSUlnE3IDizD1ErjDV8jsCNjZDymHhjs2TaZst3zUQeblizaR5wnUAcXgOFehquQl8I9Be
LtKRxJhIR/wxqr6xeumz3jkrFrHjLfyjiGHbwSNtQSy/CgPKmuMlHiq8B+wTLEKKD4is+j5cH8pe
T4lYfARAO49c577i/hUJ+XanCO9lE6gGOPgln8t8G6iwv8MUM93R33XD/D2T9/QgjOkBFpfipgE/
vu31MkW0GGw5+HT07esC886DuGVHvH3hjy9/KrlkfWzm5ZpWGgzxbY8dpvYJzku+hi6CkBYtNd5b
C4g1mf5AF3uQhrjGi4MQaYQn8uBtQl0/3RtOvFNT7tcaZT1C7BhdW3jYZ6oPV6QHd+IF857ISd+c
embtW6q5KGGxNp3m8wQFQjb9taj97UwxJ2Vymg3Ajru4uSXMOucN+UzOkZWxSmNJjSKKWd/ZbS6a
iv0uVdFNUizTI35vgbZr8x8ihdC/j3s7rXE4WsAHsBAWCs+eWjiY5GCPXcTIZlj9NsYGnPQgPDiL
HndT1cYWNNQlvdxoTleHPYLCs5/v7zZqfy9z4VO76VV/4wX9EnMYGCAdm6iJZKe0RxqoXsZABFr2
AvkHUDIu9vF1K+UQWqZVT2aqZyLw+e4pXDS7HwAQZLr6cVqpqOck8RHxz1x/nO8o8XOn6xCFy2NW
vXKKCL8bDt/m5o8UtwlkDy/tFaTZ0OfksbLnt4y/FFHX66IK6vCL+HWLdnLysoRgOEFz89XllnVo
28jalCr8c6SIKRZgj52l6Qt1NJ3LGIOACFiiVfHPJWmoLRvFK+KTlO7wSFD7+Eupv8ooZCfBbm8W
8ZMelzceONwImWIYWtEtcrjZhODtvHD4yrwUia1p43bORTGqPz+JP5MmLVvY2JXtb8YYmrfvICHy
asmS/dAa4up/4w19VlV8SlsmFHtrcGiRxasq39XaNqoBKzsQd8f50uc7ZPR1Hk5C0ph2loTE3r/K
coJWrZt5epGnDnv38NvNGv2s7IXhUk+aakXK0/Gtipb5YoHHBRJ5efLHaB10JTummZ2I0Ul8gcBl
FHvpRfwCwK1ARRwlND7Glkx5uB/0PPrWbIkNwan90uao6uc0ctkUn7U/S2Bh3vcZj9tth9NUPSkx
pEp/S+Mv4QdEBf8IOqPCKIfn5URqLXvR3KmLQ6r+DljJ2nuj9nV0zsb0o4XeuSO6nBrsmLq5hvYv
IVdek8DkM8xdcZvjEb6LIRZBKu2nSMkLpIiAFutogTsSjxZ8I+3eqPut/pOUVZrIsc6LdNUeJv1k
x3IjrCDlt1iARMipI0KRUrCnh85ymkVisnZVYrQmwx8J2eIKqi5ZFCl9eLjqDglmwrtrK+1JKdFm
tK6Bng/aR2tAB4/prcJy5+MfGT4pilgOCiu5MlKSd6NbtsarHTbj/BK9KdHd4E8Jcct30rKWs+27
DfmSWozAJdOKH5RRByxIFhJL5tBpzg9Ym5oLAtnQuMuLgLEJsPQDP8mG6QTJtC0T9zW5jvhgh2iV
ZWG38QPHe5C6srRKN5MXJG/46+1EjhirXzs6grh3F5JGuDs4adz0EmrioZzV7o+LSOMLIKkm3tMX
9rKfH9vVyE/OSD4FwTd3a8PKjNLfqpMZm9rjGXx4rtDL3VzcJv4PJQuJFisT5tBK9/LxOBDMPKHX
+It5dDdSDYQPvWol5/GT/bm82uCtAnfw/R/to+g2wqqxsm/wZ1ssgjxEruBMLklG/ont4WCY+g/9
GtM9bhRuF8ezoQDHQt7Pg2V1617Wz92b/Z8NCR9vlBw3UgpGkFjsP5r69X2or/9MbD7FJyAbNeiO
KPvYc4li3IIAqN4tr6PSHHQu28UAFcwZAbe4QVcTPoG6qLpnP4UAzIPaTH5zRxxbyf+xAZLKdk4u
aF4X/PioCttUBg9FkZw5A1DqNiVtCT6zlaYTqCobTNaVcFHqLkTBUfJj7Io/amKw4CkQ0Q3Zbpkn
pprPWDTuhjubuWIBnMbVJvFJDfYsQ2bAyLquFbXGEla5FoLKrcuTLOFKLIqWr/GLfxUN7y99yj5k
fHxAlpgOcCiKJGpUARehnSbqoQhstxhLQ3DnJaMUvYZSBNdE/iuov9BMUwPj2ZPfoS6XfMD8T0CE
58opwOWQ8FBupe6SP/adXPfeJNn9woA9YTCfmsnYMSea0Szv8Ac+UieGPBMW/bNDRq9wuOLsmdfL
ijTM9k3nXnADsgY5LUeITiGRknzIrZkYw7914sx/AStYyFIHonPpx59iVpfDfnjYI3CgZ2JfJVzM
DGCI1gAitwJAEnfptCl9bjLJQi3zx/UeUqsm7NanL1yatYzFMDPocl3a3Q5dMCGkAScfn5uGzW3V
gs0aiGenAzsdPey37FKMtMkiaqdLepooHlUNsRFNuvDoFHVz0EcRxjkcjiOzosVDmomfc2PDe66V
odpGGRQF6QnTYN4W2KuqTHDeODb/PMsN9VjhCDaCjUnv6taGBnPbhNVq5PtGpcqrZqgbX8zHHgnf
Dmxl9pTB1+k6HlpggECD+fYPtNeg/LZVVNH+vtF9R6/vjl55WVgMnohm46pkfLCYEI9yY3QI5O+v
e6Qri2EDONegrF7gDCIfsuvFOxHEYP3xSL2m7zLI/22yEf0fRBbySel0v4jbth/J2eHepWTVMRz+
PGw5PXH8+MToSCI3GjZO2tpqGpc/dkbdr21QupAFLA4c1dz2QgdBIZn4Kk5w8/CBpCaxm57+lUMT
baXGmS6XzDY47Mhc9S7q05D4MG6K/uwnLJJ5HP5+YViUVgzVIHB8YNf7yCJ/hyKoaqd0Oah9M1Qx
eSdpEZeTSWOvlb0BlKp0iEXKSS7+qkHeesnc8Vbj8dAjqcqxUrluOXY1ywOw7Kd27d5aJ9dso3KV
sSE+r0w82SVShAMSDldbtAyRSAoyhqzo+TlFLerIUGOisr2xA5Orb4zj0ikpOlDT4ZDU8qSIkxIx
wI4QxM0pqFuOZbv1Vck1RCc7JoAJW26RnsQO3yS99KTxTTisVudbgZJEsjcrDTe+IYIJ5lySo+MP
v2lbQgfrD6oYbqAbThTxx38dOVk84ZQvRcXFsj0058yQQ9Lz84eNuzfQWX2FX9rQGdPaqxRjE9TT
VXaqUcbHCkDkJS0A+LHmGF6M63hbOibbQLmTrI77hjZc25+vHt5OlQgph/sJ4BIcYEVHt0SyEIMO
9PYBwTYQt5j5dnX/KO6Qco3z4ZzhELykVVUy+a3bkOj0+jRZt+FqllTYb1+JZVVE78tO08IxeK2v
RRMF2mBUFMCT5vUVZzjZVNuMcvwyEg52a1xf5/1b7fyC5ZjD5UhaQvsi+cabSUCPNTmdirpcFT1D
TwOlPxgLPjYXnox2r/3ZiLRHnGXF0PdtWwRIK4mO1vdSe08Tu1eoT2UzgF8YyC6bmKMgNGN9+I/S
JA/n3o2wxnhknY/CQlNzOprHGnfjQTPNAROh3ZG1zF3I3yjG+5Ul2ZHrwYwgksVyJIkPLVsC4+DV
N5i1j4rpq7SrtYVtIVcF1qvJ5UpTifHVKsP8JHPHw2M5OtE++OX4yrtLbO9xdJ+TtVvL4tDrZf3t
VejZAEGTH2lNF8mh54g/k1wM+TsxRIoM5jIYqvv5VF7VT0uezJfXJova3iWJIM0CLOu0NX5YNnoD
YgS5ayG83qMb9v/PUajuk79r6+3B2l2ICO4H/ajQfztEwChlk34l1i03m9RrIt+Aui17MeG9NR1e
fjbfAMWYQXsvIyD9U0+Zm9bGHbt3wzvahmA2ScqQ8OEEzCUtT/d7oDxHmmpGq9yAq2ovR7/4yJQ5
cxb2ZieEKDUIJ2jCL9hIw4rQvBUrKSZnVQx24PYWvjr7NRS92xzW2fEXHMr8yFFMCNHVzU7derIZ
O/bBl+0Mk0fIAHxnJ3beNc8QdGHNgnSjNufPPd60crV3TV+QaIg2WWe441pEtfGCaC8+7fkdwuZK
grLTyvUBv7Dvl5eIsU/I4A8YnW8gUg227fZhkJOZ3ttyAkmwLUySHxHLaRRO+WJA7lJbnrVAL6N3
IAd1mpkwMP8bhx4ievB8LubylOTcNBznbgiAGQlWLWk2sYIiyJcT1rQ737z46PEDtKDmaC/BRA0T
11Mb6a1EsYlzAYijZntfp7baSLp+ZnlEDcWkfOyx5DPFgwxrO2/VEbjplAl2zH4QcwRzYXpe8Cfx
o0LEeY10p0mSTHTAidPJCac9W2G4VYOc9Wxu2Pfthp+d8FPKRhze9Wum2coVPe6uRyvmG4E8x9nf
c/hqCi2AfyptgBfVxajDFh5j9h+mL1ZBgHt3T8mnry5H1Vgcm16ID2G2T+eTI/CksWFprjmn7lr4
b/X8hh7DfeDKMCP6oCClwMGslILbG22b7OjNERn76tObCQYWazizqxKdaoAfAEbwOzPKV8R7ZAPt
x34EA0SoY2PwiIbzxD9G/COMltEEGq6AfrSn2RtpH3AJzx6icEK80LBx2afub48XACm9b1UQCFk1
mS+AZ2iBmcf/fWeFxnmcLxdES9CJodtaUf1amA0ujZri8vfsqiPjyjkmUHySvKcUg2N2Vs0/WGg/
hbhFfjmuydNpuHERHyEKNm0Zd/FxmB4c6jjrLa8KpTTReLtEGUFakILTM6Klvq7i3nW6ISh2oE+R
fkfXhFSZ3uzM0u9M1ZBo6+Ufj06y2q2wa5MsxzWaGM/IF/mjGbYsXhW5hvid4MxV/hwLlZSjKULw
K3HomdQULLG5853Vj8L9NhFbWmwEfGviM82PytGOp0tmUt25R9+CdjouStZv8kytUIf6Q5HOqNM0
IUHNlFbJHHFnAhMqMEZs2siV4M1DARHb7Xk471/i2aoGbbsiewthJIIiFoqZtzvLrT7hRfTPOf3f
E2kdqzRKauNIffK7lBhoHLJKdHbnwB7zH/+szXIY3fZtaQs0fyVkqVlaZQtRUItOZmchKdKt18co
7Z71UvtK3t3A0S+XosR2epsK75ngGJWNmJDEAttS1dS2qATTJC6OIQfAHWpKypskpfMcoWePmItm
n00Nlgi0jb9Kr/vaXGPHvqkcC7oee1Sm8TJtBMCPbC8E3kVUj/+eZANUSjcZ2u8TZ9yAgoY/cBDm
Ka7ktS6mPmxhZMIhKhP/8B5hkjOQOTzlyzYHAbmR6OpEgmAn9x6E03NTt0YWQCHjyvY8bYsXDRTF
Kq47oWY/w80Pakmbnk3Hg1ue7wNEe+MLv8K7NKuL96NnkEZPmDqThH3A957NMyExApnXTEd7mgHi
2DiBTtFdjE1Y6Pjqm7wtQxYCqtnRaOzNfE/T//huQw6zNyiIhf5zp3YHkg0HecaQoRYw4rUEBOrK
qLibbl8y109wJnyx6gu3qf3pFTqdRNh5u7StrwoF8PDNzYpl1S+a1nRvRAKiLTKXkPau+QdHt4AI
b5SLrEQXB1YaUv4u5HEeZzAaV2H9yUlbjgQTpbn/xvPqWTmjJ6tLlRObFVSY3IzvvCE19Iy3cg7d
xIKZrRNb8WfA6/UwAcBZTljHxkgxlRYewHBWTtVQ2x9JR15V20kRTKSJ3xI3fZ02+3Z36KL0DWpF
mqNlQHp6Z9Zd0XldzvysElV5KhnXYwTmMMSWjrvIJu12sJeI0H077f2p7VC2vI+wKaYFQeuxTsfg
pC4KDzW3LyG5qlahzH9TWIWjQ2z1oBxYSYi9omejEojNaGIgBqiKMDpiRXZxkbUlV8bLJb2vfVvE
6PQCTgW8JUcyJNQNvpFUiH8T/nYxVB9jro2jy0xgomnaO2eTZI3EJ9e8k12xgxDB8fd1hYGIj30f
95XPy/Q9pULq3F0zkB6TS6r9rmZPXDcL3parumrO/c7CoXUvnxTK44HOHJihclUKEY0lNjmXr+No
axw229+XgJ40ak6jkk9OIVw7mvKyKVn4T7syf//+oza3muQ8F2kVOXJrGdXIJOp/rP0zUHmLUUcR
y9/6LhefvvL6kvQd4a3YM4fh2sWWsJyamgeqvtVc/fWYcmXLHVv6tMHnhfrqlHu71KpMPRR+8vtj
E1iwRU56jFvEkhGpBUP4cQ8Gw9OK9yAj7rEPbGdFju54ZTWwktH+KXAORKpyHKqwOMnYwGn1ySUX
nMqGxnNntwM5FA8PeoFiGMMR6I01nrnQnzLbamAdGkxjDXC0g6xAxQyL0Ohm+Jk9WFZBJFLJvlSe
j6w+ZvJP2l+0bEjT9eXWBG7/qnHIA9UVeCdyqMGI54r1mGBV9Y37aybqFTpY1DEu/I38Qf8c7nMO
KKV83ZUhyAeeMtbW1pHylHGtL6S/hAn2UXhlzIC/PbyDQ5MGNQAT2QX5x8keoVsUIrgT2tDzkvmg
ev6lSl3MUveI2WDjJqa8uOVWsDSntmS8+Ryw7C6kqzSPXvUjZxWpP/qsKTuULcklWPJ+z2C7F2R+
oG77ksMAubT4ILk0eXg+9EUqYxUGfslfGnhKwsyMn2RjMJIJ2m2qbboPTlZwzb0mnClX7mde1VKo
B+yh7LmZFXhrgavFYECnB6S1Qjdon3a1LmJin+EhQMhMzEjWJcXDNLvx/PVf7EenT47EAI3UsfZH
+gMFSiV/48DyXR+yOyILsWw6/vqjRteYld1uOgNNx43bChfZj2s5YnltOIbN9ToF8uCFGhZ2xUaX
W83FW8zRrtct1uAL20DToGl7cxbqSgEzMmZHQa6Dqwws5YTexNJ4l/0ybZ9vlaHGOpzEhlxkhs53
Ivt8kwR6uNz2fL7q4AC/shCmXyEAoVejr6zSfUfhpLTaN9SAkgsFNBns+ZQsdchA00Qjh7Vlp8lZ
UKgLvn2OwddMfp9Vhpmxv/B4n+lgcdijo0ycwe38HKdDMoj3I2winR6jzM959a3xMMpPmX2UmqO0
49+zuM46CSGU0jekvpfpGVHgqy0pwy1jcDBQ1RV2dzxN3lkn90J5RDmugteyUuE36kiL3aEdVhEl
bYWu6Dwu2NTHHS8ad6t433PaN5mNXtIeIVTvOfMnvub50bR4+QoFcrWMaZy6RPvkV7uHLx03YEoj
KoAgZZebf5mYcsTdI3UfqOesNvtoCqoslcCNvNf3GOjLibqChycQ3YOEZgeFV/EL3OaT
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity tempSensorMult_mult_gen_v12_0_12 is
  port (
    CLK : in STD_LOGIC;
    A : in STD_LOGIC_VECTOR ( 15 downto 0 );
    B : in STD_LOGIC_VECTOR ( 8 downto 0 );
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    ZERO_DETECT : out STD_LOGIC_VECTOR ( 1 downto 0 );
    P : out STD_LOGIC_VECTOR ( 31 downto 0 );
    PCASC : out STD_LOGIC_VECTOR ( 47 downto 0 )
  );
  attribute C_A_TYPE : integer;
  attribute C_A_TYPE of tempSensorMult_mult_gen_v12_0_12 : entity is 1;
  attribute C_A_WIDTH : integer;
  attribute C_A_WIDTH of tempSensorMult_mult_gen_v12_0_12 : entity is 16;
  attribute C_B_TYPE : integer;
  attribute C_B_TYPE of tempSensorMult_mult_gen_v12_0_12 : entity is 1;
  attribute C_B_VALUE : string;
  attribute C_B_VALUE of tempSensorMult_mult_gen_v12_0_12 : entity is "101001010";
  attribute C_B_WIDTH : integer;
  attribute C_B_WIDTH of tempSensorMult_mult_gen_v12_0_12 : entity is 9;
  attribute C_CCM_IMP : integer;
  attribute C_CCM_IMP of tempSensorMult_mult_gen_v12_0_12 : entity is 2;
  attribute C_CE_OVERRIDES_SCLR : integer;
  attribute C_CE_OVERRIDES_SCLR of tempSensorMult_mult_gen_v12_0_12 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of tempSensorMult_mult_gen_v12_0_12 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of tempSensorMult_mult_gen_v12_0_12 : entity is 0;
  attribute C_HAS_ZERO_DETECT : integer;
  attribute C_HAS_ZERO_DETECT of tempSensorMult_mult_gen_v12_0_12 : entity is 0;
  attribute C_LATENCY : integer;
  attribute C_LATENCY of tempSensorMult_mult_gen_v12_0_12 : entity is 2;
  attribute C_MODEL_TYPE : integer;
  attribute C_MODEL_TYPE of tempSensorMult_mult_gen_v12_0_12 : entity is 0;
  attribute C_MULT_TYPE : integer;
  attribute C_MULT_TYPE of tempSensorMult_mult_gen_v12_0_12 : entity is 2;
  attribute C_OPTIMIZE_GOAL : integer;
  attribute C_OPTIMIZE_GOAL of tempSensorMult_mult_gen_v12_0_12 : entity is 1;
  attribute C_OUT_HIGH : integer;
  attribute C_OUT_HIGH of tempSensorMult_mult_gen_v12_0_12 : entity is 31;
  attribute C_OUT_LOW : integer;
  attribute C_OUT_LOW of tempSensorMult_mult_gen_v12_0_12 : entity is 0;
  attribute C_ROUND_OUTPUT : integer;
  attribute C_ROUND_OUTPUT of tempSensorMult_mult_gen_v12_0_12 : entity is 0;
  attribute C_ROUND_PT : integer;
  attribute C_ROUND_PT of tempSensorMult_mult_gen_v12_0_12 : entity is 0;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of tempSensorMult_mult_gen_v12_0_12 : entity is 0;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of tempSensorMult_mult_gen_v12_0_12 : entity is "artix7";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of tempSensorMult_mult_gen_v12_0_12 : entity is "mult_gen_v12_0_12";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of tempSensorMult_mult_gen_v12_0_12 : entity is "yes";
end tempSensorMult_mult_gen_v12_0_12;

architecture STRUCTURE of tempSensorMult_mult_gen_v12_0_12 is
  signal \<const0>\ : STD_LOGIC;
  signal \^p\ : STD_LOGIC_VECTOR ( 24 downto 1 );
  signal NLW_i_mult_P_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_i_mult_PCASC_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 0 );
  signal NLW_i_mult_ZERO_DETECT_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute C_A_TYPE of i_mult : label is 1;
  attribute C_A_WIDTH of i_mult : label is 16;
  attribute C_B_TYPE of i_mult : label is 1;
  attribute C_B_VALUE of i_mult : label is "101001010";
  attribute C_B_WIDTH of i_mult : label is 9;
  attribute C_CCM_IMP of i_mult : label is 2;
  attribute C_CE_OVERRIDES_SCLR of i_mult : label is 0;
  attribute C_HAS_CE of i_mult : label is 0;
  attribute C_HAS_SCLR of i_mult : label is 0;
  attribute C_HAS_ZERO_DETECT of i_mult : label is 0;
  attribute C_LATENCY of i_mult : label is 2;
  attribute C_MODEL_TYPE of i_mult : label is 0;
  attribute C_MULT_TYPE of i_mult : label is 2;
  attribute C_OPTIMIZE_GOAL of i_mult : label is 1;
  attribute C_OUT_HIGH of i_mult : label is 31;
  attribute C_OUT_LOW of i_mult : label is 0;
  attribute C_ROUND_OUTPUT of i_mult : label is 0;
  attribute C_ROUND_PT of i_mult : label is 0;
  attribute C_VERBOSITY of i_mult : label is 0;
  attribute C_XDEVICEFAMILY of i_mult : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_mult : label is "yes";
begin
  P(31) <= \<const0>\;
  P(30) <= \<const0>\;
  P(29) <= \<const0>\;
  P(28) <= \<const0>\;
  P(27) <= \<const0>\;
  P(26) <= \<const0>\;
  P(25) <= \<const0>\;
  P(24 downto 1) <= \^p\(24 downto 1);
  P(0) <= \<const0>\;
  PCASC(47) <= \<const0>\;
  PCASC(46) <= \<const0>\;
  PCASC(45) <= \<const0>\;
  PCASC(44) <= \<const0>\;
  PCASC(43) <= \<const0>\;
  PCASC(42) <= \<const0>\;
  PCASC(41) <= \<const0>\;
  PCASC(40) <= \<const0>\;
  PCASC(39) <= \<const0>\;
  PCASC(38) <= \<const0>\;
  PCASC(37) <= \<const0>\;
  PCASC(36) <= \<const0>\;
  PCASC(35) <= \<const0>\;
  PCASC(34) <= \<const0>\;
  PCASC(33) <= \<const0>\;
  PCASC(32) <= \<const0>\;
  PCASC(31) <= \<const0>\;
  PCASC(30) <= \<const0>\;
  PCASC(29) <= \<const0>\;
  PCASC(28) <= \<const0>\;
  PCASC(27) <= \<const0>\;
  PCASC(26) <= \<const0>\;
  PCASC(25) <= \<const0>\;
  PCASC(24) <= \<const0>\;
  PCASC(23) <= \<const0>\;
  PCASC(22) <= \<const0>\;
  PCASC(21) <= \<const0>\;
  PCASC(20) <= \<const0>\;
  PCASC(19) <= \<const0>\;
  PCASC(18) <= \<const0>\;
  PCASC(17) <= \<const0>\;
  PCASC(16) <= \<const0>\;
  PCASC(15) <= \<const0>\;
  PCASC(14) <= \<const0>\;
  PCASC(13) <= \<const0>\;
  PCASC(12) <= \<const0>\;
  PCASC(11) <= \<const0>\;
  PCASC(10) <= \<const0>\;
  PCASC(9) <= \<const0>\;
  PCASC(8) <= \<const0>\;
  PCASC(7) <= \<const0>\;
  PCASC(6) <= \<const0>\;
  PCASC(5) <= \<const0>\;
  PCASC(4) <= \<const0>\;
  PCASC(3) <= \<const0>\;
  PCASC(2) <= \<const0>\;
  PCASC(1) <= \<const0>\;
  PCASC(0) <= \<const0>\;
  ZERO_DETECT(1) <= \<const0>\;
  ZERO_DETECT(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
i_mult: entity work.tempSensorMult_mult_gen_v12_0_12_viv
     port map (
      A(15 downto 0) => A(15 downto 0),
      B(8 downto 0) => B"000000000",
      CE => '0',
      CLK => CLK,
      P(31 downto 25) => NLW_i_mult_P_UNCONNECTED(31 downto 25),
      P(24 downto 1) => \^p\(24 downto 1),
      P(0) => NLW_i_mult_P_UNCONNECTED(0),
      PCASC(47 downto 0) => NLW_i_mult_PCASC_UNCONNECTED(47 downto 0),
      SCLR => '0',
      ZERO_DETECT(1 downto 0) => NLW_i_mult_ZERO_DETECT_UNCONNECTED(1 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity tempSensorMult is
  port (
    CLK : in STD_LOGIC;
    A : in STD_LOGIC_VECTOR ( 15 downto 0 );
    P : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of tempSensorMult : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of tempSensorMult : entity is "tempSensorMult,mult_gen_v12_0_12,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of tempSensorMult : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of tempSensorMult : entity is "mult_gen_v12_0_12,Vivado 2016.4";
end tempSensorMult;

architecture STRUCTURE of tempSensorMult is
  signal NLW_U0_PCASC_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 0 );
  signal NLW_U0_ZERO_DETECT_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute C_A_TYPE : integer;
  attribute C_A_TYPE of U0 : label is 1;
  attribute C_A_WIDTH : integer;
  attribute C_A_WIDTH of U0 : label is 16;
  attribute C_B_TYPE : integer;
  attribute C_B_TYPE of U0 : label is 1;
  attribute C_B_VALUE : string;
  attribute C_B_VALUE of U0 : label is "101001010";
  attribute C_B_WIDTH : integer;
  attribute C_B_WIDTH of U0 : label is 9;
  attribute C_CCM_IMP : integer;
  attribute C_CCM_IMP of U0 : label is 2;
  attribute C_CE_OVERRIDES_SCLR : integer;
  attribute C_CE_OVERRIDES_SCLR of U0 : label is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 0;
  attribute C_HAS_ZERO_DETECT : integer;
  attribute C_HAS_ZERO_DETECT of U0 : label is 0;
  attribute C_LATENCY : integer;
  attribute C_LATENCY of U0 : label is 2;
  attribute C_MODEL_TYPE : integer;
  attribute C_MODEL_TYPE of U0 : label is 0;
  attribute C_MULT_TYPE : integer;
  attribute C_MULT_TYPE of U0 : label is 2;
  attribute C_OPTIMIZE_GOAL : integer;
  attribute C_OPTIMIZE_GOAL of U0 : label is 1;
  attribute C_OUT_HIGH : integer;
  attribute C_OUT_HIGH of U0 : label is 31;
  attribute C_OUT_LOW : integer;
  attribute C_OUT_LOW of U0 : label is 0;
  attribute C_ROUND_OUTPUT : integer;
  attribute C_ROUND_OUTPUT of U0 : label is 0;
  attribute C_ROUND_PT : integer;
  attribute C_ROUND_PT of U0 : label is 0;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of U0 : label is 0;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
begin
U0: entity work.tempSensorMult_mult_gen_v12_0_12
     port map (
      A(15 downto 0) => A(15 downto 0),
      B(8 downto 0) => B"000000000",
      CE => '1',
      CLK => CLK,
      P(31 downto 0) => P(31 downto 0),
      PCASC(47 downto 0) => NLW_U0_PCASC_UNCONNECTED(47 downto 0),
      SCLR => '0',
      ZERO_DETECT(1 downto 0) => NLW_U0_ZERO_DETECT_UNCONNECTED(1 downto 0)
    );
end STRUCTURE;
