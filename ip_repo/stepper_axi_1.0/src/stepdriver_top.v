//----------------------------------------------------------------------------
// user_logic.v - module
//----------------------------------------------------------------------------
//
// ***************************************************************************
// ** Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.            **
// **                                                                       **
// ** Xilinx, Inc.                                                          **
// ** XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"         **
// ** AS A COURTESY TO YOU, SOLELY FOR USE IN DEVELOPING PROGRAMS AND       **
// ** SOLUTIONS FOR XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE,        **
// ** OR INFORMATION AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE,        **
// ** APPLICATION OR STANDARD, XILINX IS MAKING NO REPRESENTATION           **
// ** THAT THIS IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,     **
// ** AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE      **
// ** FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY              **
// ** WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE               **
// ** IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR        **
// ** REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF       **
// ** INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS       **
// ** FOR A PARTICULAR PURPOSE.                                             **
// **                                                                       **
// ***************************************************************************
//
//----------------------------------------------------------------------------
// Filename:          user_logic.v
// Version:           1.00.a
// Description:       User logic module.
// Date:              Sun Jul 06 17:52:38 2014 (by Create and Import Peripheral Wizard)
// Verilog Standard:  Verilog-2001
//----------------------------------------------------------------------------
// Naming Conventions:
//   active low signals:                    "*_n"
//   clock signals:                         "clk", "clk_div#", "clk_#x"
//   reset signals:                         "rst", "rst_n"
//   generics:                              "C_*"
//   user defined types:                    "*_TYPE"
//   state machine next state:              "*_ns"
//   state machine current state:           "*_cs"
//   combinatorial signals:                 "*_com"
//   pipelined or register delay signals:   "*_d#"
//   counter signals:                       "*cnt*"
//   clock enable signals:                  "*_ce"
//   internal version of output port:       "*_i"
//   device pins:                           "*_pin"
//   ports:                                 "- Names begin with Uppercase"
//   processes:                             "*_PROCESS"
//   component instantiations:              "<ENTITY_>I_<#|FUNC>"
//----------------------------------------------------------------------------

module stepdriver_top
(
  // -- ADD USER PORTS BELOW THIS LINE ---------------
  STEPPER_LOCAL,
  STEPPER_LOCAL_UP,
  STEPPER_LOCAL_DN,
  STEPPER_RST_N,
  STEPPER_SW_N,
  STEPPER_CLK,
  STEPPER_CS_N,
  STEPPER_MISO,
  STEPPER_MOSI,
  STEPPER_LIM_UP_N,
  STEPPER_LIM_DN_N,
  tunerDn,
  tunerUp,
  goHome,
  autoSetHome,
  tunerMoving,
  tunerFault,
  S_AXI_ACLK,
  S_AXI_ARESETN,
  S_AXI_AWADDR,
  S_AXI_WDATA,
  S_AXI_RDATA,
  S_AXI_AWVALID,
  S_AXI_AWREADY,
  S_AXI_WSTRB,
  S_AXI_WVALID,
  S_AXI_WREADY,
  S_AXI_BREADY,
  S_AXI_ARADDR,
  S_AXI_ARVALID,
  S_AXI_ARREADY,
  S_AXI_RVALID,
  S_AXI_RREADY
  // -- ADD USER PORTS ABOVE THIS LINE ---------------

  // -- DO NOT EDIT BELOW THIS LINE ------------------
  // -- Bus protocol ports, do not add to or delete 
/*
  S_AXI_ACLK,                     // Bus to IP clock
  Bus2IP_Resetn,                  // Bus to IP reset
  S_AXI_AWADDR,                    // Bus to IP address bus
  S_AXI_AWVALID,                      // Bus to IP chip select for user logic memory selection
  Bus2IP_RNW,                     // Bus to IP read/not write
  S_AXI_WDATA,                    // Bus to IP data bus
  S_AXI_WSTRB,                      // Bus to IP byte enables
  Bus2IP_RdCE,                    // Bus to IP read chip enable
  Bus2IP_WrCE,                    // Bus to IP write chip enable
  Bus2IP_Burst,                   // Bus to IP burst-mode qualifier
  Bus2IP_BurstLength,             // Bus to IP burst length
  Bus2IP_RdReq,                   // Bus to IP read request
  Bus2IP_WrReq,                   // Bus to IP write request
  Type_of_xfer,                   // Transfer Type
  S_AXI_AWREADY,                 // IP to Bus address acknowledgement
  S_AXI_RDATA,                    // IP to Bus data bus
  S_AXI_RREADY,                   // IP to Bus read transfer acknowledgement
  S_AXI_WREADY,                   // IP to Bus write transfer acknowledgement
  IP2Bus_Error                    // IP to Bus error response
  // -- DO NOT EDIT ABOVE THIS LINE ------------------
*/
); // user_logic

// -- ADD USER PARAMETERS BELOW THIS LINE ------------
// --USER parameters added here 
// -- ADD USER PARAMETERS ABOVE THIS LINE ------------

// -- DO NOT EDIT BELOW THIS LINE --------------------
// -- Bus protocol parameters, do not add to or delete
		// Width of S_AXI data bus
parameter integer C_S_AXI_DATA_WIDTH    = 32;
// Width of S_AXI address bus
parameter integer C_S_AXI_ADDR_WIDTH    = 6;
// -- DO NOT EDIT ABOVE THIS LINE --------------------

// -- ADD USER PORTS BELOW THIS LINE -----------------
input                                     STEPPER_LOCAL;
input                                     STEPPER_LOCAL_UP;
input                                     STEPPER_LOCAL_DN;
output                                    STEPPER_RST_N;
output                                    STEPPER_SW_N;
output                                    STEPPER_CLK;
output                                    STEPPER_CS_N;
input                                     STEPPER_MISO;
output                                    STEPPER_MOSI;
input                                     STEPPER_LIM_UP_N;
input                                     STEPPER_LIM_DN_N;
input                                     tunerDn;
input                                     tunerUp;
input                                     goHome;
input                                     autoSetHome;
output                                    tunerMoving;
output reg                                tunerFault = 0;

// -- ADD USER PORTS ABOVE THIS LINE -----------------
input                                     S_AXI_ACLK;
input                                     S_AXI_ARESETN;
input      [C_S_AXI_ADDR_WIDTH-1 : 0]     S_AXI_AWADDR;
input      [C_S_AXI_DATA_WIDTH-1 : 0]     S_AXI_WDATA;
output  [C_S_AXI_DATA_WIDTH-1 : 0]     S_AXI_RDATA;
		output reg  S_AXI_RVALID;
// Read ready. This signal indicates that the master can
    // accept the read data and response information.
input   S_AXI_RREADY;
    		// is signaling valid read address and control information.
input   S_AXI_ARVALID;
// Read address ready. This signal indicates that the slave is
// ready to accept an address and associated control signals.
output reg  S_AXI_ARREADY;
//		output reg  S_AXI_BVALID;
// Response ready. This signal indicates that the master
    // can accept a write response.
input   S_AXI_BREADY;
// Read address (issued by master, acceped by Slave)
input  [C_S_AXI_ADDR_WIDTH-1 : 0] S_AXI_ARADDR;
		input  [(C_S_AXI_DATA_WIDTH/8)-1 : 0] S_AXI_WSTRB;
// Write valid. This signal indicates that valid write
    // data and strobes are available.
input   S_AXI_WVALID;
// Write ready. This signal indicates that the slave
    // can accept the write data.
output   S_AXI_WREADY;
    		// valid write address and control information.
input   S_AXI_AWVALID;
// Write address ready. This signal indicates that the slave is ready
// to accept an address and associated control signals.
output   S_AXI_AWREADY;

// -- DO NOT EDIT BELOW THIS LINE --------------------
// -- Bus protocol ports, do not add to or delete
/*
input                                     S_AXI_ACLK;
input                                     Bus2IP_Resetn;
input      [C_SLV_AWIDTH-1 : 0]           S_AXI_AWADDR;
input      [C_NUM_MEM-1 : 0]              S_AXI_AWVALID;
input                                     Bus2IP_RNW;
input      [C_SLV_DWIDTH-1 : 0]           S_AXI_WDATA;
input      [C_SLV_DWIDTH/8-1 : 0]         S_AXI_WSTRB;
input      [C_NUM_MEM-1 : 0]              Bus2IP_RdCE;
input      [C_NUM_MEM-1 : 0]              Bus2IP_WrCE;
input                                     Bus2IP_Burst;
input      [7 : 0]                        Bus2IP_BurstLength;
input                                     Bus2IP_RdReq;
input                                     Bus2IP_WrReq;
input                                     Type_of_xfer;
output                                    S_AXI_AWREADY;
output     [C_SLV_DWIDTH-1 : 0]           S_AXI_RDATA;
output                                    S_AXI_RREADY;
output                                    S_AXI_WREADY;
output                                    IP2Bus_Error;
*/
// -- DO NOT EDIT ABOVE THIS LINE --------------------

//----------------------------------------------------------------------------
// Implementation
//----------------------------------------------------------------------------

	assign STEPPER_RST_N = 1;
	assign STEPPER_SW_N = 1;

	// Output signals to AXI bus
	reg [31:0] data;
	wire [31:0] read;
	assign S_AXI_RDATA = S_AXI_RREADY ? data : 32'h0;
//	assign S_AXI_AWREADY = S_AXI_AWVALID;
//	assign S_AXI_WREADY = S_AXI_AWVALID & S_AXI_WVALID;
//	assign S_AXI_RREADY = S_AXI_AWVALID & S_AXI_RVALID;
//	assign IP2Bus_Error = 0;

	reg [31:0] command = 0;
	reg sendCommand = 0;
	reg cfgDone = 0;
	wire error1, error2, busy;
	reg setHome = 0;
	wire [31:0] motorPos;
	
	wire [19:0] speed;
	reg [31:0] speedUnsat = 32'h00001000;
	assign speed = |speedUnsat[31:20] ? 20'hFFFFF : speedUnsat[19:0];
	reg [7:0] limCount = 0;
	reg [6:0] sweepCount = 1;

	reg stepperLocalReg = 0, stepperLocalUpReg = 0, stepperLocalDnReg = 0, stepperLimUpReg = 0, stepperLimDnReg = 0;
	reg [13:0] manualDebounce = 0, upDebounce = 0, dnDebounce = 0, upLimDebounce = 0, dnLimDebounce = 0;
	reg manual = 0, up = 0, dn = 0, upLim = 1, dnLim = 1;
	reg [1:0] tunerDnReg = 0, tunerUpReg = 0;
	reg [2:0] goHomeReg = 0;
	reg goHomePulse = 0;

	always @*
	begin
		case (S_AXI_AWADDR[5:2])
			4'h0:	data <= read;
			4'h1: data <= {31'h0, busy};
			4'h2: data <= {31'h0, cfgDone};
			4'h3: data <= {26'h0, tunerMoving, tunerFault, dnLim, upLim, error2, error1};
			4'h4: data <= {12'h0, speed};
			4'h5: data <= 0;							// write-only register
			4'h6: data <= -motorPos;				// Motor position from stepper controller has the wrong sign
			4'h7: data <= limCount[7:1] + limCount[0];		// number of times to run to limit switch before stopping
			4'h8: data <= {25'h0, sweepCount};
			4'h9: data <= 0;							// write-only register
			default: data <= 32'hFFFFFFFF;
		endcase
	end

	always @ (posedge S_AXI_ACLK)
	begin
		setHome <= S_AXI_WREADY & (S_AXI_AWADDR[5:2] == 4'h5) & S_AXI_WDATA[0];
		if (~S_AXI_WREADY & ~|speedUnsat)	speedUnsat <= 20'h00001;
		if (S_AXI_WREADY & S_AXI_WSTRB[0])
			case (S_AXI_AWADDR[5:2])
				4'h0: command[7:0] <= S_AXI_WDATA[7:0];
				4'h2: cfgDone <= S_AXI_WDATA[0];
				4'h4: speedUnsat[7:0] <= S_AXI_WDATA[7:0];
				4'h8: sweepCount <= S_AXI_WDATA[6:0];
				4'h9: if (S_AXI_WDATA[0])		limCount <= {sweepCount, 1'b0};
			endcase
		if (S_AXI_WREADY & S_AXI_WSTRB[1])
			case (S_AXI_AWADDR[5:2])
				4'h0: command[15:8] <= S_AXI_WDATA[15:8];
				4'h4: speedUnsat[15:8] <= S_AXI_WDATA[15:8];
			endcase
		if (S_AXI_WREADY & S_AXI_WSTRB[2])
			case (S_AXI_AWADDR[5:2])
				4'h0: command[23:16] <= S_AXI_WDATA[23:16];
				4'h4: speedUnsat[23:16] <= S_AXI_WDATA[23:16];
			endcase
		if (S_AXI_WREADY & S_AXI_WSTRB[3])
			case (S_AXI_AWADDR[5:2])
				4'h0: command[31:24] <= S_AXI_WDATA[31:24];
				4'h4: speedUnsat[31:24] <= S_AXI_WDATA[31:24];
			endcase
		sendCommand <= S_AXI_AWVALID & S_AXI_WVALID & ~|S_AXI_AWADDR[4:2];

		stepperLocalReg <= STEPPER_LOCAL;
		stepperLocalUpReg <= STEPPER_LOCAL_UP;
		stepperLocalDnReg <= STEPPER_LOCAL_DN;
		stepperLimUpReg <= STEPPER_LIM_UP_N;
		stepperLimDnReg <= STEPPER_LIM_DN_N;
		if (stepperLocalReg & ~&manualDebounce)		manualDebounce <= manualDebounce + 1;
		if (~stepperLocalReg & |manualDebounce)		manualDebounce <= manualDebounce - 1;
		if (&manualDebounce)									manual <= 1;
		if (~|manualDebounce)								manual <= 0;
		if (stepperLocalUpReg & ~&upDebounce)			upDebounce <= upDebounce + 1;
		if (~stepperLocalUpReg & |upDebounce)			upDebounce <= upDebounce - 1;
		if (&upDebounce)										tunerUpReg <= 1;
		if (~|upDebounce)										tunerUpReg <= 0;
		if (stepperLocalDnReg & ~&dnDebounce)			dnDebounce <= dnDebounce + 1;
		if (~stepperLocalDnReg & |dnDebounce)			dnDebounce <= dnDebounce - 1;
		if (&dnDebounce)										tunerDnReg <= 1;
		if (~|dnDebounce)										tunerDnReg <= 0;
		if (stepperLimUpReg & ~&upLimDebounce)			upLimDebounce <= upLimDebounce + 1;
		if (~stepperLimUpReg & |upLimDebounce)			upLimDebounce <= upLimDebounce - 1;
		if (&upLimDebounce)									upLim <= 1;
		if (~|upLimDebounce)									upLim <= 0;
		if (stepperLimDnReg & ~&dnLimDebounce)			dnLimDebounce <= dnLimDebounce + 1;
		if (~stepperLimDnReg & |dnLimDebounce)			dnLimDebounce <= dnLimDebounce - 1;
		if (&dnLimDebounce)									dnLim <= 1;
		if (~|dnLimDebounce)									dnLim <= 0;
		up <= ~upLim & (limCount[0] | (tunerUpReg & manual) | (~manual & tunerUp));		// limit switches are '1' when connected and not at limit
		dn <= ~dnLim & ((~limCount[0] & |limCount[7:1]) | (tunerDnReg & manual) | (~manual & tunerDn));		// limit switches are '1' when connected and not at limit
		goHomeReg <= {goHomeReg[1:0], goHome};
		goHomePulse <= goHomeReg[1] & ~goHomeReg[2];
		if (~S_AXI_WREADY & ((~manual & (tunerDn | tunerUp)) | (manual & (&upDebounce | &dnDebounce)) | goHome | autoSetHome))
			limCount <= 0;
		if (~S_AXI_WREADY & |limCount & ((limCount[0] & upLim) | (~limCount[0] & dnLim)))
			limCount <= limCount - 1;
		tunerFault <= error1 | error2 | upLim | dnLim;
	end
	stepDriver stepper(.clk(S_AXI_ACLK), .busy(busy), .cfgDone(cfgDone), .cfgCommandValid(~cfgDone &sendCommand),
		.cfgCommand(command), .cfgValue(read), .moveUp(up), .moveDn(dn), .setHome(setHome | autoSetHome), .goHome(goHomePulse & ~manual),
		.speed(speed), .motorPos(motorPos), .spi_cs_n(STEPPER_CS_N), .spi_clk(STEPPER_CLK),
		.spi_mosi(STEPPER_MOSI), .spi_miso(STEPPER_MISO), .error1(error1), .error2(error2), .moving(tunerMoving));
		
/*	wire [35:0] csCtrl;
	wire [255:0] csData;
	chipscope_icon csIcon (.CONTROL0(csCtrl));
	chipscope_ila csIla (.CONTROL(csCtrl), .CLK(S_AXI_ACLK), .TRIG0(csData));
	assign csData[0] = 1;
	assign csData[1] = STEPPER_LOCAL;
	assign csData[2] = STEPPER_LOCAL_UP;
	assign csData[3] = STEPPER_LOCAL_DN;
	assign csData[4] = STEPPER_RST_N;
	assign csData[5] = STEPPER_SW_N;
	assign csData[6] = STEPPER_CLK;
	assign csData[7] = STEPPER_CS_N;
	assign csData[8] = STEPPER_MISO;
	assign csData[9] = STEPPER_MOSI;
	assign csData[10] = 0;
	assign csData[11] = 0;
	assign csData[12] = STEPPER_LIM_UP_N;
	assign csData[13] = STEPPER_LIM_DN_N;
	assign csData[14] = sendCommand;
	assign csData[15] = cfgDone;
	assign csData[47:16] = command;
	assign csData[55:48] = limCount;
	assign csData[79:56] = 0;
	assign csData[99:80] = speed;
	assign csData[107:100] = 1;
	assign csData[115:108] = manualDebounce[7:0];
	assign csData[123:116] = upDebounce[7:0];
	assign csData[131:124] = dnDebounce[7:0];
	assign csData[132] = error1;
	assign csData[133] = error2;
	assign csData[134] = 0;
	assign csData[135] = 0;
	assign csData[136] = manual;
	assign csData[137] = up;
	assign csData[138] = dn;
	assign csData[203:139] = 0;
	assign csData[220] = tunerMoving;
	assign csData[221] = tunerFault;
	assign csData[222] = upLim;
	assign csData[223] = dnLim;
	assign csData[231:224] = upLimDebounce[7:0];
	assign csData[239:232] = dnLimDebounce[7:0];
	assign csData[240] = stepperLocalReg;
	assign csData[241] = stepperLocalUpReg;
	assign csData[242] = stepperLocalDnReg;
	assign csData[243] = stepperLimUpReg;
	assign csData[244] = stepperLimDnReg;
	assign csData[255:245] = 0;*/
endmodule
