`timescale 1ns / 1ps

module stepDriver(
	input clk,
	output reg busy = 0,
	input cfgDone,
	input cfgCommandValid,
	input [31:0] cfgCommand,
	output reg [31:0] cfgValue = 0,
	input moveUp,
	input moveDn,
	input setHome,
	input goHome,
	input [19:0] speed,
	output reg [31:0] motorPos = 0,
	output reg spi_cs_n = 1,
	output reg spi_clk = 1,
	output reg spi_mosi = 0,
	input spi_miso,
	output reg error1 = 0,
	output reg error2 = 0,
	output reg moving
	);

	reg [31:0] shiftOut = 0;
	reg [31:0] shiftIn = 0;
	reg [5:0] csCtr = 0;
	reg [2:0] clkCtr = 0;
	reg [5:0] bitCtr = 0;
	reg [3:0] state = 0;
	reg spi_clk_prev = 1;
	reg movingUp = 0;
	reg movingDn = 0;
	reg [31:0] csValue = 0;
	reg setHomeLatch = 0;
	reg goHomeLatch = 0;
	reg goingHome = 0;
	reg readPos = 0;
	reg [7:0] readPosCtr = 0;
	reg div=1;
	always @ (posedge clk)
	begin
		moving <= movingUp | movingDn;
		busy <= |state;
		if (setHome & ~setHomeLatch)
			setHomeLatch <= 1;
		if (goHome)
			goHomeLatch <= 1;
		else if (moveUp | moveDn | goingHome)
			goHomeLatch <= 0;
		spi_clk_prev <= spi_clk;
		begin
		if (~|state)
		 div<=1;
		else
		 div<=~div;
		 end
	 begin
        if (div)
		case (state)
			// wait for next command
			0:
			begin
				spi_clk <= 1;
				spi_cs_n <= 1;
				spi_mosi <= 0;
				shiftOut <= cfgCommand;
				shiftIn <= 0;
				bitCtr <= 0;
				movingUp <= 0;
				movingDn <= 0;
				if (cfgCommandValid)
					state <= state + 1;
				else if (cfgDone)
					state <= 5;
			end
			// wait before starting clock (per datasheet)
			1:
			begin
				spi_clk <= 1;
				spi_cs_n <= 0;
				csCtr <= csCtr + 1;
				if (&csCtr)
					state <= state + 1;
			end
			// shift command out and data in
			2:
			begin
				spi_cs_n <= 0;
				clkCtr <= clkCtr + 1;
				if (&clkCtr)
				begin
					spi_clk <= ~spi_clk;
					if (spi_clk)
					begin
						spi_mosi <= shiftOut[31];
						shiftOut <= {shiftOut[30:0], 1'b0};
					end
					else
					begin
						shiftIn <= {shiftIn[30:0], spi_miso};
						bitCtr <= bitCtr + 1;
					end
				end
				if (~|bitCtr[2:0] & spi_clk & ~spi_clk_prev)
					state <= state + 1;
			end
			// set cs_n high between bytes
			3:
			begin
				spi_clk <= 1;
				spi_cs_n <= 1;
				csCtr <= csCtr + 1;
				if (&csCtr)
				begin
					if (bitCtr[5])
						state <= state + 1;
					else
						state <= state - 2;
				end
			end
			// wait before accepting sending next command
			4:
			begin
				spi_clk <= 1;
				spi_cs_n <= 1;
				spi_mosi <= 0;
				cfgValue <= shiftIn;
				csCtr <= csCtr + 1;
				if (&csCtr)
					state <= 0;
			end
			// wait for next requested change in motor state; if software asks to reconfigure the driver, stop the motor first
			5:
			begin
				readPosCtr <= readPosCtr + 1;
				spi_clk <= 1;
				spi_cs_n <= 1;
				spi_mosi <= 0;
				// periodically read the motor position
				readPos <= &readPosCtr;
				if (&readPosCtr)
					shiftOut <= 32'h21000000;
				// hard stop motor before sending goHome command or allowing software reconfiguration
				else if ((setHomeLatch | goHomeLatch | ~cfgDone) & (movingUp | movingDn))
					shiftOut <= 32'hB8000000;
				// set home position when command is received from PLC or debug terminal
				else if (setHomeLatch)													// set cold start position
				begin
					shiftOut <= 32'hD8000000;
					setHomeLatch <= 0;
					goingHome <= 0;
				end
				// goHome if RF set to off or when finished sweeping tuner
				else if (goHomeLatch)
				begin
					shiftOut <= 32'h70000000;
					goingHome <= 1;
				end
				// stop motor if moving and move command stops
				else if (~goingHome & ((movingUp & ~moveUp) | (movingDn & ~moveDn)))
					shiftOut <= 32'hB8000000;									// hard stop command
				// start motor for either moveUp or moveDn command
				else if ((moveUp & ~movingUp) | (moveDn & ~movingDn))
				begin
					shiftOut <= {7'b0101000, moveDn, 4'h0, speed};		// direction is reversed compared to datasheet
					goingHome <= 0;
				end
				// if motor position is at 0, goHome operation has finished
				else if (~|motorPos)
					goingHome <= 0;
					
				shiftIn <= 0;
				bitCtr <= 0;
				if (~cfgDone & ~movingUp & ~movingDn)
					state <= 0;
				else if (~cfgDone | (movingUp != moveUp) | (movingDn != moveDn) | setHomeLatch | goHomeLatch | &readPosCtr)
					state <= state + 1;
			end
			// wait before starting clock (per datasheet)
			6:
			begin
				spi_clk <= 1;
				spi_cs_n <= 0;
				csCtr <= csCtr + 1;
				if (&csCtr)
					state <= state + 1;
			end
			// shift command out and data in
			7:
			begin
				spi_cs_n <= 0;
				clkCtr <= clkCtr + 1;
				if (&clkCtr)
				begin
					spi_clk <= ~spi_clk;
					if (spi_clk)
					begin
						spi_mosi <= shiftOut[31];
						shiftOut <= {shiftOut[30:0], 1'b0};
					end
					else
					begin
						shiftIn <= {shiftIn[30:0], spi_miso};
						bitCtr <= bitCtr + 1;
					end
				end
				if (~|bitCtr[2:0] & spi_clk & ~spi_clk_prev)
					state <= state + 1;
			end
			// set cs_n high between bytes
			8:
			begin
				spi_clk <= 1;
				spi_cs_n <= 1;
				csCtr <= csCtr + 1;
				if (&csCtr)
				begin
					if (bitCtr[5])
						state <= state + 1;
					else
						state <= state - 2;
				end
			end
			// wait before checking status
			9:
			begin
				spi_clk <= 1;
				spi_cs_n <= 1;
				spi_mosi <= 0;
				csCtr <= csCtr + 1;
				csValue <= shiftIn;
				if (readPos)
					motorPos <= {shiftIn[21], shiftIn[21], shiftIn[21], shiftIn[21], shiftIn[21], shiftIn[21], shiftIn[21], shiftIn[21], shiftIn[21], shiftIn[21], shiftIn[21:0]};
				if (&csCtr)
					state <= state + 1;
			end
			// prepare to read motor controller status
			10:
			begin
				spi_clk <= 1;
				spi_cs_n <= 1;
				spi_mosi <= 0;
				shiftOut <= 32'hD0000000;
				shiftIn <= 0;
				bitCtr <= 0;
				state <= state + 1;
			end
			// wait before starting clock (per datasheet)
			11:
			begin
				spi_clk <= 1;
				spi_cs_n <= 0;
				csCtr <= csCtr + 1;
				if (&csCtr)
					state <= state + 1;
			end
			// shift command out and data in
			12:
			begin
				spi_cs_n <= 0;
				clkCtr <= clkCtr + 1;
				if (&clkCtr)
				begin
					spi_clk <= ~spi_clk;
					if (spi_clk)
					begin
						spi_mosi <= shiftOut[31];
						shiftOut <= {shiftOut[30:0], 1'b0};
					end
					else
					begin
						shiftIn <= {shiftIn[30:0], spi_miso};
						bitCtr <= bitCtr + 1;
					end
				end
				if (~|bitCtr[2:0] & spi_clk & ~spi_clk_prev)
					state <= state + 1;
			end
			// set cs_n high between bytes
			13:
			begin
				spi_clk <= 1;
				spi_cs_n <= 1;
				csCtr <= csCtr + 1;
				if (&csCtr)
				begin
					if (&bitCtr[4:3])
						state <= state + 1;
					else
						state <= state - 2;
				end
			end
			// wait required time per datasheet
			14:
			begin
				spi_clk <= 1;
				spi_cs_n <= 1;
				spi_mosi <= 0;
				csCtr <= csCtr + 1;
				csValue <= shiftIn;
				if (&csCtr)
					state <= state + 1;
			end
			// update states based on motor controller status
			15:
			begin
				spi_clk <= 1;
				spi_cs_n <= 1;
				spi_mosi <= 0;
				// command not performed
				error1 <= |csValue[8:7];
				// other possible errors
				error2 <= ~&csValue[14:9];
				movingUp <= |csValue[6:5] & ~csValue[4];	// direction is reversed compared to datasheet
				movingDn <= |csValue[6:5] & csValue[4];	// direction is reversed compared to datasheet
				// if motor is decelerating, wait until it stops before sending any more commands
				// if there is an error flag, also wait for the flag to clear
				// if there is a goHome command in progress, wait for it to finish unless a move command has been received
				if ((csValue[6] & ~csValue[5]) | ~&csValue[14:9])
					state <= state - 5;
				else
					state <= state - 10;
			end			
		endcase
	end
	end
	reg csClk = 0;
	reg [3:0] statePrev = 0;
	always @ (posedge clk)
	begin
		statePrev <= state;
		csClk <= csClk ? 0 : (statePrev != state);
	end
	
/*	wire [35:0] csCtrl;
	wire [255:0] csData;
	chipscope_icon csIcon (.CONTROL0(csCtrl));
	chipscope_ila csIla (.CONTROL(csCtrl), .CLK(clk), .TRIG0(csData));
	assign csData[31:0] = shiftOut;
	assign csData[63:32] = csValue;
	assign csData[95:64] = cfgCommand;
	assign csData[127:96] = cfgValue;
	assign csData[147:128] = speed;
	assign csData[153:148] = csCtr;
	assign csData[159:154] = bitCtr;
	assign csData[163:160] = state;
	assign csData[166:164] = clkCtr;
	assign csData[167] = busy;
	assign csData[168] = cfgDone;
	assign csData[169] = cfgCommandValid;
	assign csData[170] = moveUp;
	assign csData[171] = moveDn;
	assign csData[172] = spi_cs_n;
	assign csData[173] = spi_clk;
	assign csData[174] = spi_mosi;
	assign csData[175] = spi_miso;
	assign csData[176] = error1;
	assign csData[177] = error2;
	assign csData[178] = spi_clk_prev;
	assign csData[179] = movingUp;
	assign csData[180] = movingDn;
	assign csData[181] = setHome;
	assign csData[182] = goHome;
	assign csData[183] = setHomeLatch;
	assign csData[184] = goHomeLatch;
	assign csData[190:185] = 0;
	assign csData[191] = 0;
	assign csData[223:192] = shiftIn;
	assign csData[224] = goingHome;
	assign csData[226:225] = 0;
	assign csData[234:227] = readPosCtr;
	assign csData[255:235] = 0;*/
endmodule
