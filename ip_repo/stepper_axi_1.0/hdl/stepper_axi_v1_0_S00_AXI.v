
`timescale 1 ns / 1 ps

	module stepper_axi_v1_0_S00_AXI #
	(
		// Users to add parameters here

		// User parameters ends
		// Do not modify the parameters beyond this line

		// Width of S_AXI data bus
		parameter integer C_S_AXI_DATA_WIDTH	= 32,
		// Width of S_AXI address bus
		parameter integer C_S_AXI_ADDR_WIDTH	= 6
	)
	(
		// Users to add ports here
        input                                     STEPPER_LOCAL,
        input                                     STEPPER_LOCAL_UP,
        input                                     STEPPER_LOCAL_DN,
        output                                    STEPPER_RST_N,
        output                                    STEPPER_SW_N,
        output                                    STEPPER_CLK,
        output                                    STEPPER_CS_N,
        input                                     STEPPER_MISO,
        output                                    STEPPER_MOSI,
        input                                     STEPPER_LIM_UP_N,
        input                                     STEPPER_LIM_DN_N,
        input                                     tunerDn,
        input                                     tunerUp,
        input                                     goHome,
        input                                     autoSetHome,
        output                                    tunerMoving,
        output reg                                tunerFault = 0,
		// User ports ends
		// Do not modify the ports beyond this line

		// Global Clock Signal
		input wire  S_AXI_ACLK,
		// Global Reset Signal. This Signal is Active LOW
		input wire  S_AXI_ARESETN,
		// Write address (issued by master, acceped by Slave)
		input wire [C_S_AXI_ADDR_WIDTH-1 : 0] S_AXI_AWADDR,
		// Write channel Protection type. This signal indicates the
    		// privilege and security level of the transaction, and whether
    		// the transaction is a data access or an instruction access.
		input wire [2 : 0] S_AXI_AWPROT,
		// Write address valid. This signal indicates that the master signaling
    		// valid write address and control information.
		input wire  S_AXI_AWVALID,
		// Write address ready. This signal indicates that the slave is ready
    		// to accept an address and associated control signals.
		output wire  S_AXI_AWREADY,
		// Write data (issued by master, acceped by Slave) 
		input wire [C_S_AXI_DATA_WIDTH-1 : 0] S_AXI_WDATA,
		// Write strobes. This signal indicates which byte lanes hold
    		// valid data. There is one write strobe bit for each eight
    		// bits of the write data bus.    
		input wire [(C_S_AXI_DATA_WIDTH/8)-1 : 0] S_AXI_WSTRB,
		// Write valid. This signal indicates that valid write
    		// data and strobes are available.
		input wire  S_AXI_WVALID,
		// Write ready. This signal indicates that the slave
    		// can accept the write data.
		output wire  S_AXI_WREADY,
		// Write response. This signal indicates the status
    		// of the write transaction.
		output wire [1 : 0] S_AXI_BRESP,
		// Write response valid. This signal indicates that the channel
    		// is signaling a valid write response.
		output wire  S_AXI_BVALID,
		// Response ready. This signal indicates that the master
    		// can accept a write response.
		input wire  S_AXI_BREADY,
		// Read address (issued by master, acceped by Slave)
		input wire [C_S_AXI_ADDR_WIDTH-1 : 0] S_AXI_ARADDR,
		// Protection type. This signal indicates the privilege
    		// and security level of the transaction, and whether the
    		// transaction is a data access or an instruction access.
		input wire [2 : 0] S_AXI_ARPROT,
		// Read address valid. This signal indicates that the channel
    		// is signaling valid read address and control information.
		input wire  S_AXI_ARVALID,
		// Read address ready. This signal indicates that the slave is
    		// ready to accept an address and associated control signals.
		output wire  S_AXI_ARREADY,
		// Read data (issued by slave)
		output wire [C_S_AXI_DATA_WIDTH-1 : 0] S_AXI_RDATA,
		// Read response. This signal indicates the status of the
    		// read transfer.
		output wire [1 : 0] S_AXI_RRESP,
		// Read valid. This signal indicates that the channel is
    		// signaling the required read data.
		output wire  S_AXI_RVALID,
		// Read ready. This signal indicates that the master can
    		// accept the read data and response information.
		input wire  S_AXI_RREADY
	);

	// AXI4LITE signals
	reg [C_S_AXI_ADDR_WIDTH-1 : 0] 	axi_awaddr;
	reg  	axi_awready;
	reg  	axi_wready;
	reg [1 : 0] 	axi_bresp;
	reg  	axi_bvalid;
	reg [C_S_AXI_ADDR_WIDTH-1 : 0] 	axi_araddr;
	reg  	axi_arready;
	reg [C_S_AXI_DATA_WIDTH-1 : 0] 	axi_rdata;
	reg [1 : 0] 	axi_rresp;
	reg  	axi_rvalid;

	// Example-specific design signals
	// local parameter for addressing 32 bit / 64 bit C_S_AXI_DATA_WIDTH
	// ADDR_LSB is used for addressing 32/64 bit registers/memories
	// ADDR_LSB = 2 for 32 bits (n downto 2)
	// ADDR_LSB = 3 for 64 bits (n downto 3)
	localparam integer ADDR_LSB = (C_S_AXI_DATA_WIDTH/32) + 1;
	localparam integer OPT_MEM_ADDR_BITS = 1;
	//----------------------------------------------
	//-- Signals for user logic register space example
	//------------------------------------------------
	//-- Number of Slave Registers 4
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg0;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg1;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg2;
	reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg3;
	wire	 slv_reg_rden;
	wire	 slv_reg_wren;
	reg [C_S_AXI_DATA_WIDTH-1:0]	 reg_data_out;
	integer	 byte_index;

	// I/O Connections assignments

	assign S_AXI_AWREADY	= axi_awready;
	assign S_AXI_WREADY	= axi_wready;
	assign S_AXI_BRESP	= axi_bresp;
	assign S_AXI_BVALID	= axi_bvalid;
	assign S_AXI_ARREADY	= axi_arready;
	assign S_AXI_RDATA	= axi_rdata;
	assign S_AXI_RRESP	= axi_rresp;
	assign S_AXI_RVALID	= axi_rvalid;
	// Implement axi_awready generation
	// axi_awready is asserted for one S_AXI_ACLK clock cycle when both
	// S_AXI_AWVALID and S_AXI_WVALID are asserted. axi_awready is
	// de-asserted when reset is low.

	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_awready <= 1'b0;
	    end 
	  else
	    begin    
	      if (~axi_awready && S_AXI_AWVALID && S_AXI_WVALID)
	        begin
	          // slave is ready to accept write address when 
	          // there is a valid write address and write data
	          // on the write address and data bus. This design 
	          // expects no outstanding transactions. 
	          axi_awready <= 1'b1;
	        end
	      else           
	        begin
	          axi_awready <= 1'b0;
	        end
	    end 
	end       

	// Implement axi_awaddr latching
	// This process is used to latch the address when both 
	// S_AXI_AWVALID and S_AXI_WVALID are valid. 

	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_awaddr <= 0;
	    end 
	  else
	    begin    
	      if (~axi_awready && S_AXI_AWVALID && S_AXI_WVALID)
	        begin
	          // Write Address latching 
	          axi_awaddr <= S_AXI_AWADDR;
	        end
	    end 
	end       

	// Implement axi_wready generation
	// axi_wready is asserted for one S_AXI_ACLK clock cycle when both
	// S_AXI_AWVALID and S_AXI_WVALID are asserted. axi_wready is 
	// de-asserted when reset is low. 

	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_wready <= 1'b0;
	    end 
	  else
	    begin    
	      if (~axi_wready && S_AXI_WVALID && S_AXI_AWVALID)
	        begin
	          // slave is ready to accept write data when 
	          // there is a valid write address and write data
	          // on the write address and data bus. This design 
	          // expects no outstanding transactions. 
	          axi_wready <= 1'b1;
	        end
	      else
	        begin
	          axi_wready <= 1'b0;
	        end
	    end 
	end       

	// Implement memory mapped register select and write logic generation
	// The write data is accepted and written to memory mapped registers when
	// axi_awready, S_AXI_WVALID, axi_wready and S_AXI_WVALID are asserted. Write strobes are used to
	// select byte enables of slave registers while writing.
	// These registers are cleared when reset (active low) is applied.
	// Slave register write enable is asserted when valid address and data are available
	// and the slave is ready to accept the write address and write data.
	assign slv_reg_wren = axi_wready && S_AXI_WVALID && axi_awready && S_AXI_AWVALID;
/*
	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      slv_reg0 <= 0;
	      slv_reg1 <= 0;
	      slv_reg2 <= 0;
	      slv_reg3 <= 0;
	    end 
	  else begin
	    if (slv_reg_wren)
	      begin
	        case ( axi_awaddr[ADDR_LSB+OPT_MEM_ADDR_BITS:ADDR_LSB] )
	          2'h0:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 0
	                slv_reg0[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          2'h1:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 1
	                slv_reg1[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          2'h2:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 2
	                slv_reg2[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          2'h3:
	            for ( byte_index = 0; byte_index <= (C_S_AXI_DATA_WIDTH/8)-1; byte_index = byte_index+1 )
	              if ( S_AXI_WSTRB[byte_index] == 1 ) begin
	                // Respective byte enables are asserted as per write strobes 
	                // Slave register 3
	                slv_reg3[(byte_index*8) +: 8] <= S_AXI_WDATA[(byte_index*8) +: 8];
	              end  
	          default : begin
	                      slv_reg0 <= slv_reg0;
	                      slv_reg1 <= slv_reg1;
	                      slv_reg2 <= slv_reg2;
	                      slv_reg3 <= slv_reg3;
	                    end
	        endcase
	      end
	  end
	end    
*/
	// Implement write response logic generation
	// The write response and response valid signals are asserted by the slave 
	// when axi_wready, S_AXI_WVALID, axi_wready and S_AXI_WVALID are asserted.  
	// This marks the acceptance of address and indicates the status of 
	// write transaction.

	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_bvalid  <= 0;
	      axi_bresp   <= 2'b0;
	    end 
	  else
	    begin    
	      if (axi_awready && S_AXI_AWVALID && ~axi_bvalid && axi_wready && S_AXI_WVALID)
	        begin
	          // indicates a valid write response is available
	          axi_bvalid <= 1'b1;
	          axi_bresp  <= 2'b0; // 'OKAY' response 
	        end                   // work error responses in future
	      else
	        begin
	          if (S_AXI_BREADY && axi_bvalid) 
	            //check if bready is asserted while bvalid is high) 
	            //(there is a possibility that bready is always asserted high)   
	            begin
	              axi_bvalid <= 1'b0; 
	            end  
	        end
	    end
	end   

	// Implement axi_arready generation
	// axi_arready is asserted for one S_AXI_ACLK clock cycle when
	// S_AXI_ARVALID is asserted. axi_awready is 
	// de-asserted when reset (active low) is asserted. 
	// The read address is also latched when S_AXI_ARVALID is 
	// asserted. axi_araddr is reset to zero on reset assertion.

	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_arready <= 1'b0;
	      axi_araddr  <= 32'b0;
	    end 
	  else
	    begin    
	      if (~axi_arready && S_AXI_ARVALID)
	        begin
	          // indicates that the slave has acceped the valid read address
	          axi_arready <= 1'b1;
	          // Read address latching
	          axi_araddr  <= S_AXI_ARADDR;
	        end
	      else
	        begin
	          axi_arready <= 1'b0;
	        end
	    end 
	end       

	// Implement axi_arvalid generation
	// axi_rvalid is asserted for one S_AXI_ACLK clock cycle when both 
	// S_AXI_ARVALID and axi_arready are asserted. The slave registers 
	// data are available on the axi_rdata bus at this instance. The 
	// assertion of axi_rvalid marks the validity of read data on the 
	// bus and axi_rresp indicates the status of read transaction.axi_rvalid 
	// is deasserted on reset (active low). axi_rresp and axi_rdata are 
	// cleared to zero on reset (active low).  
	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_rvalid <= 0;
	      axi_rresp  <= 0;
	    end 
	  else
	    begin    
	      if (axi_arready && S_AXI_ARVALID && ~axi_rvalid)
	        begin
	          // Valid read data is available at the read data bus
	          axi_rvalid <= 1'b1;
	          axi_rresp  <= 2'b0; // 'OKAY' response
	        end   
	      else if (axi_rvalid && S_AXI_RREADY)
	        begin
	          // Read data is accepted by the master
	          axi_rvalid <= 1'b0;
	        end                
	    end
	end    

	// Implement memory mapped register select and read logic generation
	// Slave register read enable is asserted when valid address is available
	// and the slave is ready to accept the read address.
	assign slv_reg_rden = axi_arready & S_AXI_ARVALID & ~axi_rvalid;
/*
	always @(*)
	begin
	      // Address decoding for reading registers
	      case ( axi_araddr[ADDR_LSB+OPT_MEM_ADDR_BITS:ADDR_LSB] )
	        2'h0   : reg_data_out <= slv_reg0;
	        2'h1   : reg_data_out <= slv_reg1;
	        2'h2   : reg_data_out <= slv_reg2;
	        2'h3   : reg_data_out <= slv_reg3;
	        default : reg_data_out <= 0;
	      endcase
	end
*/
	// Output register or memory read data
	always @( posedge S_AXI_ACLK )
	begin
	  if ( S_AXI_ARESETN == 1'b0 )
	    begin
	      axi_rdata  <= 0;
	    end 
	  else
	    begin    
	      // When there is a valid read address (S_AXI_ARVALID) with 
	      // acceptance of read address by the slave (axi_arready), 
	      // output the read dada 
	      if (slv_reg_rden)
	        begin
	          axi_rdata <= data;     // register read data
	        end   
	    end
	end    

	// Add user logic here
/*
stepdriver_top inst1 (
                      .STEPPER_LOCAL     (STEPPER_LOCAL),
                      .STEPPER_LOCAL_UP  (STEPPER_LOCAL_UP),
                      .STEPPER_LOCAL_DN  (STEPPER_LOCAL_DN),
                      .STEPPER_RST_N     (STEPPER_RST_N),
                      .STEPPER_SW_N      (STEPPER_SW_N),
                      .STEPPER_CLK       (STEPPER_CLK),
                      .STEPPER_CS_N      (STEPPER_CS_N),
                      .STEPPER_MISO      (STEPPER_MISO),
                      .STEPPER_MOSI      (STEPPER_MOSI),
                      .STEPPER_LIM_UP_N  (STEPPER_LIM_UP_N),
                      .STEPPER_LIM_DN_N  (STEPPER_LIM_DN_N),
                      .tunerDn           (tunerDn),
                      .tunerUp           (tunerUp),
                      .goHome            (goHome),
                      .autoSetHome       (autoSetHome),
                      .tunerMoving       (tunerMoving),
                      .tunerFault        (tunerFault),
                      .S_AXI_ACLK     (S_AXI_ACLK),
                      .S_AXI_ARESETN  (S_AXI_ARESETN),
                      .S_AXI_AWADDR   (S_AXI_AWADDR),
                      .S_AXI_WDATA    (S_AXI_WDATA),
                      .S_AXI_RDATA    (S_AXI_RDATA),
                      .S_AXI_AWVALID  (S_AXI_AWVALID),
                      .S_AXI_AWREADY  (S_AXI_AWREADY),
                      .S_AXI_WSTRB    (S_AXI_WSTRB),
                      .S_AXI_WVALID   (S_AXI_WVALID),
                      .S_AXI_WREADY   (S_AXI_WREADY),
                      .S_AXI_BREADY   (S_AXI_BREADY),
                      .S_AXI_ARADDR   (S_AXI_ARADDR),
                      .S_AXI_ARVALID  (S_AXI_ARVALID),
                      .S_AXI_ARREADY  (S_AXI_ARREADY),
                      .S_AXI_RVALID   (S_AXI_RVALID),
                      .S_AXI_RREADY   (S_AXI_RREADY)
                      );

*/	// User logic ends


	assign STEPPER_RST_N = 1;
	assign STEPPER_SW_N = 1;

	// Output signals to AXI bus
	reg [31:0] data;
	wire [31:0] read;
//	assign S_AXI_RDATA = S_AXI_RREADY ? data : 32'h0;
//	assign S_AXI_AWREADY = S_AXI_AWVALID;
//	assign S_AXI_WREADY = S_AXI_AWVALID & S_AXI_WVALID;
//	assign S_AXI_RREADY = S_AXI_AWVALID & S_AXI_RVALID;
//	assign IP2Bus_Error = 0;

	reg [31:0] command = 0;
	reg sendCommand = 0;
	reg cfgDone = 0;
	wire error1, error2, busy;
	reg setHome = 0;
	wire [31:0] motorPos;
	
	wire [19:0] speed;
	reg [31:0] speedUnsat = 32'h00001000;
	assign speed = |speedUnsat[31:20] ? 20'hFFFFF : speedUnsat[19:0];
	reg [7:0] limCount = 0;
	reg [6:0] sweepCount = 1;

	reg stepperLocalReg = 0, stepperLocalUpReg = 0, stepperLocalDnReg = 0, stepperLimUpReg = 0, stepperLimDnReg = 0;
	reg [13:0] manualDebounce = 0, upDebounce = 0, dnDebounce = 0, upLimDebounce = 0, dnLimDebounce = 0;
	reg manual = 0, up = 0, dn = 0, upLim = 1, dnLim = 1;
	reg [1:0] tunerDnReg = 0, tunerUpReg = 0;
	reg [2:0] goHomeReg = 0;
	reg goHomePulse = 0;

	always @*
	begin
		case (axi_araddr[5:2])
			4'h0: data <= read;
			4'h1: data <= {31'h0, busy};
			4'h2: data <= {31'h0, cfgDone};
			4'h3: data <= {26'h0, tunerMoving, tunerFault, dnLim, upLim, error2, error1};
			4'h4: data <= {12'h0, speed};
			4'h5: data <= 0;							// write-only register
			4'h6: data <= -motorPos;				// Motor position from stepper controller has the wrong sign
			4'h7: data <= limCount[7:1] + limCount[0];		// number of times to run to limit switch before stopping
			4'h8: data <= {25'h0, sweepCount};
			4'h9: data <= 0;							// write-only register
			default: data <= 32'hFFFFFFFF;
		endcase
	end



	always @ (posedge S_AXI_ACLK)
	begin
		setHome <= axi_wready & (axi_awaddr[5:2] == 4'h5) & S_AXI_WDATA[0];
		if (~axi_wready & ~|speedUnsat)	speedUnsat <= 20'h00001;
		if (axi_wready & S_AXI_WSTRB[0])
			case (axi_awaddr[5:2])
				4'h0: command[7:0] <= S_AXI_WDATA[7:0];
				4'h2: cfgDone <= S_AXI_WDATA[0];
				4'h4: speedUnsat[7:0] <= S_AXI_WDATA[7:0];
				4'h8: sweepCount <= S_AXI_WDATA[6:0];
				4'h9: if (S_AXI_WDATA[0])		limCount <= {sweepCount, 1'b0};
			endcase
		if (axi_wready & S_AXI_WSTRB[1])
			case (axi_awaddr[5:2])
				4'h0: command[15:8] <= S_AXI_WDATA[15:8];
				4'h4: speedUnsat[15:8] <= S_AXI_WDATA[15:8];
			endcase
		if (axi_wready & S_AXI_WSTRB[2])
			case (axi_awaddr[5:2])
				4'h0: command[23:16] <= S_AXI_WDATA[23:16];
				4'h4: speedUnsat[23:16] <= S_AXI_WDATA[23:16];
			endcase
		if (axi_wready & S_AXI_WSTRB[3])
			case (axi_awaddr[5:2])
				4'h0: command[31:24] <= S_AXI_WDATA[31:24];
				4'h4: speedUnsat[31:24] <= S_AXI_WDATA[31:24];
			endcase
		sendCommand <= axi_wready & ~|axi_awaddr[4:2];

		stepperLocalReg <= STEPPER_LOCAL;
		stepperLocalUpReg <= STEPPER_LOCAL_UP;
		stepperLocalDnReg <= STEPPER_LOCAL_DN;
		stepperLimUpReg <= STEPPER_LIM_UP_N;
		stepperLimDnReg <= STEPPER_LIM_DN_N;
		if (stepperLocalReg & ~&manualDebounce)		manualDebounce <= manualDebounce + 1;
		if (~stepperLocalReg & |manualDebounce)		manualDebounce <= manualDebounce - 1;
		if (&manualDebounce)									manual <= 1;
		if (~|manualDebounce)								manual <= 0;
		if (stepperLocalUpReg & ~&upDebounce)			upDebounce <= upDebounce + 1;
		if (~stepperLocalUpReg & |upDebounce)			upDebounce <= upDebounce - 1;
		if (&upDebounce)										tunerUpReg <= 1;
		if (~|upDebounce)										tunerUpReg <= 0;
		if (stepperLocalDnReg & ~&dnDebounce)			dnDebounce <= dnDebounce + 1;
		if (~stepperLocalDnReg & |dnDebounce)			dnDebounce <= dnDebounce - 1;
		if (&dnDebounce)										tunerDnReg <= 1;
		if (~|dnDebounce)										tunerDnReg <= 0;
		if (stepperLimUpReg & ~&upLimDebounce)			upLimDebounce <= upLimDebounce + 1;
		if (~stepperLimUpReg & |upLimDebounce)			upLimDebounce <= upLimDebounce - 1;
		if (&upLimDebounce)									upLim <= 1;
		if (~|upLimDebounce)									upLim <= 0;
		if (stepperLimDnReg & ~&dnLimDebounce)			dnLimDebounce <= dnLimDebounce + 1;
		if (~stepperLimDnReg & |dnLimDebounce)			dnLimDebounce <= dnLimDebounce - 1;
		if (&dnLimDebounce)									dnLim <= 1;
		if (~|dnLimDebounce)									dnLim <= 0;
		up <= ~upLim & (limCount[0] | (tunerUpReg & manual) | (~manual & tunerUp));		// limit switches are '1' when connected and not at limit
		dn <= ~dnLim & ((~limCount[0] & |limCount[7:1]) | (tunerDnReg & manual) | (~manual & tunerDn));		// limit switches are '1' when connected and not at limit
		goHomeReg <= {goHomeReg[1:0], goHome};
		goHomePulse <= goHomeReg[1] & ~goHomeReg[2];
		if (~axi_wready & ((~manual & (tunerDn | tunerUp)) | (manual & (&upDebounce | &dnDebounce)) | goHome | autoSetHome))
			limCount <= 0;
		if (~axi_wready & |limCount & ((limCount[0] & upLim) | (~limCount[0] & dnLim)))
			limCount <= limCount - 1;
		tunerFault <= error1 | error2 | upLim | dnLim;
	end
	stepDriver stepper(
	                   .clk(S_AXI_ACLK), 
	                   .busy(busy), 
	                   .cfgDone(cfgDone), 
	                   .cfgCommandValid(~cfgDone &sendCommand),
	                   .cfgCommand(command),
	                   .cfgValue(read), 
	                   .moveUp(up), 
	                   .moveDn(dn), 
	                   .setHome(setHome | autoSetHome), 
	                   .goHome(goHomePulse & ~manual),
	                   .speed(speed), 
	                   .motorPos(motorPos), 
	                   .spi_cs_n(STEPPER_CS_N), 
	                   .spi_clk(STEPPER_CLK),
	                   .spi_mosi(STEPPER_MOSI), 
	                   .spi_miso(STEPPER_MISO), 
	                   .error1(error1), 
	                   .error2(error2), 
	                   .moving(tunerMoving)
	                   );
		


	endmodule
