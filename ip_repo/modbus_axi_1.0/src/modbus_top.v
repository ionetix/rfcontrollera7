//----------------------------------------------------------------------------
// user_logic.v - module
//----------------------------------------------------------------------------
//
// ***************************************************************************
// ** Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.            **
// **                                                                       **
// ** Xilinx, Inc.                                                          **
// ** XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"         **
// ** AS A COURTESY TO YOU, SOLELY FOR USE IN DEVELOPING PROGRAMS AND       **
// ** SOLUTIONS FOR XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE,        **
// ** OR INFORMATION AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE,        **
// ** APPLICATION OR STANDARD, XILINX IS MAKING NO REPRESENTATION           **
// ** THAT THIS IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,     **
// ** AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE      **
// ** FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY              **
// ** WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE               **
// ** IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR        **
// ** REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF       **
// ** INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS       **
// ** FOR A PARTICULAR PURPOSE.                                             **
// **                                                                       **
// ***************************************************************************
//
//----------------------------------------------------------------------------
// Filename:          user_logic.v
// Version:           1.00.a
// Description:       User logic module.
// Date:              Tue Nov 25 00:08:28 2014 (by Create and Import Peripheral Wizard)
// Verilog Standard:  Verilog-2001
//----------------------------------------------------------------------------
// Naming Conventions:
//   active low signals:                    "*_n"
//   clock signals:                         "clk", "clk_div#", "clk_#x"
//   reset signals:                         "rst", "rst_n"
//   generics:                              "C_*"
//   user defined types:                    "*_TYPE"
//   state machine next state:              "*_ns"
//   state machine current state:           "*_cs"
//   combinatorial signals:                 "*_com"
//   pipelined or register delay signals:   "*_d#"
//   counter signals:                       "*cnt*"
//   clock enable signals:                  "*_ce"
//   internal version of output port:       "*_i"
//   device pins:                           "*_pin"
//   ports:                                 "- Names begin with Uppercase"
//   processes:                             "*_PROCESS"
//   component instantiations:              "<ENTITY_>I_<#|FUNC>"
//----------------------------------------------------------------------------

module modbus_top
(
  // -- ADD USER PORTS BELOW THIS LINE ---------------
  // --USER ports added here 
  // -- ADD USER PORTS ABOVE THIS LINE ---------------
  S_AXI_ACLK,
  S_AXI_ARESETN,
  S_AXI_AWADDR,
  S_AXI_WDATA,
  S_AXI_RDATA,
  S_AXI_AWREADY,
  S_AXI_RREADY,
  S_AXI_AWVALID,
  S_AXI_WSTRB,
  S_AXI_WVALID,
  S_AXI_ARVALID,
  S_AXI_ARREADY,
  S_AXI_WREADY
  // -- DO NOT EDIT BELOW THIS LINE ------------------
  // -- Bus protocol ports, do not add to or delete 
/*
  S_AXI_ACLK,                     // Bus to IP clock
  S_AXI_ARESETN,                  // Bus to IP reset
  S_AXI_AWADDR,                    // Bus to IP address bus
  S_AXI_AWVALID,                      // Bus to IP chip select for user logic memory selection
  Bus2IP_RNW,                     // Bus to IP read/not write
  S_AXI_WDATA,                    // Bus to IP data bus
  S_AXI_WSTRB,                      // Bus to IP byte enables
  Bus2IP_RdCE,                    // Bus to IP read chip enable
  Bus2IP_WrCE,                    // Bus to IP write chip enable
  Bus2IP_Burst,                   // Bus to IP burst-mode qualifier
  Bus2IP_BurstLength,             // Bus to IP burst length
  Bus2IP_RdReq,                   // Bus to IP read request
  Bus2IP_WrReq,                   // Bus to IP write request
  Type_of_xfer,                   // Transfer Type
  IP2Bus_AddrAck,                 // IP to Bus address acknowledgement
  S_AXI_RDATA,                    // IP to Bus data bus
  IP2Bus_RdAck,                   // IP to Bus read transfer acknowledgement
  IP2Bus_WrAck,                   // IP to Bus write transfer acknowledgement
  IP2Bus_Error                    // IP to Bus error response
*/
  // -- DO NOT EDIT ABOVE THIS LINE ------------------
); // user_logic

// -- ADD USER PARAMETERS BELOW THIS LINE ------------
// --USER parameters added here 
// -- ADD USER PARAMETERS ABOVE THIS LINE ------------

// -- DO NOT EDIT BELOW THIS LINE --------------------
		// Width of S_AXI data bus
parameter integer C_S_AXI_DATA_WIDTH    = 32;
// Width of S_AXI address bus
parameter integer C_S_AXI_ADDR_WIDTH    = 6;
// -- DO NOT EDIT ABOVE THIS LINE --------------------

// -- ADD USER PORTS BELOW THIS LINE -----------------
// --USER ports added here 
// -- ADD USER PORTS ABOVE THIS LINE -----------------

// -- DO NOT EDIT BELOW THIS LINE --------------------
// -- Bus protocol ports, do not add to or delete
/*
input                                     S_AXI_ACLK;
input                                     S_AXI_ARESETN;
input      [C_SLV_AWIDTH-1 : 0]           S_AXI_AWADDR;
input      [C_NUM_MEM-1 : 0]              S_AXI_AWVALID;
input                                     Bus2IP_RNW;
input      [C_SLV_DWIDTH-1 : 0]           S_AXI_WDATA;
input      [C_SLV_DWIDTH/8-1 : 0]         S_AXI_WSTRB;
input      [C_NUM_MEM-1 : 0]              Bus2IP_RdCE;
input      [C_NUM_MEM-1 : 0]              Bus2IP_WrCE;
input                                     Bus2IP_Burst;
input      [7 : 0]                        Bus2IP_BurstLength;
input                                     Bus2IP_RdReq;
input                                     Bus2IP_WrReq;
input                                     Type_of_xfer;
output                                    IP2Bus_AddrAck;
output reg [C_SLV_DWIDTH-1 : 0]           S_AXI_RDATA;
output reg                                IP2Bus_RdAck;
output                                    IP2Bus_WrAck;
output                                    IP2Bus_Error;
*/

		// Global Clock Signal
		input wire  S_AXI_ACLK;
		// Global Reset Signal. This Signal is Active LOW
		input wire  S_AXI_ARESETN;
		// Write address (issued by master, acceped by Slave)
		input wire [C_S_AXI_ADDR_WIDTH-1 : 0] S_AXI_AWADDR;
		// Write address valid. This signal indicates that the master signaling
    		// valid write address and control information.
		input wire  S_AXI_AWVALID;
		// Write address ready. This signal indicates that the slave is ready
    		// to accept an address and associated control signals.
		output wire  S_AXI_AWREADY;
		// Write data (issued by master, acceped by Slave) 
		input wire [C_S_AXI_DATA_WIDTH-1 : 0] S_AXI_WDATA;
		// Write strobes. This signal indicates which byte lanes hold
    		// valid data. There is one write strobe bit for each eight
    		// bits of the write data bus.    
		input wire [(C_S_AXI_DATA_WIDTH/8)-1 : 0] S_AXI_WSTRB;
		// Write valid. This signal indicates that valid write
    		// data and strobes are available.
		input wire  S_AXI_WVALID;
		// Write ready. This signal indicates that the slave
    		// can accept the write data.
		output wire  S_AXI_WREADY;
		// Read address (issued by master, acceped by Slave)
//		input wire [C_S_AXI_ADDR_WIDTH-1 : 0] S_AXI_ARADDR;
		// Read address valid. This signal indicates that the channel
    		// is signaling valid read address and control information.
		input wire  S_AXI_ARVALID;
		// Read address ready. This signal indicates that the slave is
    		// ready to accept an address and associated control signals.
		output wire  S_AXI_ARREADY;
		// Read data (issued by slave)
		output reg [C_S_AXI_DATA_WIDTH-1 : 0] S_AXI_RDATA;
		// Read valid. This signal indicates that the channel is
    		// signaling the required read data.
//		output wire  S_AXI_RVALID;
		// Read ready. This signal indicates that the master can
    		// accept the read data and response information.
		input wire  S_AXI_RREADY;

// -- DO NOT EDIT ABOVE THIS LINE --------------------

//----------------------------------------------------------------------------
// Implementation
//----------------------------------------------------------------------------

	reg [15:0] crcIn = 16'hFFFF;
	reg busyIn = 0;
	reg [2:0] ctrIn = 0;
	reg inWrAck = 0;
	reg [15:0] crcOut = 16'hFFFF;
	reg busyOut = 0;
	reg [2:0] ctrOut = 0;
	reg outWrAck = 0;
	reg [31:0] timer = 0;
	reg [7:0] slaveAddr = 1;
	reg local = 1;
	reg [31:0] localTimer = 32'hFFFFFFFF;

//	assign IP2Bus_Error = 0;
//	assign IP2Bus_AddrAck = IP2Bus_RdAck | IP2Bus_WrAck;
//	assign IP2Bus_WrAck = S_AXI_AWVALID & S_AXI_WVALID;

	// Address 0: write of any value resets incoming packet CRC calculation
	// Address 1: write new value to update incoming packet CRC
	// Address 2: write of any value resets outgoing packet CRC calculation
	// Address 3: write new value to update outgoing packet CRC
	// Address 0,1: read - get incoming packet CRC
	// Address 2,3: read - get outgoing packet CRC
	// Address 4: write of any value clears timer
	// Address 4: read - get current timer value
	// Address 5: read/write slave address
	// Address 6: local/remote bit
	// Address 7: local -> remote countdown timer
	always @*
	begin
		case (S_AXI_AWADDR[4:2])
			4'h0: S_AXI_RDATA <= {16'h0, crcIn};
			4'h1: S_AXI_RDATA <= {16'h0, crcIn};
			4'h2: S_AXI_RDATA <= {16'h0, crcOut};
			4'h3: S_AXI_RDATA <= {16'h0, crcOut};
			4'h4: S_AXI_RDATA <= timer;
			4'h5: S_AXI_RDATA <= {24'h0, slaveAddr};
			4'h6: S_AXI_RDATA <= {31'h0, local};
			4'h7: S_AXI_RDATA <= localTimer;
		endcase
	end

	always @ (posedge S_AXI_ACLK)
	begin
		// Modbus link is being configured to either local or remote
		if (S_AXI_AWVALID & S_AXI_WVALID & S_AXI_WSTRB[0] & (S_AXI_AWADDR[4:2] == 3'h6))
		begin
			local <= S_AXI_WDATA[0];
			localTimer <= S_AXI_WDATA[0] ? 32'hFFFFFFFF : 0;
		end
		// Otherwise local bit is the or of all bits in the timer
		else
		begin
			local <= |localTimer;
			if (S_AXI_AWVALID & S_AXI_WVALID & |S_AXI_WSTRB & (S_AXI_AWADDR[4:2] == 3'h7))
			begin
				if (S_AXI_WSTRB[0])		localTimer[7:0] <= S_AXI_WDATA[7:0];
				if (S_AXI_WSTRB[1])		localTimer[15:8] <= S_AXI_WDATA[15:8];
				if (S_AXI_WSTRB[2])		localTimer[23:16] <= S_AXI_WDATA[23:16];
				if (S_AXI_WSTRB[3])		localTimer[31:24] <= S_AXI_WDATA[31:24];
			end
			else if (|localTimer & ~&localTimer)
				localTimer <= localTimer - 1;
		end
		
		// initialize incoming packet CRC
		if (S_AXI_AWVALID & S_AXI_WVALID & |S_AXI_WSTRB & (S_AXI_AWADDR[4:2] == 3'h0))
		begin
			crcIn <= 16'hFFFF;
			busyIn <= 0;
			inWrAck <= 1;
			ctrIn <= 0;
		end
		// update incoming packet CRC with data byte
		else
		begin
			if (S_AXI_AWVALID & S_AXI_WVALID & &S_AXI_WSTRB[1:0] & (S_AXI_AWADDR[4:2] == 3'h1) & ~busyIn)
			begin
				crcIn <= crcIn ^ {8'h00, S_AXI_WDATA[7:0]};
				busyIn <= 1;
				inWrAck <= 1;
				ctrIn <= 0;
			end
			else
			begin
				if (busyIn)
				begin
					crcIn <= crcIn[0] ? (crcIn >> 1) ^ 16'hA001 : crcIn >> 1;
					ctrIn <= ctrIn + 1;
					if (&ctrIn)
						busyIn <= 0;
				end
				inWrAck <= 0;
			end
		end
		// initialize outgoing packet CRC
		if (S_AXI_AWVALID & S_AXI_WVALID & |S_AXI_WSTRB & (S_AXI_AWADDR[4:2] == 3'h2))
		begin
			crcOut <= 16'hFFFF;
			busyOut <= 0;
			outWrAck <= 1;
			ctrOut <= 0;
		end
		// update incoming packet CRC with data byte
		else
		begin
			if (S_AXI_AWVALID & S_AXI_WVALID & &S_AXI_WSTRB[1:0] & (S_AXI_AWADDR[4:2] == 3'h3) & ~busyOut)
			begin
				crcOut <= crcOut ^ {8'h00, S_AXI_WDATA[7:0]};
				busyOut <= 1;
				outWrAck <= 1;
				ctrOut <= 0;
			end
			else
			begin
				if (busyOut)
				begin
					crcOut <= crcOut[0] ? (crcOut >> 1) ^ 16'hA001 : crcOut >> 1;
					ctrOut <= ctrOut + 1;
					if (&ctrOut)
						busyOut <= 0;
				end
				outWrAck <= 0;
			end
		end
		
		// On a write, reset the counter
		if (S_AXI_AWVALID & S_AXI_WVALID & |S_AXI_WSTRB & (S_AXI_AWADDR[4:2] == 3'h4))
			timer <= 0;
		else if (~&timer)
			timer <= timer + 1;
			
		// Set the Modbus slave address (no change on invalid address)
		if (S_AXI_AWVALID & S_AXI_WVALID & (S_AXI_AWADDR[4:2] == 3'h5) & |S_AXI_WDATA[7:0] & (S_AXI_WDATA[7:0] < 248))
			slaveAddr <= S_AXI_WDATA[7:0];

		// On CRC read, wait until any update is finished
//		if (((S_AXI_AWADDR[4:3] == 2'b00) & busyIn) | ((S_AXI_AWADDR[4:3] == 2'b01) & busyOut))
//			IP2Bus_RdAck <= 0;
//		else
//			IP2Bus_RdAck <= S_AXI_AWVALID & S_AXI_RVALID;
	end
	
/*	wire [35:0] csCtrl;
	wire [255:0] csData;
	chipscope_icon csIcon(.CONTROL0(csCtrl));
	chipscope_ila csIla(.CONTROL(csCtrl), .CLK(S_AXI_ACLK), .TRIG0(csData));
	assign csData[31:0] = S_AXI_AWADDR;
	assign csData[63:32] = S_AXI_WDATA;
	assign csData[95:64] = S_AXI_RDATA;
	assign csData[99:96] = S_AXI_WSTRB[1:0];
	assign csData[100] = S_AXI_ARESETN;
	assign csData[101] = S_AXI_AWVALID;
	assign csData[102] = Bus2IP_RNW;
	assign csData[103] = Bus2IP_RdCE;
	assign csData[104] = Bus2IP_WrCE;
	assign csData[105] = IP2Bus_RdAck;
	assign csData[106] = IP2Bus_WrAck;
	assign csData[107] = IP2Bus_Error;
	assign csData[123:108] = 0;//crc;
	assign csData[126:124] = 0;//ctr;
	assign csData[127] = 0;//busy;
	assign csData[128] = IP2Bus_AddrAck;
	assign csData[160:129] = 0;
	assign csData[161] = 0;
	assign csData[255:162] = 0;*/

endmodule
