`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 			FRIB
// Engineer: 			Nathan Usher
// 
// Create Date:    15:45:52 06/30/2011 
// Design Name: 		FRIB LLRF PED2 - FGPDB
// Module Name:    cic 
// Project Name: 		FRIB LLRF PED2
// Description: 
// Additional Comments: 
//		output produced on ctr==5'b11101, valid on ctr==5'b11110
//////////////////////////////////////////////////////////////////////////////////
module cic(
	input clk,
	input [4:0] ctr,
	input signed [15:0] cosine,
	input signed [15:0] sine,
	input signed [13:0] data,
	output reg signed [31:0] I,
	output reg signed [31:0] Q
	);
	
	parameter samples = 29;

	// Mix the signal down to baseband
	// These signals should only need to be 29 bits (14bit * 16bit -> 29 bit)
	// Need 34 bits for integrator (29 + 5)
	wire signed [31:0] mult_I_out, mult_Q_out;


mult_cic cosine_I (
  .CLK(clk),  // input wire CLK
  .A(data),      // input wire [13 : 0] A
  .B(cosine),      // input wire [15 : 0] B
  .P(mult_I_out)      // output wire [31 : 0] P
);


mult_cic sine_Q (
  .CLK(clk),  // input wire CLK
  .A(data),      // input wire [13 : 0] A
  .B(sine),      // input wire [15 : 0] B
  .P(mult_Q_out)      // output wire [31 : 0] P
);



/*

	//  <-----Cut code below this line---->
        
           // DSP48E1: 48-bit Multi-Functional Arithmetic Block
           //          Artix-7
           // Xilinx HDL Language Template, version 2016.4
        
           DSP48E1 #(
              // Feature Control Attributes: Data Path Selection
              .A_INPUT("DIRECT"),               // Selects A input source, "DIRECT" (A port) or "CASCADE" (ACIN port)
              .B_INPUT("DIRECT"),               // Selects B input source, "DIRECT" (B port) or "CASCADE" (BCIN port)
              .USE_DPORT("FALSE"),              // Select D port usage (TRUE or FALSE)
              .USE_MULT("MULTIPLY"),            // Select multiplier usage ("MULTIPLY", "DYNAMIC", or "NONE")
              .USE_SIMD("ONE48"),               // SIMD selection ("ONE48", "TWO24", "FOUR12")
              // Pattern Detector Attributes: Pattern Detection Configuration
              .AUTORESET_PATDET("NO_RESET"),    // "NO_RESET", "RESET_MATCH", "RESET_NOT_MATCH" 
              .MASK(48'h3fffffffffff),          // 48-bit mask value for pattern detect (1=ignore)
              .PATTERN(48'h000000000000),       // 48-bit pattern match for pattern detect
              .SEL_MASK("MASK"),                // "C", "MASK", "ROUNDING_MODE1", "ROUNDING_MODE2" 
              .SEL_PATTERN("PATTERN"),          // Select pattern value ("PATTERN" or "C")
              .USE_PATTERN_DETECT("NO_PATDET"), // Enable pattern detect ("PATDET" or "NO_PATDET")
              // Register Control Attributes: Pipeline Register Configuration
              .ACASCREG(1),                     // Number of pipeline stages between A/ACIN and ACOUT (0, 1 or 2)
              .ADREG(1),                        // Number of pipeline stages for pre-adder (0 or 1)
              .ALUMODEREG(1),                   // Number of pipeline stages for ALUMODE (0 or 1)
              .AREG(1),                         // Number of pipeline stages for A (0, 1 or 2)
              .BCASCREG(1),                     // Number of pipeline stages between B/BCIN and BCOUT (0, 1 or 2)
              .BREG(1),                         // Number of pipeline stages for B (0, 1 or 2)
              .CARRYINREG(1),                   // Number of pipeline stages for CARRYIN (0 or 1)
              .CARRYINSELREG(1),                // Number of pipeline stages for CARRYINSEL (0 or 1)
              .CREG(1),                         // Number of pipeline stages for C (0 or 1)
              .DREG(1),                         // Number of pipeline stages for D (0 or 1)
              .INMODEREG(1),                    // Number of pipeline stages for INMODE (0 or 1)
              .MREG(1),                         // Number of multiplier pipeline stages (0 or 1)
              .OPMODEREG(1),                    // Number of pipeline stages for OPMODE (0 or 1)
              .PREG(1)                          // Number of pipeline stages for P (0 or 1)
           )
           DSP48E1_inst (
              // Cascade: 30-bit (each) output: Cascade Ports
              .ACOUT(),                   // 30-bit output: A port cascade output
              .BCOUT(),                   // 18-bit output: B port cascade output
              .CARRYCASCOUT(),     // 1-bit output: Cascade carry output
              .MULTSIGNOUT(),       // 1-bit output: Multiplier sign cascade output
              .PCOUT(),                   // 48-bit output: Cascade output
              // Control: 1-bit (each) output: Control Inputs/Status Bits
              .OVERFLOW(),             // 1-bit output: Overflow in add/acc output
              .PATTERNBDETECT(), // 1-bit output: Pattern bar detect output
              .PATTERNDETECT(),   // 1-bit output: Pattern detect output
              .UNDERFLOW(),           // 1-bit output: Underflow in add/acc output
              // Data: 4-bit (each) output: Data Ports
              .CARRYOUT(),             // 4-bit output: Carry output
              .P(mult_I_out),                           // 48-bit output: Primary data output
              // Cascade: 30-bit (each) input: Cascade Ports
              .ACIN(),                     // 30-bit input: A cascade data input
              .BCIN(),                     // 18-bit input: B cascade input
              .CARRYCASCIN(),       // 1-bit input: Cascade carry input
              .MULTSIGNIN(),         // 1-bit input: Multiplier sign input
              .PCIN(48'h0),                     // 48-bit input: P cascade input
              // Control: 4-bit (each) input: Control Inputs/Status Bits
              .ALUMODE(),               // 4-bit input: ALU control input
              .CARRYINSEL("OPMODE5"),         // 3-bit input: Carry select input
              .CLK(clk),                       // 1-bit input: Clock input
              .INMODE(),                 // 5-bit input: INMODE control input
              .OPMODE(8'h01),                 // 7-bit input: Operation mode input
              // Data: 30-bit (each) input: Data Ports
              .A({data[13], data[13], data[13], data[13], data}),                           // 30-bit input: A data input
              .B({cosine[15], cosine[15], cosine}),                           // 18-bit input: B data input
              .C(48'h0),                           // 48-bit input: C data input
              .CARRYIN(1'b0),               // 1-bit input: Carry input signal
              .D(18'h0),                           // 25-bit input: D data input
              // Reset/Clock Enable: 1-bit (each) input: Reset/Clock Enable Inputs
              .CEA1(1'b1),                     // 1-bit input: Clock enable input for 1st stage AREG
              .CEA2(1'b1),                     // 1-bit input: Clock enable input for 2nd stage AREG
              .CEAD(1'b1),                     // 1-bit input: Clock enable input for ADREG
              .CEALUMODE(1'b1),           // 1-bit input: Clock enable input for ALUMODE
              .CEB1(1'b1),                     // 1-bit input: Clock enable input for 1st stage BREG
              .CEB2(1'b1),                     // 1-bit input: Clock enable input for 2nd stage BREG
              .CEC(1'b1),                       // 1-bit input: Clock enable input for CREG
              .CECARRYIN(1'b1),           // 1-bit input: Clock enable input for CARRYINREG
              .CECTRL(),                 // 1-bit input: Clock enable input for OPMODEREG and CARRYINSELREG
              .CED(1'b1),                       // 1-bit input: Clock enable input for DREG
              .CEINMODE(),             // 1-bit input: Clock enable input for INMODEREG
              .CEM(1'b1),                       // 1-bit input: Clock enable input for MREG
              .CEP(1'b1),                       // 1-bit input: Clock enable input for PREG
              .RSTA(1'b0),                     // 1-bit input: Reset input for AREG
              .RSTALLCARRYIN(1'b0),   // 1-bit input: Reset input for CARRYINREG
              .RSTALUMODE(1'b0),         // 1-bit input: Reset input for ALUMODEREG
              .RSTB(1'b0),                     // 1-bit input: Reset input for BREG
              .RSTC(1'b0),                     // 1-bit input: Reset input for CREG
              .RSTCTRL(),               // 1-bit input: Reset input for OPMODEREG and CARRYINSELREG
              .RSTD(1'b0),                     // 1-bit input: Reset input for DREG and ADREG
              .RSTINMODE(),           // 1-bit input: Reset input for INMODEREG
              .RSTM(1'b0),                     // 1-bit input: Reset input for MREG
              .RSTP(1'b0)                      // 1-bit input: Reset input for PREG
           );
        
	*/
           /*
           DSP48A1 #(.A0REG(1), .A1REG(1), .B0REG(1), .B1REG(1), .CARRYINREG(0), .CARRYINSEL("OPMODE5"),
               .CARRYOUTREG(0), .CREG(0), .DREG(0), .MREG(1), .OPMODEREG(0), .PREG(1), .RSTTYPE("SYNC")
               ) i_dsp48a1 (.BCOUT(), .PCOUT(), .CARRYOUT(), .CARRYOUTF(), .M(), .P(mult_I_out), .PCIN(48'h0),
               .CLK(clk), .OPMODE(8'h01), .A({data[13], data[13], data[13], data[13], data}),
               .B({cosine[15], cosine[15], cosine}), .C(48'h0), .CARRYIN(1'b0), .D(18'h0), .CEA(1'b1),
               .CEB(1'b1), .CEC(1'b1), .CECARRYIN(1'b1), .CED(1'b1), .CEM(1'b1), .CEOPMODE(1'b1), .CEP(1'b1),
               .RSTA(1'b0), .RSTB(1'b0), .RSTC(1'b0), .RSTCARRYIN(1'b0), .RSTD(1'b0), .RSTM(1'b0),
               .RSTOPMODE(1'b0), .RSTP(1'b0));
       */
               
   /*                    
             DSP48E1 #(
                  // Feature Control Attributes: Data Path Selection
                  .A_INPUT("DIRECT"),               // Selects A input source, "DIRECT" (A port) or "CASCADE" (ACIN port)
                  .B_INPUT("DIRECT"),               // Selects B input source, "DIRECT" (B port) or "CASCADE" (BCIN port)
                  .USE_DPORT("FALSE"),              // Select D port usage (TRUE or FALSE)
                  .USE_MULT("MULTIPLY"),            // Select multiplier usage ("MULTIPLY", "DYNAMIC", or "NONE")
                  .USE_SIMD("ONE48"),               // SIMD selection ("ONE48", "TWO24", "FOUR12")
                  // Pattern Detector Attributes: Pattern Detection Configuration
                  .AUTORESET_PATDET("NO_RESET"),    // "NO_RESET", "RESET_MATCH", "RESET_NOT_MATCH" 
                  .MASK(48'h3fffffffffff),          // 48-bit mask value for pattern detect (1=ignore)
                  .PATTERN(48'h000000000000),       // 48-bit pattern match for pattern detect
                  .SEL_MASK("MASK"),                // "C", "MASK", "ROUNDING_MODE1", "ROUNDING_MODE2" 
                  .SEL_PATTERN("PATTERN"),          // Select pattern value ("PATTERN" or "C")
                  .USE_PATTERN_DETECT("NO_PATDET"), // Enable pattern detect ("PATDET" or "NO_PATDET")
                  // Register Control Attributes: Pipeline Register Configuration
                  .ACASCREG(1),                     // Number of pipeline stages between A/ACIN and ACOUT (0, 1 or 2)
                  .ADREG(1),                        // Number of pipeline stages for pre-adder (0 or 1)
                  .ALUMODEREG(1),                   // Number of pipeline stages for ALUMODE (0 or 1)
                  .AREG(1),                         // Number of pipeline stages for A (0, 1 or 2)
                  .BCASCREG(1),                     // Number of pipeline stages between B/BCIN and BCOUT (0, 1 or 2)
                  .BREG(1),                         // Number of pipeline stages for B (0, 1 or 2)
                  .CARRYINREG(1),                   // Number of pipeline stages for CARRYIN (0 or 1)
                  .CARRYINSELREG(1),                // Number of pipeline stages for CARRYINSEL (0 or 1)
                  .CREG(1),                         // Number of pipeline stages for C (0 or 1)
                  .DREG(1),                         // Number of pipeline stages for D (0 or 1)
                  .INMODEREG(1),                    // Number of pipeline stages for INMODE (0 or 1)
                  .MREG(1),                         // Number of multiplier pipeline stages (0 or 1)
                  .OPMODEREG(1),                    // Number of pipeline stages for OPMODE (0 or 1)
                  .PREG(1)                          // Number of pipeline stages for P (0 or 1)
               )
               DSP48E1_inst1 (
                  // Cascade: 30-bit (each) output: Cascade Ports
                  .ACOUT(),                   // 30-bit output: A port cascade output
                  .BCOUT(),                   // 18-bit output: B port cascade output
                  .CARRYCASCOUT(),     // 1-bit output: Cascade carry output
                  .MULTSIGNOUT(),       // 1-bit output: Multiplier sign cascade output
                  .PCOUT(),                   // 48-bit output: Cascade output
                  // Control: 1-bit (each) output: Control Inputs/Status Bits
                  .OVERFLOW(),             // 1-bit output: Overflow in add/acc output
                  .PATTERNBDETECT(), // 1-bit output: Pattern bar detect output
                  .PATTERNDETECT(),   // 1-bit output: Pattern detect output
                  .UNDERFLOW(),           // 1-bit output: Underflow in add/acc output
                  // Data: 4-bit (each) output: Data Ports
                  .CARRYOUT(),             // 4-bit output: Carry output
                  .P(mult_Q_out),                           // 48-bit output: Primary data output
                  // Cascade: 30-bit (each) input: Cascade Ports
                  .ACIN(),                     // 30-bit input: A cascade data input
                  .BCIN(),                     // 18-bit input: B cascade input
                  .CARRYCASCIN(),       // 1-bit input: Cascade carry input
                  .MULTSIGNIN(),         // 1-bit input: Multiplier sign input
                  .PCIN(48'h0),                     // 48-bit input: P cascade input
                  // Control: 4-bit (each) input: Control Inputs/Status Bits
                  .ALUMODE(),               // 4-bit input: ALU control input
                  .CARRYINSEL("OPMODE5"),         // 3-bit input: Carry select input
                  .CLK(clk),                       // 1-bit input: Clock input
                  .INMODE(),                 // 5-bit input: INMODE control input
                  .OPMODE(8'h01),                 // 7-bit input: Operation mode input
                  // Data: 30-bit (each) input: Data Ports
                  .A({data[13], data[13], data[13], data[13], data}),                           // 30-bit input: A data input
                  .B({sine[15], sine[15], sine}),                           // 18-bit input: B data input
                  .C(48'h0),                           // 48-bit input: C data input
                  .CARRYIN(1'b0),               // 1-bit input: Carry input signal
                  .D(18'h0),                           // 25-bit input: D data input
                  // Reset/Clock Enable: 1-bit (each) input: Reset/Clock Enable Inputs
                  .CEA1(1'b1),                     // 1-bit input: Clock enable input for 1st stage AREG
                  .CEA2(1'b1),                     // 1-bit input: Clock enable input for 2nd stage AREG
                  .CEAD(1'b1),                     // 1-bit input: Clock enable input for ADREG
                  .CEALUMODE(1'b1),           // 1-bit input: Clock enable input for ALUMODE
                  .CEB1(1'b1),                     // 1-bit input: Clock enable input for 1st stage BREG
                  .CEB2(1'b1),                     // 1-bit input: Clock enable input for 2nd stage BREG
                  .CEC(1'b1),                       // 1-bit input: Clock enable input for CREG
                  .CECARRYIN(1'b1),           // 1-bit input: Clock enable input for CARRYINREG
                  .CECTRL(1'b1),                 // 1-bit input: Clock enable input for OPMODEREG and CARRYINSELREG
                  .CED(1'b1),                       // 1-bit input: Clock enable input for DREG
                  .CEINMODE(),             // 1-bit input: Clock enable input for INMODEREG
                  .CEM(1'b1),                       // 1-bit input: Clock enable input for MREG
                  .CEP(1'b1),                       // 1-bit input: Clock enable input for PREG
                  .RSTA(1'b0),                     // 1-bit input: Reset input for AREG
                  .RSTALLCARRYIN(1'b0),   // 1-bit input: Reset input for CARRYINREG
                  .RSTALUMODE(1'b0),         // 1-bit input: Reset input for ALUMODEREG
                  .RSTB(1'b0),                     // 1-bit input: Reset input for BREG
                  .RSTC(1'b0),                     // 1-bit input: Reset input for CREG
                  .RSTCTRL(1'b0),               // 1-bit input: Reset input for OPMODEREG and CARRYINSELREG
                  .RSTD(1'b0),                     // 1-bit input: Reset input for DREG and ADREG
                  .RSTINMODE(),           // 1-bit input: Reset input for INMODEREG
                  .RSTM(1'b0),                     // 1-bit input: Reset input for MREG
                  .RSTP(1'b0)                      // 1-bit input: Reset input for PREG
               ); 
               
               */     
  /*	
                              DSP48A1 #(.A0REG(1), .A1REG(1), .B0REG(1), .B1REG(1), .CARRYINREG(0), .CARRYINSEL("OPMODE5"),
                                  .CARRYOUTREG(0), .CREG(0), .DREG(0), .MREG(1), .OPMODEREG(0), .PREG(1), .RSTTYPE("SYNC")
                                  ) q_dsp48a1 (.BCOUT(), .PCOUT(), .CARRYOUT(), .CARRYOUTF(), .M(), .P(mult_Q_out), .PCIN(48'h0),
                                  .CLK(clk), .OPMODE(8'h01), .A({data[13], data[13], data[13], data[13], data}),
                                  .B({sine[15], sine[15], sine}), .C(48'h0), .CARRYIN(1'b0), .D(18'h0), .CEA(1'b1),
                                  .CEB(1'b1), .CEC(1'b1), .CECARRYIN(1'b1), .CED(1'b1), .CEM(1'b1), .CEOPMODE(1'b1), .CEP(1'b1),
                                  .RSTA(1'b0), .RSTB(1'b0), .RSTC(1'b0), .RSTCARRYIN(1'b0), .RSTD(1'b0), .RSTM(1'b0),
                                  .RSTOPMODE(1'b0), .RSTP(1'b0));
                              */      
           // End of DSP48E1_inst instantiation
                        
                    
	
	// CIC filter
	// Integrator needs to be 34 bits (truncate later)
	reg signed [31:0] int_I = 0, int_Q = 0;
	reg signed [31:0] comb_I = 0, comb_Q = 0;
	reg signed [31:0] Iinst = 0, Qinst = 0;
	always @ (posedge clk)
	begin
		int_I <= int_I + {mult_I_out[31], mult_I_out[31:1]};
		int_Q <= int_Q + {mult_Q_out[31], mult_Q_out[31:1]};

		if (ctr==(samples - 2))
		begin
			comb_I <= int_I + {mult_I_out[31], mult_I_out[31:1]};
			comb_Q <= int_Q + {mult_Q_out[31], mult_Q_out[31:1]};
//			I <= int_I + {mult_I_out[31], mult_I_out[31:1]} - comb_I;
//			Q <= int_Q + {mult_Q_out[31], mult_Q_out[31:1]} - comb_Q;
			Iinst <= int_I + {mult_I_out[31], mult_I_out[31:1]} - comb_I;
			Qinst <= int_Q + {mult_Q_out[31], mult_Q_out[31:1]} - comb_Q;
			I <= I + (Iinst >>> 6) - (I >>> 6);
			Q <= Q + (Qinst >>> 6) - (Q >>> 6);
		end
	end
	
/*	wire csClk;
	wire [35:0] csCtrl;
	wire [255:0] csData;
	chipscope_icon csIcon (.CONTROL0(csCtrl));
	chipscope_ila csIla (.CONTROL(csCtrl), .CLK(csClk), .TRIG0(csData));
	assign csClk = clk;
	assign csData[31:0] = I;
	assign csData[63:32] = Q;
	assign csData[95:64] = mult_I_out;
	assign csData[127:96] = mult_Q_out;
	assign csData[143:128] = sine;
	assign csData[159:144] = cosine;
	assign csData[173:160] = data;
	assign csData[178:174] = ctr;
	assign csData[255:179] = 0;*/
endmodule
