// Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2016.4 (win64) Build 1733598 Wed Dec 14 22:35:39 MST 2016
// Date        : Wed Feb 14 11:09:49 2018
// Host        : SHRIRAJKUNJA80D running 64-bit Service Pack 1  (build 7601)
// Command     : write_verilog -force -mode funcsim
//               c:/Users/shrirajkunjir/ip_repo/LLRF_axi_per_1.0/src/mult_cic/mult_cic_sim_netlist.v
// Design      : mult_cic
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a200tfbg484-2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "mult_cic,mult_gen_v12_0_12,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "mult_gen_v12_0_12,Vivado 2016.4" *) 
(* NotValidForBitStream *)
module mult_cic
   (CLK,
    A,
    B,
    P);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 a_intf DATA" *) input [13:0]A;
  (* x_interface_info = "xilinx.com:signal:data:1.0 b_intf DATA" *) input [15:0]B;
  (* x_interface_info = "xilinx.com:signal:data:1.0 p_intf DATA" *) output [31:0]P;

  wire [13:0]A;
  wire [15:0]B;
  wire CLK;
  wire [31:0]P;
  wire [47:0]NLW_U0_PCASC_UNCONNECTED;
  wire [1:0]NLW_U0_ZERO_DETECT_UNCONNECTED;

  (* C_A_TYPE = "0" *) 
  (* C_A_WIDTH = "14" *) 
  (* C_B_TYPE = "0" *) 
  (* C_B_VALUE = "10000001" *) 
  (* C_B_WIDTH = "16" *) 
  (* C_CCM_IMP = "0" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_ZERO_DETECT = "0" *) 
  (* C_LATENCY = "3" *) 
  (* C_MODEL_TYPE = "0" *) 
  (* C_MULT_TYPE = "1" *) 
  (* C_OPTIMIZE_GOAL = "1" *) 
  (* C_OUT_HIGH = "31" *) 
  (* C_OUT_LOW = "0" *) 
  (* C_ROUND_OUTPUT = "0" *) 
  (* C_ROUND_PT = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  mult_cic_mult_gen_v12_0_12 U0
       (.A(A),
        .B(B),
        .CE(1'b1),
        .CLK(CLK),
        .P(P),
        .PCASC(NLW_U0_PCASC_UNCONNECTED[47:0]),
        .SCLR(1'b0),
        .ZERO_DETECT(NLW_U0_ZERO_DETECT_UNCONNECTED[1:0]));
endmodule

(* C_A_TYPE = "0" *) (* C_A_WIDTH = "14" *) (* C_B_TYPE = "0" *) 
(* C_B_VALUE = "10000001" *) (* C_B_WIDTH = "16" *) (* C_CCM_IMP = "0" *) 
(* C_CE_OVERRIDES_SCLR = "0" *) (* C_HAS_CE = "0" *) (* C_HAS_SCLR = "0" *) 
(* C_HAS_ZERO_DETECT = "0" *) (* C_LATENCY = "3" *) (* C_MODEL_TYPE = "0" *) 
(* C_MULT_TYPE = "1" *) (* C_OPTIMIZE_GOAL = "1" *) (* C_OUT_HIGH = "31" *) 
(* C_OUT_LOW = "0" *) (* C_ROUND_OUTPUT = "0" *) (* C_ROUND_PT = "0" *) 
(* C_VERBOSITY = "0" *) (* C_XDEVICEFAMILY = "artix7" *) (* ORIG_REF_NAME = "mult_gen_v12_0_12" *) 
(* downgradeipidentifiedwarnings = "yes" *) 
module mult_cic_mult_gen_v12_0_12
   (CLK,
    A,
    B,
    CE,
    SCLR,
    ZERO_DETECT,
    P,
    PCASC);
  input CLK;
  input [13:0]A;
  input [15:0]B;
  input CE;
  input SCLR;
  output [1:0]ZERO_DETECT;
  output [31:0]P;
  output [47:0]PCASC;

  wire \<const0> ;
  wire [13:0]A;
  wire [15:0]B;
  wire CLK;
  wire [29:0]\^P ;
  wire [30:29]NLW_i_mult_P_UNCONNECTED;
  wire [47:0]NLW_i_mult_PCASC_UNCONNECTED;
  wire [1:0]NLW_i_mult_ZERO_DETECT_UNCONNECTED;

  assign P[31] = \^P [29];
  assign P[30] = \^P [29];
  assign P[29:0] = \^P [29:0];
  assign PCASC[47] = \<const0> ;
  assign PCASC[46] = \<const0> ;
  assign PCASC[45] = \<const0> ;
  assign PCASC[44] = \<const0> ;
  assign PCASC[43] = \<const0> ;
  assign PCASC[42] = \<const0> ;
  assign PCASC[41] = \<const0> ;
  assign PCASC[40] = \<const0> ;
  assign PCASC[39] = \<const0> ;
  assign PCASC[38] = \<const0> ;
  assign PCASC[37] = \<const0> ;
  assign PCASC[36] = \<const0> ;
  assign PCASC[35] = \<const0> ;
  assign PCASC[34] = \<const0> ;
  assign PCASC[33] = \<const0> ;
  assign PCASC[32] = \<const0> ;
  assign PCASC[31] = \<const0> ;
  assign PCASC[30] = \<const0> ;
  assign PCASC[29] = \<const0> ;
  assign PCASC[28] = \<const0> ;
  assign PCASC[27] = \<const0> ;
  assign PCASC[26] = \<const0> ;
  assign PCASC[25] = \<const0> ;
  assign PCASC[24] = \<const0> ;
  assign PCASC[23] = \<const0> ;
  assign PCASC[22] = \<const0> ;
  assign PCASC[21] = \<const0> ;
  assign PCASC[20] = \<const0> ;
  assign PCASC[19] = \<const0> ;
  assign PCASC[18] = \<const0> ;
  assign PCASC[17] = \<const0> ;
  assign PCASC[16] = \<const0> ;
  assign PCASC[15] = \<const0> ;
  assign PCASC[14] = \<const0> ;
  assign PCASC[13] = \<const0> ;
  assign PCASC[12] = \<const0> ;
  assign PCASC[11] = \<const0> ;
  assign PCASC[10] = \<const0> ;
  assign PCASC[9] = \<const0> ;
  assign PCASC[8] = \<const0> ;
  assign PCASC[7] = \<const0> ;
  assign PCASC[6] = \<const0> ;
  assign PCASC[5] = \<const0> ;
  assign PCASC[4] = \<const0> ;
  assign PCASC[3] = \<const0> ;
  assign PCASC[2] = \<const0> ;
  assign PCASC[1] = \<const0> ;
  assign PCASC[0] = \<const0> ;
  assign ZERO_DETECT[1] = \<const0> ;
  assign ZERO_DETECT[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  (* C_A_TYPE = "0" *) 
  (* C_A_WIDTH = "14" *) 
  (* C_B_TYPE = "0" *) 
  (* C_B_VALUE = "10000001" *) 
  (* C_B_WIDTH = "16" *) 
  (* C_CCM_IMP = "0" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_ZERO_DETECT = "0" *) 
  (* C_LATENCY = "3" *) 
  (* C_MODEL_TYPE = "0" *) 
  (* C_MULT_TYPE = "1" *) 
  (* C_OPTIMIZE_GOAL = "1" *) 
  (* C_OUT_HIGH = "31" *) 
  (* C_OUT_LOW = "0" *) 
  (* C_ROUND_OUTPUT = "0" *) 
  (* C_ROUND_PT = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  mult_cic_mult_gen_v12_0_12_viv i_mult
       (.A(A),
        .B(B),
        .CE(1'b0),
        .CLK(CLK),
        .P({\^P [29],NLW_i_mult_P_UNCONNECTED[30:29],\^P [28:0]}),
        .PCASC(NLW_i_mult_PCASC_UNCONNECTED[47:0]),
        .SCLR(1'b0),
        .ZERO_DETECT(NLW_i_mult_ZERO_DETECT_UNCONNECTED[1:0]));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2015"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
fPF16TcpNgM9dNC6nyb4WjUK+7bY8P+I62AEEiiM/KOMhIKuPOHBoWeWL2UjxSNO68WLeYIZp8lA
I7rHN/CieA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
E6OKJxjnDRUVVFwAhrQMAtoyRVVpuMKsXlca4m9CcIt6QI8vnYN0tf7gH3uVuxZ90322B7kUeFw5
Pu0UeqAoBaSyysHuDqXazxHy7oyk4BIWChvcrp7LULlVLcL76obtSwsXi1ORVmpdTi5b+AcD+WUo
OP1PSFj5jpodG+LwXm4=

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
x+agogSsgbiI6PGyBpMY8RQCDzLctIr3EaG23mH5kJHlNmNKNolnP54yJ8Y7nIFi6yl6tlyOLMoF
/kxU0pyFmIj8QM0/MArMxPTiemXbDLS2VKtonyK9dDH7VbjFnRWwzK0Ngkas0+nbW3TqGPAY98x3
251QPjQoZCw3A7W9PDc=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
KNs7hA49BKKrboRSEkqIGldOa3ndCnhjRkSn8lL1xFfKUn+p+Wbc09ogKV6YYnPU/RaF1LbzyoE4
udPSNea4bST+08IjO5GAxXqUugcig44J+hzpGKmh7oO0TuyNbYq1CnYcsZXaD9vsmNYz8fBDoW2S
VK/mYa21mBKTOuTdQ1yp3wi73aJ1G9N6Ngt7ovDUrjyd5oNxxNlvWU8JkJDinbEnci0qjZ3Wu9Wg
y44pHUXf6xqwFYJpZ1ZcGRKl83P8p74+pLzt19lw9TPlTfKI++IowVjb6wo36ztNDJS0QjQE5Riv
hwbPU/Bt3S82MVCY5NAA6bKC/8NnoWMbmX8Wiw==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
QaRubtGbYrmCghuFdQuTgTEtoVYYLcPnD5z0C7mo18fwCG17qy0y8mj8xWiwE6bo49IP1/JXSIw7
rTBwHFOVrmbm926sWNrF1r3IHB83C5cstprQ1om7vnkw9XX87SjkscphhkrHmi08jjzW4qX96m61
/ymclz5TlAocMQJGz/jwscvIMOrrbuH4SkWQOLQnRfx9GIOv5Y7PM+w/wuDSeFXsAXz7Ahq3/qmU
cylNfSufW7/zfN4RZB4u+d28AXsuFe03aSF1dpW+uBK1xtNZccvj9h9NMN0cuwxt8ZUlLJw8l6e2
hqRfTTZl1F4qnnrJu6w8h8uEGrmgnQG1AW0epg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinx_2016_05", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XXj6Nc59BeA5Kznlx14IKravf7ohERw7h0fbO7pT7/HsiPDCWh2DlTGpFUcnbNZslPN2RfE0nJNX
WMzLQtaHK4Bm6kxY71OsXEKm7MAIjEdLwOMtJTtlZrbm7chBbSxcW6sjWvI36jk5De3Yct9Ao1py
DpQ9NICUtRTwGG8SAiRkAXRh2Jv3rKvnookQrlVxIkNRSBMSgbwuTbq1ze/KMUZebBWwJNUVIC9r
RV/i9wjYXBOeCCUk+cGDC5uSpwdLXYV9ZxhQUU6C1ufAaK2m4OIUeBqPc2ski2O0qQYQ67c35k50
ynO8H9PTEROPEOn5c37S7feU+36OcOOAsVBTBA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="base64", line_length=76, bytes=256)
`pragma protect key_block
Jo8GgYS54pv3tpCWrzUUqAJ1za96/4yVQijjxV7CGO/WuLqV6I+lIQ5lJf1GPqSrWtwM4OjFrg+L
tH/TaSRIJ4+wfJevX2QJrygzj+Kxy+TvjkpLH8KP63MIPPrnUD9j+FMu7UZyLJFVyxUJl5dBcPrI
3is85YjrajXRBmyu4wMvjArQ84cdsr2HTK2THofgkrEJ1JhqucljlV2JDFbKc0ymsNXVpmX8uj0d
ZTptT1+o17dkvROX0h7gDXU4u+irgJMn1vP+msvga1n2GQPgnLiP9oFujGAg9iSiu9SfO5zvcvbb
Lml5CrnLNaFiF8rqaTJZdAHRtJkRrqeWVYthxg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP05_001", key_method="rsa"
`pragma protect encoding = (enctype="base64", line_length=76, bytes=256)
`pragma protect key_block
FD8/vTweeVchv47OMIw34u/db/FIJfa0QRLCMjKurqOLlX305XxRHgylwdqcrdao/4Pu4bhAwHU4
9o7XaOtf8LLUGBfq22mQ5dypWcFaRiTgB9dh/h7NBo091blXNfl6jMelzqPQN9x8R8iPzY51uTcx
+ZDRVkvJAwa4dhcklZMUBYnlpdAIOLGtmceh9rt9VFTsXwdGCFIueWsKhOmOEZNnu+huNDXdxr8S
rP8YSflVXhPSN3QJePM3wJ7mnv0CPc8nOS1edXshWiNsf11ZRK1X3kd2Vf0qfO65FfDTBPNAECrp
Ze9Zc78SjSGt72XloeUiJiY7Ur2Gw/bIllQR4w==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 7344)
`pragma protect data_block
dcaSwBEKa1VPzinJouFaAsFCh7+PRrH2sA7ElkrDoMQMCqgJNtaGPfSL3wSZX+XfEspezn+JFqKC
0qJrN5ZOerE1XaJlXTDkD7bML05IccmodhPcNabFYn3r60CFYVduKsWVVSX3OJYzTnRdgbMBCsBX
1KktgMMEP4j7M+27bIXClaRqg4Pve82pwuR+6BFp2rbM2JAVLDqNV/iPB8RTLHc7vLFg0qyRGzAU
MCXccfcULGRG+97pYPLO7u/BgolQoe26+W9kQwtGxko078CSQDk1xcVx402MlVvGgqPUucRJ9MNo
BYy8KT12WQObvlwSY+duT9QRgmrSVBG2JKv+jEEtxnhQa2fa0QzVWDWGRMbJXeeetoxo8HCZWgzc
VkznZcJKIhqgsi/C0YBv/XkZDVKdbXdKvQoJmPljEp2LO9BifoaiInd1txAgFWWhcgMw1vg8wbuB
6Aq7YSOJ7UzggkHk83KEOBjywsUBjz7QX5zYxWcE5/4zzCKwSlVFakS3W5Dt17ZxzOpfeN01ApU3
GD8vPEq1Ge2RXtOV/pkUDiEFFWvyDyhi1U5NYbHPXC+U5Y29ozAFvLqx3uRhYelIwqV+JHMg+HD4
FiuFczyMO8MF0LklXkIMQcayN8RL8iF0VDFRMZ66l0/gWO87dORhC5h8vJcDcQNCtqGWyp1+LSyd
KbyGmi1fvFOlyHIZniMklD++wDnalw2uyksbzfCH78tcz36DP4kHqBlysbCR9yNk2iWmkV4fX3eR
r7oshwldxB+0JnDiOPOZk9DAQgEdW71aj+VGmECEaAoju60BIunwFzYjJikE5e3rDQ+IwJLJRGC0
p1Ivp3jPMn1aQ1y/FkKywSvqvrYVUTqa7jqdclKe4uzIKTlpxb+ENL7aliF/+RdWQpp39ocd8+RI
zPu32HWEugHK4Oo8Ctj5/IYi0AkoXRXMaMzkXRCTEQ5n/k5TlSRslqoNp8wfBHzIFchhFse5LiZp
bEEWMox2Q+viauC0ijBFoxUOe38oUj4jspYnm55n4d1uuku/VExNlIuBr/UdbJVq3WgnSKROMJn+
BddFtvhNKvIut2+dAqlMOsSuJzjwOj98Tya28pD9mrYfn6370RR0zrDuOMP3yWSivYusVkKLQs8b
oGCrZoTzDtH7FsbUnEB5J49M6C2jto3/Og7Dba6Zr2P4TIgZDw8Md6Vabhgjt7c8hu2WfqXHsD86
61ZxLIoTiwiOB0fgltydmtE1y1nxMQMwjozYCovVNvGTTRJPF3Hw9NIIkBCzhlLhyc5/4GKl2jsx
ovHuCs1AX/HzpUcLqGwRBC6Sd5CCl4/xlm+AzShKCnMgkvhah6k9T41/NPeJWigWGOxozSzF4MBf
foXQDG6mDne0BQcYulol9xvRngg4VVVH+JP5tp0wd7ElraIQbiPSLyvJ5LFZmrE6zmYD0bdGq2bj
Zh4H6N0vLhNZryk+aDOUlxQUDjaTnev9JIYGCRjCBL/cjnx4B20QZqhohxEqvCcd86KOcFF2PI/A
+jY+t1q9U/hpB3Z1nD0GvIhp1z7ZHh7iWUaLZFqxGDxxx+WDWI/9UdEDI2tuR9MXUT9kNuzGVmeV
vfqqnwf+xpSFF3G0G2fgYWdTjZj0gUW98JO/f/ll5uhypGXuB7/Ai320eRuQ8s+FnVHk8jXpFOa4
XSewkZrwhsatyzKzWDrcIZHDz+kgcCeBtJnLxkU1Z/zLrEoQdAmdW2meTJXxJI3Tl5jaUrgK/w0x
ALDxLv95CaiiAdK/V2r4W9IS0cgvKnhG3SBJbGwEgGTJHJN3duSBB6/dtutttWlNHDR5WVZMV6JW
aKcHjesA1XdAq5QeFOYTfxNtzPLzzIysFpyzAg8+hix2fhOWAWrV8cSeNOZobnlnYYOX1qPopmep
DeP58aksB7FMyfRZ81mkJSYldz3AMkXAl5sH+6CkHDK1u6mNqZVfGW4RhB6+xSkM2ZqdaOyoUvQ/
6tmQNMMKqM1sy/Zh1+Tpx73HexW7RN/tUV+0XA2U4dWBdq+QA8H612fVPUbwQBnD0vMjd6r6zybL
2oFmMEsNiWz4x+Bvlbcr03iqgDVWjXaWzxzU+QhZoLXZ8TQgTDKJX0SHY7o+UnBThIP7mfDqpZJx
IACEB4ln03HaNyR4gmkBK4cTPd1hM+TlMnKssde6wFIPgL6497HcCxKrwz8RebHQgK0OqSLmpkTU
AZpja5gT/3DLNddN7UyGnNjAdCZcvYyEazAcImUcoxchF+BXhRVS5+HGlrVlA6AyyLgzNMPmP3Io
C7FzgfP6xKOSbH5SXYIceIB7IJU71dpiI6t1s9+kFgoGsR3XgA5AV3sLVNPKwOPco/ZxyuA3NZVc
JfygaucN3p05QgwxDi8N6yI9MXyiCVS/6idy5RNBi4Wa8bMt0DtOOSjkqy4+FfSrS3o/STFKXLgv
N7BUIMUJP8x1KPG+m3ci33Db8phRFFO4Gb6tZZvmOeHk33a9Oke6eCgqOHcTLshf0B+FfmpTeQJ6
Wq4Ku4OmjUhDhLYuAT3mTaeeI3jU28DjeyFsF1fWhhJC4nJXsGk9WjCYGHzcMLneQ4N6gJm4Xain
0KVn4i6rc4IpvouPwjp7z/bPK0f7UapuEOR5jXhW7yoVekgdUoG9r5oBTEc1bXdyKK5HCFvDYBQd
fmwAsDexpg8iajvT+ur/KbXspQmZY7NW02mF0QyQ1ZEZzBKTRYTJcPkmT5qNoC/6+Kfne/ebxll6
vRFuhPGXlP4XxRnv8Dod7C8JMTqNs5Obn5qRc/QCT7dxxz6VSumVKobII+ie/hXJspa6ukaAYK+E
jxK8RhQxpQ4+drfA7gfrYZwCxCN4HAybtqVoOgd89VLadRO98CwEkEAJt84ol4YtoaHHtbQOm/zR
ezFifdgaKC+K/YXu/TMEJB83ePDH2iiaO1+eOCwMMZ1ZNVevGU+qto6bY5wXD+FnfukVKA7RBZMM
qiJlwCgO0Q6n/4WETCLXUVIybFqWxpAqKj2DFmdi3V5eWciHICKon1Vrs7j4E3rDagbDy5+FQJml
Ez6w3PVhH5RpL5yMfcMRRtregTm5OfQTKNAHOinU6hSWq47xZKI31egdtkb3dJWru+HIPWPgAtaZ
ByFJMCSkHE9lamv6IawEwufDj8VYf2QHYck/MwRaOfWI+Ax/PXQ1RUx/LSRV3jdP3YxE2BWZdgVq
v2FsY2N5RuHXb6I7bPDk5VzKHJTG3qKRdKoaxqgHJ0lR6LUbyUEnK3Iz0vUvaI1UPKKUg/kvK+T3
BAqdX7XIuRJpTEr/cZFkZS4APBii+n+r9WRUSxWSk+HzcH8xjtbJ0Ei1330XtSmkXGwSFcOD8MQi
B51n+mFOwTjy5NfttkREV9BbtcgYoC61lqjoJg+ZAgm31qXmfXiDY7o8KrdDA8aXslWwvVIxF5bE
M7FlgIBFM51CHwVWiDwxg/qaFCgJcJ9tTfLz4Q15HggHaqTxhLSbrlvTZx6OdW2GUlnOlk7eO8ys
rO3H73LILUzbvy+pZMjTTWz8v2cpujkEhTYP73aHbF/qw7ZQ1Bufx4z/zW7fELCys4yeMBmGvzIL
2E8HN6lU8pbtjytkl6y7tpqL5bocVs9oTmcuLkLpT78AmJTjrFI6fTkrGWbmyWkdik8WMXTzqXzQ
WCAXs+dpGmZu+xnfgbVDPwnY39qDeyQ2WbYGe1K9f45MaPsIggIiAHPz0Vt68lvT3TcWyI9VLdGu
VEPBhf9P5N2ZU4d+knvoLVCizABvPFmEAVgwqq9fpbVEfFSLWLkHpwRxldUfciZytgp9ylRtB0q9
Dok/aUxUHPmszRcHdFGzsUPt70rYrdfGJQBf1NNHhmljb+1EoLqxjb9hCG+RZWcNS3vvGSWI2Sfm
e6bXpzBV0AZIh4M01bqasOwZ65hQamEJ8nLUNQJ+epkc6JucJ2wkZwWahMuER28q5FrYluRf4xNL
drMdOwDomMZhCYqAGBpDkSkn9ONjTi3V0E+TqeoDjpYBjcu3VeGwm1IrsCIbu2+tEPSQDZIaAht9
J9vstq05nxa7cQtpHzYqlR1vkyUY/pXKrGjyDSKRHVApfYsVB5udrl7A0BN7QJPaJlYlQX5+Wpsv
zcEiXX8tTXcFZhyZeekvk8kMznh7N+cIznxU7Kc4/A3qb8ImHfsVeRUH309vjKJ/TGDZO4UJ2Su8
GeOUq7rLXb9wZGrjGN/wunRcJusP8W6P11//4O09X6c0XLNipPkng53rSz2NgdhNju0+erzODNlP
vwLzXVhlzGEn1PK398HEBYMrH5M8uFtnnxXhES+pagMKwExeIfGKrGf0AQecBT7jlEbbvfoKs0Ns
zjNYNT4D++dpmWZbApuxVDEwjXDO3DaLPeHZILfdCrXXellgSKFW8AlgFzkyYgXVci1TsViCNfrF
/bhHnOHX0OFqoXZdB7dLQcPPBi5v6Ffc7x7mKXcdoopRsCjZHK3fbaI9ymqybPNDRSCyTWBaQ4R9
LktQcafikYE3IqQM1gs9nuK0IhC3R/SNLrzS5orNCGCIh9l8HvzRToibDXHYGpGL71MClceoiIJj
pvXHDRZ1Mb4k+ScUJcKzg4B8dw2bK+BGzbWYOHhQNH0e3y0Wuni7u1MkOURhqsCAWMqIuENW3NoJ
w6gqOU8EoMcwxsVM3/QV2qbGvjN3VHgLlwIa/gMBaSDLPHvpEOIeboVLxquvgpQrUSn2yK9L3MkZ
rmN8wesiLMyq5GsBUCIu7x8mb+ruN7uNOfbU4qDpMr2CPyOwml6yCdsYAepzWetyFFAIEXSSYwK+
sXSKpM/A9bUvQ9VHylSzDRobrxOGNNjv0Zxm3JOeZlGD1PRjJJSqe/MMFlap2wBGnE4ICwiVZEUt
A66TGJVVaSFqQTjc8PDi4vtnjBo7fnisyzVbH9L3zMEmIK7fNvUK8OW41FcEmupyQvZpGo6k0onB
2DW4K8aPw2ESPJNvje66FxUT0nb7hhBkq6l/uW0qNSchGuay3NJut/6Iq4QGnBBB+ARWJzoDiftj
RtzTIdWKVY/NaY5VpTjr5v65AccwHmD0eUcT5M+nMFNm3mvVhs3nATR5BHKyROvnfq/kMHuNdfJW
Lox3BMmJGiruKaQqWDLD+GJzhG684uYRw6ac44c1W4mSZAgw3e6Vi+y/vSKT58LfZq+J9SyPOGwn
3Luat2omxc6ODhxtCvLP6Ms+zX3qm81lgeZwp6Dri29Cw1X8+YPc9uNJ0wtBAtZPmUzyMb5dQmBZ
MCxe+mPh0Qhx4qAXbtLAFhHt5aUfQn1txHh7Vtf3orOer6+wQ8Ec0JJZpxwlBOthJ7lp7iW8xl4+
Zwrb+h47UlXRmDxuftHJPpApjcn5DmtFE0SP00r2CrmwWYCkKL2KsdI8hRaVohb++4/ruZTk/2Ml
DGcuIcLpSvXuxU2XnQmS7Bol7jCHxIiAD/z26ces1BYWnRTp/mAZsGuylNTDAeaT21Dv8A8R6zVz
apGpSmBYy961FA6Lz3v5PYh+Rge170A/2G2R/NJ2i3fYuac7wQ9Oag4Fx2PeHAGd360WW+BD/lyt
zXi7tiM5jQAaJgydDMBX/lGr9Oluygrdx2qgOK2LMtyj3T4pUJGCbPq6LOD3pDD5SuDnhyYFgfjR
Xe3pveJ+gluOL3Zh6PhaCaPIoTG6infyHBNzTd2I8pYax84ESOM+hCVvVA7FrFx2ku2fAmGAcf0T
i7bZ1H9gWXojA+HhbO+CRMsmod1hUoz8PUl0FZhHFMF4FTqj7MmSs1+Vgdo5h8LYFUCD2fnvqPki
dbM0bGtikSfb7wRfi4mvSycNtW0a37fO1S0fQAOLL7eFqjauLJlvau+jjm7XD1j75nZk+hCMzpQv
fBUoa/hZdqfG0wdS5fCOPmZWGcacxb3HMF1T88yvkn7OBuz6vMFu6H2n+M1ZR7hAyL1hp/rOI4OW
FJx086ZFVNnWgQvltTAt+f8Dz7Npo0JNXrCwpVpBBfo4zLu39NX5HL4PC1g5+WnkdqAuOQ7fbB8e
jQACB7wuI20Phi5s04MOvjelB2y+lfVNmmnNV/u+3+dB/f4xWO45YeEu6pnd4OqHOl9Vbiqolecu
L6ybl3zExG/6gxgf7JHeIiFmaMFbt+PwEW7Da+cElqNRnHIVYuZmcuDkkCgiXQ7DpZdAYVE5mXQf
pLMCfwSQDl81S5oq7paZunIpEcDJxdUKNa+5yIoJDDoUxNyrMmbyuhxqPvPuFTH22JoKTcj8UU6B
L3w6vKBbrlsVZsFANyH+RJsz5SNmltrpx5NvqbxPTudvCehPOdwZIDgMOjjiDLLycDY1NUevmjKz
CQmhWAzBgvEceYrd7wqtZo1/uznHynnVGqxeX2rUPR5LEOl/4V01cXV5pCPRPQr5Qk4Y6EsHYkfD
K2stFpsRWry6XWCkTybQ8abzLWU9YaLLHuCCYcOzPGRGjHEIRapvm1+JrAjXUH9uu+Iqf9cuqe/e
yNa9e35xFI7W89hpQ0nq46hRTwNqRiG8FvbNIA9YId5TQKutaVyOqTJXdQ1cvTrstf2kcIvowP1f
wLbEjlGI2afF3maHc8Lyo0mvY3c5yBhSg/vmeX4EQONedSvpGXFmlHWELVW8ti7CQ1mHVMy7S6PS
q7EI8HayJaOCMnoIZP1nkZQ45WX97C0eiFks2jeZJJFD1yTOaILJYej4zZrJuN4JZsiG0BRADVLd
g6D7nNBIStvwWPRDEiCa92UtC+mZ9LbpgxKsISTttJVApSeD/I1Y7VtPpGrN6025nLsgIILlmq2C
eBlvm7n7hyBru+4IJdB6k3SNQXovpTGCiNi4kTjHnJBGjyvsgO3tSGpHPTF5RQMzQfa66oyUbBrv
CU8pLP8ub+sih4sRi+k7XY+XBumw4dvP9RJDleyoEneX/tqpwLzKFMHr7fSYIrsO+Iy/SXPZgDdp
k01B3YHi4AkxIjlyHPu9+bhJeOEQ5vk9CB6/le53pUbocz+w9VCx9P59fOM7WZzGv5VW7T5YAxUc
zTd2SJjYHEzC1MtzZbloP1s2R2prs4ygehh+ApViIzsy16HIOf/BufjMdNffd3C+JReEZYId3dyN
1TxLwsJe4OlKBg/+yHzfKPSeMrPrD6MuWiNEeLbmFZduoBMNJPAiWdiFbxnR8jy8RR2rPEDaSgDx
HiL0C0moiMSS/H6qOYd+q6TTtsc8HMco0lyRzo0zVAKLJGnqKNvORJML3jIdXbWeLH/1ayJN0i4h
+rU6AUxcond+zCXhp+1YflJcYAZMMTTmF5NMhHss88Laybl2PfFSB/aUmd8J/XFhzMpRNJFf+KOR
+fkrsY1Gf+PgBn1gntpuFALgDPAGBJEUGBVXFPnNPqINjht4l0djSwPu5WU5TwuNOsunc72XnhPe
TxvpCaA2R66dpEo9/GZuYVTmkwNaCdv4rq1QEDUTyXg3QJF+kWlYuiohP9qKtOyqLXoe+Kug5Cbp
gipv3jeKXNl1mLkz5fbduvMmfWYvih3XzWVMplKbRld8yZS1HpIHSpKtmWu+Y4ag+R4vxgJXbIsQ
BAXlpPLNbzR+D4h9/noA1qtGbAbq5gVry4PhxkF4E+3NXQzI2sUml1O1S58MuI06Qf76+ncVOTHk
fBrpcUJk8fU29IGF8Y17DSAVQHB3RNSvjm743zOyTNIRXmQa1mtZStbiV3HaO6OxDPj+At5DqebF
YpbXhHAfmJdjqaniSsc5TRnPeelQqSwydYQfibuYx40KYl+IVcknt5jTH20GW/GyyPk3BCJwsXZn
LKnx0vSgIbk6NRHzIHlUo+vIeFwprJfZoZzCt8NsQ0h2nxWwYgHL87jOv2YljKLH8Dk6GQY7dNiY
a22A4S8fyWstEGwuW8XNC7M9SNycSBj9RUs91ZH9oG3v+EWO5alZa7aosolkndBDRBQfq7cobswE
ldhzo2c6/+OT2G+sgTFJujEejQWvL9GY+d2MLOb+uf06/chR0M1SOw47fmY71UPNIoXWCdMXtPGB
iEsqvyXho0wTicK3gIrMm8X+2ar2fBQL+NnEMfkcG6JSVRAlXILOJVvzsPxDdBPLz4+Zr8RjaWL4
pjy2Fg54FZ9UYcMAVmFPP2MWp2NfKtuQuWmIhsCbGaZmv6qbFj7zGEdoXI5NrNv/fQGcy7qnRA0o
atyO+53xyZiHZicu2wn7aBtnRwL7mDN4Fpq3l2qYrgZW3dvt/sxhG6Jwwg4cYKkiFIhFGlECa/dm
ECQusQv7h5lIn38c83ExDFNNEUeurgutxabnAKJQDIHICMHIzpBSXpatFy5ebQ1na5EI2g+BIyy7
63PCfPVcqg3O/vR3AQBLeRCdn+k/pt/F7Vw7TaDRJ+vDbyv+wZsoqbcaVJyUo8cTybb6k+lMrCHk
O0sfdaRFfdlAbWQkfI52+ifv+k7rwSCzQAOIUiLVSGssP/3NSQx243qFx1YFeJmMF5mwJv4fz862
RjCbohO3lB6sSVaaUHh2jZ7GXyqSDmUB27AIwWFaykiB0bSNFT+DnWEBKPc4G6252i63/LdsBMeJ
iSbeTeY2K68h/cdOU64RENOkXxxRpajFPITdC7Fgc8WH50J052bx18J8hbN091Gyc1UDLJq0Xua/
eoiBkuaqcFFB49UMicfY43WEW9AusC5i8YlY5Hz3vikyMOobSsfwHArWK2LIvREQIwh6jwFq3xpv
Xq9/zzAMKhNxDzC01/0vEQGVpeFpdb/nA4dcmMFQ9Q5voUGbUczoxGb5Dfgup36tt7xecU+VSb1K
UNyqm+Hsh9MEh4iwwGP3FrTqiezViab37ktCF/dqxoxNYStQrbfC6zW5lSo120fvPK5c3wLsdeYQ
cEy85JL1G4hW29OPBG82reo22CdbdXm/9SEsWM69cMJapSueeWYgNzN6OWwejx3nZwnZuaxE05JL
mUkV4HHL9+F2J/21fKdbotv8c6/U9txhA7kxI7gS1mm583w6wy3EoF42yEbKN0S9gNXc4Jf5Ee1f
1dpbBNUZht9tkkXsh4GQ0G3jOLEA3I7AfvTRN6z+OoCLDM+mZVOoZKnWXt4yYiZ0JkXsR1faTwDI
doGzkqjIbA8G9zTtd9tA6VhAjr4GDkp2PcnwuV0pftnk9qqVCigArhqeaH8tJfiIkl+kJF2aaj4A
59KcFcjcsdl9B4trvJ9QocbjsWEF4Fd+xAGzM6ectmhEC5Azcp4vQHKRK49Oc/hNYwTSSoMXzxA0
Dtx7064IcwRrySgJA+dKnSf1cp85AJSzmOX0W+echDIu0xlyKoam2SJCIbTMwEybtNsbFzMpIMz0
L8peTubLJ3+88kkD7uDrtSBIlUCNANGaW3gERE45swCyTqhcZ58At+nSAIM4uEpzdn47D+bJFvW1
ko13R1PKivYriYs2GPQxDqK/JiX6v5lM+6JquCVy6BSVrhImTtotwjH2fr/mCVO9Rmf5pHRxyW6R
ik0o8WQW34wulWgjvbh6ZuPH7Sr5JELMkMMSiUQviPLdn1SVspGQclv04jgjs8oeHg58/S7nUTMp
1muMYzC0vmmVxkG+EkIVkzwPWs9c71jh0xEh1MynWBR8NUnBgeI4NQ3tJgvXvI4j/q3dUI9DzJiZ
M5nZBF+xqUJpFMOzH5DP1cHJIlTPmtuI5SB5vyG4cU/9o/QFb4A5QYiCNnDSU3/8h2mlZLpKTkWi
kC0qPWBepdgY7qhXFS7gLKzH/4gnjdMiAX1J7kXwr3MnYR6h9ls0QBMe0DG3/H2+io9v1GHryiUL
5x85vTbmIuBYfmZ4EBmbsll8fNgQwSu7wsckYy/FK6AEcPN9fVsLBfjvbfzNRsE2
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (weak1, weak0) GSR = GSR_int;
    assign (weak1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
