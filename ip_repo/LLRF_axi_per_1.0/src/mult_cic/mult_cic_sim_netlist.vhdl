-- Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2016.4 (win64) Build 1733598 Wed Dec 14 22:35:39 MST 2016
-- Date        : Wed Feb 14 11:09:49 2018
-- Host        : SHRIRAJKUNJA80D running 64-bit Service Pack 1  (build 7601)
-- Command     : write_vhdl -force -mode funcsim
--               c:/Users/shrirajkunjir/ip_repo/LLRF_axi_per_1.0/src/mult_cic/mult_cic_sim_netlist.vhdl
-- Design      : mult_cic
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a200tfbg484-2
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2015"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
fPF16TcpNgM9dNC6nyb4WjUK+7bY8P+I62AEEiiM/KOMhIKuPOHBoWeWL2UjxSNO68WLeYIZp8lA
I7rHN/CieA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
E6OKJxjnDRUVVFwAhrQMAtoyRVVpuMKsXlca4m9CcIt6QI8vnYN0tf7gH3uVuxZ90322B7kUeFw5
Pu0UeqAoBaSyysHuDqXazxHy7oyk4BIWChvcrp7LULlVLcL76obtSwsXi1ORVmpdTi5b+AcD+WUo
OP1PSFj5jpodG+LwXm4=

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
x+agogSsgbiI6PGyBpMY8RQCDzLctIr3EaG23mH5kJHlNmNKNolnP54yJ8Y7nIFi6yl6tlyOLMoF
/kxU0pyFmIj8QM0/MArMxPTiemXbDLS2VKtonyK9dDH7VbjFnRWwzK0Ngkas0+nbW3TqGPAY98x3
251QPjQoZCw3A7W9PDc=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
KNs7hA49BKKrboRSEkqIGldOa3ndCnhjRkSn8lL1xFfKUn+p+Wbc09ogKV6YYnPU/RaF1LbzyoE4
udPSNea4bST+08IjO5GAxXqUugcig44J+hzpGKmh7oO0TuyNbYq1CnYcsZXaD9vsmNYz8fBDoW2S
VK/mYa21mBKTOuTdQ1yp3wi73aJ1G9N6Ngt7ovDUrjyd5oNxxNlvWU8JkJDinbEnci0qjZ3Wu9Wg
y44pHUXf6xqwFYJpZ1ZcGRKl83P8p74+pLzt19lw9TPlTfKI++IowVjb6wo36ztNDJS0QjQE5Riv
hwbPU/Bt3S82MVCY5NAA6bKC/8NnoWMbmX8Wiw==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
QaRubtGbYrmCghuFdQuTgTEtoVYYLcPnD5z0C7mo18fwCG17qy0y8mj8xWiwE6bo49IP1/JXSIw7
rTBwHFOVrmbm926sWNrF1r3IHB83C5cstprQ1om7vnkw9XX87SjkscphhkrHmi08jjzW4qX96m61
/ymclz5TlAocMQJGz/jwscvIMOrrbuH4SkWQOLQnRfx9GIOv5Y7PM+w/wuDSeFXsAXz7Ahq3/qmU
cylNfSufW7/zfN4RZB4u+d28AXsuFe03aSF1dpW+uBK1xtNZccvj9h9NMN0cuwxt8ZUlLJw8l6e2
hqRfTTZl1F4qnnrJu6w8h8uEGrmgnQG1AW0epg==

`protect key_keyowner="Xilinx", key_keyname="xilinx_2016_05", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
XXj6Nc59BeA5Kznlx14IKravf7ohERw7h0fbO7pT7/HsiPDCWh2DlTGpFUcnbNZslPN2RfE0nJNX
WMzLQtaHK4Bm6kxY71OsXEKm7MAIjEdLwOMtJTtlZrbm7chBbSxcW6sjWvI36jk5De3Yct9Ao1py
DpQ9NICUtRTwGG8SAiRkAXRh2Jv3rKvnookQrlVxIkNRSBMSgbwuTbq1ze/KMUZebBWwJNUVIC9r
RV/i9wjYXBOeCCUk+cGDC5uSpwdLXYV9ZxhQUU6C1ufAaK2m4OIUeBqPc2ski2O0qQYQ67c35k50
ynO8H9PTEROPEOn5c37S7feU+36OcOOAsVBTBA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="base64", line_length=76, bytes=256)
`protect key_block
BRkpdqm2xY5YkrRyP+vvlTYLhL/skk4q0Lx2Srh3defSuVrWiDQ/WMCG5HoX8/N+wuKtBu+VYq/e
ZA7e1f4QPubf7DbkxNuT4lZG9kZ86TS27zGl3uyDdC4ONmszAb8LEluZcZmBVLbVlaa4SjpnrmZR
AqoFWOzNAsfi5b2M7fU1Jk4dKWEBQ/4SkdasOPHHPN2OURJJICEnD6CWPGMTe2AZ2DnlzqpSVn4Q
aQr1KTK0+p7fCXjcFhYXUUUSlhWJoSL4YKAj9sAISwkagH6rfumXeAA5UChmnl1OhOHse2Vibld9
3VQnt21aVd8SEOKiMaoZZWy/kOgbHV5kGKOtGw==

`protect key_keyowner="Synplicity", key_keyname="SYNP05_001", key_method="rsa"
`protect encoding = (enctype="base64", line_length=76, bytes=256)
`protect key_block
eFMItUwErK8JnnzxdgtxAdPqFiGt8sILVJiWpliM50sFEw+yBhqDhukCwrzINawcN3Zu8KWfgYRz
uF7CwN8NI7EDBk52Bp47l2kva+qVsM3LT5aILHPlHiSDAGJEtJ73egUoBM29rVxvzMju9816a8NX
5iO8HN8EzJCw7UQUudqfEXsIhBorR0fBTwSntFf0kpDlPs3LL9EUYwftJKVLx5toiM9vcH+/Tbev
HYiaQXKfG+CkhKDsrFmBDc+o80jAJ4Oz52ZnCJy4GPECaxROCv4huZPfLOiPwlBB/XVO7M9L/rTK
/6hR6am9v+53lezrGpIWRXLjWMZsWZZz6FImBg==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 9920)
`protect data_block
aZLijipXRNj3CseW6rdLYE5Aehm9MHg0gtif4V94SCyRaY7IUATs7fA80LCPhfsnbVWr7FQFDp2H
HxcimjFhzZFiEo2LVGIJX9hwe3BRRiXwaOdQ2apOLHP6NMBDzb2hyZ2xpoUIYdGYoWs6DA5vnVha
Uk/4fRBpvn/AeWEf9YW0CR3GtVGUWVwSRIM90+nKe2LlKZmBhrp3s/rFYxIXJY6gnn2twgk3WjM4
y+tO77e0rYIwS17KSXFUKST6UgbiNDiP3zIO6Xcp6/7pad5I4f2vVQmbUD3sx0D+D3M/Qe4ml5cR
kQnXDKk6hcwRhzClRBH9XYmlzsoCA09LZIPy5obSwQvAYn9wqWL61gd8g4aIn5gHcFFVS9IV30ps
z0kS5ncNrPrq73xlBOX9TmJ0Uvbc29L00FZGR30RPiBCJwqC9GoRbWn4ULBMTrh9t6IaJ76z14fY
UUJQ+38+k2HnVbvOlj6/KfE6NYpD5WVz4GXXUugv8tRzbxePvxiOjTekAVp74+18D0piqNKaMtpY
QMZJkr0boJvRuRbTCQmDVGpRtx97l89K4Mh2mmMuw71SlZTL9mCectIYbvMYYh25uUT7mDiy4g0G
rmiDTUVS5IYMF4rc4QOVGoOTq4WCcvtVZ//1OrvQKScSn19QabcrwmlNYYl5yXtT1bBLuf+mG6Ws
hj8kvhqC+Vlba5/DM/87hVlhVLkaqaO5oSsbrvApVJ2MjoIvC7e+X0iq5J16XD1WkPc4Aw269jy9
vfH9EZcUac4J7aqPdVGjQdacQW57mHn00ueN7U+YdwXrn5rDIp9/j0j51g1wIRY13SD7jDN5dw1w
d4ckoWkm9tuDARbfSF+a/O+0gqaMJtA3eTEjYr1Hht3pplUUKl8NBoBy2A0osz0q4buFn4USygze
yMsvP3CsTVqlJcjNY44ja5RFx5CLPmk9esQA5ynuLmNWR/dsucPOTbuI7hU4ueVlfvF2/ca5XChc
BCDwil2VbZi1J6lh7svXjfuGiZeZC09DBflj32hOrB0tz+2vbHk84a/ql5PFW3jg/RgICBpyn+Q4
IGvzmteX+RVub+n42LyHdYVNobf2Hl5CmwIkkGBX+G+P6adda2JB35Z7r8TRjemOlDEXRXT/56vs
qM4u1HW9TTrAcXyQAyvErtxjUGnK9ds+XB4E8921t4J2qiz7Hy3OosNWiw2u1eR08RUF93Dsny5R
jIbL9uDs6cxf1JkyDi88cTHilRekeAenR0Vvcg3Hcy/MhiFDzGHHo73VbBI1YMPXEKqzL5HFht7z
Hiim2lZoS0Ra+CAD1Pmvxko6EtILiDN5mzYDz2NgiXINxiP8K/2o9wtzZSV2HZQpEuo8a3n9hugN
io5aqjZ9rJm87TGDOFvZ67arU5ztrEsHii/N2Z+6DfUR4VHurzz5mb700x7u+1iGa9E6VcMVPkMn
POFuFf/o4ZXFxePDCsaHux8JRGrB/IRR9HtJOWSmqum7GrkK4+uHJeUt9FMnd2/yw1SPwJDZSOXx
oRA3ICDBIn6IlxGWKVglGY+VGeHqAQErH3KLqpYePU6q9FbNZV1XgZFhwpwRgmWX9uJA0dlVFHTI
l+ewEygKLBlG/7XCI0QTQUnZQMoHniSSq4MkzBQg8AH9rl4CL7zImmnD2QMpPJSDANsAoPPpMfab
dVFb32nAPNsZGETGdysx1R+8LlyIckB64Y1buitdySjQ4ICaKUA/rh747/C3zmqCttgXfQChG/cn
2ePeimU3P9+x+guxuJzWTjEIKkFU4DcEZP/5TIu1gqlBD66/yc00vtHTlfxasIHdzLlDNqypM2cE
cljl6VXwtWUyC3WR6btxnpEM4WqGjv2Cz0K+GEV059S/onYYsfiAtWxYhj9CT16d0hsfKX1RZc4i
paptpWuiaVQkXY+mB4uA2Tjah/SWhg7Q/Kxry347/j+VFp3lV7cKUChsR8ADJs+OIMK+xnTDBp0S
p4L3J436jtPY8pxULamQeK6SVSK1fZX7i0SOOwklH0qRSmrcgxYorLQDVbwwtaHd9gu9zIaEVhMh
jTtoPvAzG466sR2sOGe/Aiy474Bi6V5HHlp0L55zs59CuNizAm6XFIpY7iMYehgmcoxOQkFW5JUI
TwK0XlEWaFfU1bjOtnr0Co5pRtkc7+vL/D8dB1Ue0S3wWfxKiK/wFXVY+M/ctZEzfb2ssJ1wAVm8
sGgohpQK6oUDBTt0RetSBFmh+yJG5kY/jO8Kmk2QGJg+yHARufmMpPDfnAnLr4R0Hy9dybQZWPzK
pG/wL0ZbgxqOvvcEkVauvXzd8c2EPwivnZCEqenaXryyHFnupcye6ZDF5EvRjKFniTXt367bcTlj
xqzC3Z30Y4wg2aQDIe8G3O41OQ4ec4JGoPDlUerX6DTq21aZiptvOKl0ODKbgF23lOIJ40qi5tEh
/Y1A8Cst5xIbtvbrRfHTgwNOxLbZGqBg46Z/6TOC/KKnSR+jlL1KLTT3qnkyneoTgMCbeJLBBDHD
lT++kVoRWw65kFzRmk/m/4WTP2j8OCij22xUXTUPow6o8f+SW8k8uSy/Sj8r6NztktpZFcFXOkPV
6Br8tMZAc6Q1BwbMwBBaXPJvhwxQQl1MQHorpsr31GrVyxJ95z88ILm+5VySD519amKrRB4PJuUy
o3AJaHKjfNFjlxhmPRQsO2kp4w1XM+LBz7X105lyOJEx5VE3UKyUA+xTxUMootFGFWX+UOsBjRdM
3Ij/5KZGAxfTt5AP2s75m2e1749e6C6R2eyiK12CjleQt7WSp0kHUPp5C4cu+z46oY8q9a68bvUt
d32euIFENLvsIMBXJ7PI4r4/7OcPhLR/bWNivGSWvDNFlghcLWAvFpD8INT7eu0O6xtJUOMKcgpn
LeaiYykrjl8V1gxxy23adRgY6ganQ2ZxXY4ueTl7l1dYP+cb2YoPGU6YRv3zrozKX9ut5q3706le
abqeVx5j34VVFUxmm/Fqa5hBFdHxS7lWzoJAozb9AXFyDNW4l/+eA1K2UGfV2QEO4eNTt5Kq/jCo
eu2EBNplCcLIao1TSLbFn2IzCspeo4cBytjt4CRfEz3Db2aKan87PVWqbzWc2N2m7hsap/FpnsxB
QeKaDxC0TvkLk6mvUsO7IvjRSw1vVJWNz2unn2Wk14VcRJSrYyZgGzB4TeuRH3V7Yo0zhA3kGivK
nm5Dh87KMsgo/mwEgThk/FdcO5nB4rJ1Hq47/l8+tHpE/D2k23osgaglYQGeDfKY7VS49N0fbUcA
EiVl3dSz9DxqYsnKalyvND2l/GTk0btTJC9RPFI010GZmh3XAkdKwKxouWwiaZTX3AguOHdmOsol
/9fNLD8zj6AihTjrXuHOwgIasoRm6gdzM+2HPQc/zkDMa6CE8v+jgMlpSgFN2l6UwmFw21xFj0e1
7FZIjOJqbPkr23XHzL9+9JEfEWOPUIiLR8QQeDlsgsh4EQWAxaMM21hMxudY5ftpXRDTqCvExb7N
zQtWtb6e+5ybAI0AAlneJKjvupES5OOnR1vvK+nyrbJEqy2lUBj+o9bvkXjJ3/DKYvpPrVP/MeMT
KHA7DlGiekINJOltkChaA1fsSxJb1RcIFrAESJayN8F/6ihLi1F0rqCITpEZtEfMm7PoQHGemc5g
WJwe28UbRMxWVHqab1KcanKVEkX0JSFHwFsbVS7h6ijGw+MtNn6CtQRNRF4WrGQmZKMs9xivRAPK
v6teyvdlhpmayi5b00d+l5fXuGqme+FLcbfLbskTfMBv9PTViyWA9F+PzvfvWDZixI52MGiCeubp
GM44hK100f4N+QuaZT7Td/0BhHCqRy9egjR86RcoNAiCw14I6zVTSIyhSnKPkoU87tbODewUiivt
j6Q4YRz1mvL28/qf0hXzwafpsS/QYvBEx6Jgd/W4uacwqVPGOQ5c5KTfB6f+GmrN3gbzuQQU3ceF
Qr0/GWVOy86CR0Jy8hpKSQNO/2wxsFq9tg0gy5rhtXqLIvfrgWn4BmQR+ZE9BeXg8lb3TvCI4yZT
IJVs7LC5BDw7y5uH2PtUsoKG/cUaGO/jmroXuUbQPstX35HxrZGP/6Opx9GM/Sas8fBmJ9g3mAUZ
xRTXR8dG+1UqaxIQaaj1teYfMelOpdPVvjWPV0bRrMX8MMKCInjMbdeaSgYk18ffxB+JOq1G1koY
x3QanXF81qT2rU/7o1xBfC6CBQcomI9Lk9AmHs7/hxtJ6XfzJksXHrgwVAoEkYEVZVgadjBtDWxr
aNps1YhJXwxRTkpJj/R3M9Quwnir2NUZ9rsjYvvrWT5XRbAr61HdNa6Spze4jUbGS0/U0HiPiWgZ
lncOr/0Nv2nfNO7uZJMCpMOgoJMXp997DYw/pCwVRlBwi3XOXXaTQJfq19YLGK18qWOLFKKX9f4V
fBXc0abHRjITVHYB2u/3X09zm1FSrZ9vcXC65d1FFv1r1rwr5ivA2Ue66AK8DTH07wirbyUgeVPB
udXLIDGZ44aONMh5AbdQDgzYffvSGWGZCXqf+BTYMlm6kkRijvRY/zec7P9V7S0wlbFiHB4VqnfG
vpb13y5G0sOM+Zqxf1EvOMhMjXjD9GHD9QwAyu1Q8ASne3Nvrnp9k7zlPNF279Ie89GSsRk7AhMh
z7zQlejmR5u2zGHZxMcepj4jbGi45ELufx28PPWn6ZOfX9zC7X2ssJcpxPudyMZ6XlzCtopp9JcB
w4yw/4c9T1+z71bvjkvTyNUvCmxAriHMy9QgAZ80MxyUnPA4+zWrnlaFWLF/7e0iD4AYQohnvqx6
EPSUQ0IBVQaxt0tB1Oj4Wvx6qxm8SaSc1mere1nxk+8ldx46A+DOgxSjAJ649uYiRmbItsODgVKG
3NN2ZuwGC8DO2SeePTXIhAhc/CQBoU87chYCWIoBdAoKfOk7Q88rNClot/a6EtI+AYodP4vRfOdC
SXMTjYUvxEoL/NZxFEoLPQELroK1fujp6a04N20xw4vOjA6iP4lggmq8gTY1mhikBD6q7IgmoGBG
1RYoTklX8WtELPNKK/zBvrCOrIpDPJtnjUPXIrFfhUyR0yBWTUSXFiUZXOfaza9SM91PUtzl4/hR
sCnwJQnrw7m85ZO4BvCJ7IY4B3PJV7wlHLz8rRpQXZcZTpKLapO9GndGahG7sVli3eF4L4yF2pqh
YthfU+0Oo3hIEDxjNXiWAGVEeNPyQoXxPmWriIrnPt5akfy/lXchkdHcc2D9O459yubCplT/bns2
5TdgV7cmuwKb2vTDf8tyBXPaQaN6pt17U/YOmEDyni/USH6iadaEOtp0AaAMr8ePtqyHWL4LHF96
NXaw0GUDGGXbSmS4dahZ3eMcrd4cOw0pYaWAiDiNQzRpXusMhfD+GqGQFgIbL0HZP35iQB+qsaT3
HaT2jhC09LTlBisrVy3MMjgRgBELjOfJQAkDuNBPU0UohMWfn41nrm6Gqzlz/TOrxkqU600SW+V5
0UJBw1xk1dvz6EcVn1NMxmgJ7h+LaOnNx6Fg/WW+nkSm3X4CisrFL8GNH2fVaknuYn6L5OfQ1VkS
/RRT8/01W7rwz2wIrd245wm207HM/0nPYM+XOvi8EdxV+/2JU1tyxvnSkBAfsTBkN4+kRigwFdiy
T42USHo3Zk+AerOfq7ihTDZoYYxu6N6eMtFmcr5fGa59BWTEmeoBrSsRJvjjC4cEdVfV8ZhcMvOp
tnIoPM/VdoPadUI6sFnh/ma/PTDoqXlFHX4s8pPrXpFslbXRnHE4cv/WVrMCyXt0r5FOSTZYywjX
3ccPJMW26kdkaApCQ/1PJxnIhsUeSIkfSI8dCvhuWzo0KGG7ZimCdPJn1CeycKdD48FhJa2/jHif
uwbInYhKz5+Ci5DJTp12YOS/3xycE9wASTNyLEwCLe06Rhc7ANyUlQBfVJiBVvwsSawOQciVRE3O
f77kiPn3amwCMKRV7aqLCOLt2Iqr9uwavKA/hIjjDYfjY+iOocj9xa1NYVXMh9xSGAobVcxDNczz
Iq7P48aIr/BlET+GBNYDLnZV325puMmofXdaihDlK8GbtUNvKCiA7zJfK06SIdQ4XwZhu/VVxMGF
tULavP9hboiEZN2myRVXhb1RX8PetM3GsEnmksv/Ap94u0qiR1VGkmOFlEiDvAJaxeKpHKsnDjGe
ZYBKynimnQ9JJ9W0gwOdX2MnfyDR5O8EOfwVCbbngRiqlx8hZKUMXAC59+7ExEZgNb+Z6ewA9W7I
n0q/wMR2JpYWAu261QSV2FTs4PMLba0ZqYwGZDwHTOb5dQGepSHCfQ8/k/2lSmB+ZT9vULLehHVL
fXAG6zpqiqDcpNKce/IKKch8w+ydSUmxlIpCskLzICZBZmmDSahlVgYsrIQku+LZev6bWObZQrlR
Til7+Y6pHT29SZtPVYghol0J7h3+Toorycon6fZokFIhloPznmE/yZb89eOCEMS5sxK8K/3qbfg6
hZn4fzc9/Ii4NSR4uhWJnD7joKrgg+sW65IHByhnD7/KMWDNr7MUeNQyGGZKgeh6CoM/KperV4I6
G7duTHoxxWtPL0I6LMRTT7zPrV2W+7vqOm//Uei+R+rkks3ySEeSr9vMGUy6tW0C3ECk0SsnoaNT
rQMyqO97Ak2cHfwC3rzp9IFBF1XqxxQoGKnsdNRK3G6cWLzTIuPGEc5vjPhLZd1oaV5kmWo7jfMH
XhoJRiqXZdAYpL92KKtrzv/zr8xgqY2CBQ2wf+kva9I9smw1OLvEX6rl8EqoSZ0B4z+MePDT3zi3
N1bW2bPpUUArSyZpC4jcMd9ksJ2ennzQ282Bz5ii2Gtv7/C0SKNTIRvBTYvp6JUJnLoG7N5EB376
smQLBduM9mxbpnjNNeLeTIPx9lb3QUQaAi+U+FNn5dkLNa2+GytJLV2IeAZtATApOEezVxwr88F2
e10hIaTUtrPV1U98nDFgWnrGchvBQbzXmroWReh7zLkPkaRyqZaXB9Mig6/kWqAPDYEIYxCHvlC2
Xr+1kg3ep/P0AlMTNsPV6hHfeB8d3mWAZ9dACF/dATFTZotlEZskaMzD3MBqE/C6Y2mU8fXErNt0
n0dDl11J2aGSMFSCzF28ZxaWZS+4YU5k+8oZLy7lvDbj5itsLXmh+mS4OhqZAFRmcLX7AG63lzMn
sVMX4fYw8HIIByFmuA3l9C1SKzcwd4V9Ucb/rsHqM34HNfe5c7TXA1gGoaaIj+yo6JL5l/+ZcKqt
0cuMuXbj7KijMOXUbYCm1rc9RilU4Cr0DWtflbU/8PSRQ76H4quLR7KOlnHAjzF0PXTH01ZTB7u0
1VD9FVfsdPTiifM0XbAgMNyGtLzlYe7K64aO+wOfAveOXhevMl7bx8nZQaI34CPCvGwi+zBfCmYV
JmV91jTJG4SyfXTvcXn3PdQWA2L5/TM3/TnL8x0ZBitK6DKVI1IHCzcJ2MJxTVbf62dSTCu71WgJ
fatuQgX5sYDCMYgdT9EaXnegNmVrnJlka67ZCUhLzDfRMJfVe/4GEfGENJjqtktd6bPihZoFV4sr
dNNI0AxLxo3173EGg56r2TyaKaU0WZyNo9lkfCwEq/rPvzFoFQMqmkUav0jNkfY6fG3Y9bhQWiPo
AeyiyB86Xh/M9nRPYTvmjaON7FtsqLBfJuOB6XWpBSeEJzqmVVbnOXp2QnFLFV39Ukfc4etUf4xx
vetwepKoRxp4v+sylCPeCMqXEojxyfjnhcfp0ZrDkyuDKG0QBkDM/SoZWW2tmxyPlypkSLKH3CCX
wnjDfmsu2U+7pQ+A2c3dvwZzU5tAtPCoUgjPrWUVzZtn3jLDAbOo1sG3IsCmMxxiBvrG7zLpuJyt
BWFEA0eJcVXI4y7xp3yTZnA4+K2ukjkeM3CfQRVphJXU/V4U/2wahtRJhKtYS/WbTjGkvAYk4v9i
FBgEdqXvayw+PEHfDaNX/i0aXdLPPSPsX++i+60VnzW8Yqf3+ELIRun2mmr9/dFjGgK7sWuD5AkP
KT8BZjHsJDhsq2U7kJGVWTqJtmgtnobN2xxJUUCpJ2cXLOTzxiX3MZSP1LT2QvSggJEZU1WUNZOt
49ppeiLdAE563Nr/nEgRHn841AmvASBV96FUyZ00eGy7333Ra9WH5WOQ4/sHj9RL+lEN5PiRbVlU
DR0RSFu7o3LShsJy9BnOmYGRYmKbpiHuJvZnR2V5xPBP8lJP58a4peFZA691oWExPC0LyFQlsXjr
yXPg46z2ac3JNmNKWE1ARM8kEH5mA8dAPUYXeRR4UTmGsTB2ZRADYuLAC/wfSw0fDiRklGiJfmH3
zzYf2/o0rJjKBnb8hI5PokHwDZ2fRZXl4a+z6tg+nUMACAFdQcUmp5dkQ2smDrh3bIsaaSVW9vZ2
KiyjnlG534axRXUUXCyfa7Wy0dv2xxNqGgCG1r+DvP3gObacxEaSA3K/OV2LRU8eeRnIJ+91tnZg
YFt+TMqzvnBEzHxyJuDZ72tEsFwaCgl7dCN+F+QlLibAju5GlaUJUjW8cUFpFMjyN60qNGWX1STH
c4unPIvMy6HBQK7QKHcC8KiWPGp8Uw90ZwoYWFWDtx3ZlA+rFaiJjZz6IVV8xl1X6kWW6OLnJwmb
Xdt9OV8owlC6iZ6PKN6xJnJi28iyOWnySf0EzIn2nbAJ4CIflc9lnjWxdXhT7x31xi0gWyZHn7c8
HGhKCduAPGm9+J3wT8Bb0ZyxxIYgwyBpGI5YGvLTeev64QVJBC0C2FhL1yqd4f1JF8c0TM0UKr5Y
dyVAnvO+OHPiGShWGpgKQKL8HzA4hOzLQp9z2o7bY4nYrEnbWX60cYbQ6KOu/Ya8B6SCG9p6BzuE
CGwGGPGZg+Be08DJwjxRJU1mO0LZ6JxILYp0TFaQNcvjuO4fx/YJLxqaa5vQ+Afli7WDcuuLvEfv
lyiD0Mcm6XRl2z2yCfofT9CEJYXmhSJVjxu5OD5BkibcKlH+yxEYMCyBsUsygKi4CUDUAWfzqges
dH8LpXznOUCYN37nVm/umTadleSTPz9s5YlzdtTu4eoPJh1aQm8f0/k4uhdk+3ZSEQZDDw2YIsxF
kTmhEdzDVCNN+96BCf8bwKPCRiKYkTxvUOjaly3OCmVm+r+CHPYPn6mQDEXSuPlrzR582SG0xj5P
9JWGALbCHqCb9WJJb9yuqBmO5tpXk4N8oxD8qJjcMVjfcn+BpTma4iZ8vqyy+mraSech0dLKXlCJ
0LIzhAzoRcfXK+jSizX4sQojTRQPZLooB1s2vhgSs9tluwh/+vECbxkRZkrapjKRuxT9nXr0Wpqr
FoGZPRt56S7KiKeHQ+Gxz9BVwwOvtWFT11/8Y/GgIW7Khg0VyClTOgmtDt0ph/K+ZG14mC75xJPN
wsBV6ukgGxs7MO7Jp9ZiBF1fgJVMX2Uwlxymk5BgkX86GFkWcXjy8FAqW8p876aJiKJYBuL2fOnD
riWAl1AQanWl9I4r7eUZJeET8ow220iSo0XAop7K+os9qnqLDVXf5PGt9zhNpiB5aPmFRUUS/Sb+
7oMt3MB9ALrBBwM0Tsm64lSS9emuYg4kONj9dpXklu8hHFy6XAgfWhaHUCUwCl3CQo9bKF262t2/
CRp7UU/8x8fhK2FdLRSMlGsztzSvV3eSPlyvw0L3wzAq/E9Cjhwr6wj5zsGT5pZ5YFzxVbRuWzvJ
QOHAI7ta3DvRCzMIf7cWp+irTMQz9udoVqp6ZZpafOtvUMeAoL2kt9nyG3yLsErmul2g7ZYGa5/Y
p9hbkMTfW/2+ERpewoAJbaUanWGS3gjoeakPukTkjnpCFRNehPrRnF05odHobx5ewc7cyTP8ZjnZ
1wIz3ECfA5w2MK22w9icLatT8nxJBrN3EgVbdOXmh35A91VOfKXrARql+RDoJUjRGY876t3dNWwY
Urz/fFSjGMpcnbquwATHNvQgnJUkL0K1BNLMsMXbOBXxXP0L5vaczP8P2z2NP0r8wHMcO9fkDemz
FjNNlnpznr2Tkat8AQr+3netbqcYyllRpkCGDRG+Uja1rfozS06ijiMfMMGN19EYlifTLUhwi7RH
XHhIIjXELpvPT39AB97TomNuqa883UeBZWBTyPykyBsxjkXm9sn0Sh6V0eDaXUfgXIPM8+rne0gU
p7pu8ZeVyWeyfkw8MaSMHChGCAdtLcf7gief9t4EEhYiMpC2E76ubwiu1AdzmkxsrmkZw58ZBchP
xUzwsGOy2iJQntvAwdHO3iUDoUoQziSZTzjLmx7/wBkMEXbaInaEotQcoYhV+mbnRtnYyubmwZzf
+WWfrovOhLyPpnDQoQc3IW7iaqmUb7OW8tMNDq4GDB91lGRizk9VXKFJTG7YefkOtJLD03qf/jvX
QMKsDuzFO9eDhXmiFQBD+xO/sFo7BaMO0jnLOeXLJZizlTR5kiexI+xkInsnLyNp7fOBgKMEqHRb
C+UhmIY9HyxZlbHOR6kCn6uZPq5xyg5PhL2PEZpHLbiElmhfB+y+tTE5H3adknpNml/pRYq0E0RY
Y5UxYzmRDaeqnd+FWHOpC4uTMtd/1F0H0zEHEzTcSBWdo1Z4CG0uXMf3kUMI876XTAX5br1ARaGm
/SIF2tOhFRz1yfkqQtAUcNDxMxxNBgbjwk+SdEnaDcoigCxx3txN6LcQ8HDhws/uYqMWy6Ur8Anp
oTc8FyNQfPrtB8/EXHbddG+6FgeximE8okHxQEus0VPSdxK3O0NhQU6rKsHS2KyVSglCb0LnsaUK
mQUPmXpfPMKvJVxP/hVre75IBhczyCjF32i21YvG4KgGfXS5Oy+eMbKWPPlRxnaUIhmq9UpN1UIa
ZtXZkgILnZZNuULkD8cul5NwXv6FzNqcb602cY3OrRS7EmYrqrXEPu1u7xLkWEi3XozEy6a82Gva
InPQagSAC8G0HS6CajkEv42zDUDOvtOr0HhyhDnLdIE+QMv7OV3xknwO4p3BWM6pVsdarGZTaZUi
Crp8VbBgGcwRrGr1iuuV/r2tEYJrJtqPoAmVqcI+o7OhNq6pBQBZt3y24Ql5gIEZyl1B6jGCdS+o
HZMyM8TKUEccMrs99ExnF93UZ6Fi+4RP5aeUXraROn34FCFBIH1IlkcE+SOrI7k4JCb7yO8zHzZJ
kiZfFVWlTObWKzkR7pvpSHXYTSuPwtj12c/Tj22MCrm/XaBydWOwZlnQ7aUYRQoVYuT3kDUjm11Q
wWi+Gr2yFkvhGbkI/4wk8TKEci51/OaWpGdR0nnWVxgO3xyKjduekZTEro6gDdNg89XzfgggekVK
dYVctyOnZeKvMOXD7ccphuiuHFmcOgpifXTYUGXaS8TN+t5kwPnxOTYCD+NLKFJgI+Gbs1pJB3EH
yFAERx5Em01cK3iamecSnXayc9yxgLQm8ruWuU3dQgVEJmgKGXO/7jY1FyVfodLrPFRYjr33tJMz
EhmMGW3QByuaj5aId6nNYgcQH9wdUEZMqJ0a73OvQ1beOeGJayYgF/uDPPR27dwAZ/6rSreoIFK3
+0cpPA7NiHDFyMSCzcwXwyjVnqyLITMWFhUjEMULV1I1Sdw1fcQk+D5pFpmqBwbsmVOfY6Qqw20Q
njxPGBnnwIfbqxQfQEiZUpoUz5S4tdkfil+yPVEoQMnFZdxutS+Oh+jV7fxbfYz7ilISM91xXidn
AIZk+ovamgiW+VATDPlrDwU0To0PVVT3gU3U6TEVnxebcklRW7zVXjd5bWu8bIll62q6anWETxJt
YmM22O14JGnhsTlzcmMBimrFVQ4msIP/RfurZ1DVbhacB1Wr1D1Q6WikRIKsFlXBurexNhHt3IW4
dvTIjxdZd/qQD//+iU+VjaPPLcioEqWkEv0H7hXZfk+G3ho+QJwlmaV6OXTMLli+1sT9FEV3tdk8
shIt2Sl1qF3h9vVt7r40Pd3vgam7y2x9FTuUZWS9UWyBBZ6kZzjIx0xEndNVpYqrawcazw23aX62
kINDr88ULLylBkZPnINkazCW1Y6HVFg+gpZUFhOND+mjT7+vzsgkt+lVzsxTWf9pR39YNyT5Rc1y
01b8Cm3AApT+cs/WfgMH3TeD6l65MU5GPAvZkEqMYp+NtWdcBV4SHjzR3o6mAOb0jSOZF7+jENuI
R17OnBUvVRcoS2kzBxLCnMY+J0R/IfzZHzlxykZzDcF+oi2Y6paY6RoqXhHNpWNH/C/bAIA0wVwN
ceGGuNAM/RV09wdLZn9LMJNNiloRBnn67jmTK2b3A/0JyYgZA2X8Fe+EOrr3z4lpT7flrTO7RcDQ
h2uP4H583q3FQHOrl7PVu9x7Whuxg+iT3jUjRNTkPzK2mhOEwk8X8qCj2TmyTkgAuZNAr+NYuJZW
eJDrmQy/HEfoZhtPaD7m1CGyXYgk+/3F3dRDVmF42leDeKRA5bjnOODUqsgdj94ZbsT/hSOXuoHf
PPNldeH1PA+IlwIpQvrDJsqYBGYW8kHhANcj52Sh4rp/Ek7Yo6d3GNbNmnfq2K3qpcAzQwYtEBcV
q9bP4xpIOWWbTyHrMy5Vtp7nMoCW7/TeRsn/1MaTY/SJ1e0OmREkcQNZmWtc+eUVkH8jlFqoGIHy
idI50zi0TlAFgOThK/BkXpOFD0nTqBwHYvJTaOJOBq8XvxsSymVFp9x3pbFiSMofOGN39+GJuYAK
AIUEisxJjQc6HVhELacvBF1XZNYBj0ZlOE6lUW62ZZvB0E2eFHXurtObxyVGrRGu5B8V+kOj7KKW
unBkWo9u4obLPgixG48rE45UbnJXzNHpDRqSXqaXZq0VyyJTogiryESYZs8iu2KT7LF6CdavK1nR
/Qrb1qej1IfEo4OGoxmWBiabfO+3sZfIVBN+MemgvZEIGGg98YD2G4vdYqcZ8CZzfdtI9gpijxvT
0YOReSH0C700qIILw7NPWbOVsycySFJH7+O8NtB3eIDhR//Bu7P55iwWbin1zHdVNTwiXvdUy1ol
9IzF1W4QO65rIYkVZuz4fSHG0kFf+Myho6oIOw2JbvUisvAFjz3WgNoXhazMrPHQzbEcKyebzOpO
t9JqjRGeuDFUhQn1QYoLDlJj/e5pO/CDv3jbALvtRlTnua1p4osu/XHkHbYBlJ9P2hWcNbYRMAjL
/MWaF/THJkSdVXvB1sXBn6U3ghE6xNNTVpoCC3EkrHIc/rUtdZoGEIFcBl1/2H2gEZyBaNyEfBQc
9Qmx40XIxrD0tHVLDxapfbY4ZpZdIEqUoffrxsyKchslhonHCYThFjsb5RUhKxeyxSUNljcsTKbb
OH0=
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity mult_cic_mult_gen_v12_0_12 is
  port (
    CLK : in STD_LOGIC;
    A : in STD_LOGIC_VECTOR ( 13 downto 0 );
    B : in STD_LOGIC_VECTOR ( 15 downto 0 );
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    ZERO_DETECT : out STD_LOGIC_VECTOR ( 1 downto 0 );
    P : out STD_LOGIC_VECTOR ( 31 downto 0 );
    PCASC : out STD_LOGIC_VECTOR ( 47 downto 0 )
  );
  attribute C_A_TYPE : integer;
  attribute C_A_TYPE of mult_cic_mult_gen_v12_0_12 : entity is 0;
  attribute C_A_WIDTH : integer;
  attribute C_A_WIDTH of mult_cic_mult_gen_v12_0_12 : entity is 14;
  attribute C_B_TYPE : integer;
  attribute C_B_TYPE of mult_cic_mult_gen_v12_0_12 : entity is 0;
  attribute C_B_VALUE : string;
  attribute C_B_VALUE of mult_cic_mult_gen_v12_0_12 : entity is "10000001";
  attribute C_B_WIDTH : integer;
  attribute C_B_WIDTH of mult_cic_mult_gen_v12_0_12 : entity is 16;
  attribute C_CCM_IMP : integer;
  attribute C_CCM_IMP of mult_cic_mult_gen_v12_0_12 : entity is 0;
  attribute C_CE_OVERRIDES_SCLR : integer;
  attribute C_CE_OVERRIDES_SCLR of mult_cic_mult_gen_v12_0_12 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of mult_cic_mult_gen_v12_0_12 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of mult_cic_mult_gen_v12_0_12 : entity is 0;
  attribute C_HAS_ZERO_DETECT : integer;
  attribute C_HAS_ZERO_DETECT of mult_cic_mult_gen_v12_0_12 : entity is 0;
  attribute C_LATENCY : integer;
  attribute C_LATENCY of mult_cic_mult_gen_v12_0_12 : entity is 3;
  attribute C_MODEL_TYPE : integer;
  attribute C_MODEL_TYPE of mult_cic_mult_gen_v12_0_12 : entity is 0;
  attribute C_MULT_TYPE : integer;
  attribute C_MULT_TYPE of mult_cic_mult_gen_v12_0_12 : entity is 1;
  attribute C_OPTIMIZE_GOAL : integer;
  attribute C_OPTIMIZE_GOAL of mult_cic_mult_gen_v12_0_12 : entity is 1;
  attribute C_OUT_HIGH : integer;
  attribute C_OUT_HIGH of mult_cic_mult_gen_v12_0_12 : entity is 31;
  attribute C_OUT_LOW : integer;
  attribute C_OUT_LOW of mult_cic_mult_gen_v12_0_12 : entity is 0;
  attribute C_ROUND_OUTPUT : integer;
  attribute C_ROUND_OUTPUT of mult_cic_mult_gen_v12_0_12 : entity is 0;
  attribute C_ROUND_PT : integer;
  attribute C_ROUND_PT of mult_cic_mult_gen_v12_0_12 : entity is 0;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of mult_cic_mult_gen_v12_0_12 : entity is 0;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of mult_cic_mult_gen_v12_0_12 : entity is "artix7";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of mult_cic_mult_gen_v12_0_12 : entity is "mult_gen_v12_0_12";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of mult_cic_mult_gen_v12_0_12 : entity is "yes";
end mult_cic_mult_gen_v12_0_12;

architecture STRUCTURE of mult_cic_mult_gen_v12_0_12 is
  signal \<const0>\ : STD_LOGIC;
  signal \^p\ : STD_LOGIC_VECTOR ( 29 downto 0 );
  signal NLW_i_mult_P_UNCONNECTED : STD_LOGIC_VECTOR ( 30 downto 29 );
  signal NLW_i_mult_PCASC_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 0 );
  signal NLW_i_mult_ZERO_DETECT_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute C_A_TYPE of i_mult : label is 0;
  attribute C_A_WIDTH of i_mult : label is 14;
  attribute C_B_TYPE of i_mult : label is 0;
  attribute C_B_VALUE of i_mult : label is "10000001";
  attribute C_B_WIDTH of i_mult : label is 16;
  attribute C_CCM_IMP of i_mult : label is 0;
  attribute C_CE_OVERRIDES_SCLR of i_mult : label is 0;
  attribute C_HAS_CE of i_mult : label is 0;
  attribute C_HAS_SCLR of i_mult : label is 0;
  attribute C_HAS_ZERO_DETECT of i_mult : label is 0;
  attribute C_LATENCY of i_mult : label is 3;
  attribute C_MODEL_TYPE of i_mult : label is 0;
  attribute C_MULT_TYPE of i_mult : label is 1;
  attribute C_OPTIMIZE_GOAL of i_mult : label is 1;
  attribute C_OUT_HIGH of i_mult : label is 31;
  attribute C_OUT_LOW of i_mult : label is 0;
  attribute C_ROUND_OUTPUT of i_mult : label is 0;
  attribute C_ROUND_PT of i_mult : label is 0;
  attribute C_VERBOSITY of i_mult : label is 0;
  attribute C_XDEVICEFAMILY of i_mult : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_mult : label is "yes";
begin
  P(31) <= \^p\(29);
  P(30) <= \^p\(29);
  P(29 downto 0) <= \^p\(29 downto 0);
  PCASC(47) <= \<const0>\;
  PCASC(46) <= \<const0>\;
  PCASC(45) <= \<const0>\;
  PCASC(44) <= \<const0>\;
  PCASC(43) <= \<const0>\;
  PCASC(42) <= \<const0>\;
  PCASC(41) <= \<const0>\;
  PCASC(40) <= \<const0>\;
  PCASC(39) <= \<const0>\;
  PCASC(38) <= \<const0>\;
  PCASC(37) <= \<const0>\;
  PCASC(36) <= \<const0>\;
  PCASC(35) <= \<const0>\;
  PCASC(34) <= \<const0>\;
  PCASC(33) <= \<const0>\;
  PCASC(32) <= \<const0>\;
  PCASC(31) <= \<const0>\;
  PCASC(30) <= \<const0>\;
  PCASC(29) <= \<const0>\;
  PCASC(28) <= \<const0>\;
  PCASC(27) <= \<const0>\;
  PCASC(26) <= \<const0>\;
  PCASC(25) <= \<const0>\;
  PCASC(24) <= \<const0>\;
  PCASC(23) <= \<const0>\;
  PCASC(22) <= \<const0>\;
  PCASC(21) <= \<const0>\;
  PCASC(20) <= \<const0>\;
  PCASC(19) <= \<const0>\;
  PCASC(18) <= \<const0>\;
  PCASC(17) <= \<const0>\;
  PCASC(16) <= \<const0>\;
  PCASC(15) <= \<const0>\;
  PCASC(14) <= \<const0>\;
  PCASC(13) <= \<const0>\;
  PCASC(12) <= \<const0>\;
  PCASC(11) <= \<const0>\;
  PCASC(10) <= \<const0>\;
  PCASC(9) <= \<const0>\;
  PCASC(8) <= \<const0>\;
  PCASC(7) <= \<const0>\;
  PCASC(6) <= \<const0>\;
  PCASC(5) <= \<const0>\;
  PCASC(4) <= \<const0>\;
  PCASC(3) <= \<const0>\;
  PCASC(2) <= \<const0>\;
  PCASC(1) <= \<const0>\;
  PCASC(0) <= \<const0>\;
  ZERO_DETECT(1) <= \<const0>\;
  ZERO_DETECT(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
i_mult: entity work.mult_cic_mult_gen_v12_0_12_viv
     port map (
      A(13 downto 0) => A(13 downto 0),
      B(15 downto 0) => B(15 downto 0),
      CE => '0',
      CLK => CLK,
      P(31) => \^p\(29),
      P(30 downto 29) => NLW_i_mult_P_UNCONNECTED(30 downto 29),
      P(28 downto 0) => \^p\(28 downto 0),
      PCASC(47 downto 0) => NLW_i_mult_PCASC_UNCONNECTED(47 downto 0),
      SCLR => '0',
      ZERO_DETECT(1 downto 0) => NLW_i_mult_ZERO_DETECT_UNCONNECTED(1 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity mult_cic is
  port (
    CLK : in STD_LOGIC;
    A : in STD_LOGIC_VECTOR ( 13 downto 0 );
    B : in STD_LOGIC_VECTOR ( 15 downto 0 );
    P : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of mult_cic : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of mult_cic : entity is "mult_cic,mult_gen_v12_0_12,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of mult_cic : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of mult_cic : entity is "mult_gen_v12_0_12,Vivado 2016.4";
end mult_cic;

architecture STRUCTURE of mult_cic is
  signal NLW_U0_PCASC_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 0 );
  signal NLW_U0_ZERO_DETECT_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute C_A_TYPE : integer;
  attribute C_A_TYPE of U0 : label is 0;
  attribute C_A_WIDTH : integer;
  attribute C_A_WIDTH of U0 : label is 14;
  attribute C_B_TYPE : integer;
  attribute C_B_TYPE of U0 : label is 0;
  attribute C_B_VALUE : string;
  attribute C_B_VALUE of U0 : label is "10000001";
  attribute C_B_WIDTH : integer;
  attribute C_B_WIDTH of U0 : label is 16;
  attribute C_CCM_IMP : integer;
  attribute C_CCM_IMP of U0 : label is 0;
  attribute C_CE_OVERRIDES_SCLR : integer;
  attribute C_CE_OVERRIDES_SCLR of U0 : label is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 0;
  attribute C_HAS_ZERO_DETECT : integer;
  attribute C_HAS_ZERO_DETECT of U0 : label is 0;
  attribute C_LATENCY : integer;
  attribute C_LATENCY of U0 : label is 3;
  attribute C_MODEL_TYPE : integer;
  attribute C_MODEL_TYPE of U0 : label is 0;
  attribute C_MULT_TYPE : integer;
  attribute C_MULT_TYPE of U0 : label is 1;
  attribute C_OPTIMIZE_GOAL : integer;
  attribute C_OPTIMIZE_GOAL of U0 : label is 1;
  attribute C_OUT_HIGH : integer;
  attribute C_OUT_HIGH of U0 : label is 31;
  attribute C_OUT_LOW : integer;
  attribute C_OUT_LOW of U0 : label is 0;
  attribute C_ROUND_OUTPUT : integer;
  attribute C_ROUND_OUTPUT of U0 : label is 0;
  attribute C_ROUND_PT : integer;
  attribute C_ROUND_PT of U0 : label is 0;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of U0 : label is 0;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
begin
U0: entity work.mult_cic_mult_gen_v12_0_12
     port map (
      A(13 downto 0) => A(13 downto 0),
      B(15 downto 0) => B(15 downto 0),
      CE => '1',
      CLK => CLK,
      P(31 downto 0) => P(31 downto 0),
      PCASC(47 downto 0) => NLW_U0_PCASC_UNCONNECTED(47 downto 0),
      SCLR => '0',
      ZERO_DETECT(1 downto 0) => NLW_U0_ZERO_DETECT_UNCONNECTED(1 downto 0)
    );
end STRUCTURE;
