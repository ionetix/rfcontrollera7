`timescale 1ns / 1ps

// Description: 		Deserializers for RF ADC data
// Additional Comments:
//		Configured for ADC in SDR, 14-bit, bytewise, LSB-first
//		Deserializers must be calibrated by setting ADCs to output sync signal,
//		then setting the bitslipCal input high for at least 20 clkDiv cycles.
//////////////////////////////////////////////////////////////////////////////////
module deserial(
	input clkIo,
	input clkDiv,
	input [7:0] dataIn,
	input bitslipCal,
	output reg calDone = 0,
	output [13:0] dataOut0,
	output [13:0] dataOut1,
	output [13:0] dataOut2,
	output [13:0] dataOut3
	);

	wire [7:0] shift;
	reg [7:0] bitslip = 0;
	reg [2:0] bitslipCtr = 0;
	wire [13:0] serdesOut0, serdesOut1, serdesOut2, serdesOut3;
	
	reg [4:0] patternOk = 0;
	reg calOk = 0;
	reg bitslipCalPrev = 0;

	
	always @ (posedge clkDiv)
	begin
		bitslipCalPrev <= bitslipCal;
		if (bitslipCal & ~bitslipCalPrev)

		patternOk[0] <= serdesOut0 == 14'b11110000001111;
		patternOk[1] <= serdesOut1 == 14'b11110001111000;
		patternOk[2] <= serdesOut2 == 14'b11110001111000;
		patternOk[3] <= serdesOut3 == 14'b11110001111000;
		patternOk[4] <= &patternOk[3:0];
		if (bitslipCal)
			calDone <= patternOk[4];
		calOk <= ~(|bitslip);

		if (bitslipCal & ~|bitslipCtr)
		begin
		//artix-7 serdes
			bitslip[0] <= (serdesOut0[6:0]  != 7'b0000111);
			bitslip[1] <= (serdesOut0[13:7] != 7'b1111000);
			bitslip[2] <= (serdesOut1[6:0]  != 7'b1111000);
            bitslip[3] <= (serdesOut1[13:7] != 7'b1111000);
			bitslip[4] <= (serdesOut2[6:0]  != 7'b1111000);
            bitslip[5] <= (serdesOut2[13:7] != 7'b1111000);
			bitslip[6] <= (serdesOut3[6:0]  != 7'b1111000);
            bitslip[7] <= (serdesOut3[13:7] != 7'b1111000);            
            
/*        spartan-6 serdes    
			bitslipSlv[0] <= (serdesOut0[3:0] != 4'b1001);
			bitslipMst[0] <= (serdesOut0[6:4] != 3'b101);
			bitslipSlv[1] <= (serdesOut0[10:7] != 4'b0110);
			bitslipMst[1] <= (serdesOut0[13:11] != 3'b010);
			bitslipSlv[2] <= (serdesOut1[3:0] != 4'b0110);
			bitslipMst[2] <= (serdesOut1[6:4] != 3'b010);
			bitslipSlv[3] <= (serdesOut1[10:7] != 4'b0110);
			bitslipMst[3] <= (serdesOut1[13:11] != 3'b010);
			bitslipSlv[4] <= (serdesOut2[3:0] != 4'b0110);
			bitslipMst[4] <= (serdesOut2[6:4] != 3'b010);
			bitslipSlv[5] <= (serdesOut2[10:7] != 4'b0110);
			bitslipMst[5] <= (serdesOut2[13:11] != 3'b010);
			bitslipSlv[6] <= (serdesOut3[3:0] != 4'b0110);
			bitslipMst[6] <= (serdesOut3[6:4] != 3'b010);
			bitslipSlv[7] <= (serdesOut3[10:7] != 4'b0110);
			bitslipMst[7] <= (serdesOut3[13:11] != 3'b010);
*/		end
		else
		begin
			bitslip <= 0;
		end
		if (bitslipCal)
			bitslipCtr <= bitslipCtr + 1;
		else
			bitslipCtr <= 0;
/* spartan-6 serdes
		serdesReg0[3:0] <= serdesOut0[3:0];
		serdesReg1[3:0] <= serdesOut1[3:0];
		serdesReg2[3:0] <= serdesOut2[3:0];
		serdesReg3[3:0] <= serdesOut3[3:0];
		serdesReg4[10:7] <= serdesOut0[10:7];
		serdesReg5[10:7] <= serdesOut1[10:7];
		serdesReg6[10:7] <= serdesOut2[10:7];
		serdesReg7[10:7] <= serdesOut3[10:7];
*/	end
	assign dataOut0 = {serdesOut0[13:7], ~serdesOut0[6:0]}; ////inverted pins on PCB ADC_A0
	assign dataOut1 = {serdesOut1[13:7], serdesOut1[6:0]};
	assign dataOut2 = {serdesOut2[13:7], serdesOut2[6:0]};
	assign dataOut3 = {serdesOut3[13:7], serdesOut3[6:0]};
	
	// deserialization of "A0" ADC output
 /*  ISERDES2 #(.BITSLIP_ENABLE("TRUE"), .DATA_RATE("SDR"), .DATA_WIDTH(7), .INTERFACE_TYPE("RETIMED"), .SERDES_MODE("MASTER")
		) adcA0_P (.CFB0(), .CFB1(), .DFB(), .FABRICOUT(), .INCDEC(), .Q1(serdesOut0[0]), .Q2(serdesOut0[1]), .Q3(serdesOut0[2]), .Q4(serdesOut0[3]),
      .SHIFTOUT(shift[0]), .VALID(), .BITSLIP(bitslipMst[0]), .CE0(1'b1), .CLK0(clkIo), .CLK1(1'b0), .CLKDIV(clkDiv), .D(dataIn[0]),
      .IOCE(ioce), .RST(1'b0), .SHIFTIN(1'b0));
*/      
       ISERDESE2 #(
           .DATA_RATE("SDR"),           // DDR, SDR
           .DATA_WIDTH(7),              // Parallel data width (2-8,10,14)
           .DYN_CLKDIV_INV_EN("FALSE"), // Enable DYNCLKDIVINVSEL inversion (FALSE, TRUE)
           .DYN_CLK_INV_EN("FALSE"),    // Enable DYNCLKINVSEL inversion (FALSE, TRUE)
           // INIT_Q1 - INIT_Q4: Initial value on the Q outputs (0/1)
           .INIT_Q1(1'b0),
           .INIT_Q2(1'b0),
           .INIT_Q3(1'b0),
           .INIT_Q4(1'b0),
           .INTERFACE_TYPE("NETWORKING"),   // MEMORY, MEMORY_DDR3, MEMORY_QDR, NETWORKING, OVERSAMPLE
           .IOBDELAY("NONE"),           // NONE, BOTH, IBUF, IFD
           .NUM_CE(2),                  // Number of clock enables (1,2)
           .OFB_USED("FALSE"),          // Select OFB path (FALSE, TRUE)
           .SERDES_MODE("MASTER"),      // MASTER, SLAVE
           // SRVAL_Q1 - SRVAL_Q4: Q output values when SR is used (0/1)
           .SRVAL_Q1(1'b0),
           .SRVAL_Q2(1'b0),
           .SRVAL_Q3(1'b0),
           .SRVAL_Q4(1'b0) 
        )
        adcA0 (
           .O(),                       // 1-bit output: Combinatorial output
           // Q1 - Q8: 1-bit (each) output: Registered data outputs
           .Q1(serdesOut0[6]),
           .Q2(serdesOut0[5]),
           .Q3(serdesOut0[4]),
           .Q4(serdesOut0[3]),
           .Q5(serdesOut0[2]),
           .Q6(serdesOut0[1]),
           .Q7(serdesOut0[0]),
           .Q8(),
           // SHIFTOUT1, SHIFTOUT2: 1-bit (each) output: Data width expansion output ports
           .SHIFTOUT1(),
           .SHIFTOUT2(),
           .BITSLIP(bitslip[0]),           // 1-bit input: The BITSLIP pin performs a Bitslip operation synchronous to
                                        // CLKDIV when asserted (active High). Subsequently, the data seen on the Q1
                                        // to Q8 output ports will shift, as in a barrel-shifter operation, one
                                        // position every time Bitslip is invoked (DDR operation is different from
                                        // SDR).
     
           // CE1, CE2: 1-bit (each) input: Data register clock enable inputs
           .CE1(1'b1),
           .CE2(1'b1),
           .CLKDIVP(),           // 1-bit input: TBD
           // Clocks: 1-bit (each) input: ISERDESE2 clock input ports
           .CLK(clkIo),                   // 1-bit input: High-speed clock
           .CLKB(),                 // 1-bit input: High-speed secondary clock
           .CLKDIV(clkDiv),             // 1-bit input: Divided clock
           .OCLK(),                 // 1-bit input: High speed output clock used when INTERFACE_TYPE="MEMORY" 
           // Dynamic Clock Inversions: 1-bit (each) input: Dynamic clock inversion pins to switch clock polarity
           .DYNCLKDIVSEL(), // 1-bit input: Dynamic CLKDIV inversion
           .DYNCLKSEL(),       // 1-bit input: Dynamic CLK/CLKB inversion
           // Input Data: 1-bit (each) input: ISERDESE2 data input ports
           .D(dataIn[0]),                       // 1-bit input: Data input
           .DDLY(),                 // 1-bit input: Serial data from IDELAYE2
           .OFB(),                   // 1-bit input: Data feedback from OSERDESE2
           .OCLKB(),               // 1-bit input: High speed negative edge output clock
           .RST(1'b0),                   // 1-bit input: Active high asynchronous reset
           // SHIFTIN1, SHIFTIN2: 1-bit (each) input: Data width expansion input ports
           .SHIFTIN1(),
           .SHIFTIN2() 
        );
     
        // End of ISERDESE2_inst instantiation
                         
/*      
	ISERDES2 #(.BITSLIP_ENABLE("TRUE"), .DATA_RATE("SDR"), .DATA_WIDTH(7), .INTERFACE_TYPE("RETIMED"), .SERDES_MODE("SLAVE")
		) adcA0_N (.CFB0(), .CFB1(), .DFB(), .FABRICOUT(), .INCDEC(), .Q1(), .Q2(serdesOut0[4]), .Q3(serdesOut0[5]), .Q4(serdesOut0[6]),
      .SHIFTOUT(), .VALID(), .BITSLIP(bitslipSlv[0]), .CE0(1'b1), .CLK0(clkIo), .CLK1(1'b0), .CLKDIV(clkDiv), .D(1'b0),
      .IOCE(ioce), .RST(1'b0), .SHIFTIN(shift[0]));
    */  
 
	// deserialization of "A1" ADC output
/*   ISERDES2 #(.BITSLIP_ENABLE("TRUE"), .DATA_RATE("SDR"), .DATA_WIDTH(7), .INTERFACE_TYPE("RETIMED"), .SERDES_MODE("MASTER")
		) adcA1_P (.CFB0(), .CFB1(), .DFB(), .FABRICOUT(), .INCDEC(), .Q1(serdesOut0[7]), .Q2(serdesOut0[8]), .Q3(serdesOut0[9]), .Q4(serdesOut0[10]),
      .SHIFTOUT(shift[1]), .VALID(), .BITSLIP(bitslipMst[1]), .CE0(1'b1), .CLK0(clkIo), .CLK1(1'b0), .CLKDIV(clkDiv), .D(dataIn[1]),
      .IOCE(ioce), .RST(1'b0), .SHIFTIN(1'b0));
    */  
       ISERDESE2 #(
                .DATA_RATE("SDR"),           // DDR, SDR
                .DATA_WIDTH(7),              // Parallel data width (2-8,10,14)
                .DYN_CLKDIV_INV_EN("FALSE"), // Enable DYNCLKDIVINVSEL inversion (FALSE, TRUE)
                .DYN_CLK_INV_EN("FALSE"),    // Enable DYNCLKINVSEL inversion (FALSE, TRUE)
                // INIT_Q1 - INIT_Q4: Initial value on the Q outputs (0/1)
                .INIT_Q1(1'b0),
                .INIT_Q2(1'b0),
                .INIT_Q3(1'b0),
                .INIT_Q4(1'b0),
                .INTERFACE_TYPE("NETWORKING"),   // MEMORY, MEMORY_DDR3, MEMORY_QDR, NETWORKING, OVERSAMPLE
                .IOBDELAY("NONE"),           // NONE, BOTH, IBUF, IFD
                .NUM_CE(2),                  // Number of clock enables (1,2)
                .OFB_USED("FALSE"),          // Select OFB path (FALSE, TRUE)
                .SERDES_MODE("MASTER"),      // MASTER, SLAVE
                // SRVAL_Q1 - SRVAL_Q4: Q output values when SR is used (0/1)
                .SRVAL_Q1(1'b0),
                .SRVAL_Q2(1'b0),
                .SRVAL_Q3(1'b0),
                .SRVAL_Q4(1'b0) 
             )
             adcA1 (
                .O(),                       // 1-bit output: Combinatorial output
                // Q1 - Q8: 1-bit (each) output: Registered data outputs
                .Q1(serdesOut0[13]),
                .Q2(serdesOut0[12]),
                .Q3(serdesOut0[11]),
                .Q4(serdesOut0[10]),
                .Q5(serdesOut0[9]),
                .Q6(serdesOut0[8]),
                .Q7(serdesOut0[7]),
                .Q8(),
                // SHIFTOUT1, SHIFTOUT2: 1-bit (each) output: Data width expansion output ports
                .SHIFTOUT1(),
                .SHIFTOUT2(),
                .BITSLIP(bitslip[1]),           // 1-bit input: The BITSLIP pin performs a Bitslip operation synchronous to
                                             // CLKDIV when asserted (active High). Subsequently, the data seen on the Q1
                                             // to Q8 output ports will shift, as in a barrel-shifter operation, one
                                             // position every time Bitslip is invoked (DDR operation is different from
                                             // SDR).
          
                // CE1, CE2: 1-bit (each) input: Data register clock enable inputs
                .CE1(1'b1),
                .CE2(1'b1),
                .CLKDIVP(),           // 1-bit input: TBD
                // Clocks: 1-bit (each) input: ISERDESE2 clock input ports
                .CLK(clkIo),                   // 1-bit input: High-speed clock
                .CLKB(),                 // 1-bit input: High-speed secondary clock
                .CLKDIV(clkDiv),             // 1-bit input: Divided clock
                .OCLK(),                 // 1-bit input: High speed output clock used when INTERFACE_TYPE="MEMORY" 
                // Dynamic Clock Inversions: 1-bit (each) input: Dynamic clock inversion pins to switch clock polarity
                .DYNCLKDIVSEL(), // 1-bit input: Dynamic CLKDIV inversion
                .DYNCLKSEL(),       // 1-bit input: Dynamic CLK/CLKB inversion
                // Input Data: 1-bit (each) input: ISERDESE2 data input ports
                .D(dataIn[1]),                       // 1-bit input: Data input
                .DDLY(),                 // 1-bit input: Serial data from IDELAYE2
                .OFB(),                   // 1-bit input: Data feedback from OSERDESE2
                .OCLKB(),               // 1-bit input: High speed negative edge output clock
                .RST(1'b0),                   // 1-bit input: Active high asynchronous reset
                // SHIFTIN1, SHIFTIN2: 1-bit (each) input: Data width expansion input ports
                .SHIFTIN1(),
                .SHIFTIN2() 
             );
      
/*	ISERDES2 #(.BITSLIP_ENABLE("TRUE"), .DATA_RATE("SDR"), .DATA_WIDTH(7), .INTERFACE_TYPE("RETIMED"), .SERDES_MODE("SLAVE")
		) adcA1_N (.CFB0(), .CFB1(), .DFB(), .FABRICOUT(), .INCDEC(), .Q1(), .Q2(serdesOut0[11]), .Q3(serdesOut0[12]), .Q4(serdesOut0[13]),
      .SHIFTOUT(), .VALID(), .BITSLIP(bitslipSlv[1]), .CE0(1'b1), .CLK0(clkIo), .CLK1(1'b0), .CLKDIV(clkDiv), .D(1'b0),
      .IOCE(ioce), .RST(1'b0), .SHIFTIN(shift[1]));
*/      
      
 
	// deserialization of "B0" ADC output
/*   ISERDES2 #(.BITSLIP_ENABLE("TRUE"), .DATA_RATE("SDR"), .DATA_WIDTH(7), .INTERFACE_TYPE("RETIMED"), .SERDES_MODE("MASTER")
		) adcB0_P (.CFB0(), .CFB1(), .DFB(), .FABRICOUT(), .INCDEC(), .Q1(serdesOut1[0]), .Q2(serdesOut1[1]), .Q3(serdesOut1[2]), .Q4(serdesOut1[3]),
      .SHIFTOUT(shift[2]), .VALID(), .BITSLIP(bitslipMst[2]), .CE0(1'b1), .CLK0(clkIo), .CLK1(1'b0), .CLKDIV(clkDiv), .D(dataIn[2]),
      .IOCE(ioce), .RST(1'b0), .SHIFTIN(1'b0));
*/      
             ISERDESE2 #(
               .DATA_RATE("SDR"),           // DDR, SDR
               .DATA_WIDTH(7),              // Parallel data width (2-8,10,14)
               .DYN_CLKDIV_INV_EN("FALSE"), // Enable DYNCLKDIVINVSEL inversion (FALSE, TRUE)
               .DYN_CLK_INV_EN("FALSE"),    // Enable DYNCLKINVSEL inversion (FALSE, TRUE)
               // INIT_Q1 - INIT_Q4: Initial value on the Q outputs (0/1)
               .INIT_Q1(1'b0),
               .INIT_Q2(1'b0),
               .INIT_Q3(1'b0),
               .INIT_Q4(1'b0),
               .INTERFACE_TYPE("NETWORKING"),   // MEMORY, MEMORY_DDR3, MEMORY_QDR, NETWORKING, OVERSAMPLE
               .IOBDELAY("NONE"),           // NONE, BOTH, IBUF, IFD
               .NUM_CE(2),                  // Number of clock enables (1,2)
               .OFB_USED("FALSE"),          // Select OFB path (FALSE, TRUE)
               .SERDES_MODE("MASTER"),      // MASTER, SLAVE
               // SRVAL_Q1 - SRVAL_Q4: Q output values when SR is used (0/1)
               .SRVAL_Q1(1'b0),
               .SRVAL_Q2(1'b0),
               .SRVAL_Q3(1'b0),
               .SRVAL_Q4(1'b0) 
            )
            adcB0 (
               .O(),                       // 1-bit output: Combinatorial output
               // Q1 - Q8: 1-bit (each) output: Registered data outputs
               .Q1(serdesOut1[6]),
               .Q2(serdesOut1[5]),
               .Q3(serdesOut1[4]),
               .Q4(serdesOut1[3]),
               .Q5(serdesOut1[2]),
               .Q6(serdesOut1[1]),
               .Q7(serdesOut1[0]),
               .Q8(),
               // SHIFTOUT1, SHIFTOUT2: 1-bit (each) output: Data width expansion output ports
               .SHIFTOUT1(),
               .SHIFTOUT2(),
               .BITSLIP(bitslip[2]),           // 1-bit input: The BITSLIP pin performs a Bitslip operation synchronous to
                                            // CLKDIV when asserted (active High). Subsequently, the data seen on the Q1
                                            // to Q8 output ports will shift, as in a barrel-shifter operation, one
                                            // position every time Bitslip is invoked (DDR operation is different from
                                            // SDR).
         
               // CE1, CE2: 1-bit (each) input: Data register clock enable inputs
               .CE1(1'b1),
               .CE2(1'b1),
               .CLKDIVP(),           // 1-bit input: TBD
               // Clocks: 1-bit (each) input: ISERDESE2 clock input ports
               .CLK(clkIo),                   // 1-bit input: High-speed clock
               .CLKB(),                 // 1-bit input: High-speed secondary clock
               .CLKDIV(clkDiv),             // 1-bit input: Divided clock
               .OCLK(),                 // 1-bit input: High speed output clock used when INTERFACE_TYPE="MEMORY" 
               // Dynamic Clock Inversions: 1-bit (each) input: Dynamic clock inversion pins to switch clock polarity
               .DYNCLKDIVSEL(), // 1-bit input: Dynamic CLKDIV inversion
               .DYNCLKSEL(),       // 1-bit input: Dynamic CLK/CLKB inversion
               // Input Data: 1-bit (each) input: ISERDESE2 data input ports
               .D(dataIn[2]),                       // 1-bit input: Data input
               .DDLY(),                 // 1-bit input: Serial data from IDELAYE2
               .OFB(),                   // 1-bit input: Data feedback from OSERDESE2
               .OCLKB(),               // 1-bit input: High speed negative edge output clock
               .RST(1'b0),                   // 1-bit input: Active high asynchronous reset
               // SHIFTIN1, SHIFTIN2: 1-bit (each) input: Data width expansion input ports
               .SHIFTIN1(),
               .SHIFTIN2() 
            );
      
/*	ISERDES2 #(.BITSLIP_ENABLE("TRUE"), .DATA_RATE("SDR"), .DATA_WIDTH(7), .INTERFACE_TYPE("RETIMED"), .SERDES_MODE("SLAVE")
		) adcB0_N (.CFB0(), .CFB1(), .DFB(), .FABRICOUT(), .INCDEC(), .Q1(), .Q2(serdesOut1[4]), .Q3(serdesOut1[5]), .Q4(serdesOut1[6]),
      .SHIFTOUT(), .VALID(), .BITSLIP(bitslipSlv[2]), .CE0(1'b1), .CLK0(clkIo), .CLK1(1'b0), .CLKDIV(clkDiv), .D(1'b0),
      .IOCE(ioce), .RST(1'b0), .SHIFTIN(shift[2]));
*/
 
	// deserialization of "B1" ADC output
/*   ISERDES2 #(.BITSLIP_ENABLE("TRUE"), .DATA_RATE("SDR"), .DATA_WIDTH(7), .INTERFACE_TYPE("RETIMED"), .SERDES_MODE("MASTER")
		) adcB1_P (.CFB0(), .CFB1(), .DFB(), .FABRICOUT(), .INCDEC(), .Q1(serdesOut1[7]), .Q2(serdesOut1[8]), .Q3(serdesOut1[9]), .Q4(serdesOut1[10]),
      .SHIFTOUT(shift[3]), .VALID(), .BITSLIP(bitslipMst[3]), .CE0(1'b1), .CLK0(clkIo), .CLK1(1'b0), .CLKDIV(clkDiv), .D(dataIn[3]),
      .IOCE(ioce), .RST(1'b0), .SHIFTIN(1'b0));
*/      
              ISERDESE2 #(
        .DATA_RATE("SDR"),           // DDR, SDR
        .DATA_WIDTH(7),              // Parallel data width (2-8,10,14)
        .DYN_CLKDIV_INV_EN("FALSE"), // Enable DYNCLKDIVINVSEL inversion (FALSE, TRUE)
        .DYN_CLK_INV_EN("FALSE"),    // Enable DYNCLKINVSEL inversion (FALSE, TRUE)
        // INIT_Q1 - INIT_Q4: Initial value on the Q outputs (0/1)
        .INIT_Q1(1'b0),
        .INIT_Q2(1'b0),
        .INIT_Q3(1'b0),
        .INIT_Q4(1'b0),
        .INTERFACE_TYPE("NETWORKING"),   // MEMORY, MEMORY_DDR3, MEMORY_QDR, NETWORKING, OVERSAMPLE
        .IOBDELAY("NONE"),           // NONE, BOTH, IBUF, IFD
        .NUM_CE(2),                  // Number of clock enables (1,2)
        .OFB_USED("FALSE"),          // Select OFB path (FALSE, TRUE)
        .SERDES_MODE("MASTER"),      // MASTER, SLAVE
        // SRVAL_Q1 - SRVAL_Q4: Q output values when SR is used (0/1)
        .SRVAL_Q1(1'b0),
        .SRVAL_Q2(1'b0),
        .SRVAL_Q3(1'b0),
        .SRVAL_Q4(1'b0) 
     )
     adcB1 (
        .O(),                       // 1-bit output: Combinatorial output
        // Q1 - Q8: 1-bit (each) output: Registered data outputs
        .Q1(serdesOut1[13]),
        .Q2(serdesOut1[12]),
        .Q3(serdesOut1[11]),
        .Q4(serdesOut1[10]),
        .Q5(serdesOut1[9]),
        .Q6(serdesOut1[8]),
        .Q7(serdesOut1[7]), 
        .Q8(),
        // SHIFTOUT1, SHIFTOUT2: 1-bit (each) output: Data width expansion output ports
        .SHIFTOUT1(),
        .SHIFTOUT2(),
        .BITSLIP(bitslip[3]),           // 1-bit input: The BITSLIP pin performs a Bitslip operation synchronous to
                                     // CLKDIV when asserted (active High). Subsequently, the data seen on the Q1
                                     // to Q8 output ports will shift, as in a barrel-shifter operation, one
                                     // position every time Bitslip is invoked (DDR operation is different from
                                     // SDR).
  
        // CE1, CE2: 1-bit (each) input: Data register clock enable inputs
        .CE1(1'b1),
        .CE2(1'b1),
        .CLKDIVP(),           // 1-bit input: TBD
        // Clocks: 1-bit (each) input: ISERDESE2 clock input ports
        .CLK(clkIo),                   // 1-bit input: High-speed clock
        .CLKB(),                 // 1-bit input: High-speed secondary clock
        .CLKDIV(clkDiv),             // 1-bit input: Divided clock
        .OCLK(),                 // 1-bit input: High speed output clock used when INTERFACE_TYPE="MEMORY" 
        // Dynamic Clock Inversions: 1-bit (each) input: Dynamic clock inversion pins to switch clock polarity
        .DYNCLKDIVSEL(), // 1-bit input: Dynamic CLKDIV inversion
        .DYNCLKSEL(),       // 1-bit input: Dynamic CLK/CLKB inversion
        // Input Data: 1-bit (each) input: ISERDESE2 data input ports
        .D(dataIn[3]),                       // 1-bit input: Data input
        .DDLY(),                 // 1-bit input: Serial data from IDELAYE2
        .OFB(),                   // 1-bit input: Data feedback from OSERDESE2
        .OCLKB(),               // 1-bit input: High speed negative edge output clock
        .RST(1'b0),                   // 1-bit input: Active high asynchronous reset
        // SHIFTIN1, SHIFTIN2: 1-bit (each) input: Data width expansion input ports
        .SHIFTIN1(),
        .SHIFTIN2() 
     );
     
      
/*	ISERDES2 #(.BITSLIP_ENABLE("TRUE"), .DATA_RATE("SDR"), .DATA_WIDTH(7), .INTERFACE_TYPE("RETIMED"), .SERDES_MODE("SLAVE")
		) adcB1_N (.CFB0(), .CFB1(), .DFB(), .FABRICOUT(), .INCDEC(), .Q1(), .Q2(serdesOut1[11]), .Q3(serdesOut1[12]), .Q4(serdesOut1[13]),
      .SHIFTOUT(), .VALID(), .BITSLIP(bitslipSlv[3]), .CE0(1'b1), .CLK0(clkIo), .CLK1(1'b0), .CLKDIV(clkDiv), .D(1'b0),
      .IOCE(ioce), .RST(1'b0), .SHIFTIN(shift[3]));
*/      

	// deserialization of "C0" ADC output
/*   ISERDES2 #(.BITSLIP_ENABLE("TRUE"), .DATA_RATE("SDR"), .DATA_WIDTH(7), .INTERFACE_TYPE("RETIMED"), .SERDES_MODE("MASTER")
		) adcC0_P (.CFB0(), .CFB1(), .DFB(), .FABRICOUT(), .INCDEC(), .Q1(serdesOut2[0]), .Q2(serdesOut2[1]), .Q3(serdesOut2[2]), .Q4(serdesOut2[3]),
      .SHIFTOUT(shift[4]), .VALID(), .BITSLIP(bitslipMst[4]), .CE0(1'b1), .CLK0(clkIo), .CLK1(1'b0), .CLKDIV(clkDiv), .D(dataIn[4]),
      .IOCE(ioce), .RST(1'b0), .SHIFTIN(1'b0));
*/      
                    ISERDESE2 #(
.DATA_RATE("SDR"),           // DDR, SDR
.DATA_WIDTH(7),              // Parallel data width (2-8,10,14)
.DYN_CLKDIV_INV_EN("FALSE"), // Enable DYNCLKDIVINVSEL inversion (FALSE, TRUE)
.DYN_CLK_INV_EN("FALSE"),    // Enable DYNCLKINVSEL inversion (FALSE, TRUE)
// INIT_Q1 - INIT_Q4: Initial value on the Q outputs (0/1)
.INIT_Q1(1'b0),
.INIT_Q2(1'b0),
.INIT_Q3(1'b0),
.INIT_Q4(1'b0),
.INTERFACE_TYPE("NETWORKING"),   // MEMORY, MEMORY_DDR3, MEMORY_QDR, NETWORKING, OVERSAMPLE
.IOBDELAY("NONE"),           // NONE, BOTH, IBUF, IFD
.NUM_CE(2),                  // Number of clock enables (1,2)
.OFB_USED("FALSE"),          // Select OFB path (FALSE, TRUE)
.SERDES_MODE("MASTER"),      // MASTER, SLAVE
// SRVAL_Q1 - SRVAL_Q4: Q output values when SR is used (0/1)
.SRVAL_Q1(1'b0),
.SRVAL_Q2(1'b0),
.SRVAL_Q3(1'b0),
.SRVAL_Q4(1'b0) 
)
adcC0 (
.O(),                       // 1-bit output: Combinatorial output
// Q1 - Q8: 1-bit (each) output: Registered data outputs
.Q1(serdesOut2[6]),
.Q2(serdesOut2[5]),
.Q3(serdesOut2[4]),
.Q4(serdesOut2[3]),
.Q5(serdesOut2[2]),
.Q6(serdesOut2[1]),
.Q7(serdesOut2[0]),
.Q8(),
// SHIFTOUT1, SHIFTOUT2: 1-bit (each) output: Data width expansion output ports
.SHIFTOUT1(),
.SHIFTOUT2(),
.BITSLIP(bitslip[4]),           // 1-bit input: The BITSLIP pin performs a Bitslip operation synchronous to
                             // CLKDIV when asserted (active High). Subsequently, the data seen on the Q1
                             // to Q8 output ports will shift, as in a barrel-shifter operation, one
                             // position every time Bitslip is invoked (DDR operation is different from
                             // SDR).

// CE1, CE2: 1-bit (each) input: Data register clock enable inputs
.CE1(1'b1),
.CE2(1'b1),
.CLKDIVP(),           // 1-bit input: TBD
// Clocks: 1-bit (each) input: ISERDESE2 clock input ports
.CLK(clkIo),                   // 1-bit input: High-speed clock
.CLKB(),                 // 1-bit input: High-speed secondary clock
.CLKDIV(clkDiv),             // 1-bit input: Divided clock
.OCLK(),                 // 1-bit input: High speed output clock used when INTERFACE_TYPE="MEMORY" 
// Dynamic Clock Inversions: 1-bit (each) input: Dynamic clock inversion pins to switch clock polarity
.DYNCLKDIVSEL(), // 1-bit input: Dynamic CLKDIV inversion
.DYNCLKSEL(),       // 1-bit input: Dynamic CLK/CLKB inversion
// Input Data: 1-bit (each) input: ISERDESE2 data input ports
.D(dataIn[4]),                       // 1-bit input: Data input
.DDLY(),                 // 1-bit input: Serial data from IDELAYE2
.OFB(),                   // 1-bit input: Data feedback from OSERDESE2
.OCLKB(),               // 1-bit input: High speed negative edge output clock
.RST(1'b0),                   // 1-bit input: Active high asynchronous reset
// SHIFTIN1, SHIFTIN2: 1-bit (each) input: Data width expansion input ports
.SHIFTIN1(),
.SHIFTIN2() 
);

      
/*	ISERDES2 #(.BITSLIP_ENABLE("TRUE"), .DATA_RATE("SDR"), .DATA_WIDTH(7), .INTERFACE_TYPE("RETIMED"), .SERDES_MODE("SLAVE")
		) adcC0_N (.CFB0(), .CFB1(), .DFB(), .FABRICOUT(), .INCDEC(), .Q1(), .Q2(serdesOut2[4]), .Q3(serdesOut2[5]), .Q4(serdesOut2[6]),
      .SHIFTOUT(), .VALID(), .BITSLIP(bitslipSlv[4]), .CE0(1'b1), .CLK0(clkIo), .CLK1(1'b0), .CLKDIV(clkDiv), .D(1'b0),
      .IOCE(ioce), .RST(1'b0), .SHIFTIN(shift[4]));
*/      

      
	// deserialization of "C1" ADC output
/*   ISERDES2 #(.BITSLIP_ENABLE("TRUE"), .DATA_RATE("SDR"), .DATA_WIDTH(7), .INTERFACE_TYPE("RETIMED"), .SERDES_MODE("MASTER")
		) adcC1_P (.CFB0(), .CFB1(), .DFB(), .FABRICOUT(), .INCDEC(), .Q1(serdesOut2[7]), .Q2(serdesOut2[8]), .Q3(serdesOut2[9]), .Q4(serdesOut2[10]),
      .SHIFTOUT(shift[5]), .VALID(), .BITSLIP(bitslipMst[5]), .CE0(1'b1), .CLK0(clkIo), .CLK1(1'b0), .CLKDIV(clkDiv), .D(dataIn[5]),
      .IOCE(ioce), .RST(1'b0), .SHIFTIN(1'b0));
*/      
                    ISERDESE2 #(
.DATA_RATE("SDR"),           // DDR, SDR
.DATA_WIDTH(7),              // Parallel data width (2-8,10,14)
.DYN_CLKDIV_INV_EN("FALSE"), // Enable DYNCLKDIVINVSEL inversion (FALSE, TRUE)
.DYN_CLK_INV_EN("FALSE"),    // Enable DYNCLKINVSEL inversion (FALSE, TRUE)
// INIT_Q1 - INIT_Q4: Initial value on the Q outputs (0/1)
.INIT_Q1(1'b0),
.INIT_Q2(1'b0),
.INIT_Q3(1'b0),
.INIT_Q4(1'b0),
.INTERFACE_TYPE("NETWORKING"),   // MEMORY, MEMORY_DDR3, MEMORY_QDR, NETWORKING, OVERSAMPLE
.IOBDELAY("NONE"),           // NONE, BOTH, IBUF, IFD
.NUM_CE(2),                  // Number of clock enables (1,2)
.OFB_USED("FALSE"),          // Select OFB path (FALSE, TRUE)
.SERDES_MODE("MASTER"),      // MASTER, SLAVE
// SRVAL_Q1 - SRVAL_Q4: Q output values when SR is used (0/1)
.SRVAL_Q1(1'b0),
.SRVAL_Q2(1'b0),
.SRVAL_Q3(1'b0),
.SRVAL_Q4(1'b0) 
)
adcC1 (
.O(),                       // 1-bit output: Combinatorial output
// Q1 - Q8: 1-bit (each) output: Registered data outputs
.Q1(serdesOut2[13]),
.Q2(serdesOut2[12]),
.Q3(serdesOut2[11]),
.Q4(serdesOut2[10]),
.Q5(serdesOut2[9]),
.Q6(serdesOut2[8]),
.Q7(serdesOut2[7]),
.Q8(),
// SHIFTOUT1, SHIFTOUT2: 1-bit (each) output: Data width expansion output ports
.SHIFTOUT1(),
.SHIFTOUT2(),
.BITSLIP(bitslip[5]),           // 1-bit input: The BITSLIP pin performs a Bitslip operation synchronous to
                             // CLKDIV when asserted (active High). Subsequently, the data seen on the Q1
                             // to Q8 output ports will shift, as in a barrel-shifter operation, one
                             // position every time Bitslip is invoked (DDR operation is different from
                             // SDR).

// CE1, CE2: 1-bit (each) input: Data register clock enable inputs
.CE1(1'b1),
.CE2(1'b1),
.CLKDIVP(),           // 1-bit input: TBD
// Clocks: 1-bit (each) input: ISERDESE2 clock input ports
.CLK(clkIo),                   // 1-bit input: High-speed clock
.CLKB(),                 // 1-bit input: High-speed secondary clock
.CLKDIV(clkDiv),             // 1-bit input: Divided clock
.OCLK(),                 // 1-bit input: High speed output clock used when INTERFACE_TYPE="MEMORY" 
// Dynamic Clock Inversions: 1-bit (each) input: Dynamic clock inversion pins to switch clock polarity
.DYNCLKDIVSEL(), // 1-bit input: Dynamic CLKDIV inversion
.DYNCLKSEL(),       // 1-bit input: Dynamic CLK/CLKB inversion
// Input Data: 1-bit (each) input: ISERDESE2 data input ports
.D(dataIn[5]),                       // 1-bit input: Data input
.DDLY(),                 // 1-bit input: Serial data from IDELAYE2
.OFB(),                   // 1-bit input: Data feedback from OSERDESE2
.OCLKB(),               // 1-bit input: High speed negative edge output clock
.RST(1'b0),                   // 1-bit input: Active high asynchronous reset
// SHIFTIN1, SHIFTIN2: 1-bit (each) input: Data width expansion input ports
.SHIFTIN1(),
.SHIFTIN2() 
);

      
/*	ISERDES2 #(.BITSLIP_ENABLE("TRUE"), .DATA_RATE("SDR"), .DATA_WIDTH(7), .INTERFACE_TYPE("RETIMED"), .SERDES_MODE("SLAVE")
		) adcC1_N (.CFB0(), .CFB1(), .DFB(), .FABRICOUT(), .INCDEC(), .Q1(), .Q2(serdesOut2[11]), .Q3(serdesOut2[12]), .Q4(serdesOut2[13]),
      .SHIFTOUT(), .VALID(), .BITSLIP(bitslipSlv[5]), .CE0(1'b1), .CLK0(clkIo), .CLK1(1'b0), .CLKDIV(clkDiv), .D(1'b0),
      .IOCE(ioce), .RST(1'b0), .SHIFTIN(shift[5]));
*/      
 
	// deserialization of "D0" ADC output
/*   ISERDES2 #(.BITSLIP_ENABLE("TRUE"), .DATA_RATE("SDR"), .DATA_WIDTH(7), .INTERFACE_TYPE("RETIMED"), .SERDES_MODE("MASTER")
		) adcD0_P (.CFB0(), .CFB1(), .DFB(), .FABRICOUT(), .INCDEC(), .Q1(serdesOut3[0]), .Q2(serdesOut3[1]), .Q3(serdesOut3[2]), .Q4(serdesOut3[3]),
      .SHIFTOUT(shift[6]), .VALID(), .BITSLIP(bitslipMst[6]), .CE0(1'b1), .CLK0(clkIo), .CLK1(1'b0), .CLKDIV(clkDiv), .D(dataIn[6]),
      .IOCE(ioce), .RST(1'b0), .SHIFTIN(1'b0));
*/      
                    ISERDESE2 #(
.DATA_RATE("SDR"),           // DDR, SDR
.DATA_WIDTH(7),              // Parallel data width (2-8,10,14)
.DYN_CLKDIV_INV_EN("FALSE"), // Enable DYNCLKDIVINVSEL inversion (FALSE, TRUE)
.DYN_CLK_INV_EN("FALSE"),    // Enable DYNCLKINVSEL inversion (FALSE, TRUE)
// INIT_Q1 - INIT_Q4: Initial value on the Q outputs (0/1)
.INIT_Q1(1'b0),
.INIT_Q2(1'b0),
.INIT_Q3(1'b0),
.INIT_Q4(1'b0),
.INTERFACE_TYPE("NETWORKING"),   // MEMORY, MEMORY_DDR3, MEMORY_QDR, NETWORKING, OVERSAMPLE
.IOBDELAY("NONE"),           // NONE, BOTH, IBUF, IFD
.NUM_CE(2),                  // Number of clock enables (1,2)
.OFB_USED("FALSE"),          // Select OFB path (FALSE, TRUE)
.SERDES_MODE("MASTER"),      // MASTER, SLAVE
// SRVAL_Q1 - SRVAL_Q4: Q output values when SR is used (0/1)
.SRVAL_Q1(1'b0),
.SRVAL_Q2(1'b0),
.SRVAL_Q3(1'b0),
.SRVAL_Q4(1'b0) 
)
adcD0 (
.O(),                       // 1-bit output: Combinatorial output
// Q1 - Q8: 1-bit (each) output: Registered data outputs
.Q1(serdesOut3[6]),
.Q2(serdesOut3[5]),
.Q3(serdesOut3[4]),
.Q4(serdesOut3[3]),
.Q5(serdesOut3[2]),
.Q6(serdesOut3[1]),
.Q7(serdesOut3[0]),
.Q8(),
// SHIFTOUT1, SHIFTOUT2: 1-bit (each) output: Data width expansion output ports
.SHIFTOUT1(),
.SHIFTOUT2(),
.BITSLIP(bitslip[6]),           // 1-bit input: The BITSLIP pin performs a Bitslip operation synchronous to
                             // CLKDIV when asserted (active High). Subsequently, the data seen on the Q1
                             // to Q8 output ports will shift, as in a barrel-shifter operation, one
                             // position every time Bitslip is invoked (DDR operation is different from
                             // SDR).

// CE1, CE2: 1-bit (each) input: Data register clock enable inputs
.CE1(1'b1),
.CE2(1'b1),
.CLKDIVP(),           // 1-bit input: TBD
// Clocks: 1-bit (each) input: ISERDESE2 clock input ports
.CLK(clkIo),                   // 1-bit input: High-speed clock
.CLKB(),                 // 1-bit input: High-speed secondary clock
.CLKDIV(clkDiv),             // 1-bit input: Divided clock
.OCLK(),                 // 1-bit input: High speed output clock used when INTERFACE_TYPE="MEMORY" 
// Dynamic Clock Inversions: 1-bit (each) input: Dynamic clock inversion pins to switch clock polarity
.DYNCLKDIVSEL(), // 1-bit input: Dynamic CLKDIV inversion
.DYNCLKSEL(),       // 1-bit input: Dynamic CLK/CLKB inversion
// Input Data: 1-bit (each) input: ISERDESE2 data input ports
.D(dataIn[6]),                       // 1-bit input: Data input
.DDLY(),                 // 1-bit input: Serial data from IDELAYE2
.OFB(),                   // 1-bit input: Data feedback from OSERDESE2
.OCLKB(),               // 1-bit input: High speed negative edge output clock
.RST(1'b0),                   // 1-bit input: Active high asynchronous reset
// SHIFTIN1, SHIFTIN2: 1-bit (each) input: Data width expansion input ports
.SHIFTIN1(),
.SHIFTIN2() 
);

      
/*	ISERDES2 #(.BITSLIP_ENABLE("TRUE"), .DATA_RATE("SDR"), .DATA_WIDTH(7), .INTERFACE_TYPE("RETIMED"), .SERDES_MODE("SLAVE")
		) adcD0_N (.CFB0(), .CFB1(), .DFB(), .FABRICOUT(), .INCDEC(), .Q1(), .Q2(serdesOut3[4]), .Q3(serdesOut3[5]), .Q4(serdesOut3[6]),
      .SHIFTOUT(), .VALID(), .BITSLIP(bitslipSlv[6]), .CE0(1'b1), .CLK0(clkIo), .CLK1(1'b0), .CLKDIV(clkDiv), .D(1'b0),
      .IOCE(ioce), .RST(1'b0), .SHIFTIN(shift[6]));
*/      
 
      
	// deserialization of "D1" ADC output
/*   ISERDES2 #(.BITSLIP_ENABLE("TRUE"), .DATA_RATE("SDR"), .DATA_WIDTH(7), .INTERFACE_TYPE("RETIMED"), .SERDES_MODE("MASTER")
		) adcD1_P (.CFB0(), .CFB1(), .DFB(), .FABRICOUT(), .INCDEC(), .Q1(serdesOut3[7]), .Q2(serdesOut3[8]), .Q3(serdesOut3[9]), .Q4(serdesOut3[10]),
      .SHIFTOUT(shift[7]), .VALID(), .BITSLIP(bitslipMst[7]), .CE0(1'b1), .CLK0(clkIo), .CLK1(1'b0), .CLKDIV(clkDiv), .D(dataIn[7]),
      .IOCE(ioce), .RST(1'b0), .SHIFTIN(1'b0));
*/      
                          ISERDESE2 #(
.DATA_RATE("SDR"),           // DDR, SDR
.DATA_WIDTH(7),              // Parallel data width (2-8,10,14)
.DYN_CLKDIV_INV_EN("FALSE"), // Enable DYNCLKDIVINVSEL inversion (FALSE, TRUE)
.DYN_CLK_INV_EN("FALSE"),    // Enable DYNCLKINVSEL inversion (FALSE, TRUE)
// INIT_Q1 - INIT_Q4: Initial value on the Q outputs (0/1)
.INIT_Q1(1'b0),
.INIT_Q2(1'b0),
.INIT_Q3(1'b0),
.INIT_Q4(1'b0),
.INTERFACE_TYPE("NETWORKING"),   // MEMORY, MEMORY_DDR3, MEMORY_QDR, NETWORKING, OVERSAMPLE
.IOBDELAY("NONE"),           // NONE, BOTH, IBUF, IFD
.NUM_CE(2),                  // Number of clock enables (1,2)
.OFB_USED("FALSE"),          // Select OFB path (FALSE, TRUE)
.SERDES_MODE("MASTER"),      // MASTER, SLAVE
// SRVAL_Q1 - SRVAL_Q4: Q output values when SR is used (0/1)
.SRVAL_Q1(1'b0),
.SRVAL_Q2(1'b0),
.SRVAL_Q3(1'b0),
.SRVAL_Q4(1'b0) 
)
adcD1 (
.O(),                       // 1-bit output: Combinatorial output
// Q1 - Q8: 1-bit (each) output: Registered data outputs
.Q1(serdesOut3[13]),
.Q2(serdesOut3[12]),
.Q3(serdesOut3[11]),
.Q4(serdesOut3[10]),
.Q5(serdesOut3[9]),
.Q6(serdesOut3[8]),
.Q7(serdesOut3[7]),
.Q8(),
// SHIFTOUT1, SHIFTOUT2: 1-bit (each) output: Data width expansion output ports
.SHIFTOUT1(),
.SHIFTOUT2(),
.BITSLIP(bitslip[7]),           // 1-bit input: The BITSLIP pin performs a Bitslip operation synchronous to
               // CLKDIV when asserted (active High). Subsequently, the data seen on the Q1
               // to Q8 output ports will shift, as in a barrel-shifter operation, one
               // position every time Bitslip is invoked (DDR operation is different from
               // SDR).

// CE1, CE2: 1-bit (each) input: Data register clock enable inputs
.CE1(1'b1),
.CE2(1'b1),
.CLKDIVP(),           // 1-bit input: TBD
// Clocks: 1-bit (each) input: ISERDESE2 clock input ports
.CLK(clkIo),                   // 1-bit input: High-speed clock
.CLKB(),                 // 1-bit input: High-speed secondary clock
.CLKDIV(clkDiv),             // 1-bit input: Divided clock
.OCLK(),                 // 1-bit input: High speed output clock used when INTERFACE_TYPE="MEMORY" 
// Dynamic Clock Inversions: 1-bit (each) input: Dynamic clock inversion pins to switch clock polarity
.DYNCLKDIVSEL(), // 1-bit input: Dynamic CLKDIV inversion
.DYNCLKSEL(),       // 1-bit input: Dynamic CLK/CLKB inversion
// Input Data: 1-bit (each) input: ISERDESE2 data input ports
.D(dataIn[7]),                       // 1-bit input: Data input
.DDLY(),                 // 1-bit input: Serial data from IDELAYE2
.OFB(),                   // 1-bit input: Data feedback from OSERDESE2
.OCLKB(),               // 1-bit input: High speed negative edge output clock
.RST(1'b0),                   // 1-bit input: Active high asynchronous reset
// SHIFTIN1, SHIFTIN2: 1-bit (each) input: Data width expansion input ports
.SHIFTIN1(),
.SHIFTIN2() 
);

      
/*	ISERDES2 #(.BITSLIP_ENABLE("TRUE"), .DATA_RATE("SDR"), .DATA_WIDTH(7), .INTERFACE_TYPE("RETIMED"), .SERDES_MODE("SLAVE")
		) adcD1_N (.CFB0(), .CFB1(), .DFB(), .FABRICOUT(), .INCDEC(), .Q1(), .Q2(serdesOut3[11]), .Q3(serdesOut3[12]), .Q4(serdesOut3[13]),
      .SHIFTOUT(), .VALID(), .BITSLIP(bitslipSlv[7]), .CE0(1'b1), .CLK0(clkIo), .CLK1(1'b0), .CLKDIV(clkDiv), .D(1'b0),
      .IOCE(ioce), .RST(1'b0), .SHIFTIN(shift[7]));
*/      

      
      
      
endmodule
