////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.
////////////////////////////////////////////////////////////////////////////////
//   ____  ____
//  /   /\/   /
// /___/  \  /    Vendor: Xilinx
// \   \   \/     Version: P.15xf
//  \   \         Application: netgen
//  /   /         Filename: multU16U16.v
// /___/   /\     Timestamp: Fri Feb 22 12:11:10 2013
// \   \  /  \ 
//  \___\/\___\
//             
// Command	: -w -sim -ofmt verilog Y:/Desktop/210code/LLRF-PED2-FGPDB-cores/tmp/_cg/multU16U16.ngc Y:/Desktop/210code/LLRF-PED2-FGPDB-cores/tmp/_cg/multU16U16.v 
// Device	: 6slx150tfgg900-2
// Input file	: Y:/Desktop/210code/LLRF-PED2-FGPDB-cores/tmp/_cg/multU16U16.ngc
// Output file	: Y:/Desktop/210code/LLRF-PED2-FGPDB-cores/tmp/_cg/multU16U16.v
// # of Modules	: 1
// Design Name	: multU16U16
// Xilinx        : C:\Xilinx\14.1\ISE_DS\ISE\
//             
// Purpose:    
//     This verilog netlist is a verification model and uses simulation 
//     primitives which may not represent the true implementation of the 
//     device, however the netlist is functionally correct and should not 
//     be modified. This file cannot be synthesized and should only be used 
//     with supported simulation tools.
//             
// Reference:  
//     Command Line Tools User Guide, Chapter 23 and Synthesis and Simulation Design Guide, Chapter 6
//             
////////////////////////////////////////////////////////////////////////////////

`timescale 1 ns/1 ps

module multU16U16 (
  clk, a, b, p
)/* synthesis syn_black_box syn_noprune=1 */;
  input clk;
  input [15 : 0] a;
  input [15 : 0] b;
  output [31 : 0] p;
  
  // synthesis translate_off
  
  wire N0;
  wire N1;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_CARRYOUTF_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_CARRYOUT_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_BCOUT<17>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_BCOUT<16>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_BCOUT<15>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_BCOUT<14>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_BCOUT<13>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_BCOUT<12>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_BCOUT<11>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_BCOUT<10>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_BCOUT<9>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_BCOUT<8>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_BCOUT<7>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_BCOUT<6>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_BCOUT<5>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_BCOUT<4>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_BCOUT<3>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_BCOUT<2>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_BCOUT<1>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_BCOUT<0>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<47>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<46>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<45>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<44>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<43>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<42>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<41>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<40>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<39>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<38>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<37>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<36>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<35>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<34>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<33>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<32>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<31>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<30>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<29>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<28>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<27>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<26>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<25>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<24>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<23>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<22>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<21>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<20>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<19>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<18>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<17>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<16>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<15>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<14>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<13>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<12>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<11>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<10>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<9>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<8>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<7>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<6>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<5>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<4>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<3>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<2>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<1>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<0>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<47>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<46>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<45>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<44>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<43>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<42>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<41>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<40>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<39>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<38>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<37>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<36>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<35>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<34>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<33>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<32>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<31>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<30>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<29>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<28>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<27>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<26>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<25>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<24>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<23>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<22>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<21>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<20>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<19>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<18>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<17>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<16>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<15>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<14>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<13>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<12>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<11>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<10>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<9>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<8>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<7>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<6>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<5>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<4>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<3>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<2>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<1>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<0>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<47>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<46>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<45>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<44>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<43>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<42>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<41>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<40>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<39>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<38>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<37>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<36>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<35>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<34>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<33>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<32>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<31>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<30>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<29>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<28>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<27>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<26>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<25>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<24>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<23>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<22>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<21>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<20>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<19>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<18>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<17>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<16>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<15>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<14>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<13>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<12>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<11>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<10>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<9>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<8>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<7>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<6>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<5>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<4>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<3>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<2>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<1>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<0>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_M<35>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_M<34>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_M<33>_UNCONNECTED ;
  wire \NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_M<32>_UNCONNECTED ;
  wire [31 : 0] \U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg ;
  assign
    p[31] = \U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [31],
    p[30] = \U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [30],
    p[29] = \U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [29],
    p[28] = \U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [28],
    p[27] = \U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [27],
    p[26] = \U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [26],
    p[25] = \U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [25],
    p[24] = \U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [24],
    p[23] = \U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [23],
    p[22] = \U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [22],
    p[21] = \U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [21],
    p[20] = \U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [20],
    p[19] = \U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [19],
    p[18] = \U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [18],
    p[17] = \U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [17],
    p[16] = \U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [16],
    p[15] = \U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [15],
    p[14] = \U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [14],
    p[13] = \U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [13],
    p[12] = \U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [12],
    p[11] = \U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [11],
    p[10] = \U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [10],
    p[9] = \U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [9],
    p[8] = \U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [8],
    p[7] = \U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [7],
    p[6] = \U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [6],
    p[5] = \U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [5],
    p[4] = \U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [4],
    p[3] = \U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [3],
    p[2] = \U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [2],
    p[1] = \U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [1],
    p[0] = \U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [0];
  VCC   XST_VCC (
    .P(N0)
  );
  GND   XST_GND (
    .G(N1)
  );
  DSP48A1 #(
    .CARRYINSEL ( "OPMODE5" ),
    .A0REG ( 0 ),
    .A1REG ( 0 ),
    .B0REG ( 0 ),
    .B1REG ( 0 ),
    .CREG ( 0 ),
    .MREG ( 1 ),
    .PREG ( 0 ),
    .OPMODEREG ( 0 ),
    .CARRYINREG ( 0 ),
    .CARRYOUTREG ( 0 ),
    .RSTTYPE ( "SYNC" ),
    .DREG ( 0 ))
  \U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT  (
    .CECARRYIN(N1),
    .RSTC(N1),
    .RSTCARRYIN(N1),
    .CED(N1),
    .RSTD(N1),
    .CEOPMODE(N1),
    .CEC(N1),
    .CARRYOUTF(\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_CARRYOUTF_UNCONNECTED ),
    .RSTOPMODE(N1),
    .RSTM(N1),
    .CLK(clk),
    .RSTB(N1),
    .CEM(N0),
    .CEB(N1),
    .CARRYIN(N1),
    .CEP(N1),
    .CEA(N1),
    .CARRYOUT(\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_CARRYOUT_UNCONNECTED ),
    .RSTA(N1),
    .RSTP(N1),
    .B({N1, N1, a[15], a[14], a[13], a[12], a[11], a[10], a[9], a[8], a[7], a[6], a[5], a[4], a[3], a[2], a[1], a[0]}),
    .BCOUT({\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_BCOUT<17>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_BCOUT<16>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_BCOUT<15>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_BCOUT<14>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_BCOUT<13>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_BCOUT<12>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_BCOUT<11>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_BCOUT<10>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_BCOUT<9>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_BCOUT<8>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_BCOUT<7>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_BCOUT<6>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_BCOUT<5>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_BCOUT<4>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_BCOUT<3>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_BCOUT<2>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_BCOUT<1>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_BCOUT<0>_UNCONNECTED }),
    .PCIN({\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<47>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<46>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<45>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<44>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<43>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<42>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<41>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<40>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<39>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<38>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<37>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<36>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<35>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<34>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<33>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<32>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<31>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<30>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<29>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<28>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<27>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<26>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<25>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<24>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<23>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<22>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<21>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<20>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<19>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<18>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<17>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<16>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<15>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<14>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<13>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<12>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<11>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<10>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<9>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<8>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<7>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<6>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<5>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<4>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<3>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<2>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<1>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCIN<0>_UNCONNECTED }),
    .C({N1, N1, N1, N1, N1, N1, N1, N1, N1, N1, N1, N1, N1, N1, N1, N1, N1, N1, N1, N1, N1, N1, N1, N1, N1, N1, N1, N1, N1, N1, N1, N1, N1, N1, N1, N1
, N1, N1, N1, N1, N1, N1, N1, N1, N1, N1, N1, N1}),
    .P({\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<47>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<46>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<45>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<44>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<43>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<42>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<41>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<40>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<39>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<38>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<37>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<36>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<35>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<34>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<33>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<32>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<31>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<30>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<29>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<28>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<27>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<26>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<25>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<24>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<23>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<22>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<21>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<20>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<19>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<18>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<17>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<16>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<15>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<14>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<13>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<12>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<11>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<10>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<9>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<8>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<7>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<6>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<5>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<4>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<3>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<2>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<1>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_P<0>_UNCONNECTED }),
    .OPMODE({N1, N1, N1, N1, N1, N1, N1, N0}),
    .D({N1, N1, N1, N1, N1, N1, N1, N1, N1, N1, N1, N1, N1, N1, N1, N1, N1, N1}),
    .PCOUT({\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<47>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<46>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<45>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<44>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<43>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<42>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<41>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<40>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<39>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<38>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<37>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<36>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<35>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<34>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<33>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<32>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<31>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<30>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<29>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<28>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<27>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<26>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<25>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<24>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<23>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<22>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<21>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<20>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<19>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<18>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<17>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<16>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<15>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<14>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<13>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<12>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<11>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<10>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<9>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<8>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<7>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<6>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<5>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<4>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<3>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<2>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<1>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_PCOUT<0>_UNCONNECTED }),
    .A({N1, N1, b[15], b[14], b[13], b[12], b[11], b[10], b[9], b[8], b[7], b[6], b[5], b[4], b[3], b[2], b[1], b[0]}),
    .M({\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_M<35>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_M<34>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_M<33>_UNCONNECTED , 
\NLW_U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/Mmult_inferred_dsp.m_reg[35]_GND_14_o_mux_4_OUT_M<32>_UNCONNECTED , 
\U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [31], 
\U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [30], 
\U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [29], 
\U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [28], 
\U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [27], 
\U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [26], 
\U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [25], 
\U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [24], 
\U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [23], 
\U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [22], 
\U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [21], 
\U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [20], 
\U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [19], 
\U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [18], 
\U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [17], 
\U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [16], 
\U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [15], 
\U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [14], 
\U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [13], 
\U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [12], 
\U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [11], 
\U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [10], 
\U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [9], \U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [8]
, \U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [7], 
\U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [6], \U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [5]
, \U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [4], 
\U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [3], \U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [2]
, \U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [1], 
\U0/i_mult/gEMBEDDED_MULT.gEMB_MULTS_only.gDSP.iDSP/inferred_dsp.m_reg [0]})
  );

// synthesis translate_on

endmodule

// synthesis translate_off

`ifndef GLBL
`define GLBL

`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;

    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (weak1, weak0) GSR = GSR_int;
    assign (weak1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule

`endif

// synthesis translate_on
