// Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2016.4 (win64) Build 1733598 Wed Dec 14 22:35:39 MST 2016
// Date        : Wed Feb 14 11:20:39 2018
// Host        : SHRIRAJKUNJA80D running 64-bit Service Pack 1  (build 7601)
// Command     : write_verilog -force -mode funcsim
//               c:/Users/shrirajkunjir/ip_repo/LLRF_axi_per_1.0/src/mult_peakdetect/mult_peakdetect_sim_netlist.v
// Design      : mult_peakdetect
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a200tfbg484-2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "mult_peakdetect,mult_gen_v12_0_12,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "mult_gen_v12_0_12,Vivado 2016.4" *) 
(* NotValidForBitStream *)
module mult_peakdetect
   (CLK,
    A,
    B,
    P);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 a_intf DATA" *) input [17:0]A;
  (* x_interface_info = "xilinx.com:signal:data:1.0 b_intf DATA" *) input [17:0]B;
  (* x_interface_info = "xilinx.com:signal:data:1.0 p_intf DATA" *) output [31:0]P;

  wire [17:0]A;
  wire [17:0]B;
  wire CLK;
  wire [31:0]P;
  wire [47:0]NLW_U0_PCASC_UNCONNECTED;
  wire [1:0]NLW_U0_ZERO_DETECT_UNCONNECTED;

  (* C_A_TYPE = "0" *) 
  (* C_A_WIDTH = "18" *) 
  (* C_B_TYPE = "0" *) 
  (* C_B_VALUE = "10000001" *) 
  (* C_B_WIDTH = "18" *) 
  (* C_CCM_IMP = "0" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_ZERO_DETECT = "0" *) 
  (* C_LATENCY = "3" *) 
  (* C_MODEL_TYPE = "0" *) 
  (* C_MULT_TYPE = "1" *) 
  (* C_OPTIMIZE_GOAL = "1" *) 
  (* C_OUT_HIGH = "31" *) 
  (* C_OUT_LOW = "0" *) 
  (* C_ROUND_OUTPUT = "0" *) 
  (* C_ROUND_PT = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  mult_peakdetect_mult_gen_v12_0_12 U0
       (.A(A),
        .B(B),
        .CE(1'b1),
        .CLK(CLK),
        .P(P),
        .PCASC(NLW_U0_PCASC_UNCONNECTED[47:0]),
        .SCLR(1'b0),
        .ZERO_DETECT(NLW_U0_ZERO_DETECT_UNCONNECTED[1:0]));
endmodule

(* C_A_TYPE = "0" *) (* C_A_WIDTH = "18" *) (* C_B_TYPE = "0" *) 
(* C_B_VALUE = "10000001" *) (* C_B_WIDTH = "18" *) (* C_CCM_IMP = "0" *) 
(* C_CE_OVERRIDES_SCLR = "0" *) (* C_HAS_CE = "0" *) (* C_HAS_SCLR = "0" *) 
(* C_HAS_ZERO_DETECT = "0" *) (* C_LATENCY = "3" *) (* C_MODEL_TYPE = "0" *) 
(* C_MULT_TYPE = "1" *) (* C_OPTIMIZE_GOAL = "1" *) (* C_OUT_HIGH = "31" *) 
(* C_OUT_LOW = "0" *) (* C_ROUND_OUTPUT = "0" *) (* C_ROUND_PT = "0" *) 
(* C_VERBOSITY = "0" *) (* C_XDEVICEFAMILY = "artix7" *) (* ORIG_REF_NAME = "mult_gen_v12_0_12" *) 
(* downgradeipidentifiedwarnings = "yes" *) 
module mult_peakdetect_mult_gen_v12_0_12
   (CLK,
    A,
    B,
    CE,
    SCLR,
    ZERO_DETECT,
    P,
    PCASC);
  input CLK;
  input [17:0]A;
  input [17:0]B;
  input CE;
  input SCLR;
  output [1:0]ZERO_DETECT;
  output [31:0]P;
  output [47:0]PCASC;

  wire \<const0> ;
  wire [17:0]A;
  wire [17:0]B;
  wire CLK;
  wire [31:0]P;
  wire [47:0]NLW_i_mult_PCASC_UNCONNECTED;
  wire [1:0]NLW_i_mult_ZERO_DETECT_UNCONNECTED;

  assign PCASC[47] = \<const0> ;
  assign PCASC[46] = \<const0> ;
  assign PCASC[45] = \<const0> ;
  assign PCASC[44] = \<const0> ;
  assign PCASC[43] = \<const0> ;
  assign PCASC[42] = \<const0> ;
  assign PCASC[41] = \<const0> ;
  assign PCASC[40] = \<const0> ;
  assign PCASC[39] = \<const0> ;
  assign PCASC[38] = \<const0> ;
  assign PCASC[37] = \<const0> ;
  assign PCASC[36] = \<const0> ;
  assign PCASC[35] = \<const0> ;
  assign PCASC[34] = \<const0> ;
  assign PCASC[33] = \<const0> ;
  assign PCASC[32] = \<const0> ;
  assign PCASC[31] = \<const0> ;
  assign PCASC[30] = \<const0> ;
  assign PCASC[29] = \<const0> ;
  assign PCASC[28] = \<const0> ;
  assign PCASC[27] = \<const0> ;
  assign PCASC[26] = \<const0> ;
  assign PCASC[25] = \<const0> ;
  assign PCASC[24] = \<const0> ;
  assign PCASC[23] = \<const0> ;
  assign PCASC[22] = \<const0> ;
  assign PCASC[21] = \<const0> ;
  assign PCASC[20] = \<const0> ;
  assign PCASC[19] = \<const0> ;
  assign PCASC[18] = \<const0> ;
  assign PCASC[17] = \<const0> ;
  assign PCASC[16] = \<const0> ;
  assign PCASC[15] = \<const0> ;
  assign PCASC[14] = \<const0> ;
  assign PCASC[13] = \<const0> ;
  assign PCASC[12] = \<const0> ;
  assign PCASC[11] = \<const0> ;
  assign PCASC[10] = \<const0> ;
  assign PCASC[9] = \<const0> ;
  assign PCASC[8] = \<const0> ;
  assign PCASC[7] = \<const0> ;
  assign PCASC[6] = \<const0> ;
  assign PCASC[5] = \<const0> ;
  assign PCASC[4] = \<const0> ;
  assign PCASC[3] = \<const0> ;
  assign PCASC[2] = \<const0> ;
  assign PCASC[1] = \<const0> ;
  assign PCASC[0] = \<const0> ;
  assign ZERO_DETECT[1] = \<const0> ;
  assign ZERO_DETECT[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  (* C_A_TYPE = "0" *) 
  (* C_A_WIDTH = "18" *) 
  (* C_B_TYPE = "0" *) 
  (* C_B_VALUE = "10000001" *) 
  (* C_B_WIDTH = "18" *) 
  (* C_CCM_IMP = "0" *) 
  (* C_CE_OVERRIDES_SCLR = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_ZERO_DETECT = "0" *) 
  (* C_LATENCY = "3" *) 
  (* C_MODEL_TYPE = "0" *) 
  (* C_MULT_TYPE = "1" *) 
  (* C_OPTIMIZE_GOAL = "1" *) 
  (* C_OUT_HIGH = "31" *) 
  (* C_OUT_LOW = "0" *) 
  (* C_ROUND_OUTPUT = "0" *) 
  (* C_ROUND_PT = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  mult_peakdetect_mult_gen_v12_0_12_viv i_mult
       (.A(A),
        .B(B),
        .CE(1'b0),
        .CLK(CLK),
        .P(P),
        .PCASC(NLW_i_mult_PCASC_UNCONNECTED[47:0]),
        .SCLR(1'b0),
        .ZERO_DETECT(NLW_i_mult_ZERO_DETECT_UNCONNECTED[1:0]));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2015"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
fPF16TcpNgM9dNC6nyb4WjUK+7bY8P+I62AEEiiM/KOMhIKuPOHBoWeWL2UjxSNO68WLeYIZp8lA
I7rHN/CieA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
E6OKJxjnDRUVVFwAhrQMAtoyRVVpuMKsXlca4m9CcIt6QI8vnYN0tf7gH3uVuxZ90322B7kUeFw5
Pu0UeqAoBaSyysHuDqXazxHy7oyk4BIWChvcrp7LULlVLcL76obtSwsXi1ORVmpdTi5b+AcD+WUo
OP1PSFj5jpodG+LwXm4=

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
x+agogSsgbiI6PGyBpMY8RQCDzLctIr3EaG23mH5kJHlNmNKNolnP54yJ8Y7nIFi6yl6tlyOLMoF
/kxU0pyFmIj8QM0/MArMxPTiemXbDLS2VKtonyK9dDH7VbjFnRWwzK0Ngkas0+nbW3TqGPAY98x3
251QPjQoZCw3A7W9PDc=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
KNs7hA49BKKrboRSEkqIGldOa3ndCnhjRkSn8lL1xFfKUn+p+Wbc09ogKV6YYnPU/RaF1LbzyoE4
udPSNea4bST+08IjO5GAxXqUugcig44J+hzpGKmh7oO0TuyNbYq1CnYcsZXaD9vsmNYz8fBDoW2S
VK/mYa21mBKTOuTdQ1yp3wi73aJ1G9N6Ngt7ovDUrjyd5oNxxNlvWU8JkJDinbEnci0qjZ3Wu9Wg
y44pHUXf6xqwFYJpZ1ZcGRKl83P8p74+pLzt19lw9TPlTfKI++IowVjb6wo36ztNDJS0QjQE5Riv
hwbPU/Bt3S82MVCY5NAA6bKC/8NnoWMbmX8Wiw==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
QaRubtGbYrmCghuFdQuTgTEtoVYYLcPnD5z0C7mo18fwCG17qy0y8mj8xWiwE6bo49IP1/JXSIw7
rTBwHFOVrmbm926sWNrF1r3IHB83C5cstprQ1om7vnkw9XX87SjkscphhkrHmi08jjzW4qX96m61
/ymclz5TlAocMQJGz/jwscvIMOrrbuH4SkWQOLQnRfx9GIOv5Y7PM+w/wuDSeFXsAXz7Ahq3/qmU
cylNfSufW7/zfN4RZB4u+d28AXsuFe03aSF1dpW+uBK1xtNZccvj9h9NMN0cuwxt8ZUlLJw8l6e2
hqRfTTZl1F4qnnrJu6w8h8uEGrmgnQG1AW0epg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinx_2016_05", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XXj6Nc59BeA5Kznlx14IKravf7ohERw7h0fbO7pT7/HsiPDCWh2DlTGpFUcnbNZslPN2RfE0nJNX
WMzLQtaHK4Bm6kxY71OsXEKm7MAIjEdLwOMtJTtlZrbm7chBbSxcW6sjWvI36jk5De3Yct9Ao1py
DpQ9NICUtRTwGG8SAiRkAXRh2Jv3rKvnookQrlVxIkNRSBMSgbwuTbq1ze/KMUZebBWwJNUVIC9r
RV/i9wjYXBOeCCUk+cGDC5uSpwdLXYV9ZxhQUU6C1ufAaK2m4OIUeBqPc2ski2O0qQYQ67c35k50
ynO8H9PTEROPEOn5c37S7feU+36OcOOAsVBTBA==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="base64", line_length=76, bytes=256)
`pragma protect key_block
DlWhGAPV4ElM7/u/5SzppYXSkVWYKkV8yx9au7k5A/uh95MQ9a1MzoV5AYlUlUep/KlvqDLQ77iJ
E9/5Fw4Ds1GqDp4591STCgFdxv3GZl10EwBkuLbLyl700gG9xcYFy9mDTAONw12PyVpoyp2+Ilsi
Gj0yU99/N7QwZI4Bn2pktRW5KcK1wdKLk1V8O13y2fRXjvPK5EadLtrQb7YZsCVsSJFHX5dG8w/A
wp/pqrNHNwkemAW4Xl/iVUrvnyjuiSahCtwMKyWzQXauuDKd3MvMDar3KLpzP82Ji3s71WbMqZiS
BlTZDGYUyjrM1xpVfqc/7NTyPcBCiiHN02v46A==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP05_001", key_method="rsa"
`pragma protect encoding = (enctype="base64", line_length=76, bytes=256)
`pragma protect key_block
CEZDDf/4iGCAOqVwHt/2btytwJq+GaLQ3r+zjaBelNiV2wVtRZgKiStg4BXIKIcfoVgFmYInWPYI
CW77Wn05lg4JXaCtOyEFOSzQS22/BP4msv4Rzg9/hDrqzcw5PoBMzQRKBwttf3n0RiY+77Nu+SsY
EWC0XNCIyAqyjk5gY6rbchE+807xymdY15vBCV7/w+kv6dZZ/vXPynTdtXsDTdrygxFGbV/Lh9Ll
JYt/IzwetzLOKafxHA/atul69ceo0qG8zSC3TUtpWHG3+LJ0OiYXHb+I6Mc2uMCoUiLi32lHKiqB
3V3COSsycC1XfWR8VVc1SrqwOwcl2Dp5cqm01A==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 7184)
`pragma protect data_block
2YNDtbtp/Nasl/W/DOAJNMmP3F1O+k0eXokOUj/Yqz+fQuNSqIB75+j644vf+5JW4BIpvZTFry+E
NM8iW5gMlUiIXt5Z7a/rpmbbYMLMyjiG691r1Tl93vgpHN5DFr0qOegKVMrbyls8SYEenBz0vZeg
3rFcXT7+41ykFC+QyIMJw0OaV6Ii0VGaqBksapNfFkRAdv7Zo2waY9m8jFhHI+6ur31M75jzRsw8
hzGdXgTqsSWYE0ja3D3XALpW4S0TUWqalAyXC35B7ygPZudOn7M4W/FXHrtGSJ7FjKfGLau0CG2Q
mqzHvJ0BrfwtOv4XFp/mrKpj5+45j+7fHzIxT3lfoqDlR5bE9zgw9hMWBApbBWu9uFFjtJPY63W8
43HVJydNX2gOGbV70z7i/uqyDDAK0nz1Sp4GJFqy0qnyZqKI7Y2H7D0oVm0qqubb0CM6y/WW1r++
1BhuIOFwBmsoXV26kMrgrpAfm825FtXU/DjG4i5f6vHsdY3JKiVxGqcwg44ZtZplRY4x2e1PPhXz
uuwStvStJmQUwJLou/H5bp/MVU/9omM+U5jRYaFGqTX5F2Ospy3aVhu5tMl+rs+bhYf1sotzHfd7
9kDKLwGe2LYTxyu0UxYNmdy/19/JQpdOrzLjeTAGOMd8+AvF35LINyqc0SRG7guH5wGWlKnim8iT
g8Qrgi9Cu7t/g+1m5w3bj52QXZXjH8RDffRAc2NoiIakco6hv8UtflzS4UYwm3gJS0ZApqxWSQD2
uh/LedT6wCmtuBpM+X6XI5DUPrC3d4zFhIpwYSBSNE43cJJjtNYxWg0wpSvVSwq20CbeylsziyXM
5n7AZmllyj3LBfEuFplIyuqbH5/w9ybFh49zP/W3CWmvdwwRxnJcpUfEUPSyEHO7M0c9V8vNDqpX
/Z56hYs/qI1kuYb9du2eT/nO23xxHmR/KqxIzj3iXY3eSrDVodGyC38h2Ujyq2mX4YiORQDfrqE1
hlANLl6Tih+fw2H0ublGXqD8cRqw7e3VmmSvxRzmHNY8IQywwmWIoz4tOxrkaIrdBU56fBohNl85
HiBSA4K3HLtXV+Hv3hj4I5we/g9DvhmtFQfC0DTetfKtVhSMDRgYjD9IuE2uPhz2/g8lAexRah94
/+3wtqIQM02jOpfUrQo62c5EBys+R59l2sTIpf/iUqW2I+jyQH22AGXCikY1kAsa7oDX0OtIDUBE
Pqku1y3ZvVXVxyxJf8Ils6CZ5dDUjC2ujXWZTid/XXhCDmlIfQcuQCJYQjCX+TjIBZCIDGBeX4A0
s9NMNr0JNdnW5IwHMRIu+VblLFpfNcTpHOGV7uvCSi8kYsTiBZgVCtAqjCtsdALZrJZQD8YhnZxr
CwkxGL3k//XLKJadxFDsLY9J33xMon5YvmbN38HNZu+VyfzQ+h7UAGPiytuqwYSf7PFSCpz5N77K
6LFjXLJYlDqqDnED6JgrjL+jwnVLNddNjRHJMDJ55eFmEHJIk/eGlOM1ruABeHqhs8QyVdh0D7Mf
OF+21MBD0z8DCxTfDJ6CUvHWyK7jJw7a2h2KXgteoU/Kv7D8mFvgUSUBLRCJconwBpJ7PqZaq4Qn
shNTford7hY/V0qwt1ebe2g5c+kMtZAU5Xa1KNzvWWi3kbdAdUhM92LTL860fNlBdvG61RnX8xXa
iBCIc/a0Uj9i0K3KnpxflgHW8jbniYKYa6NfjIbLWqN3mzDi0CP/WtYppZDLRC1upVGfGsgw2TyB
+vo8qSuZYNVYGgjpGHzSNSFsnHtyBJSKrh5ZvuBMLT9VS3QkrB7vk62bYXMa+c/iGWn7YUMdY0tK
uamSbK5WCE2N9zMMh7II7gV6zUUtFAk/SGzZJHgeGuiDFU0HXHbWx70cv1c/EhlYHMowtMzl/dtR
HuKmcseDWGYylwYtGUsOxlNxCxKo48FMI4yZbT02Z7PGaBOLLWjSNqYYffss2Mbh+uq+6FiMiA64
xJDJM5uASe+XjTuQFvznHC8S+eXnTDZfQnmQfibB+x5JKqKfxFqwsFLJsq7Pnu35Md4W5dqboUZT
eqsv5FDK3muBjhfwwDJ4iTMauu4nQrHGfLW83WLHq99n2raKtcFQigoIf/NTK6ZPF6hS9ngG66eF
e7ORxaMFm0SblnQvRHJG78ZFiwFBls0wMflmmPu6VESLY6RnxW8oGOJSIHJMFj8tAzbgmoQuDD/B
R7F52MUhb/6sBfBboKzWmNvjBB6t3+PY+hYnS6ZraOqcNBtzN8/AnLhObu5Ln44UmwQvCA9mw5wK
uESYzYIlt9z5+iPaEOdvRi95HNO5cCmj9qMbpqBRz6oElfZNNUNsftEbfB+GK6LWdYM2eOcVcVhr
oO29HiBjhVVtoK8wN9x0Psp5KxmyVyKE7z/DpBKjFs9sz7pIteyNnLbAAWgkpvSXHw2xB2b5DHzC
2C3GwYo/xEKtH4fzju5Q0b3Qgzb2znIW4jSRO2autTU3Mr0d/U5UYm5k2oGASnUbl33dHdYJXHga
hEYydAX1URNpV1GN9nTtertCHiqsowHiyQIRVPHfiIErxDFJUsdqc02F+THV7+SWI3W6MxwIVvzh
IC91VENZk0criuavnRM/pj7l5B/DwIaMZHTN33084801+uPElffdcnWNM/GbRSAe7AiYUXvzOWPP
eIgILXeCUXh2dRx0y1ld3owuShkT+Jw/mIBY7LUpTmlBxTkUzKl1RJdMbc+CYX44TDmky47/WVeN
UhZm/hCLm6Se5iMSDBCMYy8Y96tI3gbtdxkl7VsiTYRh3XFyHj7UNZ12GNHtmvLVsyg0LUtNnqPz
P/q7d0V1FYQFbkjxqDTZfrnGoXVKdd6nxq3qQzAO6KdM97FOu4eaQ5R+uWG82nEf46neSrLF4K3X
AiCJSpHhBkjk/qPTrBFjnugFBu2f/vD+F+DTnav0SUS91eDIJEQ4Qej40YGQSUKkwga3PTkE6Bjt
CEJ2dGUvOVXktszmZMTpB8OlE0bTmg5SZZW8fKDEDSciqIETSlSP/fprTw1Ce9S9s86E4jwWPwn+
apQXataHYRjpLkSplJ3DMeNb7XBYV5AXiaXSBmSQ9+gpleoIiQ5+9/8TDZ3OGSIj8nntf0FkWvSO
GzrNHuhJW+w1BeyIIhhD2rjgCy8usARYke7zEshC9CUSOMUbvsCZPUBUBoGz81X5ifTRqR94v5/D
Dc3LTpCA3L4JuXKF3UoaYlJbCV85nojHEBiYZpFXtDs3t53L5/5Vz9OPEOXKF1sua+vjjk1ejSQt
oOWWG6cdwDvFHyaS8c2PIRkwgkJygRWZXuRRW6e7P1EAESEZ1dHvJerFdHsy03TdhTCh3mgtVaK4
x9jaj4UfA2S2AEk0HDBjASJ/4tZ0L/OsYlnRPpvcw+0JG07y94O/+UxZBnZtaCi9FpTyF8yLguCZ
QQ2p6uFjkUp1CiIzn5bCcIshIGorhwB5yaOidUgRx5jpOwS8TLzNS47UpBImwBv7PXlHZh4E6ZRh
g9fj3tcuOW4lxPHDz6tVC5iNpCMWd5u1H4+NMnvgBaeOqbHzFRWJVN4js5OXLZsLYwRf0Q9mS1zZ
kMVlFsJmf4a5tYR2LswnGZmdQpIMZM9BkQDaPsAjYWM2DN0ShM0+rCGmvh022rtk0NNJPuyQru2Z
+2OtgSzdB96f+6zTYMIy9yJqM0GT0WJZrYo1j2ghFRwPcAudXg8KBMq0x5LJ3jDe0q6RQ1Yi+5Y8
n/xtlCBuEtUVKFtTT9t2F6YKC9X8GFMl4IIKjR9oKQypCq1gp9UTwdLC2OA6QsC4RvSEwho8G9gy
RvOEMcPZkShGXlFGU0WGj1Z9xop018RVZNLqIYgfLlxV75vViMm39h+zoTMIozh1npT1bSXt+R7i
F/z/0gvDIpU8NQRfPbVDKguRHrjRmOabYeKQeTxQGX2Lln5bI0RRJ1TygDAQebJaNl8mt9//hBdw
gQHg8v2M1ukwYeYbVn4ZaJQ7FKaBo3XmXgFgF0kTLxzGfu+kGiwiyLmlJBvVhrp4e6XGZhhZGF2Q
irWb+qZsO6pWLdIrMkksIyvVag6ZSlOd9jAi3WFJ5Rg+fKOV87/7SK3GW9qXI4B6nbo0a+ewTbBS
NVreQvSzXUU+1YFbJPttWWSpEnkv1noG5cb8fJIfGZ+elJcNLSepm9nstN+dpO1Wk2rxCaGob1l7
qUZtaoWv8lcdhNr8pRHVJI33GT52REKQWUPn89E9lgwOpEKJ8ISzO5eI7UvcBvF4IdXgDo8IZlNd
hlk6rFE83XE8Pd+XNi3OeAZafKNvoqxWVhrcJk8tnWzDp3qzKHsH1orckY1999Q8CBcUs2s48AwN
eZNm/dIZyMtRjLjyAIn+6aojjDzm5NXoiyoxwlduOmtTOxHw/zrmZESdOCRoBDzEm/N/pR1DXjcC
TJy9IcKYL0GlGftXLSLXkn+9qBA/JXp98ZSvnTM48Mc6nIZoFyqeHuv9mTnq4fFgh3OQE48qD8zw
hFcKTdfinZF1DpyfTj4DHpCriDLyaKhz4eDv3xfKmFoujjCJ+nN943By0k9r4enRlHFL5QjI7nXg
AE4gvgo0/gSpLKuCt1UdtqKpn6Hs85yFxvjECGlxBITWkAqJ6uhDmOp21cAY5pnGv58CQ8wzTqfe
1yBTdqJxWRd1BsMBZ9XzDzXeptoOGkBMLUsCv0W0MviSznRbnbBMaHF42ERwQ9NAy+UMa5gcPESf
ywPkHM3bTNWtZICUev5mR0gtIMqVQvHn8SEQHAUHc3aiqsN2H+TRl2ZGtRRqGfBRLHCVJclnKOfp
47bLQPx4tdfbZ7Xj1zA3t7hLeaINTN0ExR2oBdDUSraQq5BFTZlsME+jQXnormzijNUuogCbX1mE
exJ0uLNtqbv0E7iQCgPplo+i2/Ygwf629hunBwgxV7M30mKXK5bhXWV+Qxkiy9rMTOV+P3sTlwO0
43DZviND4jIEOx4AUnrPM0JvZnqAMG5dIsQdGj5KErKDS86J91sXSdTCP1gXswh5zuQmHVQRRvdW
n92PQbZGkRh7rKo5yvYcP6xnW4Q51WdRNVlGfjrBVdNPa+YkyRNxakNyAAFbE8h4fxrV0YFqC5sJ
oKHG2vHpJdyhpwmsEjlZU2QkZuN1VWysid/RJBvefFbqVLiQhKLA8mjdp/1mV50oRvtuwksxJnl/
AQ6O7Z1P4/qR0WUosB9PGt2CZX/iBoOUPwA9MBfwmI/Bfru2U8dc0MSXYX9SYJ8vvjglnt6s1mF6
O3L5r1JKWKT8IYg91+DCHE0b5Rs+FqxuISKUFPNMaxl7eU09FdSZ3xFXbpsW0f+HsUyjgv8/jyMK
bN1vWCxdRN6I9Raz+PqxZ/77rOPAM7QXTpXHnX2J3dQqfJcTMFx1ontb/r1tloQiqX+O0G2CoEfQ
9izL2YUKxDCJdWpFksOY9p/Wl0278YtK4ZkEa/6TOfzX7zZ5S0kMupoDU6RSTDI9GHaA1cUAuWrJ
3meqGMLI8dr3RhJnPv6CzwNp71BPRv/CCWu9XXTFuwQ+iaFXMSwrtGdBqQeeFmQdBWb7ONeupU72
U7/8z4sfMDQoJyNF2m4XiR4qlIaWxMsuIMq9516KrzDYBxI7zn6pPXp//5it4zeiAUzXRpSC5rq0
Ztcx8QeJOEQ3b9Oh0vtVPM+nYdC7GroZKvdKsn4d4Q9piLhWLYJxUurRA/7OymGemfI6lryKc5Km
fuZIdidePNKcB5eV4tYs/vbH4/X91rw2xJC9tnq0HnIlvUWmkvRj/o5GJkgUHCAN1fhqd4E/76Xd
fPf7wMv5WmNijPdiE9kQzsqZ2OxKTbQpeYUMqa4BBqkSd1oxzHb2V0j6EzH0K7Kp2Ab3ebJ3iFId
Un/JLqdMiZ/V9mw+RLmnw2cLqEjgKLjTdwZi6Lf1SnVOe5S6F5iKFqTrjbPjSPSdOEEDCxBs3Ife
f3xFRBKEpn021Yn9PPHjxwY+okQpdkEX6WJ8xYCHbSW7AxzJPk05+rACnPjdDJNXpVTmR0ZwuOso
upzdyVt9pAmTI4qo0+egVlL7h4Uc8MTCkroYKZQlxrejWZlAsnZIYq7R8cnuQaAI+gX9s3r/ZrYl
yY3S93IAxNk7YuwrdOSTO5Y2rY8vtl1xEK1dchZk2urUhyg9BYhIDcFLQHXnV/x+GoVKgL4diiKT
P54hDJ94AV1YCGdvO6piVXnFA1bGwW/Q6C1bh9qYIx5PGghRfu1bZog82ttw5f+M0Lds1SMwD+JW
ZVKUBr95q/kG+pxiQNhyyO2++HT9O7MzP433GFUjgAJWKOvYHFqHtclPH/WwD6Jn8vO1yBuEjq5U
cdPKUtwWzmE5lvMBwD/J7we2r0bfl2UzFRNznijcUD0SHY76zOYTZJV/ujfo4YjdaQqUxa7rI7cW
Ro91O+KXilww0KkeFI+mA/W3tc5sT1lUcH/WeTvN3ts41wgP64Gaba27+Q7GK0k6bubpCac/H1Cw
RgO1YhrysWiZxAFYVDvAVmHgnZZrHprgxBriizHJDQ0sjLFVIee8fP1vcurRp9uWLj59u6LG7KmW
1hGHeD+CwksiBHcQbvkDf+DYjkt801aZVmyMEmLFaLC1kPrFcix/TO/f/9NBtqrJjTmbt7CW3Aro
Xrp8ZkmCJA/fk+Cw5sgt3FsrVpb/68sXt9noqElt2NBpzWdiQ+5z/FcRYzAL05i76ZUxwxcwWJaS
tJlrmqleOo+ws8gfdRoxojgeKFJfahV2ePlhM/sJctEukTVxDpcPnqGwr4Hw/Cq6ccgWUPpdcy/x
pPg3BiA0XLPz5jODhg7NN+2dM+BqKfxzwM6Qr5m1UUf/a5UJ1bTCnlX08NhJUFAypADndOhHA/Wd
u7ojSNcYKcnTLpNlHC3XsB7F+vRjuJ5pc2x1WORfFqNYP28xTU4yvxTjMMqefKNr0BjSCusvgM2o
3wcgD4B4YyiRqu/9FNksyHQDOlO48nOrrkMaBuPm6GNaVHd6Fo34uoMGFdx7lBQYWJFQN7Z91y4e
++mtmUVozVe8o6wqbi8+e02o3X+j/Aqf5DG6wQo3VpTLBnGAkDO1uxWfZEddpFJhOI6S/H1rQ9Pl
OIVzJaJY13xD/iFRAKEOYsqg3jJrh2DmghMO+VL4sC8QaEWEpenJhEkbXhGhjhvdJW6X0h3jTgOm
FZHmjPfxbrqQaDtKqI4RV5T37atNMca2tFOGcJJ5BZ6u+TbFemJnaYx6evO64l+TRQtVKnkQ6Syc
EHFunC9OpAM65fCDWDZFVDGWrPaHgMe0hkWCCqLirXeBI73h/hN2TvIGqL8l5SHE+v1J3Xsb0oz5
7jGjcrr8qQ7FBJmeiTFpSjbFXXbN5lYHQKuNE9m6eWld076yu5Vs7Jk4X550jPgHCfR2KJHZBMVO
MJRgDiT2S6asL4qsiX6XONqhzX9uEZdbKiquLgzwdzoTq06b2Mr3RKI7YpaUosk/0erqkuSChwOC
alB5geprAMRhsVKUejr7RLQohMhXaqwPN2Nym7M6tB6FbXbZHNQjjCfmWeH6P3Mw6a9TCSgCa1LJ
XzGtT/30ejnBR3B3D8N1Bs3DbfjMENvlQzLK4W+kKFiDDyDgbGogjIKyFvR1CP32/GQjUj5JeG+z
7ibzWmm+8HorqryDXJ5ZiOrU8TM6XKNROeFKoJwJd0TZ5bLPxVdjSMLq3mUgTMOUp7D5o1RqrWtr
x2OSYdCjUTXiL1shNPstKjlaTxDncXZzfaS/Y0bp2e15N6tQeksWjAdOfMzC+ECgsWlM4tlxAUFI
xAL5NM94LrxnHD7plENvr7LcYry7jMsZ976aWinTaYO4TuqXr4jkEXTPgHteAcFUSjMAnqL9zf7H
7oUdy7Edl0zf9l2G3zT0RvAkhw0R8ZWB+9kHyzgZVkmt4D0h+RmecHtT0qmO0UlnVcf8RH8MiXZ7
AYtXrNiKiYXPt2EBfAez6hPc263VmADM/M5cZTt0got2c4jyJOZo/jkWmYNfRCWALenJ7uzn3ST/
B8nBh2rnP/csJik4z4HJMaLy29dB5a/P/TFJeOPpTD+PvVXi/y2fOPes8K8i0jNlBIBeJS7hI+qR
/dfqSRnzeeIORr53E6jraSQKdC5lIcqr6EhmhIiRUO9KlyHkjyQjqJXAHIcQ/YYtUw+femI7AC+k
fi24TNOfC6ODe6EiIQ3jsKEZVTYaeAX21AMI12c7T70eTouo3/qf6nkmt7yH84kU9dMZ7eXTQeOg
J7hIiER4EToPs1fFvsq/qcysinq6OaXVpeF8cD5hEvImIrfHvwtADoJB5DsS1cpmHmK2VjxMidEL
7eClhSqw4nHmvC4Z8l/sa+Ez1sYDOfDijCTRmTt8pa4117/6bQR2xJUfLr8OjInlr9XC+E7RV2lG
HAjNUwd2OT4D+eSx4LLvSIqjUAgskvBvwj79cQE+1rimlYt+TWX/mEBTd3odrcBXvy4ItakTOb05
s+HTITB1ZrJU95qjzxY2pW9xpObeNMvWRFB6Q1FietFzG5YNMaaLPoReHb+t8Qj2sFRzcV4d5JCS
LO4lnE6N3ldaSZRlIsRAZUswncm3W7Qq+UthRV9xmZhvlBmGezRYVtWsApI6Hhw/nEI6K1V3mSQR
SFH6kCDhgtp6R28qklnanIs1zIPHADWYGfaIv8dLpxQA5UyYh9P5oNORB4sFPmW9mAxyUeGz1ihj
EXMlfPRs5rbJOGCfivuKIwaOGWYLgYHUjOTzfhBp6Vc2Ekpy2Hf5a6zCrf1iMy9A3eeJ936uaO8G
lq1zVN3okSYosrN6KHLp08jKYQH7CX7JUxkH5UfYdaH6JuiJW3r9H0d3hSq6u67Ei2hyDTLb0hLO
gx0WBgP6IdJYIJy/q/kVi9888O2HwcptyUzB77VuZGS0+/SGwlMXpAJ3L5wYpe7CXtopeH1QBSod
4TjvaTmcjVfmFqczy1APOVvrd5/zvSFvLGtJo/6qwbb/AswvTzDJMnQZvitbdcoXPWE6A7x4HE9l
pmpPWjjpXZA4RjdG/GwvUoPCG3kWHHoMbUbk0uwRew1UYXC3mqZo1zEC+f6tsH4VrJ9zdeyXKrE4
P1VeIlYuX4/a2K+PFsWuVlERUbT4hxv0Fb5FsIt8PJpoNf4UbnPbwi3js7gQ/XI5PVkVKtZuYVMr
weWIHuPyuVB7c2HoZT/QAUeLZFnwqrIWHM/j41Br74Ry78kB8z9BFnw0+cD+CvZYTl36AMZpjYCt
EznSwjq6c8r32Tz8wookXIAhP0J1ttETuc6s67XQh/K1OPuFhmvqGIpOjx6YTsF0ytuRLW5MAF8d
hu/wCd1paVsyHRseBdUyXGy8uxOW1bena+J6WEY0fdIy71fPzS/dU3DNaGtUmzdmaz3FV9i7i7Ux
4+IMJDndUgQ+FUzv5s7ZnMDurH5hpvtnPaFOrKkjuLQju4CeTr/km1aJxdqnrVcczKvDA9mgeRdC
hPlDzzJ0PMev9YuTgnnlD0186PT2WIqgPZAsOVElsfoAU2GBFWLKBNPUdIG1ZKrYN0xwRz3w93K8
hShv+MmNu1JEs1HouDMxh6VIQfaw2BpnSSvs7aLch6hbCv1Yuo5v0WTJwgAGfkKyQkxmRl4KSIG0
v04=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (weak1, weak0) GSR = GSR_int;
    assign (weak1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
