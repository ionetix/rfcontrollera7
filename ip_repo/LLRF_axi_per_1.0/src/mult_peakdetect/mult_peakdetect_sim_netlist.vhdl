-- Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2016.4 (win64) Build 1733598 Wed Dec 14 22:35:39 MST 2016
-- Date        : Wed Feb 14 11:20:39 2018
-- Host        : SHRIRAJKUNJA80D running 64-bit Service Pack 1  (build 7601)
-- Command     : write_vhdl -force -mode funcsim
--               c:/Users/shrirajkunjir/ip_repo/LLRF_axi_per_1.0/src/mult_peakdetect/mult_peakdetect_sim_netlist.vhdl
-- Design      : mult_peakdetect
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a200tfbg484-2
-- --------------------------------------------------------------------------------
`protect begin_protected
`protect version = 1
`protect encrypt_agent = "XILINX"
`protect encrypt_agent_info = "Xilinx Encryption Tool 2015"
`protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`protect key_block
fPF16TcpNgM9dNC6nyb4WjUK+7bY8P+I62AEEiiM/KOMhIKuPOHBoWeWL2UjxSNO68WLeYIZp8lA
I7rHN/CieA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
E6OKJxjnDRUVVFwAhrQMAtoyRVVpuMKsXlca4m9CcIt6QI8vnYN0tf7gH3uVuxZ90322B7kUeFw5
Pu0UeqAoBaSyysHuDqXazxHy7oyk4BIWChvcrp7LULlVLcL76obtSwsXi1ORVmpdTi5b+AcD+WUo
OP1PSFj5jpodG+LwXm4=

`protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-1", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`protect key_block
x+agogSsgbiI6PGyBpMY8RQCDzLctIr3EaG23mH5kJHlNmNKNolnP54yJ8Y7nIFi6yl6tlyOLMoF
/kxU0pyFmIj8QM0/MArMxPTiemXbDLS2VKtonyK9dDH7VbjFnRWwzK0Ngkas0+nbW3TqGPAY98x3
251QPjQoZCw3A7W9PDc=

`protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
KNs7hA49BKKrboRSEkqIGldOa3ndCnhjRkSn8lL1xFfKUn+p+Wbc09ogKV6YYnPU/RaF1LbzyoE4
udPSNea4bST+08IjO5GAxXqUugcig44J+hzpGKmh7oO0TuyNbYq1CnYcsZXaD9vsmNYz8fBDoW2S
VK/mYa21mBKTOuTdQ1yp3wi73aJ1G9N6Ngt7ovDUrjyd5oNxxNlvWU8JkJDinbEnci0qjZ3Wu9Wg
y44pHUXf6xqwFYJpZ1ZcGRKl83P8p74+pLzt19lw9TPlTfKI++IowVjb6wo36ztNDJS0QjQE5Riv
hwbPU/Bt3S82MVCY5NAA6bKC/8NnoWMbmX8Wiw==

`protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
QaRubtGbYrmCghuFdQuTgTEtoVYYLcPnD5z0C7mo18fwCG17qy0y8mj8xWiwE6bo49IP1/JXSIw7
rTBwHFOVrmbm926sWNrF1r3IHB83C5cstprQ1om7vnkw9XX87SjkscphhkrHmi08jjzW4qX96m61
/ymclz5TlAocMQJGz/jwscvIMOrrbuH4SkWQOLQnRfx9GIOv5Y7PM+w/wuDSeFXsAXz7Ahq3/qmU
cylNfSufW7/zfN4RZB4u+d28AXsuFe03aSF1dpW+uBK1xtNZccvj9h9NMN0cuwxt8ZUlLJw8l6e2
hqRfTTZl1F4qnnrJu6w8h8uEGrmgnQG1AW0epg==

`protect key_keyowner="Xilinx", key_keyname="xilinx_2016_05", key_method="rsa"
`protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`protect key_block
XXj6Nc59BeA5Kznlx14IKravf7ohERw7h0fbO7pT7/HsiPDCWh2DlTGpFUcnbNZslPN2RfE0nJNX
WMzLQtaHK4Bm6kxY71OsXEKm7MAIjEdLwOMtJTtlZrbm7chBbSxcW6sjWvI36jk5De3Yct9Ao1py
DpQ9NICUtRTwGG8SAiRkAXRh2Jv3rKvnookQrlVxIkNRSBMSgbwuTbq1ze/KMUZebBWwJNUVIC9r
RV/i9wjYXBOeCCUk+cGDC5uSpwdLXYV9ZxhQUU6C1ufAaK2m4OIUeBqPc2ski2O0qQYQ67c35k50
ynO8H9PTEROPEOn5c37S7feU+36OcOOAsVBTBA==

`protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`protect encoding = (enctype="base64", line_length=76, bytes=256)
`protect key_block
U7XdHgK5oSLfNnQ1RsL99arf8KRKQdkwLVldCwMIWw7woRsLogI0J4g+KIknrlYtcteOkZQ6MmS7
7ANiKrqDHx3wCeDKRoEkB/cBSTsyMeThzUfeU8jV9Fa8JyxLheC1qQDcQFZPUuXAAVEcyxRecssO
lpz8hXmaoz6WURGoGkNVZ9pPvW7PlUzERbxMgwAT7+6Tgsmknn1ycT4yqrv7Lpi/wmHo1Nq/F/2v
ez1N6h82FlHet2raFI8C8M0katjaNE49vW4+dmTSel5MW+ic7uMVkPEQ51Qhl0eBx5GPS4m5lq+R
NmxB1WIY6r+tEsrEzLliyzgNybyM6a/viS/eRQ==

`protect key_keyowner="Synplicity", key_keyname="SYNP05_001", key_method="rsa"
`protect encoding = (enctype="base64", line_length=76, bytes=256)
`protect key_block
qvwM7yMABBEZ0CnGwCjZu20Z/w/JYmXU1nGS7uoBdRkSdlZFK3zou5s3Zeixbj19014CcFp63LWR
w5FJmQFghENinomtPYM/VKjtZrB3nVG1RVVYRJ+vfgjOd81J1LYHAJRTJX9KvLOg39MyBsnA0j6l
iYn3euc7Dr6H9+8tBZvIAJIrKWTk+vyxIYD8PDrXtcBr9+r+lZmAWLxhhausUYAEsVxbvH2gmxaX
YMGLy7NN6mw3Xz0jH49GNEUawOpxEVkKNCD6Wdrn85eOM3sDEjodQya3j7oOK4mltZhgL8nm65CW
2Vq5lpChb2qP+9TIayMWgB3dL022pRg4TARpkw==

`protect data_method = "AES128-CBC"
`protect encoding = (enctype = "BASE64", line_length = 76, bytes = 9824)
`protect data_block
qrOexbT4bL/ocpI7Anh9DQUs4RX1G1tE4l/If+zFa/37lYQekQmwxFx5i1vCYamb/TStE0EN5ke4
rM3egRmgWWVEMg4M8IWQ/Fb3hCnbFRiYw2eaMnwlpp53a2b3pAtWwgH2fsdFsVSHPoXUzY/Z6O60
BOvTtjA1yGu/XBc9towIB2HVTtn/EB2cw+0+avRfCPgL7drQulzOWjOAEutCkSqg5+tNIbhIyGHK
IfLr6IHq7jb5a6/a+U4x2zXlQcLSCxz9NEGiPgmx028DkNKuswtU/5rfFNbl36BHgZ313+zDS/v2
sI3te7Os+pEjbpg6y7yeAdO+XatN81o9FjGvxD1EZ6Tj/bAh4hpMBsmg4ialGq1HsNb3yYRQCZ6P
NDitAIACtRcyA7Zno2k0a1uo5nm+LUj0x4yqyGt8azfupctfOutdVHHc8iQLSPiKEVZFJgvcvGT9
14hXfP+s/6Kwl8F+5dk2BP6HDRo3baLnDWP6ejXCd0iar/KA8VPBkHS0duBGo3uTcjtyyxFT12q4
ZUAu0aR5FK76ThWiCR5rnYxqbTG4GxC75kC8uOK4jCL34ApCMwP7uHn94lmLfoUipXYtMpoy8AgC
zS9PEbUqfWjbp5fEnIqgH9Tth+v4SV61Ab1cxCZo3G60AU1TZVgqAKaoECvvhIciGIrm0iM55nfR
I7+I0pq1oxYu1jp0Sddlk89jyHNvnwhzILit60lIX9tEFZpXVxmbddXMSAUt5gAk2YMwhm92laok
XtX/1xZa9DrVuF2TDJcJ+a+/PWyjiwITaStidpd45+6oz6zoPzG18snL+O+gBwQJRg5L7opF/Rsw
knsJafq9Z7fcUi2aAB0A46LLbKa0eoxPoBrWAPWyEL5zientjMnBTgbVfUOISWzwZrURkPr850Lm
kOrgnacPBSn+ifcSrn5F2ATs7oHgWrEvw8GUNNQkLxC73lpRUqzeyQ6eYDPLBF2xa2NWhxo5xOlt
dMoMJKej36Svd6l5DG6H450LMqcRf/JO8C5ZB6VOuCxOc+O8CpmnJwO/CsfYsU+FrjPdZZsfihwU
s3PU3fGAwY2CpxHSY5d3kVajj1nVMRBB+rku4VW1KHD/ye1tsD7szWVFqqZmLRRow9U9CRCpckL0
m5nYa3vDGaiGdoOCs+FenU+4zZl9x0owYQDS4RfZW5z0fGKD2TfLizv+N0hRKeAdufnqnKlDvq3R
RUP2H+jtebsidLU1URTbftqEBvxAOFi0T3wZLV1F4NfW8g64QlUBP1Fa3fPicARQ5LRYYdClM8AJ
wZrEge+mEJmwSO4mL+JDi9WG8iPG5us6RVaRzOnJQdB6U9UV9NfGsJIbeVdABCuyC/IB15cnex0u
w8xkv+yp24dyEPc/OdTYsTOL7T9Ynl8y/XsAWGJVLWWw2fTprnGFI/8JGTBekNskRAuh3SBNaspR
daDyokZSmX++foGHQ3uzs/eyaadZhyvrVeIK8pwTVW3H8lmpqECLXbr0rlY0fbGxaFTgXlDdL4hE
XHagWkqk/0UTsf9maIkXp/6ZoxvNvMdAvqCWO1v0eTDGszTVrySbgq75Mf/5w5M+8kKG9MYtGpKi
1oXgeqk+DeP6lH8sLDUqqQ21fmsxnEi6cTLKi8COmXs68hAGKxTiMWsGxwXB/QcuyoEZRfqL5VLJ
LpkRpzPgZ3qdUJWJ7XadM6sW8EokgmhqfNJtzpHwCNuLoobekG6z9Z2h1KE9rUzhtcquWtsRzaMY
XiVIbz/hitTlNw+ajsv3j4k4tIsXrJkz8a69lHJTgrJo640sxA4iJyCUivJTAnhevssKUoM59BPZ
+OYWsplKUug7J1HYORRefXGdZBMGrheNyWrBIS6RUIcXS0wb7J7XHbN/HXSBBaDbbhMzKAjIf8iF
mlNlUjBEjrQ5/pwcpbrBAx9BWAuDgNCslhceA+Uvk5Gn8WO4/3QXVegOMq6AnzQC8zwvj+mOjH/k
TSPDxLl4Ta4mj8OzU9VGLWGhQBRO+GDZoJCPvQ88e3CD3jBrvxHLpCINVc5Fe8b7KQJpbQ8j0paf
vMdpGaH5Y03mq2arSUBSDgL62BgFtcjb25Psz38QdDnwdbV83GZ1rJ3SWfo2gblGtyqshAipuyVh
+GenZvGbuIEvet0ZPx5chEGr5bCrn6r2r028HOeKuDQmxsNeROOHQURJ2AWm5iyEZTOqySH6ZX8m
d3zEjtbSD6A58ZEkjGRQTt0jefBOEthKKWRqs+GTImAtaHF39qg+lIxPerqdEwdWEHOAh/fJziHF
KXAfUqyYGm/3lY335WOyK4nOG4EMS3pTCmFfr1vi1GEY+9Qs8K4sZq7tGgBXGOfvlztA63iIfqMr
ifryy3+6/20nVGFBYL4HiyUFLav+BsFqvAbxFD29/dIxHFQvX+TZhDRnfaigaPNORn1r/J5nY5y+
a1FmPVWLR0I8NbIUP62X5bJQgrd96d2D7GcAfNEk6/vbKo1zBcB0mBhtq3FHw2OSOJz0TR1LI8vF
r/ROpMzm4RiiKVDJJrTDiagOMeBrumkPbWn0BP7kVrcvY/oHnMIJ9uHypL36jKFnJqW3DfC6PxRP
byLaoSCN7Fg8/i4PcS4pxYXyl4+GMa+r8c8zyBOw1kDOgLu9mN1sFnznC0dMwQfbbABZF2dzGVC5
vSoHb57lpe1tHGcOXcnk2733rW+pCOB3BADBRYSfO8NiOplrKNQ16JZOcEFmSe4dmNGoMJd4tQP6
zzmfL7bKiLZmq0NTLP+AfxT5LEYE1dTVA1V3zORppo9rmgeC2nkxGxworGH1EmsMfz+SBmueT8fA
m8ICIgifarvylUcInoEPbXqzrh5fxGAgUJNup8+Ehfhz/q4KRKgNzRMw1+DYgfQkApdv48DKzN4/
iouUxA7fSasf+8MeYhNTt3cHBR3JN/0KgVIvF4fxgXyAVCWSudHt70+jfefAOzh0ccD3ZFdZh2td
HszoSy0c7psruWftaF0+hYdiMAZNfYhrUvvAR9cj6g1P5W8qZZLuv2POig9J/P9dclbW2UzRRrYw
SzOaPEX1d6ZTjhiKHg4Pt2THKpRloiD4KxHDxB5qzBv9lcCfV08FB+Hv7xBIpGklItt7mr0MvAxg
uelIT/cpQyYtUNHiA+k6FxxN6U2Ue2y/9mTDuXwlLu8/o8pisqizFEuhscAxdViU4GOROy/I5khS
fF9WAnY49WqswguUuEvCLJIqFIPy+hbmSDSgiY3N9Jklii7u4xwI/AYfgIQVUQy4a/vDFObsUUsP
FX/YNl4V1wDjsGJYWQVmTS7DWL3POrCwItnGd/YtKvUp8oKJhR4kQdWotAbBZqXFh+KgIt24Ivwx
gBjJQ+pzz7a8G6TViey66+3DXmeaoHIWBk54Zdj/buyiof3JoMO+Q9ZzJh46Q1s6isyfsvp0gW/O
LxaPRZYwL+bQP2Dhb7X0hPcZw8PokQsnIWDmdg0+xVwv7TughOneWYW32axPOLio+cjLJG/ApaKU
bpFXbf1NV1FhxSY4RnM2C2dx9P9lZ0xoPWPh3NqsfBTO1zeQ1baDz9vT7pxesHh9+wjHg+hQAiUf
AY+aNJrVOvqcninwtDInZIxmVH2RpBXm6krBEqs4ivzHO+M0fq/stR/ndpIK2ym8mk1ewISBz6gJ
P/Ne82Xldv5Y5quKFv2ut/twpIz1vYdD75t/tX+DY5V2sjqvd+gLYGjjIz1nz2Z0gkvAkDNpOO6w
qI/1nEpI/F8PXaA+4ZLkf6lyVAzd74s8DfIPbI7QUSx+S8dXJZaRKiQEKquhtK9sdtDeuMJdWFYr
ui6ev12uItjjOoExVbqe53/an8BIx6DlByuljYSvvizh+9DOhZQUipCM52RObuAN9erHG8bG+4n5
0It1V+F8Uud9NqyxuJq6Ad3piooPB1QivCQfDn6Ij/G2wdK0XMjzLQ/q3lQPF/WMYcG4QkbFbcj8
ZfXdCkF2MjGv1tpUiNYEY1uM9GX72mVuSyLEZRxKo5GyC2ZxcO4h+EjPcnApGpeNpkvC01Ulhsrk
AprlonpO/1NQm9KTr7wy9ycMZiMDECjjK1B5rDdXVbUmMFTVkaRhOkmOVKpbN16bNs1RcFv6ZXU1
yDDT78MAU0aWc/wheK5NUuzTGj9tgW7GlTMQN8EDPfZaU09bO3QU7yQGgHCfqBM2p3Ti8AxYgN4D
QMxymMb0t1/lL68jRSNz7okv+Yo4iLxLjEu13EQJGqephCKdjsWDxj7v+KP/+yY5aluBI/spM7k1
a20epg17PbRqOUURBRAKIL41+hKiDYwPTjxhJ46A0rPdUZDkkvceFqLYivSDX5gbFfq2LrDJBKHt
N6+XVPJLYNzr+jEENumntlfAh6Ry7BkLF96vKxGKFTWEBDXpz20XLFzlnPUglGPOqnqXpbyZF5M5
PU4NiD5I3U18tFfD7QPh9VaYxE7lhld0rahT03MhKXDpYDb1gO3RRoh9EF8HgqK6lADpJd5gExlL
tPAi83/Q7nDw2ReLdsF3CY8lgbiRP/0YKVDcaWJ9AF6DlP61bGtFmy/hdkTjolwIQT9ICSKiLbvw
bKX4bwJW/ovWAEhatWNLL+hPW8kbSMYNMjfsEXd5SYDsqmLKWnu/SRQq48ta24A3NmJm0lGqblQD
KUHpQOre+501nMv6eMZ98XW7+LXYF9O1LMkugQ3tHBx9mRQ/sWQQTLTKeXQzxWIXnmA76n1NuiiT
pKPdvrRikPrbDdN/irWsrPSPJ+hlnIaQaXbsUR24keol/i8NT5l8Ov+uStTMoY2dvo7r1Ryp3n1J
VpLTR0jTwwMrhYJolT8cSUm2Ea4MRRmxU6nts0W8oQ4v3227ZL4HO81MH1KLXZ6d5ydCWy++krnj
5P8IeWkK4X640PcUjTUp5+H9r/UrDlAupJVuHjDFD+viDpzRrVIRSmjPEKxRkS4w8y+5dMp41J4x
6X6cfDezMESj44DlYb2R2ou9Hg3vSe1vN6b4Vxf8DaLLsG3lI7dslQHhc54wIR//8Nkyjq5kdLhT
XD/LFrawejf7Fom8tufK40NXE8KUJCRZo/wWy13mLL+lrb+WU2lpp1iTu2DuX5ZbR3+MwcYsQYhW
uc+qvFYJ6OKO0WqqhP5BtSOBjdpF1GRSongkVg4tEpYk1K9bKkvddELIpXvVVDrqZIeQ0P507k/B
IFwzj2JlMCpqWwWwpI8aEvXvE0C7W2MZouf/9S5C8P/T8Hv51iqsSL3k7tU+z+jtCDYgEbnyIlW4
XqDRrheqeMjw7AdVlHz6afOAq4iLlJs2f2sksCpjehTvBcIQh7Jf3LFW+INuFUSNZo2bD2Em2rQZ
iVFOf/EZfQqayLFFBqUVDlLJAxdSIDRDA1CgNrx9HK1yrxO9WhfRFTBmym2UwIi7MsNduYuN1qAA
kyCs+rsyQlgfXsThxGQIh4XzYDBH1CU2v4kLr1zkoX8UFWz8Yg3wJEF5iyOT2zBU595TUpp4R23F
Fjgm+dYyNajAXHLTM9Yguq+9/S5gM9/H/tf31fPoLFjaHx86ejAPHzewnDrKmqzAfTrO1PPs4xSk
SfocEFNTNj/+Ahc5RYGOsFtN/v/9Tqau4MF9XIcxPBdF/Uce9X2ND0cvK5ff7CVncJ8nVEIeC4Pk
MfQFB2H4o0iTu7vUBcpS0z2QlGcsA0ZGOXIxxHoLwNwK+OqueBgKHLffQ1BIogjaAY8+W588ivaX
/CJNh0DIAaYyU72ZVyogbuxBgAqADrZooATgC7zebwnBJeBvKEXuRfPveYlRhcrT2JF7MuTTdhL2
tIhu+uBLzNTUu9paNAqtjoRsZY8HVs1g6x34GtLUfKvzggd7eZm3VF2AnIFT6hUoleG8ZL076sar
kWewJVwU9fUawqCtiddueLG9uE3ljULsYAyiY+nACEVQRtui8kuX1/466+IZYj47d3rwn7HqIznd
TxVLaMIxi+zSs03OW+Zq+edP0QdPj4Ap7N+A+7jTJzRdX7rAdvyMPrGlm0Z1UZQTOWPnc9w3mG1s
Olz0FFwhG/Pcyk1p5p0aCcBD4a7JOSQP4Mh4dpfeCz+pgV3fRerNR6wY5fS5N+hQxUwNtSdEMgQp
D8I/S4sPK/LXPlhHozq2Q/3z5mEqRsJepZeMS3A/cYCrmOxCQ1XEhOL5o1Ls8F9Ner6V3xpg7n2d
otnORht3Ucv3e0QP/9Gtq/TXymB1/L5nCrqLm8ubsAniH5bsZ5e5etlxSytSqWS4SYHuQ5wnlDhB
xbi8MNHlLClxynKXSl07N4WFOSKAyFgW1ihknrVZl12Byup03sGC0KccZphYwCcUqgrwiwG9ch7u
gMDU7dImzj+lYaCDI2ohBbtuUTluPah2hCiG3QXZMmOEHvn+0mLt6F2RnkGUhJZpN/k0NFPy0LL/
OXi63taDnE3Xkx2yTsZ32d0ZhZJvggJfVSYGzz/lesDA2cXMoZ1CJi1rIJ9zP3LXtWJwfgiOv4wY
c2Y0LgSlW6LG+JFVXLAtCDZ5wLtm+tKsnRtBR+t/ZnOg9fGZ27k5I2BYOLbov3VpONJRqAu/keKv
fQKP/1YUTJrBNC2Sd9SQEq1CL2RD5POlSJcjcEgGlnJrd1hlKFZ4WsUJ5uP6DI6GcYmH2LA9IiDH
I5+HREAjTl5izKs50l2FG+II0Zh/fwWdNqf/NDM+gLfE5BFe2/HD8+LVyiImmEHAB23IwCRU+TTi
yt4V4afPebtTDTMJKgiS8hOR0PLnkTM6w6AS7HZc32473B1fJJXjLuzTE3Ptq7ah2uLb5E01sADB
NBKo8AHyjY3uJ9lNk/sIk+22KP047ncOICftUe3s7c0cE9MGlmQK259MMjJukM4kY3Pw8JDp+VNG
W+jgfBQLRWk3kJa0qQ2WJAVz0hjCq/gtvYO4r0fRT4su4AGrzldt2qKtGIgwpv27x/GiAAGZta/f
UAzcRuiCAXmscvUTtKiCZpDlyy20oHon+oMJtGeDkqfUnPi+2c0PZ8u2x2lksWdA3sc6bb+306/w
AsQu+NYR2YMoiAs21/rwTTdCUgyKMXjZwtLPmH2JXu4L828wscf85ePjkDHxnvswo8FB5PmkERqR
J5YHhZACxzzBCOpKtSkgeWLHyettFUkbsUFHvZDzqss1V6ZVaqmad1sHCJgsh5FHzlFJHCCewJLW
TYkaxQK67c4zGFArkGHDfOLqqrs+jh4Gl/Iak5K2GDj7IW0gHooFRV3HQnDK8KOmiRITQcTIb1Rg
MdIYRL/l/YvbMorAGI2wvftmILMKMSXdVRZRyf7ixttE49IdtqGUZip+ecCNNcHfNcKcoSlurQz+
4YBfzgylMVJlcyw+4QDy623Qaqm75Dpy/9wuwsaGlhPD/DXC+to4vcmmcVe9Sfgkaqge9X0bq3dS
0jFGbuNAveAlR7lBf7ddvIDfVvMzvHMITIoDrhYl1iGCZuGPz+Ct13qbPXn8u0PtzFqbQHklTQAK
XZ1sbVOF5tcv/zoFJYZNwf3n8F1SssIU6aAb15H1ZKz4rJTlqJGty6F0OE1g6lEwl8ORefqDvTOu
xiHIKEeRkrSxoOvX9huJb5EcGoxR1wZ7QTZbHMdDP32FhYw+wKerL+u6HmJrdRGeprRltUyVGJgi
LebNxsQJhrFbi5uQVy5lC7p5wyMlD88D3g++74Xb4UGhTgMwO6RTLGzDbCCQzeRmIyb0Nms1HsTE
SV4NJ6YEHXwk2WecRmp6/OsmfFtqf4e0/N/Z7WYqnPPZ4MBTmvrapt8MZ1gOGZMj9VuO3qPNtTxV
ss9AZTnm41B8+3xiLgBJHINwxQhJRwIrR+oY1XuWkjP2pJq23Nwm0A/wOolI9gl9Cp2l8ttYEFsU
R67Id///2uuE2rnbPPOTadH2PbHx7qzd3mcfsU125KGMVaNf2c7qhGXR7ysrnP3YqzkqMRCkqYPi
CpiagVj/JGrlWJ78QcweVOAfukWpDVAOiZRtcYbPoDqgnzK4aLDvXWhAfHcQoUokKQtN9lNHrP7Z
NOx+li5Q+F5dr3SfTNR5QtMTNoVBAlMePe0orP2XeATwSavRw9afE/yWWTUTxGnbSlo7+2KXhH9v
VOp9+IlJDv+WUSwUvLLTyPbtsnZ9ZJFO0Uov0yjZgo+QyoEbtzWD4b0pUCRgstu08S9C/rvROxB1
sirMx46fhwmj+Ess6nievcrxhN1C3a8UN/XILLhNQoDIulMJDc1T66rpekOoZ+rNP+9MTCipeIe6
7ryQ34WE52F5S5XgbsO+G9b1sa3+Iux/X5zn6c43ZhilI0lx42XhpxV2FSYtEEVT3Qdhxb5sc2cw
jODRrMiOinbuHnfghyBTf04cp2QP5P4gUSQTr0/i4hwrVq16tDoK+6z25l6U9WOjuKdjRbWqMvJD
a+6i3s+z1PTd18cgFFz3xGcvO95h/xpzxaX/IJ2P87Qw+o2kTPU+488GpVquHVvIk9HiuokUmeSE
Qp4jGRXSy9ym87yR09Reu0h2zUoS/qtfnGXgTfXBpKKb5dEk/7OGaPjaG6IUaxzaJZ6C5lqkWcOw
XrEiYKrGAFYYxz7IdkDPjvpZrZv0+YQNvimaWA143DI37DJzAuyvB0jbh4oZyubG+OQDUYBXOyse
Nri/ggQ8MnsC4TZuzPxwpuaZAw+hMm3geoVgJbwymAAKzdil4U9SzEhV5gL0pPoP23bjAGjIyFSY
L4dN4B3tWRXr8ej7hr0+gd1dIo0FOHBj698vD2Da5z+OxNR9QdkF/ayaji600cv9Gm3ZzFzArzrp
r1MOGGkoMZ7/QUwyQ3H81QD4nWmP1VGsTM3xAZ5nXm/RSmM94putzwcmydH5dFMyIlwh8qnXBCBl
jx2kOKIzYCpYfGP6VDIiBqH4k/28oLaF+UOnIIvt9pVT/xlbdpnqa4uyorbdGw9juuOWUE7suYDx
cKGUrcWE/ow5leDPr4z/nhKkRey9g0+4+HPqLc+BebkU0yUaRAe8cuGI9CbZMzAZOT3+MO3YxUVL
am2FkDonbmBNd9qoTINEXG/vPT3QbdbugZpM/J20qWsTmWI+GoykeoBxYWTROcPV3QCW/9b9V5S+
eLvazk7RWNiuMEWocnD15LKHpNmzG/IvVMX0SahthKy/8uZIquiH2lTtAtZslD+i2EOrfr+eOaxK
wxmcEGWC8C8sYyDa7c9J6EvJUrC+gO74yzZciEMsXLmmuWK9ADyEQw+vDNTpdc42Hw/+dLVvZEL3
OMR0E0rygs/DKYsQgSPAs40dUVgrlwOhXTGxA7a9zTcdl5fajtMBLrzj6BwZCf7/Vva40rvDvBY3
KAMEM50ybSzyH0LcQ4uJK6bThBoiUKZNFsQUxZUkNNrXPSXnAqxMvPlGV3qDTSKOjNQQ5qo/OzHJ
tUOyekPjfG5xmz4pFkb2nGnxTB7qvSXgp1YOJJZNdVPdNNN6sYqgoga1KJD9tyoGvDPrJp5F+WTi
1kAuTXJirTViwxSSiGbAiaEnSXesz0MqWmc/M/bBVDIzSry7iL8VB0BGZPaxgfQ/nDMO4U3RfnNr
iurVkheCy+kQl1hrzZIdK2MKIhkK+NXlp3tsIV/3vLWEhyvYUqd03Q+s4Cvly/8pDnA2gem24tFQ
Q9jM9EtljDWbeKoyjBLlXk0TmdjOc7D4XQgCVjlQMUhtqi7j/dPs/JxPs0NDvn69nrdE9NMW0E0t
6oS3sku6IwiFBD/XZY8IADBbs3uN1G6mmA4QHHfnMzxTMw8pcF4OKSRx0NzAzhMKQoKLwSrvf4OS
J7Oo9klLk9rq1BSEuEdXT0PG8OznNGv46j9SAX4MEEz10mgkdji/f+DaOhBFomrAPSNXVBgLRAe8
LvKH6hvl7poL3dDbV1lSx5o4NPI+fFQ68MlpLbvYz/6WZl/KOcFdQHt4QVyhYwBRhd4Czn0/AuzW
JTJaa1YRqozECgXrWfPamgMbzmSfrptgQtwBhZ2BgcmrjGAFFoWbC23x6MY2/2CKh3ddWQ6FduVp
lI/6oczyUJCNrCvQJqx0tG7MzHreQkjZxOYyKAZ5/gRwAMisidoSrViTn297PuHKkLMf6tam8kDm
n6x4cAyBIMsDFBGJzJWjNqWK7j4WotLdysXQkyUNmVK1KjqOOE0jpvYE7kJHYAy/sQA6sGg9/KDf
a5MsgzNMuAJo8ETDq06sXrQLQddJ1dmbD4ccGmR/bQ+qcEs1KB5fv4HAvlEoR+rRZfk0PZffMFSV
SHGhoZlAnjRBSVB7+8AJRvXyOHzEpdX/EUFE2T0nT8kosVTSDr1DarpzyQWhiznZAE25O6GKnDwp
Iac60mSN7D11nPbbrPt0sfuckANEaV9BAbp6lubdazlX+bWDM5RJkJQKi7Un7J6JYUvZ9usRq6aZ
mIUDSShON7al9hFKQxFhf8fcG0wztWaemp70S/QLlvvfvNI2tDVcwSqAZh8a0j0kq8fdeFufnCXZ
1AwPUvieDd2YUIR47mCVBjcEEjNpvgjhWWnQzPy4wD2QhRdkZ8kMHgMKMr1SdyLnGs4e0zncEC+o
HjlCSznRU15Wrokpce3bgVJLAFHCbcJVBtwG+I6BX4f4QUsq4BwxNTVcAHgR6st/7l7+MRoKHRx4
Cs4e+2/0u2MKqI2f4IjjZEIoqJeO8Dy09skEsGjJmBPjmO4eyMD1bqZcpNuZS6aeYbjTe7fKImL5
iXTnZvBmjl1/brBdZNmy5DjLrRvCxFkT5d7vvfHcjGyn5llQAI3Ci/QKhqImMLt5KdsnjwDrKPdx
xGJH5XfsshzNbvumctSvWiy0MxF7bO2bW8vFP0I0hMhdy7mmY5nxc4uGCIX9MGHI1/W44SYsmBuA
gX0JbVSc3LQTwsXRS5UBddqt8E0sAne7X/i8VUbadJtBGkpZ2kV/qfqcULCdrQod409Q1q3VaVlE
CTiy5utQmaA53j0rStdrqg/vmBVfR3fhSvHEMliy5rXeKMnWsKZnLoCk+sa0iy4YjgT+DvJ1FzJ2
bnxcCEWOKABTWSEiu1etTh1zBPM8S/dg6fLI8Avmfzg2nXmSj8Ti7BT1+Sqfr78puMtIl8SU/c/N
drBUWT9iKnDggOsccwS23SIre7iWt2cQfV3ntCF87tuTjSffSI/ou/3ezDl3CwXJC3Ngu5WDy1kH
kcV1zgzyrX196aO4CpQSt5CIppvJE0+z8tWZOeDaR7HUis4CGUEUJv4SYc2Cv4uuxlyC3dju46Bm
jXDflX44aHMVqN4t7FRnPjPEbhTqZ2lmpw/ZPuiGYUoBLDwBWgfC8w7ruCHAwLa+75l+++/auoO9
PubSZ4fXXWn2FiksCB02Rxe4DYFcBk6ELYRUMLkOVLfvGcPdPMnLJ7hv/5rLdJ8FpwCpzoLFzv25
wNDaVnVW6VICqLWO3QofAD+ZB/qBhvXHyjvsch+kQb5AccSpEEtF4YvYm/X/cfT7dYpvKmoGQcM6
tHgPfWm0g2Fvk7mgG/59986GAndcsWl4KdrUx/4krYco9Bm1lg5w1sh/igBOazNkGuBEGF1AQnAW
8yJAl5Plp7rRmjjfXEH24tssL/9lD0V8jtZi28Q+TlDguzJKSXKG0X7GqmIdeCPdX8/7lPlifNel
eIg19+mEDnhd7qYWSQCMA4+IQuHaTfE1RYvsDfKKecxIyqXv6+nfjD89ConF2Jq1UnvJOqz5Wr4W
83gD9yjsDwthohJiQLh0dpiI+TGefhstp4Oi9uDPagK17y/scdn7PddCZEr7Ho0kATZzSHZS+chX
23/eZB7Hya8MBAqPueAQphNDWwK0J4GLz5XtwkyAME+MOdzOJtB5LXrRB5mOtLCNEv9RgK25V7SW
ISfIyyNVZAk/CeoU+mFmtW9EFASUd7mLGQS7LdTaTVXrYYGNA6w2PI/G3WwgRSER1QoOvK5eKMck
2elbzMp05NkIDtVYpRknR8ezLoYdREMKeCl1FhpD3EQl8AUJ+3s9l7iC5Puq/sZ1RG73I+jsBsA/
n3Cy7uqCryWSKmLZkTqsmFPEhITc4v7m8ue/+MvDyqPna9m1IlkUrcre22sM1ukdUd3ilygoy7r8
Oifx6TXP+krqx1lVSImyPJb00/93hE4ziQ7SiTOg2rjPCYInm+MYTxGtAA9KTD/ZBfJoCamGPqw6
DU/ygQqIyKDIv0VX+YeSrQdLPJmcTtkIz7rSamka+WfahhI1HpBD+x24Losx1M5umbHR53mNM33W
k8AIj/PC2KVe3EZvS5m8yHpkQV4dhH6qklFXnO9GdWp2t07nb6PAy9dTHnX6ipXg8nslNgk/Drc4
GE67zTxINUT/48iznekqFPY/1RyVUCEHB70Uqq4dVC3bMP+SMLeMX1QwO3SgvjYKu8SHWiKSwmNm
x8gaC1Dv3Flf/pwdD1+FPWZHrHyVcyw9JQIHU8N3b493dYoaaNrT0/4FbbC2lIXOuGwelCLNBqdf
BhsfwfeH5PNWuB3JLX4LzcVFRBgUCasDOdk2n+BhWojMp+gHF7ZajUgce7N0i0qNGkY6iI2a7JhJ
SWmr37U3c4BaR9jClfyTjmXB7WCMBy7mwj/vzrnjhgrajlEFO3hYCAbrosbzN5TkcS3hR76YV4mE
jfwMCKv8lvbA1WQ33XNDe8FDORevtPBrGFq/KB9J+UiUdVQzUajVahNoMYRP5fGLfdi+u/hXU1m3
ZCe2MRq9hpLycMaewtzJqGdP49lqbiFcY6c6vuyeM8HA8Pf3975PQ5Um3haVAOHWdIirdMTeIYSE
CI5kE738ApQcQjJhq18mRts75WyV/QwZRHRTRuXlYpYq6i3vBREXYJFowBlFGnlQD+WZpRFSBZj8
UxUE8lPMOrz0844lfdIB4Z+BLhBmH4SSL9rtkYNjwVPGAc14vYwd0m7pfXZalcFFh0v2O+WQ/bQU
A0pvhDBzTRxkiC60yDWcotQAHanzY5wuX9bs4lpM1GakRhrn+CBqa3EEnQgDltw2EnmLtZW7Y3/G
tZfkN1PnW4a1gaMepPgpz/fM74mVVonyTMhHzGYQ7W6CVthAfDPYuqlmxqRgfY1vsTpDTLRdzRqq
XaXOJBoYOn13rFi2ZON9aRjZMgGqC0ZcqxVOqPYCx6ubESoOkRhfKhwyFC8d7ZwGHbrcACDcC1LE
L70+XwFCOFgtEJG3JejbL8kOD/g=
`protect end_protected
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity mult_peakdetect_mult_gen_v12_0_12 is
  port (
    CLK : in STD_LOGIC;
    A : in STD_LOGIC_VECTOR ( 17 downto 0 );
    B : in STD_LOGIC_VECTOR ( 17 downto 0 );
    CE : in STD_LOGIC;
    SCLR : in STD_LOGIC;
    ZERO_DETECT : out STD_LOGIC_VECTOR ( 1 downto 0 );
    P : out STD_LOGIC_VECTOR ( 31 downto 0 );
    PCASC : out STD_LOGIC_VECTOR ( 47 downto 0 )
  );
  attribute C_A_TYPE : integer;
  attribute C_A_TYPE of mult_peakdetect_mult_gen_v12_0_12 : entity is 0;
  attribute C_A_WIDTH : integer;
  attribute C_A_WIDTH of mult_peakdetect_mult_gen_v12_0_12 : entity is 18;
  attribute C_B_TYPE : integer;
  attribute C_B_TYPE of mult_peakdetect_mult_gen_v12_0_12 : entity is 0;
  attribute C_B_VALUE : string;
  attribute C_B_VALUE of mult_peakdetect_mult_gen_v12_0_12 : entity is "10000001";
  attribute C_B_WIDTH : integer;
  attribute C_B_WIDTH of mult_peakdetect_mult_gen_v12_0_12 : entity is 18;
  attribute C_CCM_IMP : integer;
  attribute C_CCM_IMP of mult_peakdetect_mult_gen_v12_0_12 : entity is 0;
  attribute C_CE_OVERRIDES_SCLR : integer;
  attribute C_CE_OVERRIDES_SCLR of mult_peakdetect_mult_gen_v12_0_12 : entity is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of mult_peakdetect_mult_gen_v12_0_12 : entity is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of mult_peakdetect_mult_gen_v12_0_12 : entity is 0;
  attribute C_HAS_ZERO_DETECT : integer;
  attribute C_HAS_ZERO_DETECT of mult_peakdetect_mult_gen_v12_0_12 : entity is 0;
  attribute C_LATENCY : integer;
  attribute C_LATENCY of mult_peakdetect_mult_gen_v12_0_12 : entity is 3;
  attribute C_MODEL_TYPE : integer;
  attribute C_MODEL_TYPE of mult_peakdetect_mult_gen_v12_0_12 : entity is 0;
  attribute C_MULT_TYPE : integer;
  attribute C_MULT_TYPE of mult_peakdetect_mult_gen_v12_0_12 : entity is 1;
  attribute C_OPTIMIZE_GOAL : integer;
  attribute C_OPTIMIZE_GOAL of mult_peakdetect_mult_gen_v12_0_12 : entity is 1;
  attribute C_OUT_HIGH : integer;
  attribute C_OUT_HIGH of mult_peakdetect_mult_gen_v12_0_12 : entity is 31;
  attribute C_OUT_LOW : integer;
  attribute C_OUT_LOW of mult_peakdetect_mult_gen_v12_0_12 : entity is 0;
  attribute C_ROUND_OUTPUT : integer;
  attribute C_ROUND_OUTPUT of mult_peakdetect_mult_gen_v12_0_12 : entity is 0;
  attribute C_ROUND_PT : integer;
  attribute C_ROUND_PT of mult_peakdetect_mult_gen_v12_0_12 : entity is 0;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of mult_peakdetect_mult_gen_v12_0_12 : entity is 0;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of mult_peakdetect_mult_gen_v12_0_12 : entity is "artix7";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of mult_peakdetect_mult_gen_v12_0_12 : entity is "mult_gen_v12_0_12";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of mult_peakdetect_mult_gen_v12_0_12 : entity is "yes";
end mult_peakdetect_mult_gen_v12_0_12;

architecture STRUCTURE of mult_peakdetect_mult_gen_v12_0_12 is
  signal \<const0>\ : STD_LOGIC;
  signal NLW_i_mult_PCASC_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 0 );
  signal NLW_i_mult_ZERO_DETECT_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute C_A_TYPE of i_mult : label is 0;
  attribute C_A_WIDTH of i_mult : label is 18;
  attribute C_B_TYPE of i_mult : label is 0;
  attribute C_B_VALUE of i_mult : label is "10000001";
  attribute C_B_WIDTH of i_mult : label is 18;
  attribute C_CCM_IMP of i_mult : label is 0;
  attribute C_CE_OVERRIDES_SCLR of i_mult : label is 0;
  attribute C_HAS_CE of i_mult : label is 0;
  attribute C_HAS_SCLR of i_mult : label is 0;
  attribute C_HAS_ZERO_DETECT of i_mult : label is 0;
  attribute C_LATENCY of i_mult : label is 3;
  attribute C_MODEL_TYPE of i_mult : label is 0;
  attribute C_MULT_TYPE of i_mult : label is 1;
  attribute C_OPTIMIZE_GOAL of i_mult : label is 1;
  attribute C_OUT_HIGH of i_mult : label is 31;
  attribute C_OUT_LOW of i_mult : label is 0;
  attribute C_ROUND_OUTPUT of i_mult : label is 0;
  attribute C_ROUND_PT of i_mult : label is 0;
  attribute C_VERBOSITY of i_mult : label is 0;
  attribute C_XDEVICEFAMILY of i_mult : label is "artix7";
  attribute downgradeipidentifiedwarnings of i_mult : label is "yes";
begin
  PCASC(47) <= \<const0>\;
  PCASC(46) <= \<const0>\;
  PCASC(45) <= \<const0>\;
  PCASC(44) <= \<const0>\;
  PCASC(43) <= \<const0>\;
  PCASC(42) <= \<const0>\;
  PCASC(41) <= \<const0>\;
  PCASC(40) <= \<const0>\;
  PCASC(39) <= \<const0>\;
  PCASC(38) <= \<const0>\;
  PCASC(37) <= \<const0>\;
  PCASC(36) <= \<const0>\;
  PCASC(35) <= \<const0>\;
  PCASC(34) <= \<const0>\;
  PCASC(33) <= \<const0>\;
  PCASC(32) <= \<const0>\;
  PCASC(31) <= \<const0>\;
  PCASC(30) <= \<const0>\;
  PCASC(29) <= \<const0>\;
  PCASC(28) <= \<const0>\;
  PCASC(27) <= \<const0>\;
  PCASC(26) <= \<const0>\;
  PCASC(25) <= \<const0>\;
  PCASC(24) <= \<const0>\;
  PCASC(23) <= \<const0>\;
  PCASC(22) <= \<const0>\;
  PCASC(21) <= \<const0>\;
  PCASC(20) <= \<const0>\;
  PCASC(19) <= \<const0>\;
  PCASC(18) <= \<const0>\;
  PCASC(17) <= \<const0>\;
  PCASC(16) <= \<const0>\;
  PCASC(15) <= \<const0>\;
  PCASC(14) <= \<const0>\;
  PCASC(13) <= \<const0>\;
  PCASC(12) <= \<const0>\;
  PCASC(11) <= \<const0>\;
  PCASC(10) <= \<const0>\;
  PCASC(9) <= \<const0>\;
  PCASC(8) <= \<const0>\;
  PCASC(7) <= \<const0>\;
  PCASC(6) <= \<const0>\;
  PCASC(5) <= \<const0>\;
  PCASC(4) <= \<const0>\;
  PCASC(3) <= \<const0>\;
  PCASC(2) <= \<const0>\;
  PCASC(1) <= \<const0>\;
  PCASC(0) <= \<const0>\;
  ZERO_DETECT(1) <= \<const0>\;
  ZERO_DETECT(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
i_mult: entity work.mult_peakdetect_mult_gen_v12_0_12_viv
     port map (
      A(17 downto 0) => A(17 downto 0),
      B(17 downto 0) => B(17 downto 0),
      CE => '0',
      CLK => CLK,
      P(31 downto 0) => P(31 downto 0),
      PCASC(47 downto 0) => NLW_i_mult_PCASC_UNCONNECTED(47 downto 0),
      SCLR => '0',
      ZERO_DETECT(1 downto 0) => NLW_i_mult_ZERO_DETECT_UNCONNECTED(1 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity mult_peakdetect is
  port (
    CLK : in STD_LOGIC;
    A : in STD_LOGIC_VECTOR ( 17 downto 0 );
    B : in STD_LOGIC_VECTOR ( 17 downto 0 );
    P : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of mult_peakdetect : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of mult_peakdetect : entity is "mult_peakdetect,mult_gen_v12_0_12,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of mult_peakdetect : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of mult_peakdetect : entity is "mult_gen_v12_0_12,Vivado 2016.4";
end mult_peakdetect;

architecture STRUCTURE of mult_peakdetect is
  signal NLW_U0_PCASC_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 0 );
  signal NLW_U0_ZERO_DETECT_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute C_A_TYPE : integer;
  attribute C_A_TYPE of U0 : label is 0;
  attribute C_A_WIDTH : integer;
  attribute C_A_WIDTH of U0 : label is 18;
  attribute C_B_TYPE : integer;
  attribute C_B_TYPE of U0 : label is 0;
  attribute C_B_VALUE : string;
  attribute C_B_VALUE of U0 : label is "10000001";
  attribute C_B_WIDTH : integer;
  attribute C_B_WIDTH of U0 : label is 18;
  attribute C_CCM_IMP : integer;
  attribute C_CCM_IMP of U0 : label is 0;
  attribute C_CE_OVERRIDES_SCLR : integer;
  attribute C_CE_OVERRIDES_SCLR of U0 : label is 0;
  attribute C_HAS_CE : integer;
  attribute C_HAS_CE of U0 : label is 0;
  attribute C_HAS_SCLR : integer;
  attribute C_HAS_SCLR of U0 : label is 0;
  attribute C_HAS_ZERO_DETECT : integer;
  attribute C_HAS_ZERO_DETECT of U0 : label is 0;
  attribute C_LATENCY : integer;
  attribute C_LATENCY of U0 : label is 3;
  attribute C_MODEL_TYPE : integer;
  attribute C_MODEL_TYPE of U0 : label is 0;
  attribute C_MULT_TYPE : integer;
  attribute C_MULT_TYPE of U0 : label is 1;
  attribute C_OPTIMIZE_GOAL : integer;
  attribute C_OPTIMIZE_GOAL of U0 : label is 1;
  attribute C_OUT_HIGH : integer;
  attribute C_OUT_HIGH of U0 : label is 31;
  attribute C_OUT_LOW : integer;
  attribute C_OUT_LOW of U0 : label is 0;
  attribute C_ROUND_OUTPUT : integer;
  attribute C_ROUND_OUTPUT of U0 : label is 0;
  attribute C_ROUND_PT : integer;
  attribute C_ROUND_PT of U0 : label is 0;
  attribute C_VERBOSITY : integer;
  attribute C_VERBOSITY of U0 : label is 0;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of U0 : label is "artix7";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
begin
U0: entity work.mult_peakdetect_mult_gen_v12_0_12
     port map (
      A(17 downto 0) => A(17 downto 0),
      B(17 downto 0) => B(17 downto 0),
      CE => '1',
      CLK => CLK,
      P(31 downto 0) => P(31 downto 0),
      PCASC(47 downto 0) => NLW_U0_PCASC_UNCONNECTED(47 downto 0),
      SCLR => '0',
      ZERO_DETECT(1 downto 0) => NLW_U0_ZERO_DETECT_UNCONNECTED(1 downto 0)
    );
end STRUCTURE;
