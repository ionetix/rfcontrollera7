`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Engineer: 		Nathan Usher
// Create Date:    17:51:05 03/29/2014
// Module Name:    dacCtrl
// Description: 	RF output DAC driver
// Additional Comments: 
//		
//////////////////////////////////////////////////////////////////////////////////
module dacCtrl(
	input dacClk,
	input dac8Samples,
	input [13:0] I,
	input [13:0] Q,
	input valid,
	output [13:0] dacData_N,
	output [13:0] dacData_P
);

	reg [2:0] ctr = 0;
	reg [2:0] ctrPrev = 0;
	reg [13:0] dacI = 14'h2000, dacQ = 14'h2000;
	reg [13:0] dacReg = 14'h2000, dacRegNext = 14'h2000;
  reg validReg = 0;

	always @ (posedge dacClk)
	begin
    validReg <= valid;
		ctr <= dac8Samples ? (ctr + 1) : (ctr + 2);
		ctrPrev <= ctr;
		if (validReg)
		begin
			dacI <= {I[13:1], ~I[0]};
			dacQ <= {Q[13:1], ~Q[0]}; //inverted pins on PCB DAC_0
		end
		dacRegNext <= ctr[1] ? dacQ : dacI;
		dacReg <= ctrPrev[2] ? ~dacRegNext : dacRegNext;
	end
	
	wire [13:0] dacDataToOBUF;
	genvar x;
	generate
		for (x=0; x<14; x=x+1)
		begin : dacDataLoop
			ODDR2 #(.DDR_ALIGNMENT("NONE"), .INIT(1'b0), .SRTYPE("SYNC")) dacData_oddr2 (.Q(dacDataToOBUF[x]),
				.C0(dacClk), .C1(1'b0), .CE(1'b1), .D0(dacReg[x]), .D1(dacReg[x]), .R(1'b0), .S(1'b0));
			OBUFDS #(.IOSTANDARD("LVDS_25")) dacData_obufds (.O(dacData_P[x]), .OB(dacData_N[x]), .I(dacDataToOBUF[x]));
		end
	endgenerate
endmodule
