`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    09:03:46 07/05/2011 
// Design Name: 
// Module Name:    pol2rect 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
// I and Q are now 1-bit wider than requested, in the inputs are not saturated
// prior to the CORDIC to prevent overflow
//
//////////////////////////////////////////////////////////////////////////////////
module pol2rect(clk, ctr, lookup_p, lookup_n, mag, phase, I, Q);
	parameter samples = 29;
	parameter outbits = 33;
	input clk;
	input [4:0] ctr;
	input [31:0] lookup_p;
	input [31:0] lookup_n;
	input [31:0] mag;
	input signed [31:0] phase;
	output reg signed [outbits-1:0] I = 0;
	output reg signed [outbits-1:0] Q = 0;

	wire signed [32:0] I2, Q1, Q2;
	reg signed [32:0] I3 = 0, Q3 = 0;
  reg signed [31:0] P3 = 0;


	assign Q1 = phase[31] ? {1'b1, -mag} : {1'b0, mag};
	assign I2 = P3[31] ? (I3 + (Q3 >>> ctr)) : (I3 - (Q3 >>> ctr));
	assign Q2 = P3[31] ? (Q3 - (I3 >>> ctr)) : (Q3 + (I3 >>> ctr));

	always @ (posedge clk)
	begin
		if (ctr == (samples - 1))
		begin
			I3 <= 0;
			Q3 <= Q1;
			P3 <= phase[31] ? phase + 32'h40000000 : phase + 32'hC0000000;
			I <= |mag ? I3[32:33-outbits]:0;
			Q <= |mag ? Q3[32:33-outbits]:0;
		end
		else
		begin
			I3 <= I2;
			Q3 <= Q2;
			if (|ctr)
				P3 <= P3[31] ? (P3 + lookup_p) : (P3 + lookup_n);
			else
				P3 <= P3[31] ? (P3 + 32'h20000000) : (P3 + 32'hE0000000);
		end
	end
	
/*	wire [35:0] csCtrl;
	wire [255:0] csData;
	chipscope_icon csIcon(.CONTROL0(csCtrl));
	chipscope_ila csIla(.CONTROL(csCtrl), .CLK(clk), .TRIG0(csData));
	assign csData[31:0] = lookup_p;
	assign csData[63:32] = lookup_n;
	assign csData[79:64] = mag[31:16];
	assign csData[95:80] = phase[31:16];
	assign csData[100:96] = ctr;
	assign csData[127:101] = 0;
	assign csData[143:128] = Q1[31:16];
	assign csData[159:144] = I2[31:16];
	assign csData[175:160] = Q2[31:16];
	assign csData[191:176] = I3[31:16];
	assign csData[207:192] = Q3[31:16];
	assign csData[223:208] = P3[31:16];
	assign csData[255:224] = 0;*/
endmodule
