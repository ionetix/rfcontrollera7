`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 			Ionetix Corporation
// Engineer: 			Nathan Usher
// 
// Create Date:    09:45:52 02/25/2016 
// Design Name: 	
// Module Name:    peak 
// Project Name: 	
// Description: 
// Additional Comments: 
//		output produced on ctr==5'b11101, valid on ctr==5'b11110
//////////////////////////////////////////////////////////////////////////////////
module peakDetect(
	input clk,
	input [4:0] ctr,
	input signed [13:0] data,
	output reg signed [31:0] peak
	);
	
	parameter samples = 29;

	reg [17:0] absData;
	wire [31:0] dataScaled;
	// Search for the peak value in each set of samples and return it

mult_peakdetect peakScale (
  .CLK(clk),  // input wire CLK
  .A(absData),      // input wire [17 : 0] A
  .B(18'h05F7F),      // input wire [17 : 0] B
  .P(dataScaled)      // output wire [31 : 0] P
);


/*	DSP48A1 #(.A0REG(1), .A1REG(1), .B0REG(1), .B1REG(1), .CARRYINREG(0), .CARRYINSEL("OPMODE5"),
		.CARRYOUTREG(0), .CREG(0), .DREG(0), .MREG(1), .OPMODEREG(0), .PREG(1), .RSTTYPE("SYNC")
		) peakScale (.BCOUT(), .PCOUT(), .CARRYOUT(), .CARRYOUTF(), .M(), .P(dataScaled), .PCIN(48'h0),
		.CLK(clk), .OPMODE(8'h01), .A(absData),
		.B(18'h05F7F), .C(48'h0), .CARRYIN(1'b0), .D(18'h0), .CEA(1'b1),
		.CEB(1'b1), .CEC(1'b1), .CECARRYIN(1'b1), .CED(1'b1), .CEM(1'b1), .CEOPMODE(1'b1), .CEP(1'b1),
		.RSTA(1'b0), .RSTB(1'b0), .RSTC(1'b0), .RSTCARRYIN(1'b0), .RSTD(1'b0), .RSTM(1'b0),
		.RSTOPMODE(1'b0), .RSTP(1'b0));
*/		
/*		
             DSP48E1 #(
             // Feature Control Attributes: Data Path Selection
             .A_INPUT("DIRECT"),               // Selects A input source, "DIRECT" (A port) or "CASCADE" (ACIN port)
             .B_INPUT("DIRECT"),               // Selects B input source, "DIRECT" (B port) or "CASCADE" (BCIN port)
             .USE_DPORT("FALSE"),              // Select D port usage (TRUE or FALSE)
             .USE_MULT("MULTIPLY"),            // Select multiplier usage ("MULTIPLY", "DYNAMIC", or "NONE")
             .USE_SIMD("ONE48"),               // SIMD selection ("ONE48", "TWO24", "FOUR12")
             // Pattern Detector Attributes: Pattern Detection Configuration
             .AUTORESET_PATDET("NO_RESET"),    // "NO_RESET", "RESET_MATCH", "RESET_NOT_MATCH" 
             .MASK(48'h3fffffffffff),          // 48-bit mask value for pattern detect (1=ignore)
             .PATTERN(48'h000000000000),       // 48-bit pattern match for pattern detect
             .SEL_MASK("MASK"),                // "C", "MASK", "ROUNDING_MODE1", "ROUNDING_MODE2" 
             .SEL_PATTERN("PATTERN"),          // Select pattern value ("PATTERN" or "C")
             .USE_PATTERN_DETECT("NO_PATDET"), // Enable pattern detect ("PATDET" or "NO_PATDET")
             // Register Control Attributes: Pipeline Register Configuration
             .ACASCREG(1),                     // Number of pipeline stages between A/ACIN and ACOUT (0, 1 or 2)
             .ADREG(1),                        // Number of pipeline stages for pre-adder (0 or 1)
             .ALUMODEREG(1),                   // Number of pipeline stages for ALUMODE (0 or 1)
             .AREG(1),                         // Number of pipeline stages for A (0, 1 or 2)
             .BCASCREG(1),                     // Number of pipeline stages between B/BCIN and BCOUT (0, 1 or 2)
             .BREG(1),                         // Number of pipeline stages for B (0, 1 or 2)
             .CARRYINREG(1),                   // Number of pipeline stages for CARRYIN (0 or 1)
             .CARRYINSELREG(1),                // Number of pipeline stages for CARRYINSEL (0 or 1)
             .CREG(1),                         // Number of pipeline stages for C (0 or 1)
             .DREG(1),                         // Number of pipeline stages for D (0 or 1)
             .INMODEREG(1),                    // Number of pipeline stages for INMODE (0 or 1)
             .MREG(1),                         // Number of multiplier pipeline stages (0 or 1)
             .OPMODEREG(1),                    // Number of pipeline stages for OPMODE (0 or 1)
             .PREG(1)                          // Number of pipeline stages for P (0 or 1)
          )
          peakScale (
             // Cascade: 30-bit (each) output: Cascade Ports
             .ACOUT(),                   // 30-bit output: A port cascade output
             .BCOUT(),                   // 18-bit output: B port cascade output
             .CARRYCASCOUT(),     // 1-bit output: Cascade carry output
             .MULTSIGNOUT(),       // 1-bit output: Multiplier sign cascade output
             .PCOUT(),                   // 48-bit output: Cascade output
             // Control: 1-bit (each) output: Control Inputs/Status Bits
             .OVERFLOW(),             // 1-bit output: Overflow in add/acc output
             .PATTERNBDETECT(), // 1-bit output: Pattern bar detect output
             .PATTERNDETECT(),   // 1-bit output: Pattern detect output
             .UNDERFLOW(),           // 1-bit output: Underflow in add/acc output
             // Data: 4-bit (each) output: Data Ports
             .CARRYOUT(),             // 4-bit output: Carry output
             .P(dataScaled),                           // 48-bit output: Primary data output
             // Cascade: 30-bit (each) input: Cascade Ports
             .ACIN(),                     // 30-bit input: A cascade data input
             .BCIN(),                     // 18-bit input: B cascade input
             .CARRYCASCIN(),       // 1-bit input: Cascade carry input
             .MULTSIGNIN(),         // 1-bit input: Multiplier sign input
             .PCIN(48'h0),                     // 48-bit input: P cascade input
             // Control: 4-bit (each) input: Control Inputs/Status Bits
             .ALUMODE(),               // 4-bit input: ALU control input
             .CARRYINSEL("OPMODE5"),         // 3-bit input: Carry select input
             .CLK(clk),                       // 1-bit input: Clock input
             .INMODE(),                 // 5-bit input: INMODE control input
             .OPMODE(8'h01),                 // 7-bit input: Operation mode input
             // Data: 30-bit (each) input: Data Ports
             .A(absData),                           // 30-bit input: A data input
             .B(18'h05F7F),                           // 18-bit input: B data input
             .C(48'h0),                           // 48-bit input: C data input
             .CARRYIN(1'b0),               // 1-bit input: Carry input signal
             .D(18'h0),                           // 25-bit input: D data input
             // Reset/Clock Enable: 1-bit (each) input: Reset/Clock Enable Inputs
             .CEA1(1'b1),                     // 1-bit input: Clock enable input for 1st stage AREG
             .CEA2(1'b1),                     // 1-bit input: Clock enable input for 2nd stage AREG
             .CEAD(1'b1),                     // 1-bit input: Clock enable input for ADREG
             .CEALUMODE(1'b1),           // 1-bit input: Clock enable input for ALUMODE
             .CEB1(1'b1),                     // 1-bit input: Clock enable input for 1st stage BREG
             .CEB2(1'b1),                     // 1-bit input: Clock enable input for 2nd stage BREG
             .CEC(1'b1),                       // 1-bit input: Clock enable input for CREG
             .CECARRYIN(1'b1),           // 1-bit input: Clock enable input for CARRYINREG
             .CECTRL(1'b1),                 // 1-bit input: Clock enable input for OPMODEREG and CARRYINSELREG
             .CED(1'b1),                       // 1-bit input: Clock enable input for DREG
             .CEINMODE(),             // 1-bit input: Clock enable input for INMODEREG
             .CEM(1'b1),                       // 1-bit input: Clock enable input for MREG
             .CEP(1'b1),                       // 1-bit input: Clock enable input for PREG
             .RSTA(1'b0),                     // 1-bit input: Reset input for AREG
             .RSTALLCARRYIN(1'b0),   // 1-bit input: Reset input for CARRYINREG
             .RSTALUMODE(1'b0),         // 1-bit input: Reset input for ALUMODEREG
             .RSTB(1'b0),                     // 1-bit input: Reset input for BREG
             .RSTC(1'b0),                     // 1-bit input: Reset input for CREG
             .RSTCTRL(1'b0),               // 1-bit input: Reset input for OPMODEREG and CARRYINSELREG
             .RSTD(1'b0),                     // 1-bit input: Reset input for DREG and ADREG
             .RSTINMODE(),           // 1-bit input: Reset input for INMODEREG
             .RSTM(1'b0),                     // 1-bit input: Reset input for MREG
             .RSTP(1'b0)                      // 1-bit input: Reset input for PREG
          );   
    */      
	reg [31:0] tempPeak;
	always @ (posedge clk)
	begin
		absData <= data[13] ? {~data, 4'h0} : {data, 4'h0};
		if (ctr == (samples - 2))
		begin
			tempPeak <= dataScaled;
			peak <= peak + (tempPeak >> 6) - (peak >> 6);
		end
		else if (dataScaled > tempPeak)
			tempPeak <= dataScaled;
	end
endmodule
