`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 			FRIB
// Engineer: 			Nathan Usher
// 
// Create Date:    16:20:27 02/22/2013 
// Design Name: 		FRIB LLRF PED2 - FGPDB
// Module Name:    ilkRate 
// Project Name: 		FRIB LLRF PED2
// Description: 		running average of interlock rate
//
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ilkRate(
	input clk,
	input [15:0] ilkMask,
	input [15:0] ilkEdge,
	input [5:0] ilkShift,
	output [7:0] rate
	);

	reg [63:0] acc = 0;
	wire ilk = |(ilkMask & ilkEdge);
	assign rate = acc[63:56];
	always @ (posedge clk)
	begin
		acc <= acc + {ilk, 56'h0} - (ilk >> ilkShift);
	end

endmodule
