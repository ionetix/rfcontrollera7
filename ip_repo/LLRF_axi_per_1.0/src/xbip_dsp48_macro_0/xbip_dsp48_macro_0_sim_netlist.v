// Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2016.4 (win64) Build 1733598 Wed Dec 14 22:35:39 MST 2016
// Date        : Wed Dec 13 16:07:57 2017
// Host        : SHRIRAJKUNJA80D running 64-bit Service Pack 1  (build 7601)
// Command     : write_verilog -force -mode funcsim
//               c:/Users/shrirajkunjir/ip_repo/LLRF_axi_per_1.0/src/xbip_dsp48_macro_0/xbip_dsp48_macro_0_sim_netlist.v
// Design      : xbip_dsp48_macro_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a200tfbg484-2
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "xbip_dsp48_macro_0,xbip_dsp48_macro_v3_0_13,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "xbip_dsp48_macro_v3_0_13,Vivado 2016.4" *) 
(* NotValidForBitStream *)
module xbip_dsp48_macro_0
   (CLK,
    A,
    B,
    C,
    P);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 a_intf DATA" *) input [17:0]A;
  (* x_interface_info = "xilinx.com:signal:data:1.0 b_intf DATA" *) input [17:0]B;
  (* x_interface_info = "xilinx.com:signal:data:1.0 c_intf DATA" *) input [47:0]C;
  (* x_interface_info = "xilinx.com:signal:data:1.0 p_intf DATA" *) output [47:0]P;

  wire [17:0]A;
  wire [17:0]B;
  wire [47:0]C;
  wire CLK;
  wire [47:0]P;
  wire NLW_U0_CARRYCASCOUT_UNCONNECTED;
  wire NLW_U0_CARRYOUT_UNCONNECTED;
  wire [29:0]NLW_U0_ACOUT_UNCONNECTED;
  wire [17:0]NLW_U0_BCOUT_UNCONNECTED;
  wire [47:0]NLW_U0_PCOUT_UNCONNECTED;

  (* C_A_WIDTH = "18" *) 
  (* C_B_WIDTH = "18" *) 
  (* C_CONCAT_WIDTH = "48" *) 
  (* C_CONSTANT_1 = "1" *) 
  (* C_C_WIDTH = "48" *) 
  (* C_D_WIDTH = "18" *) 
  (* C_HAS_A = "1" *) 
  (* C_HAS_ACIN = "0" *) 
  (* C_HAS_ACOUT = "0" *) 
  (* C_HAS_B = "1" *) 
  (* C_HAS_BCIN = "0" *) 
  (* C_HAS_BCOUT = "0" *) 
  (* C_HAS_C = "1" *) 
  (* C_HAS_CARRYCASCIN = "0" *) 
  (* C_HAS_CARRYCASCOUT = "0" *) 
  (* C_HAS_CARRYIN = "0" *) 
  (* C_HAS_CARRYOUT = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_CEA = "0" *) 
  (* C_HAS_CEB = "0" *) 
  (* C_HAS_CEC = "0" *) 
  (* C_HAS_CECONCAT = "0" *) 
  (* C_HAS_CED = "0" *) 
  (* C_HAS_CEM = "0" *) 
  (* C_HAS_CEP = "0" *) 
  (* C_HAS_CESEL = "0" *) 
  (* C_HAS_CONCAT = "0" *) 
  (* C_HAS_D = "0" *) 
  (* C_HAS_INDEP_CE = "0" *) 
  (* C_HAS_INDEP_SCLR = "0" *) 
  (* C_HAS_PCIN = "0" *) 
  (* C_HAS_PCOUT = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SCLRA = "0" *) 
  (* C_HAS_SCLRB = "0" *) 
  (* C_HAS_SCLRC = "0" *) 
  (* C_HAS_SCLRCONCAT = "0" *) 
  (* C_HAS_SCLRD = "0" *) 
  (* C_HAS_SCLRM = "0" *) 
  (* C_HAS_SCLRP = "0" *) 
  (* C_HAS_SCLRSEL = "0" *) 
  (* C_LATENCY = "-1" *) 
  (* C_MODEL_TYPE = "0" *) 
  (* C_OPMODES = "000000000011010100000000" *) 
  (* C_P_LSB = "0" *) 
  (* C_P_MSB = "47" *) 
  (* C_REG_CONFIG = "00000000000011100011100011000100" *) 
  (* C_SEL_WIDTH = "0" *) 
  (* C_TEST_CORE = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  xbip_dsp48_macro_0_xbip_dsp48_macro_v3_0_13 U0
       (.A(A),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_U0_ACOUT_UNCONNECTED[29:0]),
        .B(B),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_U0_BCOUT_UNCONNECTED[17:0]),
        .C(C),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_U0_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYOUT(NLW_U0_CARRYOUT_UNCONNECTED),
        .CE(1'b1),
        .CEA(1'b1),
        .CEA1(1'b1),
        .CEA2(1'b1),
        .CEA3(1'b1),
        .CEA4(1'b1),
        .CEB(1'b1),
        .CEB1(1'b1),
        .CEB2(1'b1),
        .CEB3(1'b1),
        .CEB4(1'b1),
        .CEC(1'b1),
        .CEC1(1'b1),
        .CEC2(1'b1),
        .CEC3(1'b1),
        .CEC4(1'b1),
        .CEC5(1'b1),
        .CECONCAT(1'b1),
        .CECONCAT3(1'b1),
        .CECONCAT4(1'b1),
        .CECONCAT5(1'b1),
        .CED(1'b1),
        .CED1(1'b1),
        .CED2(1'b1),
        .CED3(1'b1),
        .CEM(1'b1),
        .CEP(1'b1),
        .CESEL(1'b1),
        .CESEL1(1'b1),
        .CESEL2(1'b1),
        .CESEL3(1'b1),
        .CESEL4(1'b1),
        .CESEL5(1'b1),
        .CLK(CLK),
        .CONCAT({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .P(P),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT(NLW_U0_PCOUT_UNCONNECTED[47:0]),
        .SCLR(1'b0),
        .SCLRA(1'b0),
        .SCLRB(1'b0),
        .SCLRC(1'b0),
        .SCLRCONCAT(1'b0),
        .SCLRD(1'b0),
        .SCLRM(1'b0),
        .SCLRP(1'b0),
        .SCLRSEL(1'b0),
        .SEL(1'b0));
endmodule

(* C_A_WIDTH = "18" *) (* C_B_WIDTH = "18" *) (* C_CONCAT_WIDTH = "48" *) 
(* C_CONSTANT_1 = "1" *) (* C_C_WIDTH = "48" *) (* C_D_WIDTH = "18" *) 
(* C_HAS_A = "1" *) (* C_HAS_ACIN = "0" *) (* C_HAS_ACOUT = "0" *) 
(* C_HAS_B = "1" *) (* C_HAS_BCIN = "0" *) (* C_HAS_BCOUT = "0" *) 
(* C_HAS_C = "1" *) (* C_HAS_CARRYCASCIN = "0" *) (* C_HAS_CARRYCASCOUT = "0" *) 
(* C_HAS_CARRYIN = "0" *) (* C_HAS_CARRYOUT = "0" *) (* C_HAS_CE = "0" *) 
(* C_HAS_CEA = "0" *) (* C_HAS_CEB = "0" *) (* C_HAS_CEC = "0" *) 
(* C_HAS_CECONCAT = "0" *) (* C_HAS_CED = "0" *) (* C_HAS_CEM = "0" *) 
(* C_HAS_CEP = "0" *) (* C_HAS_CESEL = "0" *) (* C_HAS_CONCAT = "0" *) 
(* C_HAS_D = "0" *) (* C_HAS_INDEP_CE = "0" *) (* C_HAS_INDEP_SCLR = "0" *) 
(* C_HAS_PCIN = "0" *) (* C_HAS_PCOUT = "0" *) (* C_HAS_SCLR = "0" *) 
(* C_HAS_SCLRA = "0" *) (* C_HAS_SCLRB = "0" *) (* C_HAS_SCLRC = "0" *) 
(* C_HAS_SCLRCONCAT = "0" *) (* C_HAS_SCLRD = "0" *) (* C_HAS_SCLRM = "0" *) 
(* C_HAS_SCLRP = "0" *) (* C_HAS_SCLRSEL = "0" *) (* C_LATENCY = "-1" *) 
(* C_MODEL_TYPE = "0" *) (* C_OPMODES = "000000000011010100000000" *) (* C_P_LSB = "0" *) 
(* C_P_MSB = "47" *) (* C_REG_CONFIG = "00000000000011100011100011000100" *) (* C_SEL_WIDTH = "0" *) 
(* C_TEST_CORE = "0" *) (* C_VERBOSITY = "0" *) (* C_XDEVICEFAMILY = "artix7" *) 
(* ORIG_REF_NAME = "xbip_dsp48_macro_v3_0_13" *) (* downgradeipidentifiedwarnings = "yes" *) 
module xbip_dsp48_macro_0_xbip_dsp48_macro_v3_0_13
   (CLK,
    CE,
    SCLR,
    SEL,
    CARRYCASCIN,
    CARRYIN,
    PCIN,
    ACIN,
    BCIN,
    A,
    B,
    C,
    D,
    CONCAT,
    ACOUT,
    BCOUT,
    CARRYOUT,
    CARRYCASCOUT,
    PCOUT,
    P,
    CED,
    CED1,
    CED2,
    CED3,
    CEA,
    CEA1,
    CEA2,
    CEA3,
    CEA4,
    CEB,
    CEB1,
    CEB2,
    CEB3,
    CEB4,
    CECONCAT,
    CECONCAT3,
    CECONCAT4,
    CECONCAT5,
    CEC,
    CEC1,
    CEC2,
    CEC3,
    CEC4,
    CEC5,
    CEM,
    CEP,
    CESEL,
    CESEL1,
    CESEL2,
    CESEL3,
    CESEL4,
    CESEL5,
    SCLRD,
    SCLRA,
    SCLRB,
    SCLRCONCAT,
    SCLRC,
    SCLRM,
    SCLRP,
    SCLRSEL);
  input CLK;
  input CE;
  input SCLR;
  input [0:0]SEL;
  input CARRYCASCIN;
  input CARRYIN;
  input [47:0]PCIN;
  input [29:0]ACIN;
  input [17:0]BCIN;
  input [17:0]A;
  input [17:0]B;
  input [47:0]C;
  input [17:0]D;
  input [47:0]CONCAT;
  output [29:0]ACOUT;
  output [17:0]BCOUT;
  output CARRYOUT;
  output CARRYCASCOUT;
  output [47:0]PCOUT;
  output [47:0]P;
  input CED;
  input CED1;
  input CED2;
  input CED3;
  input CEA;
  input CEA1;
  input CEA2;
  input CEA3;
  input CEA4;
  input CEB;
  input CEB1;
  input CEB2;
  input CEB3;
  input CEB4;
  input CECONCAT;
  input CECONCAT3;
  input CECONCAT4;
  input CECONCAT5;
  input CEC;
  input CEC1;
  input CEC2;
  input CEC3;
  input CEC4;
  input CEC5;
  input CEM;
  input CEP;
  input CESEL;
  input CESEL1;
  input CESEL2;
  input CESEL3;
  input CESEL4;
  input CESEL5;
  input SCLRD;
  input SCLRA;
  input SCLRB;
  input SCLRCONCAT;
  input SCLRC;
  input SCLRM;
  input SCLRP;
  input SCLRSEL;

  wire [17:0]A;
  wire [29:0]ACIN;
  wire [29:0]ACOUT;
  wire [17:0]B;
  wire [17:0]BCIN;
  wire [17:0]BCOUT;
  wire [47:0]C;
  wire CARRYCASCIN;
  wire CARRYCASCOUT;
  wire CARRYIN;
  wire CARRYOUT;
  wire CLK;
  wire [47:0]P;
  wire [47:0]PCIN;
  wire [47:0]PCOUT;

  (* C_A_WIDTH = "18" *) 
  (* C_B_WIDTH = "18" *) 
  (* C_CONCAT_WIDTH = "48" *) 
  (* C_CONSTANT_1 = "1" *) 
  (* C_C_WIDTH = "48" *) 
  (* C_D_WIDTH = "18" *) 
  (* C_HAS_A = "1" *) 
  (* C_HAS_ACIN = "0" *) 
  (* C_HAS_ACOUT = "0" *) 
  (* C_HAS_B = "1" *) 
  (* C_HAS_BCIN = "0" *) 
  (* C_HAS_BCOUT = "0" *) 
  (* C_HAS_C = "1" *) 
  (* C_HAS_CARRYCASCIN = "0" *) 
  (* C_HAS_CARRYCASCOUT = "0" *) 
  (* C_HAS_CARRYIN = "0" *) 
  (* C_HAS_CARRYOUT = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_CEA = "0" *) 
  (* C_HAS_CEB = "0" *) 
  (* C_HAS_CEC = "0" *) 
  (* C_HAS_CECONCAT = "0" *) 
  (* C_HAS_CED = "0" *) 
  (* C_HAS_CEM = "0" *) 
  (* C_HAS_CEP = "0" *) 
  (* C_HAS_CESEL = "0" *) 
  (* C_HAS_CONCAT = "0" *) 
  (* C_HAS_D = "0" *) 
  (* C_HAS_INDEP_CE = "0" *) 
  (* C_HAS_INDEP_SCLR = "0" *) 
  (* C_HAS_PCIN = "0" *) 
  (* C_HAS_PCOUT = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SCLRA = "0" *) 
  (* C_HAS_SCLRB = "0" *) 
  (* C_HAS_SCLRC = "0" *) 
  (* C_HAS_SCLRCONCAT = "0" *) 
  (* C_HAS_SCLRD = "0" *) 
  (* C_HAS_SCLRM = "0" *) 
  (* C_HAS_SCLRP = "0" *) 
  (* C_HAS_SCLRSEL = "0" *) 
  (* C_LATENCY = "-1" *) 
  (* C_MODEL_TYPE = "0" *) 
  (* C_OPMODES = "000000000011010100000000" *) 
  (* C_P_LSB = "0" *) 
  (* C_P_MSB = "47" *) 
  (* C_REG_CONFIG = "00000000000011100011100011000100" *) 
  (* C_SEL_WIDTH = "0" *) 
  (* C_TEST_CORE = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  xbip_dsp48_macro_0_xbip_dsp48_macro_v3_0_13_viv i_synth
       (.A(A),
        .ACIN(ACIN),
        .ACOUT(ACOUT),
        .B(B),
        .BCIN(BCIN),
        .BCOUT(BCOUT),
        .C(C),
        .CARRYCASCIN(CARRYCASCIN),
        .CARRYCASCOUT(CARRYCASCOUT),
        .CARRYIN(CARRYIN),
        .CARRYOUT(CARRYOUT),
        .CE(1'b0),
        .CEA(1'b0),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEA3(1'b0),
        .CEA4(1'b0),
        .CEB(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEB3(1'b0),
        .CEB4(1'b0),
        .CEC(1'b0),
        .CEC1(1'b0),
        .CEC2(1'b0),
        .CEC3(1'b0),
        .CEC4(1'b0),
        .CEC5(1'b0),
        .CECONCAT(1'b0),
        .CECONCAT3(1'b0),
        .CECONCAT4(1'b0),
        .CECONCAT5(1'b0),
        .CED(1'b0),
        .CED1(1'b0),
        .CED2(1'b0),
        .CED3(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CESEL(1'b0),
        .CESEL1(1'b0),
        .CESEL2(1'b0),
        .CESEL3(1'b0),
        .CESEL4(1'b0),
        .CESEL5(1'b0),
        .CLK(CLK),
        .CONCAT({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .P(P),
        .PCIN(PCIN),
        .PCOUT(PCOUT),
        .SCLR(1'b0),
        .SCLRA(1'b0),
        .SCLRB(1'b0),
        .SCLRC(1'b0),
        .SCLRCONCAT(1'b0),
        .SCLRD(1'b0),
        .SCLRM(1'b0),
        .SCLRP(1'b0),
        .SCLRSEL(1'b0),
        .SEL(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2015"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
Ur4xKLLKy5HISLDPmDwujZQ76EqYDzJO3k6TLFX4jBfKx11fkA/mTy4JrwXgMFVL97cPTs3GKCuc
XFomZNzRDg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
IX+9T0A3HPJ5KaKL9csnW5NdXlmE4+sXcabIaz2AMIZgXFDVjfP2NRrz5lCxbhk3jEKcwwyRtBmP
CaQf0GDa4ByvHJRgE7pP5dea9LxhHX/WZ892VxP7yXZs1GOYngLr32x9Qu93eSd4WnKXTjq16IaQ
8ElicWOjW02Wil0IZAc=

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
SUm1OFfErQV1uRaSArVgydhNEzOk7Gx+2VqifF1+bFyLWPofYKUcSVW34y2mPoloaHEeyPRnoOuB
3FaXiqslyy2CH46m0XvsDqHJx+jM9MVyaX+JLbDUwrNZcz16JFbMwTQASKqhlsQkp9hNtufeMDN6
wfc66Kr+KRL5XfVx4bU=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
wU7LQZN0JkS8Pp3ezj/+iXIMp+v8DzNbvc1uceA+Gg5Dz7XiOANdFsovTN4RzgGspzujCHcPgkz4
4b3QUlFurmdSwRFhxSI+zhsjDyzGFFYdpYGSHeN36I5Q4OEcSdSsTHknxXcyxYwvb/7SD9SLhTp7
DQpvHiNIMYX1wWndWCE5rnq8yT9ZW9HvVzQBlxcapkTgXJJL6xGq3dyECOgS0eyM/dhmPoosihBV
zZOLKfZ1l41sM7tnylZVoSV/Dgzy4gwHNXbDCKehml9wegoq1G7nqAyIWliXJvLGm/OF9XGALB0w
I8nxEmWX5Sl/GZS3lL/1W6Skb2LpkXiwdQFWPg==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Ty+E19AD02OpR6KU/Yl69qqogG+WCbemoH4FLXXA7qI6aTqDoFUybB+MeHBLJLp4fLlgQy+zmdeY
9WwDglDk/NVfuHEzIHSOcxaukFOsH8BAVDnWi0DNNLpvkzq7UfRa0DqbFtSxqptXgqljkmRobX3p
RSzasOPzficEWLj6XGW/EqP3Rjj1QbBCm8IbCaWZZWep1oeOPYrmwbF0N00qogTDJgwt0YfTzFzC
tOkxA18oH1u9bT1cdIM/zZ6rESFA9sbm6grhap5j2ph1LFm3ZJZXBBf9LsQO3x5BAAkxQBtTwOcW
uqo21kOeoNyW6cqIwXB7b/Q/ouCIcyEwFMcKBQ==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinx_2016_05", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
B8pDbuq7116jWtgqFUwW72EAE7/wG6og+BDTZ4AZ/Lxj6DKIYIy02BJQ0eYSPom9793kMxlxu9aN
Yx4XjA2WB5SaTxBhBRCzG67ZlyM/yJqRiONkkKIDX3bv8YXjVVXkXFOyywdHxnYBB5N2P1d/xbO+
Wiz4BeNwwG7cf1ueMheoIELNkVcAB3Gp7cNNohBhzI4p5IJGiVc+Ex9RrsZe/s7nM166OIh+qKsw
9ESwbcGx/sDYCcQUE7/2Da60960U/QJxHArsusefhFxNroLuYE552tWqwROkKABbuspieKR5MYd/
VT6DixkKusgEguiEe8I65A6YQC4m9T38y4/3gQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="base64", line_length=76, bytes=256)
`pragma protect key_block
DrA9iJBnKbZ/xsOP0bCtY6lBtKWiqsUIhMbxxyDQYl+noHge6gk/71sOgzY2IzdVK7R/tyIn3gmP
zaUcpmgNdzlPe0dWItAtBv2PpfKQGY1zUSMq7VcW59iEi7uI92T1YnRunPoh4+VOMhkrjEFMQIhg
mandisJxW2eMzrodjLNG1hJ39SoUGCUn/jomJN1w3WUWMlazswDP7INoi+bGOKkXdLK5JzHIS2E3
TDGQz0QHBi0CLRbUip04tgK1xt+iES1377ce/7UxxgfXSEZoQh2MTcVnbiMKhhqSKCkgCNeh72kD
31D8IAi16ey15e9cH9SmtQSLN+PIp+qydMiGUg==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP05_001", key_method="rsa"
`pragma protect encoding = (enctype="base64", line_length=76, bytes=256)
`pragma protect key_block
wrN09Xz0F8FLWRZNpEXb1WFJ1XNZj9TmV451UOnFlgiw5rMNW+J9ssznWC8Q2exnrXOX6ViIk6kt
oKaN8Km/ej9AITsOnSzAtb3+o+Ks6iitdqD8Gdf/fynvMQKpx1UC2KRjKzmO7a1nUZUDdDLBkFlx
S0gDTS72xB7OoNchMbS7QKtOsckuWawuIq6cji5zQAoPaoQzl279cOqgJERtIj3J++TqJO1PYUac
f4u7OweQ4WeEhxheHx21m7FC5IhSh38S9HIsYnB+h49NrppAQTRh1Vfh0KuNptTALww9kUpMDxGn
Js4Ddd+++GAwVRG/zQKKx/uSEcaxdOEPcVNpeg==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 62448)
`pragma protect data_block
I5ZVivW9nV020sT+x17v3JyGPEO6CVuWBSxyMB5M6TVafE3AxnGtOunVVU3FzBcFpoybQuKPDUco
Y48NP6SP9T6leMVuG9jecRiXYzVFeHDeg6HsfKRprGE8LJ8os4xFIkUp77ktpCv1BUIF0VMAUAHV
lmXrHvtKRW64Ufo5QJEH/Cw4r2cDAYkezWT7LSfzF7zaXRq6Kx5qvCdw3HqTa6uwTTdMSwJUBSlk
afWQLzdgq1nkZKL9fLpPFmXtQdse/rd/6V5I5idwwdbZna2mSYf5eqvzvlEjzllFSuwWeh3EhNqH
78JI/p9NSWMQD/KheFEAhPNAUNqtJ2CWYD1a7NB8r49k+LM9fD/reb5c040DjR+16C6uXkz4VZ60
Zf0F7I0Ihm4+PQmMYY9qndZOh12k39/pyTnHDyje/iqRjdmtP/kTyK/OlFvuUNKpIJH9sOzCteSa
hBka/qxh9C943yK3VQlrdzEcQxKVqcEh2lvJ5KCYc4hRKKQFnnYlpmWeYKTiIWuocGKVRt27x5fj
93Tklg5SO9KMvOztPMnKB3PcHFBXc3nEQURjPHz8KYl6mTTP3JlKg9pcj4txfXi3NvSlt4FSsihD
5fZzRoA/c7fiHDzQmJkZw8AYbKXdnA4Qb6WfCvrcfpbtOFZtH0Yfx1biDUEF94k9ZBb9/HN1jOtx
JPfJm4XR5ke0XQcbfxPttEZaditv/J6KSzGNUed17nwMihM9I7VhnzHaInvyo8zVFWmsrk7i5TS0
2Dhvd4t3/14FgscYBeAFs7IG5yk/FMOnrxieED92Qx4rMqswwHGbHFEJndnPIpIq+5fpjW7a9Rjn
/A/UMNw/t5p96P9BDs52Pq4S4zuWiPYkCGCGEy+9rd/2FDA9ePPbwXlq5qX7tj3S6XeQ70RIEMt1
6TRqjH8LaleNnwGiq2vj+ButeXMgwP6s/36SL076MtMs8mLpLzsLSA1vPPYchpFcUpSnswy6odaN
J4ebsIK/upCpmXclHxxp3DdmMrK1P8Lk1qGs2M/zHoJt5qbOUCxFZBd9Z1J/s5t6lSASjrqUhVCi
LgX1qlokZM6G6I6w9Or2Ajy+Wv14OokSHqDYDL0cXM4yFwdIfWLXKF5v8FS4ksBUP1/OBQjetLiF
GdIexgLZB7evndbmuSThsEXZZxOOdkQgQ+uvUCwre+v17fN9VJ/VwIEfUXyuc8UjYbB9QG5z67I1
DwaPc4Qga+LO37uZV3KsjcTEPZ7Z/KPV7vtoviyxVa8W0za9Dx4/AaLipwixKKtkwWdmN5gMZcHX
1FkgOIUbyLNLOVuANAf2RkBKB2V1QY2FgcKC+pIXNPCyAAsibcaYBmELVsmy+MjIkdcfxSJ2vZzo
hsebQmm4q0Tm6UjJu7jOS4bP8EHg2X8ujSHoQRSgcDqePmBGZEEfa11Z4HzBGF+MuEyxffwzYH/g
w6F7VXns8zcmIbKEHxgGcOGwjF0Q2K4hTBrTtJtilYjkxqJciGqmhbc9Xujm3CfxcYwdcPq6vIAI
BbvIdrLu1AmnZxhAjYkmOV2fNFGZ8tRwnoaQtRtwgPUqCezKXd+PXc30Jr0Zgv9J31GojJkMnSYU
nANk/JQ39vc6nrTX1AtWnxN8EDb1yz5VdpBdoTrFFnSZpS81WBwlsg+DEl/+1MY6adMi0QC6LaaC
XrNvEohB6jnn8SLLpxjRYfqUKXmMBvxdIszNjFR7Ts6wKjsob3Y7PjPrNFMYicPw1HuQm/zUGykn
tFtMMmlINh4GtVilPqN5ChYXR3nEhOTchpCz/wMrPQk/0mw2r9mgHTYEa9TCQDMd2l3SGkXVjAjm
JbXu7yrC5Uo+tOyJLRskBrF7IbEcRTsORZfOz67F19UDLJQslvzAx23sOAQ34yY20aJW8NvPtQhh
mzM2Oi3LS/CIWSyb6K2I8LoPHkFPHrFoRiYlQBCn/8cYsO69tye7NLPmZqTc1pJawNuvRlyLPIlJ
OlK19OEJIYRppSTKMfJQrhWA1IlwuBuZosO4fXoNcTzdV1rwCsjdtzWUjIo8v/XTg08FHBOPjvDP
fLj64RRfEm448+UpQZU70KIrL81rtjTCYSulp83yvGaamGyIDBDNyxKzjkn2IlIeqNSv/tb2hLZh
o/j2G8EnnOO/HaU0oKmbVJ+mMJvTMKttmS3oDjn9Q6vyPgaaOziqpOo2kewXlVfvFqUeeChsYaax
jYJC3WJVkhHF85b0qKJRyRVPD+VPgn4+Z7Ij1+AVixK1Aa+4M9ncJfM5/2yjhSJZPVR08XIs2/7W
b3PPxVlPOHnwf2TBLG0/sbCpVVH5SEW0G36xtgKzC8vFuhk86m18YaP1h9/vLOvnidxsL3AZrHuh
QQyhwunbxw0tW8aOAPrHKsKlDVGCT+mSeI7G4b9AHONFJL3Ha5NYY3BEXTCfkX7L0zSHoVEDc7kU
pOvqnY3cVGDoRLqittOk3tjSWer/9Ei9viRVxQKhlqa27tuXd7leHpKm5AXiyE8wxwDJ8e0LhOsA
jXlmpb9DUKsUmgY0E/f1J7J1LVu82EgnrCrWyXWcJfZMt7AUTEd9cWA6sJ4fc6fy23qhLBz/utkz
PqegHEaBM3cB5iz8W0Pem5aiNofYsKzIdaMZZpx9lbqbInoCe3LVHezOVK7jg8C3kth2sTE/7wdR
CiWj25jKuk5YSTujyd7rhQya7jzPym9T/vAUW0nnN0zJNGUw4qqztEWZNxB3sIiOZGQVFZw5duNd
RS5NajBEw02jp9cvwMDTIpxLHDaiaeZjGdwKFIuvu1OupIuwOWPMYxW1vXjj+lISzavaMPSO1US+
vQzaiV5bzq8fWkn7x7DfGJZtgC6U+vG1Pie9zOKC/wfd4llIbgNlKKUbpepj5uu7SZ1KPP7ODrIs
Ldtwpn9L5C1oODVvbjeY3KEIiOYGcrAg+7jpz0R0ByJ/rpagg3HscdY/ki7wX6zANHedsdXj8HrR
f4PTEx5OxYbn16wZd3hVslOW4sdt8cvcSuaLCDuhbHMzbIayZ1+cJjQeXeekq6MRRFqIoLUdEQA4
OplE9QZBP49c1TU2kBfI9DyWg1rW4P+BrqB2bajd2ce25rS/ohgPC8/OawOULHLjvrOU1oeluT5F
HYawJjtO2aQLzyC1LqtAUFUoQ9o+G+BRDG0RAQxKZZaaLK2z05nDSl3MVnESN0WKj5gHX7d7Qhrh
ya2oEMEE7B2VK8Q3VNBIGldXa8eQoUE/pWNMWDSdKKup4O0cho3ENOrCPoMWfhoWlEpx9IBI+Pgq
Xm0PrTD3UGIIBbnlUluZkTBjENDDSZyEjC/zAOaLjwiWOrS1f+sTMH2gvGovZGIBX93jy3QCaTiV
eIcaV2Rt8zkHrvm02i/Z3Jl/Rk3DN3on2yD98jpoY1xXHHnMRe59PjVSa+vbHLqg5QwzLkQxHem2
K+qN2FRvV4MDmu1sEQ2JtXqRBvACkZZXs2ZhwadwAboVioaZ/Oxo9g0A6XSE6ljsxSA3mJ2Z63dw
j8Rz+GIWUmkxt+2EF8iri0+1848TtE9OfsoTaosR48u6ijF1Geu3YwZuUjny15gj0NY0KzvUjwEg
rV+CSQ7w8cyT8CkkSJ+IldVjy1LmkAS5oBlCpNo5fgOpH7wNKI/qoYWMpqHaMVmIoyyED5dFv1Kt
egnr42WJFMghAIGqEUGmFv/twFQ94W9mJwnRwVWigbv1zflegzRCZVHhxOoqnC3kUvZkPXbOw/H7
s3Cvtkk1GjJ8z+FQy8NqTmTnl20u4C8bVT/SZS/EmAnk573rZIPgJr7Xp1c2xdVcsuChjc3bhCrP
OzgQ9zgifrQio3WpGzZMO0aNu+19mNxSDCir+lr5h2iaOtRp4LTkjAmZiEpFRveWm2J3llmWN6D0
0tbY6f0bM8akoYYPEeSnlzsUB0QLRClt0GXwogZ190HnFoDjnbhzU8LDI3bfZJKNVGS9nHzn6+F4
UQzU8q+wVDxCWn3AwHnkYyFSxzNnCjnYc27AdIkc4MajjkjY5+faE+bJNBC6bK51tbWmBt70ARxn
gAAbAgHz2CAnC3zswwu4qolBPXbX8l1Pg/X8nBpq3IYlP46e1wx8GzZyR0L2ZNjpLn3lydNV1fjS
HdF6yHWC5vhVfabGTcBDfov+t53Vwx3BKqma69WuaSoJ9gBbHxv+/UIwYVKjX0VcFnYmmQt0r3Ss
LcE49EEihgXylxyfM35L9LbK2HSSYV4f9VNmXVGNRGD3TJKchYPi59SWMACgWLnlW7jm5/48K1Qs
O8j8yXw7jk+3nUiFLwPTaOU5T/Mv4jKCodFjqcjxMeTyFo+hCJly2LkE7T7tyv3kuWzhpc+Cv1bQ
954Cu6fojOIekOowNwvMsBKIHgnJEg6q2CKIM7A+bmCHThL9O+8zYdRwfcp29S8k+lr7qUgHuLtZ
XYXACchevOl50r0D/x6wL+fF6G6sdwOPV36rrPVA7nKbIz8AmyKsEMk/7hDAUhGt44bUyryRwMN0
eLgkjI6i0uMx6HM9eB5XhQfB4UG1DrZSwetFCCwgPrnnfgtX5bq8QWQ4y81a7z2T8T6/gIn92v2k
Ab4UrDj9Uyw7q3NU+C6xIcPY0XPttXoI0xgl7trn1HWVjPouarwIVZXNAVexvd7W0GZdYkoxBTn/
BmMzBk8ltB48oZptjm2tpsH/9RJbZ1VXqdXRPS2+mlunABHTwjyXcDqAjTYuVc4b8KlMHluZ/SgB
uz+TcKoe59ByOrAX8sEosLKzUvKoElj5+1kv1Q267h3gbFS25vhCix3N5QpMbwEgcYi2Qsr0iwtM
hYHKPWqGfIYoLcRqYW1a2y4Oh5crNTofeucHkxUFSctDfMu4cU8C7WZY2JFJnnlUUnFK7hHGvH8y
YdCGr0wYbQYzM+z1YDRbti2XM81fskU3EfIvGS8FOUi99Dw8cYJRV0JOJO2wkUITz0rbTfqwRAFJ
H0gDUkvS28oOdKkz9xyrMOgjn8CwJ669Na3h23tkto7/kmP32hR7A7x3ucfZV8MydkT9CpmuuIyi
L/M6lYGjD3kemjGcgZOGHZXRJnw3FZcH6RKfMQlvkaD0xc6reX7ab+idy5RVsp1y3cG3hkVwbgIU
UHHNsQ+Uk6FJj9nVceTn/4uJUsQSNhLPUW+L4gEN6HoAH2g/ylpoeOxve5U/KiBlaazlSWL/SKhS
vT3Ika0r72sJRTOI5VqghE6bRmjGD9lTREql8FPYjgitd+0FH48wKrWEToIg/x1RxdBF2Ncc/zrJ
nQwWnjVYx8BD+ggQSdfhJxuMSOJ6gmZToiT1KoyWty2qL2aMEyN8InerVuDyiBLCOyt59BVg8n/K
DHo3YSqgO5P/zI2JWudFzvvvEMvl2oxm66lTneKJQqOiDNyBMDDkWmsgzQDxbJQiuXH/HZhd6tKd
baWMw17V8+w/dVTDU9C84QuW2m+BsO9tVTwQBq2a+BotmryUw1s3+BaoqW6JwAh6XRbecNGE4Nbp
bVEbrJTdi0kZiJtm64V5PNt7AHNexN1IxA5Ji0taRw5sLZlno2+PC0y9zMrfIflfmwz9jQKgK/jz
uL3t1+SK2HVl+3pynji8oKZrsSPDdDFpFQiwjSaFRBfDSl9eqe5rV+RSWXxAWmP/EpJrbHHcxvVW
+oqOiNsRWXAygtC+7Oh3PIcAcLaDMDihbSnhz3O4XIzQ9yn6Hxw+Hj43/dVzDoVMePuad39CbJNc
1iSRE8TucphN3fTqdab5URdBaaR5XFzQSsX4brOsBLNT039WdFAzmqmQtuXYoW1hJpDPWF9JuuOy
kZtiz1xjd2yUnYTrsk0zl5GUE549zDqi7SHIHzG2cnFsHKbj9IbPTO607AGaBOo6jc49GBsGagbr
yDYqo+0uABoCkmCoVhaJSzxS+qVxVCFMhAuY4c4MJSDYkp316h41DYM6HJBHyUCGJr1gww7pcrMX
vbLJHnbKYjfwwGmvxV3CXlM7KWXoHGqjG8VpOwumUIYeLaQqOdkT+rNMNynCkU6NkXAQ97UtfuWE
IFf+4Hot+r7Pu21xdC0tGLPvZn4uTJiE7GVH7VJb2z006KilE5PrVgJLkNvZ05lb2pt0sRad8Rrl
lVQyh459RH18XKbPMSD1/CXoi6eQCyX1tBI0Yh87fgwbRglQlZCQ+6UltnHNQQ2JNCn1hHuuqmbS
NbdUlT1fr6BMiT7TvcPntevxGDSqJeLXhhtVdCA0s06fuUyo14ReXIg4H4c3+HPRnLmIbOY0RhMd
KmT9+LRH/i1C1hsYqwbBN+rMp3VTiJNlmmPwRrmGg6SpH05maDZ/pUqbEFPbMreVwgqhthtyxBev
ZUr+fRwEsHhNjOdEc6Jqu5jeNNis3T1hZ/YZ74Xge1vU0171nYzjruekDuG16nVbA8rNnK7OhJch
4TafZqiZs7+k4HiVHbtHXYT7oKxgJG07rqgtyP/CV7re0qGtUYNlg/F7aql1NeNDVcj49tk6ODCq
N3ptX8Z1tcF+eC2wI2ldeMyiNtqQcyJcm+jpsXrEwpp6KTadO5irZbZ7GBFw+/xTtgvN36RyZ0A5
EGzteP53ibNvpEFlMAUnvWYsTXhxmNH/8gtV91qm0Uc1AIYW5hCrPQ+xbuKJZz38wTy3KcWgWNcK
EEZWHSa8lsmCOJRrnPnb6aHaRx7/aB46xCIMeipp0icsNbJL/8ifyCDPoZOqi89RG2YZxbbsb3Mm
XQIZqdKxca5nT0Uma9fl5+tB/FZuyVIsVrlmtE7z9Aif6EOoHrvZ/OyTi19KWcDGmBqTsWfWTrBb
TIlOz8K2x7PiWYFquaK2TSPqANc4xJfPnD3xon5WqjOpFhuT83mmrExBU+cHmuGJUkoveB87hlUv
QUfJhEOxMiPDAWAzHsx25jyBhviobWB1qn2c92B9VS9JPTUEmAKGYK7CftBWKRKD+w9b64BvSDxH
IYt/4pfJ0jm17TMaKodm2lZzCasACXu7uV0tvVtJFEQFNrbS4OdtUeTjfbQs+u06RaTKcQ+MwXbf
sv28j7aC9LfxLuNyX96S0P3ZtzF2miuw3qNr+TJS4Jm7CEiYwgAjolvtig1qukGy3O0+3rOhkIvx
WnavmLE+4eqcClvz5/+bXpVt1+Ta1g1vYDoAOrCKIqOYyQ8FlMOphF5Y2geWfb+k9JlgDqgqJb3y
YrqOaN0xMDo7xH+a5n1kurD57U6a6JQ6UgkofOzav1x6UiL+bUeDaiA5L1IsOJxKQDTftpnJo0GQ
YrTMrSRz7e86+Wszl4jQkBklpqWNreplGZf0s6bKGrD5eniumHOlBquHZ7iSHN9OCtfW2rRy3pfs
kX/MNPx0LHr/TjoOTTrnzbigSA6/fcZ5hePHNwNk69v1tvI1/pUVX9KJ83S2LTO6WqIMWt8/Yvgj
p09KpZZmFxQgTaVVXR0A0fGVXzxBD+e1qtitDZw2HudPn6ndTHyFdR/rQxf8goRqaLQqBtuHz9C0
eFKYNQ5TY+RdjtKl+UtWRTJ6DPOV4crjcZuzmXih6ILYh0TKH1W8hxOS+XpiOY1XWDR4lzaCYh5c
aSifSkd+YTP6rnbhzVGPJ7c/RxrWc02JZet+yM1C6sjX42W5dla+fVJR4tVkPVBS1+SQ2LYz7+0D
u5sXYqnCY9VqOBf9LQnnhjVp+rcuDwPMedDYOXBbn65rhIPiBikvSt7krJcpAekVjAIq4BvSZJlg
CzfOB5jcx4PkmlUnS8yO+uxqDQAoCaeZbD/1eeJi2AzT22hLgevtzTd+8U1lhTsu9GuBLwuOfUji
8hMP0vdTZb5ZwpBrOC98Kxka1LZneyHkpjNVA+trmv/bueUsB8cP1xt/lW/o0vSJkj61gUvc/bEz
WJtXowAsui3HNmk3jRRqaCbzWr9ofjW5OsGg9lfY+vzsatwA1dJEYnzxgnnRbQonc8dzUn+Psrpl
+wi7mD2K9WySkR1XHiVi5JBi7nNUIGv6mFsWq3Q6NmpQnQtODuLprpBSlw+tyuA4pogxA18RyabE
fdhUZ0vahDwqSag7pa8LqbbqS69PVKL9RzHOH2ZiBT1Qm0FEChwBu3QCR4OS62vpE4AD1GaRKM0S
xEKU2thDlmEPzeD6zvIZ0AcW/mLq18uZMy1si7eRorJVMPXhPMTy9HhB0kdJV8L+erHX1FPvyyM4
INFROjfuqqj0nxGUcepOmTsWHyI/KrY4Tty0N0bYDeIQWbPO1aOLTxWOrmR8pZ5z2qaMtD4HelU1
ulj0oAWahpv8ubC7yDi92aVv9MlMa8yEL6ZVh1mT9u1fblwrBJ+Z8lGJ+Faqmak6lK4Nl+2BPCqK
hsL8fUgScO0OYYsO6ndXSe6uwe7lxGZsqYFPzE7+av6UiqTJI7aE11Ap+0qAJwhQGJl83x5MDFWb
A2cRQA7xXM94BNr//T8ze+IR7C8GksL9gySSns2HW6Qr+E/Pdk4sgQsQemoRr7wfvPiF6/neNeXv
d4MxnCPTTG9katMWDxmHiAQG06cmA/lDKAZUMJiBBs63dQuzH9YvvQy0ycnlh14Z5efAQ1zukBg1
tAz3uRPoF/yfXSP0OPboj7FbMFAD3AVtjbgc4Vdt63+qCkkcgaaBaHHQ1sncYCyD0AFh5gDwwHq3
BHBXNJgRWWfz9YkCeG6T95deKpdHq/VG4szr/2gZ53ESHcXsGuXZngDdGOz6vXRluDZ8rBm8ZCmi
O/nYnA/ctxs8zavrNnXGhWQF7ORfaYt0/00d0R4IYsK00/W+w4vDaD4p21QF+dq5AOu8Qml6bgxu
6rVy341ag4wUnaC0W/gQIgB4hrwG3sGedT7icpikwupdHiBQcFkyE8b1jpZ1md+JjuLapze2pPAJ
EKbK11di9qNE1zBIDzNv7WT7vhlYjxuGl4jqsUwW3eGX+FGE/uC/QpGHitikKaPLEQtQDRPXKoBy
RAVM8aGS6qOUU+FZlVGBzOXr3U9ThMBrrsDCVWqVVIruGW9rG8KINQhZnudAz8cXRKth1F5Zw76W
DRJ7HZ6hvOUolUtCcQkKg1x6DfdcyrS95ppa/KuhgV2vLa8/dbOT9/ZsbrXxRO2C/phuDVP30ELM
vSQdt1SLvuwCGZd83/12ipdz5F8/mAosQH4cUTMcGnDmbk0Mp9FH1bA3Zac03Ln2o/G2BLlf0Yye
j+55343TMi6exK/aZwJb1ykSryexfpFldP7U6t9/Tvf8bh+oOcMcNa3VlRfq0n+VKwGAwJFBWVSB
KsAPQ4jFe0W1yVunhWho0V+NW+ZWN0fUhTJIV/hUeVn1yiiExsDSKU0L6mRiV+d0WqOvhDKtZr+A
lPrLO37wJDk0YaYAIxAf3/eiECuMHGWkoBbbMFTVEl0ubkz2ARlOv38Gw3OjtKfR9d4AvCNrXObj
/EOKWL8/7g9/4/dfXOxs05ly0btkeE/DqtPN9S49qRIcrrDj3pgEOxz3IDBsPfzqkLH6tWQQ/J+R
GjAU46Xbu7Zwb41I0YRoBI3sKpgZO3SExrTDOH6rnRDtFb4xeQqSJVpy94YpjwkHn26jInvi66Np
XHqSYoDlKkzuleJ0iAzYU90uoQIUdHelRSaysK7YbyDxZK1+FQB0fE7HuzZj8XbH1waBUatowkUV
SfsQFCD08bAZZ/FiWpkUxnSB+iIRnvbsMt3fjgW626iI9hCfE11wL61/7rbeTwQVt2h90vKd3jM5
klhSW4ITcqr/4oieNg5kDnqcFIAjB1SHhiNP/91vk6C5dvQA+6+5kOI7SKeqLN8TAknD988qRvcN
Pg57BDuV0Sp32JWNMeDWDsQL/7cHlR2KOQOzGhC+odVYuIFlG26OghFMonjhWyU9dhWG9JvmrQhE
mBijB7Vf6W/z1s+OgcxSv4DZRKjtlYzo4NmwUey5lul3e5xjDi4b0weknEI43mBY756kxXfWXY6s
LGl8Ixcavalemtg7/KeykvBA3FzH96sOuzQcPoy00/cjepXlaEjxmngIC9B+c5d2IAZO5aQiOXsI
MUkEOq5JetIgq0Te1kb3v2vEei4LrRaF+TxpQZwcMYhngc0SSw9ZZeWMV7ew54mGUb6DunAOq3t4
Ldp7521LIFe82ZlR7AQhnM7onlPm5K1objAyNxcR0wuYjJGaZHPDc4vGqRHMPlnBckt7Nc7YNBrX
kDawroJBr/Wo/c2pKxGyOwXx6LwKWuK+1zcMey5a/9xzGfIwtkijOpAaWLkfNf5FwMt3VJr4ya0K
AQ/5MYHqwUstfJV0p6AWUc2+1a/Rma18GBRPvnFTl1zCSp9dnxgug4DxuB4xO9FQQzf9w1je5R/6
xm4R/bABPSP4FB47zfhHAzxGLFK/v0z3H+ih8JFm+VCKOEpnfsgjs80qdla+KiX0yh5ZxfCk2UFp
y67EqKpgzEelpzW6EsDneiACxDAut6Mm3vTKlHavTRKe1//7KKfQ3wTy6yO39jJDWdoTwUDsiA5D
MjrpOrL23NDt9C7OLkZZU1X3vAsP9jmvb7aS6x7mkJO6OtIi6Dz/uKz4GkgQ9bx1qNIESOiG6skS
ET1MEIOR3U+BtXY6TfOCDDXSmMCFBJTfkMZLSrLzI1mB6fVZUjgFSB25n/bLrQGi/9ydpzF6WGMT
ORwwzn8TAfhkiTUaVjVSRjy2VvMkuAkfwkKy6A0mtd+KF70+fGNrUuN207923b/Fx/zre3Yu1EW1
FRxePbMV7rZKrZzcMJZpFeVd8XL1GnYQS8UvB6mSPGDwi5pwSnZV7NBRCqZaurx2IFfgJaNYnEKJ
SwGnEe9VPvUtun0tYJXGHhNSoGTIx5O20EH7m5sHDkanIhtTuF2403cqXLXpDp2RCod0B1R8Jt1K
POU/w/tlDaKFPup1rTnSKn7H5MMFNUOfin0tJg6IO6sqObrRqrlPiptrkVsQ6UbjlVaS06QR/Nzm
3Sf7PoEuQ5kPTnLi4mZ7EbEHAlkqRWz42O6Xc2eVKG7FCi9qnDrRzjA77yWbv8ZAQi2jncTQzZIU
vAcYH0Oo8jpjpOxOWtmK7KLMnViAFXniX2xMa3YZ62w5OYJapVmtMPG4J0r2YQVdiDPvw1iVCoAE
e2IJfsDUwz02TITj8bRBOfISbyRU0JvXqN3atA0eS1vPMRpXvc6v++EdmrVb0KJTw8HT08/0j+QA
WR14syjOQSk1P/x3vCj+4H8eb+2Gbcb5NsKsdTsyYq86DydoaNgiFo33h/kYnPqedhaHgTXs6NIP
/sKmopRZ8cDOIHZ4NuEVPdf8atEu7gEl9YCE7AkBTdXuibBH8NClw2yEPrKuxx35MdpMVOSxufnS
g3QTVoBbC3T420YNpoQf0ouAN566L3O8/grkIqs1hEO6vo59CSsBRcDEp70TaTxqjU0OsN9VZ2II
OLw+EhJbC6qPEsszK24IuGnZOxYNWniHYDGXAtHUPoRButD4uksnoSoevqV7hP1iR/KAT+KBVzdp
trWcctrk89DmXiymv1oVXxIjnI99qPPbFZJ0HtLdN6R9yplgKwZ+0S0bmtUbaiekLk8Odwu6QcnM
+59P2YNbYryJcrRJDviNpHhGZEaPiy6LbGknDjj1c7Fcs729UbJTg3cGNhMVERlzTCf5jXdxwaUk
KXpq+U1Fsi2U2jF5yj0ons2SHRxk5kTrFX5ZeoLLSZd8gcmMOXWDO5rWh9XDqYRlg9eMy/ngFphA
SvrqJNJV2DXeHy4xEH5IFGjSyjCdkBOtm7fy0ZhsEJaUGJRFGUDGylqyuX6/q7Ff3rDXgPsD+YOZ
WOShPPbLC9leLzD6SmecTf6L37pwQ5BesvMP+0jUduQT9g1+aZdPKY3S2mE8HAteUq91oMX+T5PR
xd0KwIMu/Wm7EQ25FoI5W4vL58TtsP3nv78bxX3v8xUXnP93BxYdKYDedPbGRQM6UtqvztwjScFt
0K6gK8HFxql+RMCMzQmLbho59Iend6CVggj7FwvrGDFl6JOCoXKdwjYfJqqbRIRMgie6gH0qRJ8w
2GismPrDlmzm6y5E7M8f08jjZmfPBHfD8gMjywjl0lwEkmCTylcrBfuol1IUOqCyPo97Q5B9OGGk
9/YpTiIgb2bBzptHk/IU3glrtG1Noi9ugWUZR6hBF9ktyE0SNYnLBKgEoa/512gVDiRfcLI6yKSa
rlmLGwYyNZwfm/X8SKuNe4paEjqCRr1an+Nd7rWzSBmatweETQoDgkNMVegWlks7Y4wvRo0ZA5Ia
g3TRenL95vu3SB3HwMiV3FCKEC90UMO4s8J50mCaHXQuYhtcRFxySOzf8BnGw7KtQvkbTout1FWt
xmCwxppZHNKzBVoHKQo5ApkUcWRMSZPmb4Vn7PT0cNwj2t1QVxyqUdDYXiWpgT3rPbi69u8ytEca
/FgcwooH5G6mdpv5sq7lmHxk9CVzrQgHycM2RHOHNXwXBjGx+/6WMbk3jREyNpnF88EmqVuMV0Ol
HfOK1+E+sTHtZ8YPrCjLhjKwJA+nMPQKenWhTDXpxIJ7vPhxV8gKT/rKzCkzWMRTRjDm0Dc//+cy
yC+f+tx/dMrT+5amBqaKZrXzZpfm5U7BmoT92t0M85m9HGYFNq4j7UMvi2TEZevv3MSK8CSzBNAA
BWUg9qNLFunL7MUtHDlajwht+0MCAd2Q2lcGkSDycjrH7+ucbwNQEcH0Ms0ReKLORwaHqEU3Le0W
S2YHcAOxkXI3xj6jmM/L5qYj8YUkQa+HPlLG0aLUB1tcQV3LLk5ewuzZ+YZy2qczSzgXM2oWPtmt
heqx/EKAVFWIbvycEO8MJL565TlAlCt6WsBYF2V4ZHIvpjiwAa2BC0e9mvZaX4tcgZ+Sa3Xug66L
ATKJU3GbmzK/CUjnoT2+044DNNO9+NRMFLhPvuq7cLeP/YKmOxIVJx+dPA7Mhj1K+Ywogyqg+jqy
ZcBqbfqBvHH4H7fcy+bFkdoctpXxgrvpQDeyf1+JhgzPD8E7Q41wXQo0iGYHiRbBfHYCVQWwNvCP
Qd9Bczn3RQy+TpQ+r5xnXgG3Nl0uQD9yZyrGPmlsWK6Qo4IZLxWUpZLEpDSHtEUIcQImDJuKLEsZ
rBlZsjTBUzOzlz6ufVtR8PBFDnhIc0n2gc5NGczgnkciUwdfXd3wDhhkAW/nvgXotFE/Ycgbwsxn
Q29xEjIiW4y09W7sEshWDsY5CAcomWlig4jLRYNRW41TOPkS3rbhwu024yQ30J4VnMV9O4lLLula
PVZz2/xXVjRFkxFVQXYPtmc4aPFksD0w72MKVxkIafo5EtW11s3cl2nKdOH0flMUlAr+HOnbgWTm
NUHDcn5DYiCmhPXyZfSTWAOV4CQOc7NOE/qR/WJAS8jx3Q5al8zYzmn/HE/ui7U4yxlQjDCNGjt0
RhmLZ7H9aC59pEHmudksa9VQV9AVB9lkZKdWXvUV42+wvikOsGSp2pHPJzP8QwobtUX/2f56a9Iv
2e66cxMjVgVdxeS9p//O7/bK1lUEzh8fg1UWkpu1QDAViJDqsvNcGcgp0Soj45m+lDs4/7BgxGv2
sx9YlG1du2ftK6uMQe3CNyz6GtOd9qVCTutgAaL4s0h+bmSMqm3wjQ9P3jRamdT7asLJucZniSV4
bEr1IdChgiD44whrXO7nS3DGSiVowBr+OxMDw8aYx/IOMxUvDAMBxiIchGo2XMFb9stv2oRBKB4p
inDwJF06foUMlMmoB2sYh8SOHAEfpN8K82wjFSfIrvsNaoRdDLUQsH9R6+LOdIra5x7vQbzm7QK4
MgAPlznqszgL4e0hPStKbhOOVsisXmMtpoVSp6ykzOY1jJoFzS08oxV+Svd94Z74pWtPJ5OfJNeg
MM4/hjSp991+aH3NWrXZG/Cx3tufT2vEp7ySa+qpAvrj0u/4oPhuSlkLU7Gvze4Qo0ta0PVOjJgg
G5I8Hh53cRd4COMU0+f8KDGeoRUONbGnGoC+KIzauFUZ+2FOVDu7nN7E9gzIWXaespJxkM/QHwTr
fXs6RU9+rOoSVsCJJzGfJYR/2UmGRWqn3JnsKKevrLLug5JGsCXA3MyvyH6BvWN/uG1x9hB7ZkUB
lsGWe07S1pELNMkPOUealKFMZMWRJyzphubkTbSOzqoDuKHiZJUkfWbGGCMQsaYHn2DF6gowZM5c
C/WmL31jOZpo/u6jUYaxFtOrfqvqJ/+HOCJ1SAEA0QVVMbFtXKnebIiO23SDLYum1eEeOlvDBdM/
dpK706HhtAVML3NYvJO6zJh3Ei249vR46M7ZNajMR9SMFKIKvPmECFljMDMtYuWQsLaBRmv6YV2+
GctilrR3pjj4PZwv4ALLb8h9YVT4PrSqmHhrspy9OpKp7YJPO7aEJ9EVaswfUc+gXmzk0q2D+dis
iBU1WzQCQ18cbG1Sf1WJktU0yucGsCSGP++IfTeQIwT2HDBfTtaWohj44Cb8twXgJjLkEvLTs/9k
c8Wqwd9en6y0nyQtWtGX6xzR+wLcaU/zwJvnih1JCCWKail5miqbJQExKTJFh5zu1Z7aJLQrPfGj
dIckPokPWBrJUaN2M1xBdC7D16GpHIBGwlYqqjh/phUMn45aPkWXtXDlKUhvsA1CSILAPO4Lm8XE
LbthihR5TnsTv6IBPSfVm+6B99kqqroSA2KP5oh1e/2GPU6fT0aN0/1e+HnUqjnsOskWd/VRI0YV
r/HwTtHOHrlL9I+GlVQSjAkWuECrZp9ttn9IL6biQNungq3o0obzPZ9FXoWyh+87RFTvkWJWGAQ+
SYsdDQTzu22Sg+ypWhw7uBi1NZtX4WDe3YfQP3OU2nIy9M78OT6eSqykH556yxa1FygQq+yUAc/C
tk3kdJDBwUSuGJteDa4P8WFVosgbj7DllsmwtgPBXzCD6qbj9QgCNiLTA+Q+gz5YMPJcX+8kWgHj
ldjmvtbE++Uu59BWAYQW8j/qesfQRhToFlK7nLh5pVnneSf9TdhCstmiRMrq+HmmN43UDVBsmZnf
CFBXVxpRXK3UnTwj3ymOx0VZ/+VUPAdojAAebSn4bGQG7j2dlddNJ/+KZKABbYQ0FbTmMQzivp56
GDe+qZheSIrcECjaAeiIzUL/vURke6Jlq08Q650DVN8F4x6aF0K81DONVt3V4Jw/HumB2SSmzhCX
w4UGvTHbXUECoHtvLxbAavIKAYmUlfl1mWyl7AHcA9F+ineeoasmdxVeGFtGWntfnZ63f6i92cFJ
wVDtb4Ivp0ar7XqKbkpolaIi/QLlZtkEXjuIy+P0dSKX9dAwga6Jkma9RGe5G+8pu3Ld1c1X3WhN
XKPissAMT6+v0ACsFaWQ/my/r27FrfSAUAxFcuoyprezhiWtnG1lcN+Q/Jwp9xdQu+ytzc0UxngD
5xLHs0F631GmMr0KFtRyXcEspPr5z+ayTHTkWSN79obHGKPvD1eNYrSIKfPdIIRs7P9/Xp5cV61N
g3sOjxldjN0NZOUyjyTI0xcfld8cQypy7foDb/IacCWwAcPdvWOX+GN8Yd3YswXCfYxry2AJIh5l
uqkWkif0W7QmXwEiQEsF75i6TGiUq0YfOS7wdkLwQ4sSGRKCkjYH+Mq4tIQuoxTTTjg9jyC/IccC
WP2VWBGraLujJShFcSS4g7GrZfLj9JVeO6dvk5SU0HMx53BlSAcEf7pL07prVI5ed50/fJkmzw/T
8bQwcyqJqgYme47mmYkPULaW49XJIdCdAuirAcfQP38JDd9JflHsCapdZaDqWK58Q3Vs5Eljqx7r
hKAdmuBzH3m7BRDbM8vQ6njj4rKKPE1zhqtt3/NK0p0uL6XvYi7TUqC9MTNeZuBQKt0W/eufex2a
mMSRa05c9wsygA1CwcR2eRDtTOyz0ik8TOevJ0cRef/342+JZmL4AFn8RUQRFokE6V2JoABNELgR
C6p2q2UqnNZmpbAVqRNSntkxNWKT84PtBqptqUc3yIrFCnBY6iBUOAiM9FJVX8hCIw4a5XF2xp//
p54yhjpgoxNGkVY9X5NOdK4AaBbfXBMDPIKmXZWOJELSRmLgJrMSWoARoa+gmWiTarU9vOdSWzMG
zvty7RHv+yN5LknSR39TUObSiOk/aPA4Nb723HzoRvO+xOIrNznrL6fETkWsKlBEyehsZr3hUy66
ayrrWCqQFcpun5d5sC8N8TFHQitb4QhFiEMouRB13c0bljxAXPqxEQ4ctTIJMztOwYD+59te6rRz
tvSSDsWgrEakPcX7mdz/YV3FczID5ZTz6smxxg5WQVfwTqEA3yTvHV5dSavXOY84yFXcOdSz1oXd
Jymp4ynygXocCQKC9IEyGXyoNpxkVZfg2tfWvJnC0Sl43G1EnW/H4g8n3LtSgs662iuRmH3djmJf
IAmc0gjAksph2AuL5+ZWY7ImXQiUgKyKY2qH36L+GondVUptumlhjX90ik6EScx8Xare8QCzw3Q9
mQ47mgqVzUniFEhyy8xmTU8rlEFnYW3gytJjvLfaPJC/HTk8fyn6G50iaEl11UXY+qeRFEBWBW6B
xaXgy1OuDcZMrVRsN42Uif6EQm8lrfrb83UZH5fvOfIt82fx9JcDCJaEIij3Z/pw+5iMJ35NgWEG
Zh46Co46CbUOKcGe7aR7y6U4V64X7FX920r5KDqP9CJLkAu8EAesBI3VEp5WUPmUaVr46uTOJV11
T+TIgAvRjx/d8D+mzkzwb0WWFQWQ4Qcr/RLeK3llM2OiPDHxn3nprJ8NXRBqAC00S3DDnEWSuPtW
khy6BjovbVigVlpjJRYNuk4qbNC+4XVRuMQj2nsxfvBagQTXOT3iqjfr1GR8Ti6pRwBALzM5LjhW
hxmFWdeUM4vB/EQTlLx7NcKONgXbhvsOt3NzLV2glbtvuB7FbjOYTDiBuK+gHVFju7leybqZ4JZ4
7tgnyL62LhHnw0OqjhQU+yYjU8Xsv2KvmKyTkWztcgoZ56W8380YMqYXrNF/ybi/ALDsgkDtcp2r
5jONScCg7o5R8pMJRkm8TWfx2ZhhaoW6SAMf5NSYzy/62AmXuGVl2lCFhaefJHIIX/WPvgu4cVGv
9Kf/16VPmxyBTL+mBIiKaekY1y8dfRcFo7QJWPBgSsFzU+l3EPJ8hGt4+Zmq0/ovBd5tH27c9aiy
umwM+n5+rfDXBdDTyytaKkcP4d7zZO2X/5ineqYHCju3ZB9mgTuthXfpADn1dl3EaRMLgOMKlunP
YJwUtDyge2QuHWt3YXJnonFRg3UBwJQUFCVmqYWhA9QcLPhRA6vWeMuAR5M8oKAce9ctx977XKxc
1yc0Sf/dnzSDcFB/S8ULh4nRh/htpSgK/KIOUldsDcYKoSnOBcP9rqnyn0pwVwLSzRpsoJufmhns
nSPqFBCGAdiw4yPFLWki6atYLlcKJPwS7nxAnTgCKVRMxh3r8f8qIaUaBjjAlklbOyy4Za2aFT5c
MhGf4aDlTML7BoJmb6UVHIvcWDrDPY+XOogdf+ngphBVjobwrE51ioJgJOvRMJUiEoCm2bC56iMZ
1+aTApH7RcRcuRwOiXsOUjLUJTvcIPFYEON/2N6QYQP7xwj81giw0eCwL77jwiYqNSkasDyh+GZh
lA1F51f8O+xOGSdV+JKHg0zxaTbkfk5/H/lJqsDbmLmXg9qjfpZe6KYv7bC9EeIYM7fMKKHoFSc8
LtqFY0ohnvzl5cF37Iq/mBgXSuwx9ZnOYXA4Vk89OvhrNTKv28/O1gW6vfU8Zwaf9qAW6+DgdlGa
COg3e5RSA5vjkXitRxF3pquPS+y9FIi+G507vKD08J4VmuknTY0zjXKH8vrMlvJsZ5OgXGcaLNwi
zCF5eakhgAQqytWMR0XozaKA3WDdn9sKa0ZFHgiLBiDEdG7qpGVYYbMOLvYAL4WB/WDRK4GBut95
WsfQ09XIYOgbLorCLtTq/V62N24VXF/zbX5LE500zRxeosKA8zeMyFbB3wSPiOVf+myw9afP2kfU
U0+K0rW/lHRSN9/30cAK4j2qnvJr3WMG4H9fkNpwQXf88B7SJqMmjwSM/yyrqByOeSI8AzeeZyEm
5EN+kx0gC5OH9WNEwJfaXbxYD3pl3etnb06bN82BHGkOvBidfG4xJoI6uNjGq9gF7mFMT6mGQrRM
BmX7TUG8CTvbJHrPdIU0y/HGZekyfedyPF13PvZtcQX0W6/t0SyBpXXdW5dO3l1PgIevqU3t0yGb
94xgEIdy+KeEVQzjuQkjWX1pOaZpb3i9gFBIXnTnF62Azhp0qu/1DC9p0hjMUe3Ij1kEmHW+WYsy
ZIQudiuQvGDfEaSRrOlMWQtU9YKMS2zUmaHVpGKCzCzTWIB5iXIgFz5t28s0t+5wLPg/VfFFZbZS
+yGC/FH9aXNdujCJgWm/dJy8nQaT2msABOGl8Kzp0x2pLxEPcIuyJ5v7gOMbN2MDwiM1Wx7UAVDw
Lrf/1N27sQ8IWgOUm6BA9v5DVCw92uRgEAAWUMBjoZ3PGVdsdmJtQG1Kds1Kt8Ma0uKQd1KY4x90
9TQRMjEEd/6eEHfTABtC6v8M3nwOTQVBsDsslFOFJlKABDhpOX7QrJA5zLzSMWLzxeLF4tyh3sQX
6Q4QDKm8HeYzARuixw/nSLFtSaVSLgIC4pZyckDPgq7Y1vqkasUnICYUXOtWhHFH4RX99VdBDBJE
dd9vC2ProVUEotiZavOI5F8qv++L8EzwXRmNhziRzqo3OjC+Ov9MXYes6N2t1iiFWxXE6hpaOb4K
1kE2d4jWYgtpcceBZQcPq+kWvGCVoKSYs7U9FLuoWoZEY1NjrGBs803GTp8RCUykVK9wlitjuBk3
v8d4GTEYFC7mmrLBJNfbbvRkxJr7XN3tn6KfEBCuIYrYIIx0CsIyVl/G7jDzVZ1GR9BZkAycqtV4
Quhvqau5ASx+LrtWy7zJ6NEL1Vzo6W8O4Lx7gIb9o90+6tjVoLP1/FmxuVs7RzMWRFKpkRAmDTBl
SSkdo00/cl+f79JnfFdcHPFFeoh41RLAgDxGaqtAD9pnZ+XhRVL3GqBEssNMehzGSLaxApBSFQKA
Y0XFKNfbE6p4h5g4+NrNrSDphSOY89tQ/WkmvruFVIFmgAhGMm+njtFXbnf0+LZTRllyBvODOXPI
qlIVP2EiKRgSP7IlwcXOwfWtqU/wwcqgbwvtS7ZvU52h+JNpkaenaIg2aC+39fGjxmJjMUKLDR75
XFy5ZeT9ZfQXDTGhyPBJ8gbxigGYklDnL4jv7x75l2IKrwsJ7/NP9PIp+qLSpGHem9LzIEHEvLZ1
NsvAT+wnQ6aHZ5lRlYrzHGxuGyso/MbfnHiNe7+iJiUsxebkG/C7i34apjPaX+M2WzA18S/qT3JD
YmfNZTelUtZ+64hQr2Sm6+ZN39vw4R1HZKt9NBKeFf7QMrn+TKuaep4D4aY4mpxsfLSnXzToj/lQ
Jv0iS6SChcrNu6XOU5nDfcH1/Eh+7pX1Dl8OWr2sxFxb/CDJiLmgE9yanuM9nxFyqwvEABxMrvPv
s1ohuCFii1bN4JjU0+gjJMEd8Osc7LoKYwk5/opTKLQLZSMKSEDse/sW1r9HqsdTS5sNfilfhOkr
sJbjKc4fqkrgfkqRter5qzRt/6h68wUWGMcNeiA/h9P1ymYMrYF0l3qV7UCQvpUNH22OYA9CZOkh
SXegEmYIulUdcEn81Z50I6D3W+dqIb+bbfbWsbPMgD6vlediH6G4lS7UYp+v+KgamXUUiQUIxMuK
zRvFI/E9F/Nmson9mSPoXKtShknjXprdh0+d/ZOUKPyoYUoyVqq1YNeWLIo9HVpX/Bo59UJx1H56
w6xd0ZMNz7Tw+K5jaOwE6l3LMyS0X9/1Rd3wy5azuVaWsHN71F0aiKOYXd+n3ABddwYkC9g3knxs
oEQPgqeLDK3afGTnndfmyflQvNtEhCPN6zglNyaGsddB8Wie6U7i80w9Nj/fRbc9Ya9JybNakk4y
+f0iM0hLqvNlboZuHXcBqnUEJd9+XV0pkoJjJWDbhhJhJaPF4gFTOsTXPdB+kouIsXxQniWpuhWt
kHEO6EMtoUrYf1nj0G4LQSKRuaeKnaXxG6elOWiwI/uM8UNdBFTYlTj3m8vVlhZT4Ikr4j+DWrCM
byW3v7mAMZSqRz3OpMeX8few7ISoCQ2HRSkuY+7DJj81pKkNiU6S4zFq+CdFHz2y4ZeV5edWgoiq
SlY5PbQMX8y4RKgwmOU+5Ah6vC6VFlC0xYiymRY12w8rgqDK9FSs64Loom0ElLax/iGY2lKi9yuy
ADqYz+YNcJTjTSPkuBf4mIHXyTunZhmTGI53i4O1ElFEn8reGH5oYhOB9gTb6yxdQLuDUNEzCIOq
jYisMuQfDKI2Be6YXl6EnC/UNGOCVVsyE67lo2A9rVOlkSahr1nZwajuuzFjhlaweOHlUZwty1ad
AyYhkBs5ejdQcSQqcDclR+gMh+EVXbtyoMmeQR4okqLXHE5V8GdVqTZISw1/67Pm7Dm0phu2cfVJ
ucruNORSXK6gci3DWSxXOQ82T5Ubj0qa5pq0MzV06f4AXDUyzFoEMb4D/F1UZOtAiLKUCPUaO2ru
U3QR4Baajj9cBbyoURohmPLkVwi4inUq79QkewzdZcEeDpFNvW2kDth3SU0rmZH8OlJEVZsMezc5
vJcBYwJPFOedRpGSc4k0n3RkcC9IJ7DJr/dUwhWnVhYINFnbWJCzGQYCqBJmDebYe3jVEnI/x0U5
2v09f9JAO9aMz8xEOINsABEEq4qJLspewL37imBxE3rLO8s+k+3qKsyMZKuSW6OFMSAEYMuIvo+W
DJHZUM/XoRBiewMZY/xo0yo+tXcNCHilTKlxcEnsPZXSiW89eF/zhUigIcw6HQ7oI6ZSFOTZbXZ8
F0+4/F3Jev3Uby6EoMzx2sefC3N6Cc/lZx7Ik52/rWjKmCjBOSYhMOnM42XS3WY5e0Z/HtrEdzxS
44FQ99Rc6OSc0C8LHOE7MYzCY390HV0bDnczj1mDFJ6SZdMbM1ewTsb9/8b3Do4ouLTmp6HgXJ0n
SJKbn+0kPgHdywhJip4cmYF7lpIgLzKEFDgdx753GI3l8cV+bN7JhvFCumVgaV3cbJz0cC96mEj7
gJ9pbgiuhSuNBQtrBVCIbiKurJ0ZQw2gQ3M6pHLQ0cbYqZMm5I6J0+snAvXlzHNjMHYBKt16beBx
JqpudlJ0zsAP+G6qzHaP8O01YFAhg1PA9y9f+mCSVHMka1zi27eBwiBJaZJh1D//SLw1IHUnslvH
Zy6+wEepsi2aipijR9yhfj5uza66HSl4JrfCmUcw169HTB0iNj+V1Ydo7BWMqzNSavLw18hHMkN6
e/4/Of5hAvUWMjkfoEUwSCn5fE35KlAfBrnuQnVZG9tu40G3GE/YfcEUhMDb0DtWGBWriV8Hz82y
iNZ9kR+I6CVcszNPoBeJTUYuMRiL+HjIcLflgLNl+RGMl1qGKahAMvPDjnBcxsaF2R2VUf/zv+lF
FFQag83rmaIFmQA46vTro2ihfAYk8luu+Ko22bem5aHbL+AVSrCedyflx7AxdAeQOVclJpWTvoxr
SYOQzRjqYJM8qHc+RlvyBkjSp1fgube6CKdEig2ur/Q0AzELhjfn3tq87CbmfIzfSk4MrahQtNee
AS9Pkd5Vm0PsLOvUT6/M6jhEWw4aykJQBj+JhpA6P6Pv6BsZoLH29m+1tgJNG/aNH7KJxr71EtX9
fX4pOTWGZAUE36zWTy/TUWOus9CSXJt5mvamSG60uER8nwuTJxEeSQmrnPBUf3tGiClOhg+IWm5O
P6JpPLoScsM9vrQ6rl4GWqcLkHVGQbn4IG43sFMEoMvpUDX9d9j8oN0Cfduhhx3aBa5amL1xqU2S
NGHeEHmjpbXNP+tQLFSTYc9XcQNX7GR0zBsNJ9fmIAuWtmMVIpgHne3cCVFxthHJ61zidaYc6PCY
Dr0WMRrkmUUHeQkXpeEQHxQ8721KF7YJo3gyw6Lx9UZYEZrTWJ+TgEzPFcl0rOQ51gafCwKmEBCv
rD0HtULBm++nTKXtBNlC1GyXLNWN0p/jRQT+DYjY6Ny6jJEvxCFR97U7dMDv1SBIjbBaDwxDsUlN
i2OiedWt/nW+r2yBa1sTmH7vEsZT+zZamrKvuj/OfEXS+hS8hzT11ggHvP5DQmwc50HBlxyWgKNT
8PbviXyQ7to8IczlEKlADGOj/+JGu/a+oeIaf8PDNSxtzzkSUYPcTSKGUOME8122HV9uZ44toEyk
ZUR9Rr/bf1lZkNBuQK/1GSKYjHbrTbXPl8cyiNFuBvCt50ZQ6bODNKN1QFgj2hVlYFBPHBvoFeT2
6httkpm7D0uMsI3PRHkuw9pMqvVE4qkTUC1wQ+PPQXO+Xfsf8try2I9Q5rBQRYqO0y2HUVsc4NzA
O3xg3uHc5NqXLAohTbs5GY7pOyVOKu6E438wPZoSuq8Jg2PAe7MsSblZP06v0JIYmfkMxwVs6bq+
KWEf52w7FwOVx7/f/QrL/BgNGzPFbGa1nPfvnR1xEbXiTp0HNiGs8yZbckPBX+/XevGm6LTckV5J
0cbECpE867dUftyMD49cu40X6gWinvf5MI6nhL7fVCMczknAjWKlqnmimpIqihIpPj9CVf+FUYI3
DnkFa8sWbBM0dOK6eg+hC3Dx4aSp2CKJcJEP9ke7pSRZiiCpeLc2alRh23LuG45V9kLlznebomg/
cJX1J7b9MIOF5zmSj4zWWep2ZwNEwTMukRyy1EYNYhGVCShxTeaki3EFBRM8wlXne2dfFzVmdjk5
+XvJVeO2dDbrAAAME4cLepVntsBVcPHeJoDmfJreO4ZiMoWR2r7u0TT0mU7eTMglqtIiIIolCn/e
o2RorrUJOjKYPS5R8xRUJYJmHQ+/eht5J59c+c4yDajOxVUWRIiWehEh43MthIsinrHyNjCoa/iA
vSPy5OSCHDITn5uxz+g1/0IP1mJNgBHdV34syTLFVm/ywjnv7Et9ogDiDaRfcrkeBjPSc0mrRjc7
rdrG2lUMgzw2hZXpA+gldeVwRSQdqjR1D0QUGqDf77ued2tauVkmkQd3O1JaRTNbnPfez2bUfxcy
u1gQ0+fJFqYZSjVzlixpdFffSb/31P6rTp3ZZFEh22/EIi/0RARJRKQUBI0FH002Mt+0Z3rbzLF2
K0HKlnpEKrD7fDC1IJ3dusVp/MmSf7vvcLGUcGo03eiFa7JjsHCjKNhAoCddg719fRT8j7mlBYDO
rZsvimZ2AtRlQLG+c7lbpvCPRNkB/T+H7wCH2ZrnPk22tVELwaVj0Myvik9txMuVDTGiTKee9DJo
CPfP8Ak+z/eTjCZ4Or/+IyMNcRYyqQlUpCgTvoecdQs0iD/613iu+N3VVdqlJKX33H9BauwWtgzF
oGbAxNEmCxjU8R9R7vgicBEzQlIM8qM7dUBIveCJmuSQHYF2nJPUaj5jgU9ARgnBkzYz4P4jFrFF
0p7l77iBDCw3ZAyTg3fcaxWxyJWKSj0ag+iTeG2s7xZ36R0hyoNxAgwVB+GWKielLqWCcrmlZ6bl
/lW1k+HvwwEEOx4H5/odCCinsq3zz/OmYPrpQIsZathy/hQi4okKAczj/rD3doO/KQxsQuiA1ygI
LsVIv5H9oiB2iljOSV+Ln55eHDTKRhwtkikqJ6JLIc9K4s+IiEQjwneX0IXoCaLAgpgN21of7hrh
BwVprXRbVeZjy7Jn0egQGbiZrencbHj3UlsYQaFbKNa1YTRh3Ygt37pYPpUfAvSZvOalaQu3opwN
tX/VM9ds0hWRh1zpuCKmri5WUMpUzwb8YWJo7SbNJdH0vjrBN+WFNFgIcetIRhCAxhJrmuNhza9M
qAKU0BcMdKcrplAdaqmVZsKQ386puxo7YaJ6RKyK/fFY4NZFP0+cybSrRVNK1C/zxvLqwDqyaoq1
zyfy17+er0qADltFUXBPS67zI3ndSHDa2eEAzQqM2+yQ0jBomjGbT93cNG+nCKVDV/AqFRK50MMf
WIdFeWEPDuQI2nSOxVG4wr/taBT++HMNi4ovf4EPuPE3EH+AktAHG+T+HpwyOFxq011ODsmF0J/0
FEX2NFFyxogYsXvHo4bryDjMubVinUKu1xHUx6+3blOPwtN5Xhv3A9IFaq8B3hmZimmktPRF92aC
JHIJjXofTUHoTy8kQ/zpEFAoWrKkYE/OUM/gWI4w5RQOpjOVhPmqVHDKl0FonYzw6YAkS6sIlzrh
1zOYABaBHuVqsQTSrlI53wGXMkP8mY4XvhyPCWtPiAQpp3FqbR2XBl6zGqxnAGiweJLygEeSsePP
tpxWZGlL+CWP9abGQATGMH0R++dLMTjnYK56DWwuakbIwlRE+MBZ0Z+0AfcLCje2Bv8qt3IFGq5m
Qn3o/46rqO2B3dEAq5yhOZJ6v4nQ8EhhRLoqJkIKVOh5GsYJMcZaaxE9IP7TXW6+PMYWrNXbrdhr
76lmK28dK8QgHQMxN4IkPj0jgoS6g1J+nsSMnB/Q4YxuzVoaP5/BAmVMSexanH5GZ4LVMH6Qmz5p
c6dZGsqYMExCNo7JSvli4st4SVRK4qz6NEQuEDcgn1imTMBgasfSIB1h3cjmMIvBzlGVKK5liFl/
aWYsnkhs4UsAUKybs8r/X+sSmuaD5sLnxVudetso6S5qFjzhF4QK0lUdY0GmmG3Rt96ZjeQlKZOw
qzBhagqOK4QMwVPeQlQFQ+jk8T+8TEl3kokoirtpeCu8P0Qqskhu1eaoBQULcMsqwbT4e21uNe4F
98wd6BAgP54jOweo4uy+0o/5xwV0aoATHslsjm5cAwoa0ZxhVzGbfe913xKfzjYZMBztRpfdmo5r
pIuNpYxZJenEn4gXh0w9CmzHVaNaIUGlzExm6aO99tMNTZsO2vo3pdPVad9cPkQj826oMVr59J4l
x14ZTD100x4RhlczX9uOcm97eU5E2ckJr8vHrki+EO4ZSQfL/gYD0Gq9pUYwC5dDMkjIFCfpv2ga
wEWO/BUZ5BGPmdeP03AuvGweCT3TXLaFReIKNnT58BALOlFpxXtKjByAVQM0t6Sk3dsL2CQXNogn
pzZnv0Dtj1Y65R3aaPt+VoYR0+hP2CAMGte0TDVZC3seNyITOWyG5J2fdlUK5Xrv4I+bL9yddlqY
JtRKicHVkwHRAP9ZpCKWg4xzamdafJE/OMt67VEwlT0PQo5vpuzfZoVcBKdGMJYllLo985NYSgGJ
HIFJHor0Shf4oo5KHfm8X3g746umr+6qKm48sd2d8ShWkLW8DB+WeGrMUrt0pxvX+aEB6qSUkixI
8wiLY9wQ3FhJPsNz5aIwhcI2ihgSZbrxu4by75Goc/tUCa7MibHvh0nJtJGLXSBMe0k16u/m3Y2m
tcP5NjC1ErupFOhMDoqlLbredZ3inV2dzIU6Og+lPBj4fqGEGbmUowmpoCBmAjiIc9i1GCY7aKfk
zTDQpFrdTINM4d8daiujznuN40KXm5piCRjezZ2Wyqn7NUhoOyTI6NMOC+b8ne2GiYpNEQ1nHMUg
G1MOWSIm/j2GGekcnS4a9V6fi5HympbdidkSPw7StD7U915Rs2TfWdUgWQ5856iPEp0W2sOkrELC
KvTTqjyJbTUZqfPk3FOR5Vnn1NtXT2M4kcwO5BvsijzAXn6asEa6g7oy886RU/gZkTLka0lbDfgC
gAD0zYCcWtW6jdcIS9ZMvBTGWjAxaQYehhB3S/RhrVxpc/v+tWlt+W8abh9XTia2A9FIw+fve5f0
M0OktAe+/r+Btv3Oy2q4j/+I0vsvcAgw2wJos0VemSb1R5SJDnynr2xAm2/MMd6dwA1e90cUkly4
s0CftM/XFX8wW5t8gJ85AvvFb39W9qyaL/eSGRjfMuY67j4Y88jQndEEx6Nxauj2PpbwzcQBUvE1
b/BvEpYk6wQ1I3Fgvwbwbvk5Ny76b/0T9C38orhxdS5R80Ark2cO1nhrXEGGrqlvy/CVuasUnOAf
WEbh03qC7DuQ0amkxjPZTjJ6Z5lpI6O/Ms6gzkG3USZf2ritbqrG+y/ZxSQ6eKtaqQW2IP9yuSuV
BbkWA3U8jmB4JMhqDOklEI+q2f+wGz77ReexDdWO1LpkGHYLABvm+XoMj+tdXpwXLSgvgSyWIoMA
Ov7/RW7svFGU9BmCowgzG0J2xdgr7Ht2RWyYO1FtnNA9nuoTN4XZzNa6gP64GJ5+ERw+mkxg4jK9
Yjf8bJUWTCsh7Fa0WrNSv/4cJTPshcWr2OVaterBcieiNpe0enoHy5H8JZx0Q6V1sZN3z2v7Zag8
4omRmxiRY2B3UjaWS3FBBHmcF8qimuMHoVZkmR+VMEQh5VnqJDOAiPnFXnZmUeRUqP/cC+gem/lm
GjKyJ4JrnbAnVXW8VArqiRp83l6txbEkcZzVWOpyP9TI77VMeS5e+mlxS1KmT968Id+NSr9xm+hN
6dyrwkODs5OkrgyWxHyKp976NJr6qJry71Fy0AYLaNLkhS+ocJs4Jvg1n0QZbq9ZSjXb74pKKGoW
ExwK9l1tIyIvILKANZGY+hKg1/lMzqc4OyGFHrGnyM8o5PCpJU6ihBzzgywYybZZZwgM9cl/r+1n
4uY6dNDdL95oxDl0mnBRPpTEcgWb83by1sb7qZD6l6H//TjUcYOrr3iEAWM9pbLNKaK1ICepkykm
on2J9Q4XeczlXaCDS+rPqLY9qTMJCbMuN5NrxtSSZTZWbuvVzfSAm/FhVEi9+Bzfq0kTb0ieycLf
35DOpTMnfzsaHmTbEFiWZO1nywASw1ui1R/TkuKRadtfJjBuZ8w9aazfKU8AQh3b3c78yEU/1AR+
cT2hoiPeb3IIGNiaIZnqSE9nqWQEeKBZCqw+dO80vRChWW8m8U3fnqzRZt3Jq05zbDulQfrHrMnr
SMlfpAMJqgHiz80cwyiIEe/MK7qclfBRoU0nfefHj12eTjFF/EhoYx5UgGaugJijmCGyOdoWUK2Y
ix6Jyci3nDu1ekQNT7uwrsNlBPV8QAQuJvyedK6+HjlfM9KGVmDd6kA1kkeAdK8c+7nQft/mqTfs
AX4/8bNk0J7uulqlot6WaLIo/t75S1gvMedFvKEKSmOpHhIGqLFtWL7pKDqay5hoVcJ6rpOvbQj0
353j6SY4rlfjyL39hq6UBXtJVezzM2/ZkXC7gDRt8FZoO+TX3uA56wue1Nc+8b9EI56LYTu7dRb3
GwjVrAf0SRPC5IyErh4NaNHIK2T2BSEs+v98Ti7odhgd6KX0Ykaq6AiWzo4N5FDZngVsFxqaZya3
Ci2H3AyjekTpdSeh9WAW3VShQPOoHzQlSDWOY3HIVQOUtJZfq+b8rSmBi6EueDDhOtV6i4M8Pnqf
VRfKQ3r4qepdbL68LP+rl2bwpkSYL9/wv25yNRtrNFWNHcKHmgR5oAbOktjguYk3DjhhdkBJR6pa
Bd8BIoDcr309QseblkePCLWds0t3W71uQteJXp8nafqPAxXRZKwqabiP/HOvdlyW0GnC9aIqcFZI
xTlhHbbAFdGMV47hjANDq4hc/XNU2k1hSPTiynfJ/8IoqfDZUnYWrcZ3fhBFK2OCREB3c6zgsh2B
+DXRHFdOK6/3OwsR2Fkwq2jqHifomwjiknGSTwZk8g64/lxvgiNwke89b9aKIY1pWT3wzWXsdj95
TFXlrQSlYBK3zA6xOx2nuw7MbbJLwFxu1UEyuAERKeGBl1Po46TXunQiN09FRXo+hDfzBAWPg1AK
l4mgvN2CEUzrLqE8gn8WUerwV3h+wspcUpCvZHeG4HBRTrB9PRFyJa8FPBGwN6UkERUOVU73RUox
BAjvk8K+D8yRk6PjkkuTmwC+PWuEdwicHjsKbdQWWkPmIUfT14bi2SGLE7XxptALl5CaJdKVXCEP
IAHN7ZWMvgEVB3nhC5uCx7wprHEMbpHmDmytBG39v0v8JxWI4YYbcjvUhh2ggwg14NQmdwrQBYKn
6Ongv3sxuM7brtveMWMGLys+L8D0hCNZA9eV7akT9boI5wU6hrW/aN6WId7DWYZ7m7wMt9KAXWRC
7vAhFf4LC3uTSQOeaSUTZSlc8vfbUNzOVkh1/3eBkjyMqqDMsXOo9qsCfL8varNg7cVQ4vankPKF
gSGzk8DgQWFpR2EBCCyKPdOTe/UOSwd/80XOjmqX2iwjIsLo+B/7g6rcltqIsDHPfFOTEAINIaSN
HdX0tEJkCQKbsRLPiXQHMsQuirCfqjb+CxAwdRmrxl9ucjceIo9KTNPEBOcg7Vl6aPLVa0ZRALv6
om1xolHW+io/+b+eGIcs/85YpJ2J2/g9pA8zy9UL3PqHyt+2hVweySwv4uHn8lHnh78XY4KpYIvO
3225xAQrM1hD1E3iYWAlsCNYACMSIKucJo8BLj9Ib8bD1WuGA6Y7bK7DAXkoab/P6DPTSAOywKpb
6CVIN4IW1asIbtbvUMFYGWRda9TybDNyw4Hy+xAehGPg8jUn01IV3ml4L01Shgk7BO1I3CelxqeA
Lnze/15jhSaQTBwOxS1vjBB1kxIZdTaOJKKtypu338STBa3ZaUKE4i4vJw7z2FzIaHvF/Ls0OZ5V
9LPS1sNG4ZowXlrGYx/ASc2n8WV6JzRBwF+1a6x6wYcwEtWvH3SQoKZlAGu8V9S93lhKv25WchiM
Cbikb7tQTWh4/a9a+vXptSWVsWAJL7NzarQC2Mos8/Nf16YwmqKMw+uFTKahSOiXmeZR841MDxw+
KMu7k9I8utRSGa4HgeLEcuzrpclm7hC1K8JSbLPGe/+y06G8Mone9UkGam+brTr8wLlcA1MyOQWL
SJrEurppLlstAa9DtmyyVewfMnVHmMeX+EZFAVmO+nbvkxW1FROUfTcai3uZdC17sJbzX/KsxPU+
vfWxVgMNPJONAzq0MvPy2+zhXvjJQF2+OoqyifoYWgGPpUp5P3PjhvrTcNQU/vVjFicU+Bl+Y3KE
0CbtDepftmN8b6mTTsb5KnjZl03rsw8rqVIA7WJzzbegglQKWqfhlpTxIYBqC/IByAHFioqCbKqZ
ipc8YTe5rvwQN11rBABvbWqlMFyiwJC+XF5GEIxcD2SJt6iSUIu1jYPDM4CabHDNLNjk8NjBWl5X
1g3eDcj8ivaD4+KNfIK5B3n76z7/fcOrtQBwEOIJpxzhRJEjrj8/KumFMGVxIDZCS9uUO//Uehd1
rYcWqR7YTJvm2/STfYLw10KQKLF6GgxeLHnu5fwkeuslu1xa9B+6I7aTP9/MaF99Oza1HMMlOVZ+
I+SpQa0oOcjWZlK43B+Fv9mhy78gEyzQKMqJBD/SduIdjRw1/JSnqTvTPnrpFAKP+1VUJjF/QhzO
bDBrESuzdAXz3SncvrnZUL70zs8AJiSHOQqKycXzOvKnQRpVGeMZqzuK4j3/Rb+s18lxXZs1Y8xe
evHCGNgzHu/pDMK/G76j0fkhB+DLQAEXJdEyecTpyD+pjKb65IF98ydA/SJaTpg6ziYdqo1mdLEB
kmUL5GqdvXlBtt6K3+CE7xU/NwdZNL6Kd7fSH+W0LKY8/DvOry/1upy5mrkDKfb5AhSBkHBEJ37b
DIfH386Ahred9XF8wGik8FC6kz0ZUy8EQsd1Lapf7IQJnm3xZZorLOILm/h57dK+/Cuqfki952ra
tIvIplY5OrfTxvJPi7OW6doeNVDtW7Cxml8xu+iIaBBiyhDFqNVRhxh/e6azsaFiDqzYnBask6Xx
DMONmilRlDm6neSRVjia922d0dHa8J8aMaIMSdDXOW8M/hehpRFodgKu/kzH1X9QZ+RXr7CV2aoD
T3dG/9RbEn2Xx6/pod6Iq7PIKiEkfGeWDjS/YT0s61touBomFmah+936LHLJ474jQj0A51u3yvLv
IbzfmzrJ2ACPtfJXbBpmhz7Z0WIjBEOo/6e+iTQUfVUvvkXFXa2Ay0WOqXVlvDsmA9yOria50GjS
Q0CI2+a9ivO0M0JYF9oiCTqQV5pdJ0mtuq//H12A45mR/UOdF164rPysQ1FpuzWxvxzpoVGthoqT
AXsTkK17HD27PhVf65Ip6JU1MnA5ZfP58Fai9pDyW6bU/Zt7x3ltqK+WmJZZ64/qZjPYC/mNiU64
LwOAEgTMaNlvUcLzLx9nmZUJ4hEbeDyo722pcMf3dWpH/ai0gh5fw/bECB92fyZ8fk5PgTrRRYe0
OUCcIXAqu94vrKY+BhNJSp3dBq+bxAI9H1P+tmOsLFusJTJNR4Fo2Lm1K701yt8J/r99paKPBq6C
pdugEU+D5ATwMRiHKi+aP56eHos2VIaAv85ec+JQ7/SH3eYmB/1R9Xxk4ZPzg/sFuwdfQbiwneQA
fI+GPF1fSjzyeVqisRYdUUkv6QPmPFe+W9Idl8LkOHbLT/ZN0qywjY8cJcQV4jKsXyuMA6NUYFTC
QncAUl9Gxj+XMSu1227281kFnKsuTdL0HhQjXPRBFTALzkPbDVdJpQXRFicJ4Yf5qScQKCsK+dIX
4AhupN7i5xJ0WlTyJxK8nCm5TsSQjWtdQIKirlPLuwJKy8yV3U0cs+vU33rNAYYSHcuCsgDcjihx
TfpEjBBM0ltvFKczBytx6p6GHsfvW8gZX7vzVCjAPLz/sBCDhcFmIQhQB0PP/NMfTAeGMvLBaZu1
tCDkQyKQHVPs+u5Fbb1tLI7QH5UXSZQMOBFNiKwVCfVT/p3fC1acTOeAFQmlvyCW7sgX94BHohp4
5zRdgEpdxPpY2fcWoo7hA0kyZm88Gql88MpQdERFkicY170iB/kwGGMTzgNnAk1aHc6QX1qIkcXO
OvdoK1AaiSspFp6nPIiY+LApEHHCo8YS4i/r7IAs0I1wYZly34CP2i/8vRioGWdBKNcFDDE+jjG5
a12J11S2p0QIA3Z5mQEfVfajW9Lh0d+WFp7OnWkAmZIObXOdhzhd3UZ8VS10b3Pv8sHlG7SmhR2+
z2ghS+dsV1WLmjIJ/UJEZ/f/5dmb0x3jD76EM6GoKvV8gRAY8W/GnzH/8zFaC2qlEoY7f2tqEe9R
f6t2EsHxhERLxMX7Vfsgk5o0PSrhTCwgEdDSN1Zc8Ynq7nA31Z0b/XtsFD81z6zpdHA3O4CuINdc
cGGB94ErtR88H8FlwlSJ5SFatAfmjkCHPA/Z4E6adI2GjVQ2FzsQrXQgGTmMxDQO7qIXkXx34HlB
RJx5X5FuiO+B0P3gkxUPuX42DlFIevwJK/saOzc45hG1oPbyI8un2w4YI59aTDTy5dTxZCpZDVZc
7KcvDjzDF+AOvQND3NRrmIg+OBR5SSe8yLOpPjPvf/dcLZN95xq5OIks4S/LrP3RXVp4/+pnoYLj
qgF+RJwCHEPEDydgjPcZz7VglThMbR2864yiMLWpRuYgTABgrc1HoNw1K3Wvx/jSdvSfBrS47UDV
Ca/xGjhWM70nEmtmSoS3T3HTacGL1N4gdZMlRTfsIHPP6H2huv0+cmzR10gqBLuMjoNq3oGllVkE
oE/3Y2BgFcR/CbM+xegzxRdz/rHtVn+xug9Vg90VL/swz9VihSM/QJu8x7ymZyTz7H5/MeLTYQdw
VEgSD/BpdAvXMupIw8JOCOVBw4as8eBNkSxkQ0Yl7GMOhvFszcSQVJrZGpi6YZ4uKhHmk4eVMMJ/
U8uqYiOjDA6YZjSHXQXgGSHvept/3IXnL4R8fZ9StDye9jBsYQFT4Ud0CDNdKpyubSMRPQj06THo
968paHwEXjZf0b/2otO4/zx6iANr0hC66OiUtOSHUCPOoHDZHv2WW3kblajVxBzEF7xUao7SdCCd
Zu8jQ9OB4MduX4Ea/58l57tUbisJDtv7r1CE5UyAlL82yQH9200FrHHz0px4Sggh2w63gZiPRA3k
tkcnSXlp5jobtjLV73vAx8T9JcP2eJozBTgpOEpprLq61Qt7hoMwnbDa5B+n4uPwYdqeLohEliuC
PfQmWfqcwksHn01wrOvWCFkwtwcE6C2VbkRXJmC+WSN5n21pol02FmVhESn8oQJohRj48/qvVl5H
5V/bxbw8pxex3p0tiFy66l1dBMA7Hezugw/ro8YSQ9N0tBIuhVKlmTk9xvp9N0/l4lQKeUBeY0mv
nLyRROLl/3XENvT2RM5SjxXFryHhWfb/APE5tYjfSOXC0yg9ovkWT8e51DAHt2pVA2P/mIQC/nPh
+847FjWS3XcEufg5NVY2yMO55WvEcIDUQMnsSyAVP1CwLotYuU+TGExGt8oHuyxwRpZYND+Oa4y0
hY6jmBK+NSk+HGy1Y3/tHT+xL9pPbxJW7leqaYPVa4PoJ8irM7n9hMwSKxaE7h6Afg8dtgnNY1mM
H4P4t6nY/j6yYL/zlTYzarBLPhLA++autw0rWDfsxdxrcJZ9zSpp1KrEpn3QhsJrrHmjrzfJ3LGa
WF0KIvNerOaX0SjrP9QJLx7IZJ2NQftgAjOM3osNYb/scWM41SjgKfpdXJFLao+zSYBsQZoKSNQr
d8ysySHhLvY+wxTP60RN/BxtXG6RMnqS3PFTz0irju9FatH7C1PAS87lneIhg0xio+oEgbs2goHI
c/gxHP9qxhBA/0zN9RjyzhGaqCGCUelQueyP951rEsZqIME6TQlXacduUCy+e2nJYqXC/EceX/qm
clI0uQjGh1vhzz93D4m8lOEqJSmOgdp4Z2bLzaYIsLUspjtjuveshHOvHfHSxAE6hDqt8Wco2jZt
WwlwU0vMHxCflomjQjmUnpqnBA+fRovshNg0ihthGevkjDvxEotcWDUGBZIPNuanwxmP3MROMiNI
y4khrytpzwqJGTzk9q1bJgF2b6Jv6reYkiRjxyNFvK6YKGkb6A0ADJDEbqH83sJ0PY/MHyiAtJoC
srl3zHqjn9c0g2jFjU2MzbEBsy1Bow8tusz/Lv5RFGjHA1rIfqldzapY9drgd8/pgkYPrPom8BRk
iNR75Zx3hmc7BphjEGdxy076YA5G9WddvjyPp78wlDeCazxzd4J7lDQFOb4YfUQ03BXgpbJ8/ReP
1zg8xJxhhQrvgFvJX01nBwtpp5a81tn2O2Jj3Iu2xvcN5fkuCj1sPHOU4yoLmUO1Hn5fsnKOrMfe
OQ3zAwOzGn4/10C0Dh2LlrbDD3z4+eDMk9m2DC1ewnTCS3V6d5gLcf8QmPGYA+crkszkfamNojud
adRQbUXrJ1TdxZ5kGo6LFIBxa39ltfwvUFHQFVGNTtWJpK0CdYiatZ7qo41Zn9WuAnio2IoOZrz+
ZCAENLzjS0DSbGa36MZ7HhX/6QsQ+oRUQRGv4HLMJHIpiQzD3fcacWVaKVAzgy5M72QfYzOI8R7I
BkAgNLs/sDEQoNod5nKwoHo8+H0NcN2yFkvhcDxoC/Tw1HnD7TvrYO1vzgHDy/MF0llF7ny8BACq
Y1aXWENSEug+QWwnbvLr9rPP6MSZL/puCWkY9p96qNTdPmuKL8OyWywqH6g0zI8j298ESFcS/oYm
NE8mhuDTOkhFgUurdJRef567o6uEyQpDgqgimQXsBU9efJKtoFj3Yb/6b9AuxuNUUQn26eP/vT+6
wkwaucpJ4JQ5+h/tIwAxnSstey/pDkNhcF/Wdi3D3afk/7FfmwxTf8vCd4TtoZ+plZYEGbpqa4Uk
kgy3RsnPUydjOIWeK6AgbwFS5tiA/4ZgSs/qT+mZvyvmeE3OVhR6Lqn7MzUtCSW8WxaD9dCPkg1c
lfGVwQaR70RqTr6DE09pjcfsayx8Do0hqYa+BFKu44h269dfhLjsSYfEV/3gGznd6B4ym3u47yr8
eFVDs/bkkhfHlKHmQTqYy1tiKSPv/L8zYSdGTov0OCdTUDj/r6GQ+4GRbfK/gRzs47NZxrZ2d7Yl
mvwTlUAkBWch7aCNmP2dlRxT0okMA9YoYWrpTlUF5Y0/TvmtzUMbPpLIv1l/A6EPIyUzrQDBesl5
ll+cOvhr2y9mPiKbEMU6TYDLunP/v0BXa5HPzUKyioHOUmnTI2QgkEEpxk8JuFuLM5oNzQZSbYDX
2Hnr8J9T2VHrFrYuuyK12SVx9luzpLhot/J+NGeBDzzoQrtV3DdZWyuM0j4/tM6C75hiF3hfGrbf
0wenv2hOWhoARocvt9XaK2xsbglVm00bigNIqfLMDDd23IKCwbAc2GBPXmxRb43ZRHaPdLLvT0c/
4RvV3ByPAi2WNaREEyaj21WCu074nr5nPOcZOozey8ZQiKXH+Bf/1zrQEBlYw90n70xxMWe228px
bYwx/73AVVfxZBo226yh7WOnmEZarC4QCCw1JHSJDISagtMA5asKHwB11fwGOXtBiLWpwbdNosIG
0nC2xMbPugck5zOQ971op0NqGeZbIBMsVjrKjSH64HjSHuPFlISsd4+vu9pZ6+JFpFpinYu2dQMh
bO6oNYx11WZxqEbAmccwBDkri6+fyi5mQrJlb+HmUdkA4/Mp4hdpnDuTeHC8iH1rk9Qoe6FK+vn4
DKa26pH+NqNSPktv8Cqf3uXAfL3UQPavgUY5ewaZ/pB+kwE/9xTiHRQGFTU/QE56ail8Vxpmrqxb
9FRViw7q/tVRDcJys6UsAEroLTrqrztQp6+TsU3MM/EjPIQdW+/SiU+nmRY3i6XWugqd8y6pvRhM
VsuwLindWYNY+Uja3Nkf1OsDbNcehIZCsgQzTKzsKOYxNzpa9Cp9G+i1OFf4NOG/3g6+tAyciEYO
jN4/Aytcx/FDQMJCN0wWzRbhMrbMxRIwxN5MLHAE2vI8mGPw9DA14kTUPsOhn1jN8cII2tXd7d4y
TK/jZ5BFKH9Uo4VYxhetrOZfqwVGT/ztXThQ1GzaKUtZ9BNo20aznqnZ+9xCfzNHLSMNlhSxd9Td
lDAKtWb7qzJNNGzuZf5n6tdZ+YmKKGywAJ++QydI1qspuZcCX+Cjg92piW9b2+7MIVQhX2KmkK0R
AC5zEFHSoDCsTNzLwn6WzjGWZl/F5t8fAejINSPOtxRb87PIrtipWxOB22gmoPcqMxUt7J5ES+Mi
LRZzne6Wh4xGybWipCs76hXmQQ9U5AWp0zyZMvO4ciQoE4S2ZixpaI+5VXpcDGK+FFqV8EFG1+c+
kzZUzJP1risUnEnSVmzgYMnaVtXOIjq9IwXjo4RVuVWv14XHnB+oRXMGLiHwjIMS8uTwY5dJIM5R
XzRpy1vCzK1kYDzwW8JmtXm+jedv7opvNkvv8ENpyN8L34mvn9g5mPhv4BeTAfnJbvMJ2GeGgG/T
HpyiMb0jETWeWiUnzYOAvhWs5QRX/P5KA8di687dz7QYu03NPtpRPTmQCoyQU7ctUrq6K4K95ZQQ
wh+EEIW2gSa+ZvTwsqMI9pDa1MiukVrb4HzgLRZbzrgANsIu5Nu1VxKY1x635rc6qxcQ6iOHbmUX
6xAOOP1AlFYQFXEoW4w2MvtXkStFmNx7sKgZsSlRAkg3VuAn/LUtT1X5QS7aJp0ru7vtzdlNLipY
dFc0d6Lprd3EyomX8xSF9DZN+qVkEhvN8VUGx4Us24NAt9+PmY9mBHFj+4p53nRO2Zd5J4R/g8Nt
FdJLmV4G8TaQiGpk+E9wQOXg9FtmOlyoif7V+2aDkv/fHJr1OvGxfu8APAGUcvNWoP8MXaCyI7Qg
3rtJDx+Ym2aatss3VirWDjHeR8ZtWwKRed34RRwyRwIFDDWhd0s7kexCNwf7KZdpr3ECy6F02z+9
QfswWqHoHUh20uESM9qrxAKVsnSabFn7vNLaEXh5bUY7b6P2m58W9zDQIQ4rKeaxXrFvZeUK8V5Y
PJkiy+pz8xkxeXAgcJHznzSkQs0A+H2pIDqw0zjOHm/HCGRU9wewVUdptDGrjBnnzE+3kEtxZjR8
QNgBEcptllUjbZlVaocOn0xgERMiTxbXSMdAkp7OD3B6UuKhsjczLvlRutP1DcWx4zrJn3U0ptne
zpJZ0B7dup2KvnK5Me1VX90qfW56T2yHjIpeYSJnbAXf6ICq3SYQ9UpNEeryCaeKwCXSl7U7T2k4
bb0uiqN4fnjgXsdS/60n4KNdXGs6WAEEk72gzFQ4DvSYlo5yRSt3QsKVgb70C666yRvJqyrGeewn
Fuyj7qvCrZuI+ztBb9pmDl6Ac1fetw2ezKa1nyg6aGQ0ktLTweNdsNhTt6xRoe9/p+x65M5mUv7z
pLIBPDM+wyACnSRr8VLWwPk/Ac52hGeE3Fmugcyavo1yauf4jjBIV1qu2RzrM8bA2sLKwrk2uMey
E8vpg6IdEEu2C1vi4fXJIx7Fd793PhQzkj4BMHll+hm//VpBPHpSA0TYpvveAmkjrVEsw9cdw0Ma
1D+1O/eiYZRAyJPIWTKtD4dots5srfLwMKOCmdkWq1GqEdw9e5Wp7uUTBP8duhDiPc9H46fbi4Rk
k5Ar8Ef+56B03TOShq6+yI2JyYMFdVT5lCfmSsW0R94JYaMfguzJTzQA9znPzSFNpjp1+mfnqikA
VZwfoHhbLSKtXl+p9oBi+2qSyaEqTHEtIufvLol4LJ83iQJ0mtnDhdHQidnBgD5Sa2wmpASunCor
d+Pn9v+lF6HNz4x6/8GdKn9/goNhHXX3sElkuY1q8Pa6mgA3/RjlMAl+lCmOZDF+2tH/ZMOosVe7
JLOcV6eHS7L4g1UgYTCZRdzqjcLFPnzogqAleu8pzd2jrBqsT9iFmLr7BVuGMTUQgJwUmjnag7p+
OwmxAPF7T9hhqmUHpjy1LVorXz/Zb9pze6gQMZV0e3XaOG/aD51v6mSHfHnIsNOOkrzmVZT4lM/y
7t4FOzVWzOJFKdxlFV6l0cWtqm76leAcYAu5GCHEkOJ2gGe1zBeUG1kfAMCKjviTBx9nXfQdGydw
pHGS6PkWwUkjUMFEjKhmbFiYYOtzNMSq8a0qFQpnkkIjuqz8vxoHQfwB6Z5d3bLXZVweLg2NPken
I/P+UfQkvKWQ51DlFTXfLdKDm7FQ119WxHuhN9j+Fypy5xdgVAvRLZcQTdU8FonD6Yu8WZeZzTUD
a+VEu3Utdo/AHMhJUYZ4MBi0AJ+MGl/rOssvj/J+ARSso1IY9Tnq9YwB6+JpLv4f0i4aRfbZErco
wHeWkJQyE34TfaHgcUnAz/37FdPqSksxfDrNEe7jSMPBtKbjhAK4D6nffqXGgCHUQZYQO25L9+rj
piN4lgB7X58NgNqYXDWLmRQEZdgGQLPJXkueqY4Sb2G2CroPR3nVxPz1JoqUIRDbeXxrClcssMAi
x3Xx7ggsWzNJ5tV2xoT4ewDrM9ISCgVg7H0GVJ0f7wzXs6CzdgAxgK0lwr9wstp3HqIKUIPLQn94
u35Byo76Cjtendm2DmDX3WwEd8ci1h/1QEjq4EsqsBt7Z8xzIdWu1hI+0coYw/byR57MX9Him2hl
B/kYcxYYC2svgYigHicTyjuAIST3cj2QLQqF07xaMuT9tXdwoHoc/w93xxznhk3tduFvMjtzYWTP
somD1FqCtoQ/MGdVyAriYiyiwc6MkzD0dcxkZMAWzEVjLKBjU7HhbdWRhtvD1ScwOxUnFM2lupSH
NxPrj+JBQ+V4shjVi3DajEe4FKmoOS1VmfA/yqEd+Q65568NNcV6nj0gXBvpknc6U5qVwR1Fi0CL
3AI315OatASayjEvG7mlRwq8z85arYH/WzLyQQfLmnAbBAfygTGTddESxx1ZKhDmUGWV0w9urhDl
pkMsTTGN41IRljwZUt8aBxstQ5F/MVmf0MI7GD0PUMHPTKc2N5nXWKEnQgL5TCfMvsCOnP43BQhv
gqr1UuCstkyUn/TwBaTOSuKRl/4Ovd2EliVc0N8kIltEt37BPsULY5ALbMF2Jy5qJ68XtcIyOH0t
IzvrJR7PTQXauWMI342ql7lx4x3+DhRAfHQsTkPkYem8AV8gCnO54SVkdumYFBrwjoSiTHJZo139
hb24RLcOJpiyrwFrRVBaZK1iHX4vfebFe3epIQM9sHnUmAwuDW8HDSyPIkvRAbGKc3Mn4XNkUQ0j
klZPNlpNXlc5O5YdoEQrYelMvARu1flsI2RTu3CwnggiXhKzdu03fQ3RRAvv4Cy9XKNINFQSDyJU
QhnAEsT6QpIuEQdC6Fh54nwHxh8f/vU7IDenwX4ESq1ayenAlG+pD/LF3tzPqSY+93AscvIlqvfl
AoUsAM8C+YJVOM9cKMJrS3MjwxyDtWy3S29XHgkYvI4HMWKsY7iN7EytG3KD9ZfeJx+FOfGD3jwE
C/h8IgS6bXLm9g9AlgW8iYLymVuyM/67AH2ZLKl3QAo4bJ35KZfhjcF9l6mstrQk7Gif0bnFNcV0
/qtnmgVzNcdXr7Ur6NC+ABRD2yoSQyFpv+85aH20StMRAw/8kypxey9NwTVy4hvaQmJiPKlhhRAX
abecSnnbETMhxKcgX2ovamb1HFL0iPnSQ9kc2V2wJW6R8UMwr7xF3309EOI3/vJuSKOBkKoTeEVw
T7EZRgcDvSD7wbAVAtDuVZDg+Exs+EUiw6CA8iZK3JjRhj6rnt9PXZ1NIQlzI7Q3+4YRK7q7Lr2N
1y2nbZRyXoKFpc5qUdlTUWRZUAh/O/UpOckpkhQ/2/XmZaHMXd+aTTNXSli+dd4KlY8moQxjVWTC
VNjctURKrI4xgTP1vwZn/+rmPHMjISG0c4k+0ep0n52OeFA7+ScEL4IEAjZQ7IhcfX5FHDXh4jAo
MHHkSrKkmxaXbtgEJDUNRpypyxGz7rFa46+Fgb0dMBxDRhN62Jn7vynw5vkR5mx9KIeg1oyGHQhh
4ULa89W1n8vlb8fAFP7jreWdA+fDoHIAxpeFP9wW8xMSQSZfs4OYAodHtfFxgDLmkyewcpcw6TM0
aEbsclhUgQ1WpbbjcPVm1W3gs0uwQcyWPZ8rq5aCSFWaspbPJEdG282znCqXpunPrk9CLYH3SSXO
1q/1JghtEVj7SdIwpzidDZduYwCO6TQKyIyQ50V2tu+xB3QlBrS8zNKJQuT1yzOB6tIPC8WzhZk4
VaZ9a0kMw+/8spF/teXAylOICZI/jDmIB2CbBomRuE1H9wWGNJCFniZCm0cLt6qsEWgXMHASAwmD
z2ZI+6Sh2CfvIgk69xe950obOCJ6DJ1d6dmZjcpw4Iz/ycCbQ6A0OWaAzKNseyG4mierV3k/gg+O
Y2/3Zv6nyVeAJSMtxj43pbCG4K82BW7o7nGeZ9qtGUjrTpUiCQxLctZQZFnWsX5pWXKkJpp/f+Du
7TYFkwDjweRl2s3RNJVCWshQ9+H5HXo4Il7RTlmGQ+RFAgx2ZmZnkcmIxi+IwURfIAaErPWRdg52
ITwWCDghOKO9wA0Lvfmj/w4mz5631C3ZN5n4H/KCna34b8YN80Z1/QE9E8AZ8DgaWwrZ3Tln/kc9
Qe+f/qkRx72aevXH0at35xtw4GaS8UaD4tABT0NWi/Nz8RQHcjLgN5K4kEMq/tmzpHNIkPpnnvV4
nC3osWq6mxXymS9N5QTZuiSSPUyykfLfltPwZ3OLRkSNq7O/3E++BWVzwj5piHDBDpOLnOP5UiZO
rJEJr03t3C/UtzOq4ghnN95uGKffe3pRWgnwfIJo8vBAuazKScWnqmU8s7+UJMBudZh4I8fVFmGu
rA0HvDF0Km5I4V7QKtwZ8peuBY3OLstypUXsjhIKvb6XrEb7dFTpPQYHL6JDTebNWGoOpzMfHWNf
YpV8nX2/e0lOKrEgdrLYjnQJ3vOiQ2oxAS+UBQhrMH2Hon5clnZSeVbL02CpMe9/86u8OCa+WfFK
eLvvhxoqbDhINuL3P1kfswyFpS/BZ82xl6ZksYFGbK6i0ZAtKJa6GD3yfG7+K7vVkx8kXOcRrYXP
2Sp9A8myaozBq+48XdzxICE4nMYwj3NwcheCrN70Z5IlyTBzBDQkMFnRKKFoVW2wletzZNIyaHlI
PzXmmZmyNmLc4P8CDS7Idvyde5jYrYEviERNdRgxA3cZgzW/V/Vhvqyz0/981mfQPgV9BH8fixFK
m+DeHs4H+kLA7x4jKCAqvp4PcTQ+3qSzmlQRUPY+hVC8OHXOJ+CqGi5hCq9iDe7/nau/aKY+G1Zc
i9yGDqe31HF11do4gwN+c/ZR3Nn5VEgJFVm/9pMxEl9SzlSHWYyXPLlk4hDvcB/r/CSGtrel4NXk
ugDrj3qiYzmHtuX7JeEFiH4uT+W1FMpgyRYiA4k/NZ6DsvoYXsfTnFxui5CeEWpoWUxfah33lAm/
mXfIE3Wle/H1GE3rKMMwnbQqrx8eVJMMGIGh2AnvQ2MdFtST1E7j5nRtzqUBgWEWI0LiAP5cviKa
aUReB/M0SPD4P2f3lIykPOWu4mWbcxQ/A5WNmDd0NAm7A+F90JVZ6Udkb2tSVzxQ2//FokXpj2h7
LFeIRuUl6h7QO9wFcaX3M4h9INuaH6/LjxG/4b779ys3Oq5zP9Q69OLmMZ8gfQHfXMb03+rp988c
A/3E1OEWA0o3MlomT0BiGbHDROyV9fdKcfQkPsQpUyMBNKAo2tOuXGZNrUYQu8EmJ0o0MNObAMzu
JqqQ5BNnBjvyq6KaRuyfznyeA3Xjz2THvfnb1fjx16rz/h5fY5W0GKnrwANm8Uuv2NAD6g2UzA0+
CG7JCypzdN91OKbUs/AKAeueCc7zf6MYXvEWSZBlJeBqyonsUXyz59OWUMbQCLHOLnNmSyOmnWO5
XLvx0r0aTwDO0ctTU9NvhUySsl5+yUagKOaHiZUHumbOaAbhGd2wEgDHd7KSQUCwzFwQXnDsa+zr
9+MuMTjubP5xjNkJmWQhqMF+RPFEdVMOu3W8HDjAFVka0kPH7gQHpL3++8Ec//FB9S9Xynruzm8N
V0IjXTxcv2OLBc71yWhz+fvmYcSTKtQ1vwZtoZWrrOKMjqFrd163tm86Xh5LMa5spSkm+p0tf3pL
3RmXrdS7br10hufXbLrUd7L4ZHWSIOr7ZJwbMeJzyhfRPp9+AIOD3eQsFE199oBXiWOjCCXEy1aV
fnFxLIpV1AkHqpZCZfcsStrSrDAnKorwbcLFFYHh7n68vhKGGvx/0b/I6M2wqBSWOfPXQyxUUgi3
8xUCkHrHQ4s9idCe2PQekeaqCenowDNjeepburTRpMia8jfimCgpLgzBJM6/UEfo5nTPkuZxQcIG
WC1XeBXYEIuowxCqCF6Ilp8PbjxBmSdGjzhBr2Eevmo5ZzhPIBt/ZaHrFOTIeqjAS1bK36RD0olf
PEEPDQrAcbzj4M8ps7cLBzFm4YUeQJn0te9tYjS3Cq4VlmDi95g1Zx9tfd75pCQN1bl9qpwk93rE
lXZReiWQ1cWu0a8TCXUPgtUr+rfxNKEKaXA8nrMGiCa1C22kMXEsG1Ab6x6NGQxqpA+FgZoVvokn
tONLDqv5EbEkwvk5t70GcqGyEKY70ADTm+TLomXbHV6/K54fO6n6mrKpFOJqD2z2bBxx7kuCgPDW
66gtWmWYIKfh9SIEOJFH0ZAtJI/eAetgDqA48vmgEpsU1RkjPlOnDbxfIUJNkU3vvELzOIas2R9k
7aQGlHbAJMRWrXKlrkuRY5ewQ2CTEcKDPH3p2lphJmuRHXkEBNFVyU2Uapgmp4YUs5VKHjZgmR1E
NhCvGTifm/dExXcp27xO2KBUvkIfG/ykHJt/1O5pWGQvIidZ8qIebAGzdeE4inlG3l+x7yWPyBfa
9V+glqKV4R3azXteWslcdUZKrXaSUcEgLaajNLyrE+WN7qMyg1vS6Gp7n9pQiuQmNPDh7frTq3dU
nkFfjrh5JBGjYmSEG/t5HhDHv6v6RlU5RWZwaMVafANSB/tCAjrW5g3+TcnnlZqovvV09KH7JX/F
BD3PTXGkS9MBA3rNDxjFmg360El3eZIkti0zKhNp0Zr3Clb2PdNBNDU/WX8i7A/vV8Mvs83FJtBo
dkTcbzeDJkxT/92chP+FZrOcxd177oOsyPsErXUEljp5Mi0BJsaqfv+NEQa1lSpc77jjEpABQEwJ
LwGnPcn2F7PVizJqpJGFgpv/JptlqLFuhJctv4UmRT6RE7x2AIzIGsmz557J7gLhbJrl9mnUCvrV
nZyZ2wQ3MftTmNy4W7XVWv5/S1+6cfceaOJsPp63SVQFR+xHnA72KfgOO5q3nvtkQew9a4Yd4jtb
WXzET8tnbvCbzw2BMpMef4kkhM4n6bnBmNKE3oKLfEWyAs0UlQXxylCTee/yeLBYsKJZ2QbkGFgv
aXALb0Jp6uIIQaiomP4b+fjaUyYacpMKo4+y9YItp8Q/ISSrPYdHkdSzgsC+ijMvLHPSg0ddWDpd
Jk4pUTJt+nta922rWRehtCEd98nEINhsMT8ecHi95qQcCrsvKxKqjt7M/iA2JvZYtX+tM2k85Zmp
GF9lYe8ehFchZM0A3luqgbhQ5BF0HJKOKdGorHinYKZIbK+pSMhx4OVrYifRSNM1HKAbJaJr4ll6
XGAhg3DJRMkQl6+yswH0oz0ixNPpTqg1cVaRhd7QYoQN84Tg0g2pWGLQ7RJ01L1/rz/MBCGiE4lY
MGoM1yHmGChZSTIX786cAx65XDxmy5nBgV3kMBoxdtfmTTqCkC97Nyzk62G/Qi5BcBQZSVrnqzGo
WoEQxxDEa/sXZDKWBBaqSekgsz+UfbucivpYh+58suzh3V7W3XPC+UM7xK8y66gGEv41yCEK5cJb
Jl+R0/FHNMlonYUqCSC4mBh2szVQJAgGa2d4vlY2BOIfUOjHvyz7nmGX7IfR8KONqGFT2EEgVdyP
k7++YZkRjizwJIVqppkqrWe4XE6Nus0yuqZnp8AlBElw19HI8/plMuBBSYJK4BHOOLkuCEA3Ls1u
30ImijNG193IjevvH4Vl8/T9tJ1FBAPLd/ytW3+OxWx+s5m+P6u0GgJXNNhWs7vEzxdDlHfSiNe4
4H1PHTXVTGLevT/XFTGxwXWmiWgF+z8T2sG0v2AcMIOBRPlyomloSI5J+u1o0V7EB/D9Wguxo94x
vLWcREHbkaPO/xoDs2Rq9/GR4juO2FbKo7o48QGqF7MGScHJurY5bRu9S2ESdnpklhHQXnG7zDvq
qPRLOPrZD2by8U+oUhQfA/6WmIyfdwvNp5WlJ0CGO4/oCCTTScMthImALAN0DGhi1zNNKBCMHS7T
s0yY67IGXCwM7BM+J9AmXtZIAMn0QQERAq8CGPxaaYf7/ZosHUDLyDsN9yxufHE2WtgXwAri6pi0
jiUmd6zCE1YHYREbTP15JxlwniaaOcKtgA9QTBXzGIlfyrCzLzQBz7ZJ8clK/zoazD0UGfe7Zgv/
VnGG2zLM7FLOiSoBOSt/RbvbOTM/OOR6U7IDRVA+B0Lev8qrB/baKmw6BNKQvtEcXUlxgqaE9Sc4
XzSOzT4CX93D4pdMuMEJZ7KV4VJiL4MOXbPo9XGVyjJ5eDHPMWAxB47OCQv18m1/WgK1GEjYPNGI
A4cyRRWk5/EKnwJylGOIIhkJrHK/ZJriO3uzdh/w75L8sKWC3KQXrPWbTGEQdugqPzFCoD4YYPdg
aa0Qf0uBECYZbYruxPmWpgthx8Mfs8FBt60+26CLGI/wxzFSTY/Dpxw4W6/HbnL7oBOT7dvnPdrX
37Z8qipfd9wsOm6PmMd98fgcnGibJ4XZ8Y1FeVITxacBK0+a8PnW73oQR85eb9hezh+NzqcaPbZd
Y0PSAWvw9ruS+kmWl3t43Uu9wsQVYFNnUc/UZslqNiriqlHYmmyDNqhVfLrnSn1Ln9SaT4YTDNKu
bzc5fFaWYsfuXvT8G4HX/Jlb8HzOpV29lhUZPyFI62FDghwoPvdSUs/R9xlrHQTfU8T3LX1uyxd3
3S77ubwZmH6QPQ6w+36RlPZEnkaLt22cocr2xw8iu+dn4n+ZbsuclsO0IUB1qtmSkAvHVpKUL53j
ASfO04KdvHOk1lsp+vKHim9W6qB+xBJK13chmduuRU+bFtp7vhqV4Vx86CybtoCZ61PUq1aC9Rfu
P1yfQhD+akbBPi++7ho44hwSn8nWtO5rH97QA7Bfmh/5MxO+VHQvhGJzQr/XtcfdJoMUwaZfoDZZ
huCHJ8E9FxqmCK1Hu0mKO/ZPu2EA/P85TrRDN7peLTKE9ChadZXk4Hs63niuIOfiKBTcKDYz/urC
Vv+yNxEfoQpbWdo4pOKiHarAMoGc0AWgkZB86tBWy7s14xbZx9ZYmnOx2Xy4lKvHfqN4lWGoJP54
dp4NyxlmVI4fahgbxVD4cUdHz7yYG+pd+VLW99HbRngUjIjbGZgOd8AQKhV9VW89JusIVQjSDzmR
GhFmoEu97eY/7wsObflfxvgzSUp4jbgIS9sAsOsJr00YHZI8vqiO3LB3fOP2WIxV15PCFj+jTB8Y
oJXmmMDmGXBdlbcbdFeg447q2x0ECIcy/VLounfl++/M/4t91d98quEx9LzGn8LEyu8SN3kzlMQ2
P4dROWyMZpuvxgaN6UyG+640aBgjKPTAUZihM9zWGMQrQAbtSVjWkpcC1KguUvEA+1vwrfNVyOrV
7f26izVOUH/OI0Qm8sciDpqb7cG8nfzjfQGuOuIpPUVA2aFeqeLK9FJft2jXrrqzsIMLNBfUvlKZ
PlCx+ftEGX6lfF5Qe/QSztqkDTOwA3GoupaIXKs/wvrKjiDHUFEeWb1yJnP3vH0Dzniw1Ob4pW5M
YHngFUaqv5QuZbv+te7lUwZj9IgNjNVFEVb9yWgMRBalTveBvQ2wWmZ/VeKas+lInlY3sRRK1eF5
KLtb9GPcEsuOvvtc8kU5ubn+9PGjpQTgCwFOnxMLJ23gn0H01kG1M2Rezo0cVmddYN11xJZs75Py
alNiMYFon6q7f37zVplsbkqSlM4aEJB7cQyjGIr7IBvFbkdQ5eSPpFKmbe9hu2S08sgIvZbMRrsy
oriwEvG0qjvManSTKtctkrRNmlMoaJJqXQWNbJZUwjQEriSAx/MCXZVlCmDhDhSudBiH4M2COaq5
i3se/CGogbNW56EDD1nMoihHYBSj9m8MRg0wjbUKo3VrWOuScprruLOgBwJV2DZE4/hwybf9Y3pa
Nh7WcwIIqCvm4rDJZ3wt0uAvE8L+kTBlYpulK7TeihJu3jYHjQbEV6Pr6qiKLR3CQUwG/kJWFq7v
3XmpBb0/QFfr1HMFsSZNhA/WIdSb/MMIb56vmseoeGcFZpU/gIqhj6NnRA7G0dSQz1b5vqHZXHSt
+WaIQXKQyCGpNsygcGEIUOUuHUPufKtjQz8aTGoprY0vK/7OB36FMTH5gsrFELERNc/cVW2Q4xQA
ywNT0xY2oEX0RIvfTa4RjbtRv2D7Bxvsi/yqdyiLIdLBXvgcox56ZSJaEIIN62+WnS61HMREc5I7
FIG36IyiIC7/VzuPaYp+mXzsR+x2s8gQ+UVrn21wx86ERxUOtx9QPpkR0FVQ5wyNA+DefxCmwES1
un0fXj7ky5WCmodPZK1KYzXMgNpHYMYqdhdURBaR7VLI6xuGytrJu4RjJH0+/MOVR0bVP5evolOg
e1jhlpubkb26ZFsROPH4K3w1HXGy2APor/mhflFvvmL8fjPr/s4PULoKl7p6yYdkQ82GJJYu9gD6
YObxw28AIakMRqvq2ThQM8P9DCcd0TfztTKCWcFHyDqazjFO0+3P2hY3F2MHpbyMsme/b5jHEhli
kgHha6FwFB3W4nUoPb8XwwHH38VRHQTVu7UDtm+FdjDd4f4JZ11hX33FVtA5AuOAzKGsKZva3KG7
jegMkeUQXxdvMyoKveAQX3Lth01JIreSGKkcuuTcqxtet3DIAcLhievUXtCzyWtnc+itDQRDaHUr
ABoHBiIaz2H/39GgDkOIUYUx1w0aXhjE909kNesgHdI8Jpdj7VIBy7pFvZqO60yatx8oOL0ZNi0/
QHV4yUG8Gw1f4whkuLurZnS8vejhV3IrkNNlZoNE58BgWuPIPzYU5WRSeHfZWDUu96XPesrdYA7C
jxrZIZ1NnCzpgjnBmUdIViqmNxi5nbPT6RbT2BRg/Eb0Fm6Ep0r1/arUa2Q3mYvBVf4GTXdKL2VS
jIFCmAGQMdUpzws6UvuJnW5e859yArlKt7AioN/xK5YvyJnoYxkAwAvcs0dX1yshrcmWYFciM24s
yowd3DPh/eMEov1IvcONO3viDrpNqnIpwalOqEPYXHMLq/LPbzBp9uunj1fuXE82qA7vMT9lCS3O
ffWqstp4PKsq/eOVKQFqUoJbXVvWzRvc0Oq5SV7vyjjIZKVReHoKr2k93WVyf3cMDYqMP6WFs/IJ
kefG2ZQ4IUfIFAK9Id2m0u8OII02Gg02nPrN0IATz7PFPgwOMk5/ch1GsEbBucsWEneem8jCOrzD
NZOK7+1+cSkoahLXj23XgoRyDWPpYhHnxIFdR/H6sAUJee7LKX7hrw3iUu6rMuVw7/m3e6Lf2Siu
CQrdJygXIU4Xh23nG67h4ejkTAkW6Q0LThqqTdr+YslEmWJ5ph8n3JdxSDjEG8mbovhs/Zzs+DoJ
rS3Uj9qliw5vCBYw1lQ7ctni7Cj5Szkc0oFLcywyxVzj7SaQXuhXU/g7iCYLVDHYJjvIK4rFKq91
Ut4674e1g6xccFecwaOEzWO2E4EcYi7SNi4UqpZeVbX+x1Ttfmj7jDJSWTlKDfRjLPYoM65po/OJ
OwhXOh8fWpUUjZx/LQnfib9jOlRd/iTNjjL10sTG8C+dBfhQXtfOsjN30ywX12gScyIeYOoEQmbg
UWI8yhVd/O+2ATP+Dk7GVQF8wHgdBjjnrVKLX+0siWe2qeqs50YvoJAgZPpH/9/elOL4h62fxB1q
uuJG4WTYRXog9l1xcxU3Y1QMSWN4aVWPGjzcXScWKPNtCJx/eKgFzDzp66gAxHo5U+SQahYBut34
UbQ/vGvpKVwBJIjS66hRn0DIVAzMRFXRRBW2lLT8fGAHhhjHZmwTeyaRR8aIKs+ymJnH0LVgsRzS
fV/AvwFvQrSxlfKQNoWKZP0sb+RIjEIJPW8GN/9bX/jW3jFb0a5s7oVroQ1O4ahzdvoNCp9FRrYa
OEiI20XZVB/ih+GN1ZGbErBnWOueeRf2yh+w+E2jExvMR2Am21HalPeItVgVl1uJFF42Bq4pi+Qr
enOIbFd30kNTvJ3JQki0L7E6oZzGRfWfoseq2uEMHvOXEo3YxwCndcFe2GVBBbUndABs0JH3mNaP
6WNLh+UfQBC8PV3r9pPoDP2h5PYgmSGT/KCXaH77uzLTSm5n8W86s8g1nzswjrSgPXJuU/jR6fGb
UOvzB2muXzjROZ5OUlz3cRyRsJjrtOhW1rTB+P6TH8EP7cz5tkncvtI4uSTU/PkCw8qLrfCPAurt
LzO4I3wmhXb+aQCqdlfcaqn0hjYt4TqbuWyMijnGf433AqXFgfoVTOuOAAUp6wKTcZvDi2clayFV
X8e7USrxTCDPlmTum0UXD+HCcEzi7XfOL3/jaLK9Ukoa3sU8r6C5422kNLh9oLheQQT50patHk5y
qcq1NhW1BZu4vCjADesJowJFtVN4ecsbQfpd/mQjNBOxKN7+x4SPpvzrnPT1UtXqzTOCECINrwyI
TS4Kh5lEjR4y45nDb+nRRJyI8RUXVGsr29jg+bG6VL0sO8rI3RfxfkBImaGc+jm2tussAaRZ9AFn
G97gSLDN4V8oFF6XlzCMX//gvprQoW6GqYvM24GvIxzqQ46XfQ9TEQDOZJ0DBd15FC4WhjeHpsL4
uStZ6shtgXofWkmaLD7BZdePEZX/NLvpIZYGtLNWYD0I/O8vkr9UUfj1Nl3WCxETJIqBI+pA0Ro0
GZxUe6FPUMAg11KyaKYFofWJSpE2d+0Xa8TtncS5ZWUKQd/TN21nwMKpdQriw0kd7x6VI2pFjrrJ
nKnctZzBBmHNpeVD5tEpgKb9uK6BjCihbCXsDPub9HOtugD6DC9ZPqUIALW/yXEgRflBpKv7ZJnt
Zex+MLwtCMl4WOeFH4dFKGqcHGn1qOkexvH9GaLY5zGs+vKqXyn2tPxpi8BKfziUSSPdNtQAtgSl
AFvTY4NIEzqXJRiUAfXX2XYzrnMgNlsm+LcELoHiu5FhaqUkiB4fMbSfIGYHrg/AT1FS9nPji9kQ
8PZnMr52RfwD+1Y01EQ00/RNpnqp2eXtNfNCCxkisRkpdm+W6oVXcqvHnmJjVvwvBSgstWA1eGrv
iEQCOfO+fU0B6nAd5gEtg+D01sn5zyyQ4odC5JUpiw9V/nXVGNkFD6aTGm/a15Xiz+1qOFVIkGp/
hRODZHKssqn0F0Jdt3VfJf+cW46K1uhEoi/MlffCFeOd4xRheEnVbiPSTW856VUNQxyuY38FmLjC
/RBcGQQ7LOw+rr8fows5T0BRrCDAgq7aYJi5uFDGbRs3/GBMOpTgqODu/lcTz2BzMPJU4z+r3MdE
Vim1eo+pdL3GLq4qmBV6EMfymOVzOqosGl81hsoW7HaVR44yY+zfnOcTmeAIOCZPLSNWzLntlkfY
CthXnc1PSnoQji9+/jguiM4gYtcHwmDuJF7PO2r4NyY9kXJA9t/ZKBzFC0IYAkMDcwX4m0dsH67r
vzIr/wBQGCj96iMVgY1i2mduhbPI6HFdgzlECXcfx+/SdqB1SRu7/MeMb8dlBYQf+azQAgjehpQc
eAc5ylyrazoqmVuZ/AVwLj+F5Iz3J6fW8osi9Vgr8vrSOtFm1E4q1LqcywaUh7NlmC7OJEYzuPpB
GGfzlJ/57NfAqltlhgzB8bUZWHQJCfC+7Jli8Qx0aEefymEwOO79oqdq6YSjYh7ghwcp7QOoWQ4+
l78KmsjdCBx8rOmv+dRAZxMScOYsRjnw7Xi29EUpzqdk3j/gnqZJp3E/DtEwZqSOk6klUmJRT3Fh
5weKxgJUTOS4nLzQDZepsZthbH3u7t4bRET/XR30y3pfLuxtOyMK5f9yImfJ6eYSlqeqfIyr1HUy
RUx2BjuaIHZdx5AeduadUl+A7M0a3xIGCWKK5fCdMxdvRVlVeQoYlMci05qzuqNTkLDOPkV5HesC
/acIuXXFojTyoEdAtRtWShvyX0PPE+OY6ELfx5+dAmbztRAgBMvEOUrUKpSspUtAL+AkiveC0LJd
xFdie7B0yBepWkENv3uxshGAH67cL4TiajMb6Ilt/dMpSzfTzZNrwpkyEHLBNgmV3BBwanvvgY+A
HJA2cAL0F3LShP+VgXA/Oqn+TGok+tZFP6Wl/xwAA+SP+XLDVfpxRHgvbbnT39yFn9NLvHkgFg50
B3GLZBrom3PUyYtjHLUwzYXZqLadKQW4hZSU91M97IAviOdzm3JTRXRJ44HIXPSyldVEvJ6E8Yag
zeKTSI6KVD9c7JesIvYHuqY5r/VueHTQ0M1kfFS0tER2Q/k0BoCm+Z9IMkaX9T7kV4DbEh7VNI6n
A/yHxrMPHSiLbn3YJTNUH1ZlS87fY3G2z9INjT1Q82peO23LE8y2M5zP2vquVQUKjd8rmVHG2ynI
bkOqLTrzQGQcr6kAvKzfaFzOqvsJNN+ElUhxaN8HBtAzroiGh2H6QEFeXq1evoheD5QCtmKlic+E
aSvRCLu9SEJUbNnSjvNnx89IaGhvhBJKLG2WAuAnZWR4+CsfwQ3R/xTlb6/POV106wgX4TWUcO2f
mvKBb0HAt1aCS5OFkLUmqH+oliRYoqIRYOc3TFDRo/uIjnSsLyBcLNw6UpBo/PszYNsaK1Eiwetw
epEL+xuS5ioNl3HQXIJJFFr3V/RiDCTCqYuZDLIc7GNDy9Z+/vaUS7eTsd7xAh/IT1iagO6cwrzZ
yiubH+cBd7Vtare9u2PEFK15g2s9xmQp72RmQsGnfSjYWRCuWs3lzx0WIuuFjhfbLFk/TuHI2TkL
5hv5qeOJqoD9VlpQXMF2wbyhmeSh2AtjZ4Ghgk8oh3mG2wecLKfOXTccg82crfrTAUaMi9rmBcmn
U3fzHxqtoeaNdlfO/KCilZIDThztLKTmIRJ33DVC/TnVGxVt7zAGsLyi/9S+13jJNzUusVWVbQ8m
BcCy9aRDypoZXUn+4m4FKOvnFAMo/nhufusClG3+WHiSsmZXTDHAHaRpvPR9ZkQc9MVTgvZLPipi
tfuHQFCQ3MTDXjTuBoq7iBzxb4TmueFYr1lxeP0NAHzxz0wxW3E5YTNH5AlhMfum+Wypn9evYSim
i08msUBSsrWJ8MkU6R+lwlX4rDhm/ZGyitwM8wSORG1tT5E/V6yOSGff6+X/bYBE3ZdbkRXuxbXR
vF+CmjYXF4hzi5zBPH/eEShvgI6Xkh+D4KPi7D6P52aj3YqQQV6uyrPLaYUhsPifF5fElMvV3KyD
Y4H5lCnzivEsKvxbcSyw7gSq0drw1AFwKCAnOAlWFVbaGOiYk/ozFAyn0nMvczT70l0l7GAQgVEf
GmLYeCnqEnj7JG8bP1kDa6sIH6d4cC/AIKgR29ZBqKkHflLZEU2e6OTTIo5lNbyrIOmpEJT04yZz
BQhWuShNPLFmfmCVNMZ3SvMJCYk+wmChVMZ4iYvLoBZFZJr+V0p5coSnRJj43PRSsPYezfZ8UAgV
/9WIfUuei8H6vIbUBGTnCU7CuRBkn2F3/WMCJzL7kmLh+njFMjoBqNyMZcozVt7qHPPcil4vVpqF
DPjziSoSL5vefv+DknP8H76s9r3OnNUlp16keX3dyyfkPGWElt9PBHpOm9VWakm625lqnUSubDxI
nqiyBczLEOzU8/iXGFASb/Qna+A4pPcAqdbIAHNWvnWFPfoSKWuXijEgyfhU3nKAB/yNbwHeCX95
kuq//j7SE8MK/nsMe2Avz4ycxDX2glc4sr77Aeh1gMb7NXQT6MrGqCX2+SN5j8VkqwdwXgCU/Zq4
BpA6Qbijsh/Rk1DhKyNnWgVortQfpCWzDO6WXQb4MoGOS2uSnuXwbIpRqEJUjNbW+Pg4VzjXVs2T
a7PgwEsbJZgGJACapLvhJmxx/hyFzFuKHdUkaA0oGfjr8ShCkDOJN+0JbVylBkoSVdZzZPIBhF/l
Y8HdU1kKpqyHxANucoUmZFLUu7w/UCrydtvQC5SN+zcCZd8rDO3Wq8wyJaGmzVRvkSvRoYSrk7Sm
9WoqKur57SMg7a+kQ0+7ET/bQqRIt8OUUL+9eUJzOKTnMENJXPS+N726RFO8XT/NM2nJ1yHqRHjo
s5gXpdta8DqcTibrIg53kGUzSnfTUfwM0F5JqsLJifvHhQY/h1cy4Njwnk0XLeQk8F6x2jO4LGri
IKiY2OzHVXf27EpP+q5X252ihBJ8zaRQSre9P68arif+Nx2lp1LAQuCboRiu+doAtvp1Z7sIXzfX
61dhOu2RpG+SmcB94jRfdlLTZS+FP5NojuWzg57RG61gD/7aZRjjFmOThnNzOrzcNOG349P9RyQy
85Z00ZiXnN8j8pG/M9pPLouf1czmKYpyakfaqQRnPM1eGOLUYQOfdSlC4c5Wlkrx6Id1AUHVEaOA
zlM5+p+r8jeqGRko1V4BCI9Kjo4vOq7K6DUu3H8cVMO5ltqH4AIe5cT+tOXQfGNcbEo/pt01+VDw
fZjaVCnlgyNmrIVCteRYrm5xnxjFbfcp4Qn9GTi763w9OQQQuPQG/XAY3EahF2Jocu9PCFVX1eJA
0KiY4C9FMdvt4UPqY7kPqrNCbX5OASgr7/4lIw5Qeh9wafPFRy30Vp0a5SmOALBVJAlS9YiRTcYP
H4Qs6HlyLbVBXuCXqC4QylJptAt0DBW2c0JAO8P/3voKh9xZpz8nbDHzRrTXImzbH7kIXPO39dw8
Okjiocdn8854CODSztblEy3rpmDgAAA/qyVRHvvKzf0PUcBggSP2bFvFZ/wRMaDPLf9k0G3WlXdK
zaTq+p1fMdZ9SWoUc5Bu1s4WHtRt7ODcq3Zjf3q4EG7Wt3CFCbCuRL9O9Gb9vY6xro7HZYS9gPUA
2TZp+raRwbOfhDvb5EkGqZaI4jMR8Ej+2Z71ES2HccjESNerKoxeoK0a6x1imisS3uQTI2dkQVHW
KJZ4RkahUCg1TT/zJK6Co/kkW/mNFl4/OgTZZoxRFdG2EfIjPtFz+p4XyEKlP6OdvrSJoAc8Etyf
eHFhWZb0DovB6oWgth3efHVBHnoZfo96oGZDr+8hFkX9S4m72eDNoQsDKFtqaitzNUd/m3IUNWOI
dT3b7HG4Z73pMuvNDlBhKRkQ4BViVOzGwORwA3WX0wdVIqaaMn5wExVA9Htjd5pqydTeQQVgxp5i
ZIf/fX33AzzsrKh92HNvvy6bwwHtbBx56RBsOlBgyu3DbxkGLVd68RUJIM+VJ2ykNb59LZIkpVcA
TdjFRX9M/fzhRD0op50J2JCZfTfhmDack+pI6cDBlkMNsm6MSTgwaB2mzTnfhRgbGU83lxlHxscv
Gx9vcXq/FFlJZ8655TpHCRS55nDZ+oLoItaMfg+T232K6dbhhdVGFZjdNeQaTKFowB0i/utoeXuJ
zwbssX47KZ6/A3v3i+814EVZPwwikhVXRVq//Ttm3+9CtuFMD5GoXOmUfW54i1RsyjXahuJHoBE0
H8k2HzJFBznCLhP18Uk14e8QZ9qbFOz/zuT3ajzVSk+Wlt/dVIlssCtnDE1mXNHdE+IuRR16ze/R
+mHqXbcXbq036OCsxPFitV2GSm35Tjw4Jopap3yIgwvkCuj4ZOLIQp1wPedGSEiB71TbeYqPgUyl
if0vWpY1h8QZZtwKVZTIlEQbijBIdKoq0SRyVsveV7+X5L8tXmrnsKxikBVRlYIrFfBg7WUVODRr
FFGmuxmQ9PJBQSfDemnXtIyFANkt2K9aL1CMvalTEGXIOc8UokStgIvmOLGhBk9ByaF6CJdngzKQ
4Ol0lMcSqvgs94XwbHwj/gWIfOXVcTIQYpyMAxdaDkCt1QC7WKMR4EmRz8X198QzKPx1UXZX3Rry
MRuw6AHN7aOOkSIXGM+1ZjFEcyXXISi5eQ9glvEi8iPaKUYvuxkYG113P5PIS8yzmycCdnJdr8py
XqRQ+lUwZ0YAVAgObXquThriHSPRKpY5Xzq6U1uYoH0LsEjuae29UElI56MsflwoLWIlR8ls1PCL
zOiJCK6uxyig2x/ru+8TKuWGeZJMosdqd5UQjmOQbRsayqaiTADQ97C/pQlBVk2yJV3MfkmcmTVP
rOtIdqeXAbM3pm+pOZkctt1b0DfxXOgU9nJJbIlsnwgvJnAUW/micHg4BfQ/UYOExXnYr654tFHw
gKuIeSjnwSCkuBN2EX2tNQPVR7POGvZItFUhUrKWP8uncka76I7X3NZagspd7/cwVVhG0HPEQ37d
SLQIOgM4Aj1ijk0oT7uM3Kqnsjli5rnIC1V26FUpRckb80LT0EzS9UVRrBmPGPlTUgGxdb8/ruib
4lUSlB44iUY4FBhcGejxhmxMla3qz+zqWaO+qMz2ALQxAuZraUEnS1ld8igsVTzOoVTFD+a/CpOi
eeVkxqa/Y81eC7/Sw8GBDumj5mJ6DBGIkmZ7QyCA+PtBisRgqWShMxqwyeZ0sYz97ileQwpJrPVp
9cKm81+O24xPzfid0qI3uSahvI+sxKJSFrO8wY8UoLPyW96YgGgIt/UTQvuPz4JKXa4IhP9lerf7
7wNFXZ975JtuB9+kfoJyoL11imf/uyMIxPY5nqfiXFnODijQsh3py8hI4yUJ9PYXVc2c26/vfRgW
f9E6KNT6bzZDTgLrGhNjLxAFIhghx0scCsp43v6hDz/XjfvPtygCl7Oeq8Iav+3U9yBu3/It+pZR
ENRgg7eXKEztacbCZz1PT2GNFUwOaFR4rI14yWLAEdY78utvwGm7cMTqQviDBG9p8ZQz0nmsyvMo
o2JGfI1e+UMpMnhwQTSq4ahgTlMDsoCiyk/HN282OM+ZVArnNeTygINB2VctNesP7qLExpteNt7m
3WSs7Xg0M9Fwa1/7pBJWJCfyp8tYUpEhWj4sJxj3h+OhJLNZ8t4gZc3+VzzwPGoFQO1NqeqPqJJg
nPbq810IzdvyTwxDL79S3oiTp6NCwC1+vMRmj8b6WTDaPknfV2UPbJxr5aDadTddyh65IsfHh7e1
C7fIbTvirQwGS1jkMDGz04OFlFGyNRS7g5HcrIw3DsGlmX547yB+v6UJLuCsDClIX8i9nH5K6QgI
JJyhAoAcLHM6UhvHV+inePBojRkSWHHjAIWkb4KR8QgHwHzN+REUCCNuIpjXtzzkM6usQdVwFya4
R9MqpMDTFldjkNVdiJaqmEuKb8OR7HoNPdGKhcZhyGgXUms6l18K1MD6EYAu6P8HzSrG02lPEmO4
N19Duoy7ucNn/7xY8EnZsRFns0zlqDs5OoSrbK/BC9guHfj2G+OBvnnOqS9CZUPxobAPfPwWMolL
OOaf7/IgDod0/Pxpci6TWCGW5BA3chiiOk6KKozOthWg27PYytzMhHlgDaj5XxutFptVqrdAwdiL
DsYUQMy8Jot4AJQ3PXsvAufwfbXjaNa7mYO0mBV/PU38BOErBo7+EvdAniSwAZhEgKoMMmKXNqdd
bC6tbDXmrrFhMEk2ou0kuB78C8Dc934zy+4hx9++wX3hy3q6uEAYPivgFZhQVIuTwLwS/tYekkpo
X5Nnusxd+oez99HdXk2K/11EAFphd+Qmxbbev0CCPNKqrBo+FlW4sMMYPWqPDmlhCROm0JpmvC61
/2RNDHd8yOg+kcZ9OMLTIfPtQAsUh2k0FW4qOWJbn/rXB0FhASKCaQP9X5oH/Piitojm6gYSER9x
ahckf2lTYkS2IhjdTCm/zeLJxsBwtAWwsKVifmwsx/HDwFAlAwStzY0Fdxzf/PgMeFAcOVdvkQOT
c2CDOcpZ1DqKQvkzxJtqftpyFB9sJ0bIl5ZGwGXqaytTQoLmeE+WnDQ512fzuM8pyge2TKLHYle1
2ld4U9TgXNarUaLzkeTLwDQcSOz8peuhC78+Ucg6pFsehI0oLS+ME2MQFU+y3iPW1EiRbNPntwbG
lEy93063qoUpPPc//98bxGFiwyI0M300CsxfXGg6IHCd40SviZHbrSyWwSN7uWHi5KWIDHJu7Djw
IyjM9V2Xe8/ua2XilUojl1c/YYNGR9ulW9CEofNuiynBTVFSdtEQJYZmw1y6Im5mF1+wOEWL3RbZ
zNt2buvaHtTKzsxkl02/ZvlxMC7uYbp0VDcuLARiM/K+dtrPEuZrt/fjJY+3BMFYoNwrTltbK4Sd
XU+L2X9JmGGtz5CqysCL0OJQ+mXhCXlAxWXJ5uYGQkKfzsgQWVNUuX4QFkNpsCUM9GHu1vWrZkvO
+0sCZpFX3TsU+YbXu8mWC/H7Mkw7U4/LrnWZ7q0ct+wB3Z7Zbz3f5wCYS62pXk8ypXPh63EuZFsS
Gn+BCIVwgbn1dJEt61RtMq+oLGXTp5+Nm4Rs9c4U6Stx2H+8qF4hrP3g8ZWLJSn1B1HC2CIKpA6U
0Afa8u7SQNrXnEsHzXRH4VRwi/tI5sibf7PXnqGMJaNCN0UF4LHnOnA79i2JG+hnFO5Ba5CmsGqV
iX++obULJNXXoPBdDo5vWGWGaGQRZDI/Ja4b25I3p76zsjOkAHiaCwUYtGWmNST3IC28Td2gkazN
L+mCpE4IeG6zsZgqecesgq3HzW9bIjJkumEwQEjCSzE2z6nLQ+nQpkrzI2SND7bF5nUIA8DMIQeW
QPD07T99RePCfqPWNLjZoSNUtX77p61+CoDd37yGwXO6ZbTc5PerzsXI95mzypNLBP0IwjEpGxVr
s+FwsAwn/Uuwz6OJhN4EcPT1yqbqU4QHTXsY2uSV3J+jPErk/P5tJkX1vlESfHkZBZV6aRdxeb7i
J1B7fKl56+aIyObvXbQjfJK0n1BqBISDP2e7MEYKkiwQPLz1axgmo7JZ/haEGfEqliK8cL19nJyt
7WxwIU/JYaFNIioEDaXvRz6LobYIPk7DT5/aG/tSQ9/3MUE452RwKhI/ZQC23xgW7WM8JeiHme48
q//JT+YczWs/rRQol4L9iFgnxQkVHGygaXUAoSez+DA+irpLKkmn146w34mT4umSLm3JtPdBe736
zLn7Z5LYzilf2VLbTjXrFTcnA1wMU7Wca2q9RUR4GRuFIQPSPdCTzU+vwWMnKbKIMni4m8ybbMBE
r526Jyo36rIbRQZhVxh49p925kDYA3LYW9U95fsOOR9kdfV00N10SO+f4SWee7G3Otq5c7m+3F6M
uNJWJ+wgmi723DWv48s9ZhuyZP+QwyFBFVl4cySGi6hbN/B2NK1VV9QT9T3ZNZ9yUAnbvsH0aGF/
XiqE8qgRFr7FSU/V/wqaSFg/BUEt151M7mV0fd3CFcBXQLuQQ5hF1zZFCUdUumrOk2oJEZpaK1+3
wbnqIPsJmbCOqwP8DmTY/SQe0fYN6dNy7dBd3SYh/XtMkVFr5kZbpoyhObzX3cKu8x96JDOObhMP
1rWp+mqIwbFzYy1gnZKO/1U9sFVSXymvYs30ExnNHQr2xzMtZSgPtr5oFMIxzWlswegqc0b/lqgQ
HDB5YqldeIlpoU3kTCXDIDHV+9qEvmo3i4gR+H97IT9xaxGEwEZ152LpHySv9ZnfL5nOcbjHusCO
FrXvAn1786HxBOa+rBJbDjcFUYlSzBHO41Iiw8JLynMvTXANYftLaWZRtDXzToBAd4JIjZ+cpMqT
mWQENtFOWs8MZnLWMMUZIQ3R8YJfLELrG2nw0BtttBvFDrt1S2pCVfTTeZsuN/plV6gB7IGP0sjj
q8d89bw6c1WUPyHGFZkdGFBvFHHrEduLtPHzi4I80rC5GKebFPKyzuoF7KkSkziz0KbZEI1Pmn6/
OKiA6j3UbtWl85zZnF0LiRHVKYOHMJbfsTbaVy60wJYXGCiltZ700mH8w2gDXH2z/xr/fvQJPsqk
594g1Py1UuCOFGVy3GVYGsxTdmvTGDmPLDTlja2X0QqPnMAJ559cTMpxIAPlgoFy88J4W9e8GyHJ
yuMKeeNlpVQ1i83rBJc+IszeoDUC2+14OcGnr3WzmJSxEFznZF/uT01lIDYkPX3uV3TLCbvNt2cY
jRt9Zx+pR5ijd4A0/hNg6BCuN49TosHxPbVkabBVSAdRgVjT+MReYc2H899JNjnFyk8tIbyOe5aP
ner574bR2JlwoHCIA0joNKTiQ1H3+sP5jzjHRS77y+43EWHH4Ii396bkg3tDtgVwkJ0fVActmnhI
hYyAGhTscKPIFRgHzaK9Mipoy9PpjZg28RcbRHLqyIZfEaiZsxI51I2kcL1eNx7ORa+YvEiXlEui
ku14PuNr+v51nJdgX5OLXyPJbrkSbH0rTac7bsAMGaAdTExxNmcaWGiP7kT0Wfqwjb3wdwZv4QB2
+MsvVoq/UQfcu5msKmTLL/4P17oXM4w/CY092AoJzqXv7HmV2vO5CpA7X+QGPGyhN8fmHTz/50iP
k/fytzD/5u7sbdqlH1Ipr5HtqcQ6717Wg5VDVrAA0PfC/XqgwBJFrY5hGXXNAnDxGmohv/3eZjcG
zjIygz6pu8kN15dht3bHeOjlQgAKbo7kBq68uPKgAzBJGdu/g30e+EGt40LVC44/yEcuxrlq4SAG
E3K1v7UYxtDvli8uoYP4C0hO66MwsDFmrjs/YoJuvRUkAheQgGJB93sQAdRFwbVHOfTkXpQaBkM3
MMVtUkl2uGo7XVN+G2VtRoNrjSBU4ja2fiIK78azU/o8dcW/T5DnHRrVQvpYEu1es1j2HMmLr8RF
1bvq6FH7k9QH3CzrH/5VD3jFu2828FKJj5S5Ntztb1aXcvdgrgLk68rXSTZhon+80OHL5LMlx8b4
9GWGW4gZOX6ki6/hXFaAE0N9Qqdud89XJn7RINwbwT+UfoH2suWmPjdSN7JNPjYmXYBmBWERi79g
ySMeY/gLx4EyRhjA3xbQNolkdjgaMl0gQYZWvw+UdRP8iXmHogp3oYJyvB481XokRU766Seh0cHo
utHbJzSd7YG3as7QC1PO5Xg9FwXHxadY+rdHswJNINJJC4KhXyUeqxrXkKq2EzCNkbRBbCUcFz2y
MNC9LIlo2bJoQX0g6y331m3LXisyz59K6UN6Rvx2Z3JZbHjIZasxg0/MNCmYM3uGT4JdXQl+8nyD
em94VZhfnb9VPIpk18DD4DcTaO7bgp7zvkPV4Hjwls+0G6Ideh/X68m3yBU+o5QesZueuv8lbwW7
+A7hqMtrjQntYJoUx6ZA3pr7Xb5JT08URlcIoVDWf6KTr8Y462xS6P7pxZR7sMGSQxJU2fEUL7fp
dKH/EFv5zrsj2UvtRxZboLJIh2TYwdHUPcJGm4zaIDF3pmD6dkiA7Yi4i8AGMwYIFbMgIs10rKbE
HSs27cJQ1iRVqRmQussuTzG0/XP9XOV1G+Eboy9fmU1u6eQ2D1TGZlJJbu2Fvk1O+6Gk+ZZIEr1T
/Zzc5DGSBSKvO7HN/1Eb1EXq/XX13/nB4LJqL6AVLDMYQ0e/p8tPKBsBEPlNCl3dnYuehAJN0+bl
uXH7mjqk1JKYVhTOi/69ldvgB/jIVNbXv4/L2RLrJkbMI+j64+zPfyk8w7RHYgtTWaoIK+N9hFEn
+hrG6FykIGiI9xpggHZnsOkR5lP1bUAZzao3Kts8x3phyAHi9RkzhQOLkTkX8JTYjKZA95cvLEp8
i6bnhVIRqnNfy1eil7opEfTkPiOTOfByWJH16WGUEs6pBbN4dLq8cQye+J3PQgIgDRdalcrPVyLz
zVPM1DOKKS5SNnWkTDeiQ5ZroQ0H67FuQgVHZ41ddiZdETkD6WhHP4+PoYVR+3PeDX33YTuX/I4M
d9TRrK6KrQ5TU+kuixIxHbT0vH6J2AT09WCrmmDVfx2imN5EKmyCOtDhJQLPTwjMiKeDnEQA6BdE
3qNkZy3HvSGJ6Tca0QZFqGgiTjg7XapNhgstaE/8Skdt6rEUUVwt6/1Ttc5cB0UJdyqORCAiQ4Oc
JSAsiQSL4zSYb530Aoc0rHqIf4T+CKsHDrJzLlZwMwktfYrALY/jRHMqHd0zizl+08q9jAvsYSgU
SqaUnNEiSoLv9bsPVkJykDwUDgUOJjYA35H2mmI4ooKdZMoIlPuLVBMqeY3VrItxzkNPP7t/6WAc
CqyH4SRbc220SN9v15c8oB4whgYpEnSgYxYH8mK8YOvfhNGrjrZs5nrzbJlrMt700VOvwKiJ2GgY
tJVRRcYjpNLRDz3WZPnmhba3NJ+L1Sx3ZL2NxOggHCD/E3e+XsDfHuvPBLXWAxRa4YyhfW9LGvCi
PkYW630BiJrYjsU+vAloah7JYVCz0QEWRhvXRVEFqplLyTq2DT0fMO1tVrIiDMrsbgpZbq0sXNBX
kXyE0z3uruQAQElVr2TBz+55O0zNGfvL0ja6lWIQFvtqStONcS0FlMj/AUORJbFZhbgODffkGPm0
+XZ2FrUtd4qJ1WPoBg2hUl/0TEUZiA9rDD908qFHNUfCh6P6JcItZxc7KA7y29abnYjQ6y5mbNXY
0VYVT0wQIws3AR8mSqE56g8bU/5agKNb3P68WQ1Pt2IdtED+88NcrsPHzoGG2C5hc/i2Zs3Quotq
SlJvsaczOHAAa02sCQIJxbF9Gf1XpP/vGTOVDkpkUW2HRGLdGikhItAuX/rWq2e7xwifMHrBI+Fw
QR1nzRkQX/WZRSQliAQwYC9rw5KZqHb/PM3cFw0zjtnCyae9rv6fTnz0z4H7nipS5dgRqzTlovWl
XOaAon+iX+iV1NTwW4Zg9RpNJEzjewBrHlPXZofwb39gNQLeMP9l2mX/L/UVVaEy72+cPF+ao/5F
s0YXdaeD+lFkrVppWmFdIy5D24OaPtNCFHh1+VM6I3W6xiu0truIm1Wu4SeNGrTT7AsoFn6yV2VU
ZjVsYs7ld4hly2+Heo3H09XTQU99CDV0cvyZ4Ewkk0qcfy1o5/uNk4sM8YkT4yBqBAmierMmak5Z
xm9ebWqJUkbnNqyoAVAhkO29rjz/I11/bAXgxy3zauqvWsVNbidhLwWumNLirWVQ1slq8s90Jg1q
Ik60kqCeX5x1NdpSW7P4d+mZ2WTsyFNpW/Pl2Ii+m2JPDQu1B7zDGKWg3L1yE/xSgA4KCzwyOv8c
uhYTT9WhjUstfCGcdVSMfe2b5FDklaeZmob7EmVdjD6XLGJb2qqYmZCIreZzUCr7kJtnlchcn7mp
di7I5Eg2NcWPJn9gtbCNRryvXmDSH0fIELzGP3AhcAPNxAIbCudHnWIRELuJ7Q1HXQ5nFnTF2zEr
DF9l5eoh8v4/gyF9gDDvW65DhbB8YdNDV3jo5OhAFPLAUz5muzWnZ+LDBYZh8jifArpOvqrN98m7
HSh5xngoXrw5iInYMWGTuyMz5j20yMbSr4inFuxPTcT+y5Z9cASLV8NEDFKaes1lh8aySz3X9JU6
C1tsLygBLxjkLUc4NEVF2Sjic++4fJiV3t16/37yFbWIdQ2dh/hw8xaSfEXjypQ1vd1FY4WulijN
JlAHN9jVhZerHS9LEns4RxehF30z82fWbsj52+3LXW8pPDYR8IUW5gfOK+Ia9sq7YoXJqCXIlMbr
Jb6cwRQz2aF2M+8ckiKSAXaNzTtoCggb8ZO20gy+LA/HvaoBdZSz32JVymSXuGw7qJRywkTrH0Gq
GQQFS6j9r/MM2vauGhXvrOrC7kQ6P5ug3Po6drrBzaL4qcz46GLBronTf3oXqFZT2CKZEvUpn6t+
FPYIGoNHco+KjjPfMehCtFe4LGWnz5ETbZbvKrBGO+GmPbh3U+HU0QshycsPt7UGZrfEB0p5qOoR
MMGdt5j2eT/wVWmqKhNyK3Mi3dtNADC0/oEsau4KvcHQ4AktOAwXATSQALcevak9YrWlrAERQDhT
F1F/ZnLtAOiAAWvW2U4ZB2mzBIEpOUfRd42ONE7MnVVk5V5q+bzTBP0iX7znO/nJvG5XlMtcjkGH
ef2J7zIZAkmpyqoY2IKzUMIvntG+6P3PjVxV6N4ZgdcPuTfE+V2r3ICcS0/c8g8COVN6UXRyyo3n
b2wnIhC94oyWNoEYaQ+7rlMhcicz37b7hcAvpJIg53uA4+g3hzs7FeuLAYQ8iNs6FHNXK0V3BjPv
uWK/5H2NSGd3ivDA8743IQJpb15eQwMDGxM+scM8HmpcuDXzJhNwH+/Q3rt2+QLz9K9WJuMrM0iI
ii0gDdcI2RNVbAnuRX66XOZ7qQRX4SebwgHXyL+KsMWaglAZeF7pCxVHbBC0DtymqmVgYpxIQvFu
voqTPy3ikwpkS4IlGi9F6bzfaIBci8AGOsmVm3X053b/vzotF0RVcTWXy76s2KTCz1lBPrgdSIg2
ElDsmDe/yrV4GR93FXPYSIkaI570Cgvw8TN/x84jMR7X2hJPOTlP18htWjtYkq+rCQtsAs7xd12h
c3Z1IEItg5MG4ukK7oPRtNAhUHkQs5wSJHTRDJ2Ce47CPfEEsNNka3oKmIVY94+steYE1L5ihC8n
rtMY0SEL0REfGEX09Q+gyQy6GLG0+hBkclHyy5CBk0HmWx6gZBGQmAhZrjfP6AHPIsFYI1oRMyyv
9tu4OmCJ39JD9pg62020861Tr1mJUlYji7KjhgtuBZfg4yiiyZhoNG6GQpZku1naVrZQEWE9LLKx
GPdzs8pnI73eD48AIdR1HCY2lVEfM7uRYmjyZbygclNQ59aR8dFKVCsNJqpmACPiqNyRZ3TUszUV
kks5DzD3Y8XQpwNzwU/XT/eVMvx29zeziPvJNBqty8Ln3l26rbPFQVRXsWvlqQJc+A4RMcoZWOPY
AcjMhsEXkqM2LuaVGfLxxaQ0wyoTDuYFBleWnHo9VqOIsW6BFZGJseWgI2mHA5fhJhCmH2Qc7WLj
75NN5xuRt4n7mL1ID61mz0Xmh59+duRUtEfSx7tuXHJNfMhn3Koozs5oOrAay0PunloE2aVgWuoL
YBGo7IXE7hiZBsCJjr8Wr6uhhvvAoPAG+W0E+XRxsG43Zd0gwV31TRLuQNBvRHnx39tOTwmsG73r
n8tUy2AsyFaHPOonxmah4kvUOJvgTh4qv76t0UMKXqxEM3UgSpA0XaBtt4vVx+ekgh3kQuiRuHQr
aued04FJU8sztQ8aFmBp6ovxpYj6ybfcSqCXgO4qb52CHFSepjy9BleS3P3ZkOTQYRuaaRIdhC0B
dJ4yZrY01L3DIrmXYwcqDgpeWZ8S3rkUG4oLPgi8HCG4rfx35hOeXC1Y/Md2bEIjD2qLSYfevtG3
GzCYMjNudDFRYrUerBGgM6ol0kR7I+pmzee7mVkfhUZeii2fBy2Ei3lfsGniZn0+3NL/FuDWmSu3
03uPeppqumS7ay8jl6OBZ0b8gkpQFbMC/P8pgURFF7MQ29ainOwnLbNrexLIrQ7GTz4E3rkIekHi
m6awpjMWu8cqnO6iLZd/N1tQQcH+wYQSSY/ERA9j/hzck53rY0oYALFL9ZTqZm6pHBOzX5L4dBGV
nIq8aOUW3kNZ4gS/4bBY2uEkZ1dAN/xlR7KGVCWlxaPGgciTEpXlvn8aFqRPPx3ke5NltYkp1Rk0
n5OHx631AuX146f1AXk83Laq97mIsQfLb4JE9UZkbQdVrSwKlU7oRDLIPYpU/Nbw8m7gF5+eYc07
I6mDjN0EnuV6TypTOUxfDRwlyhSe54+DoHXobG++t088JEr41dlUuvjIOm+dwSRo/p3VxTQD4jNE
LszhqdVR86TbQtVNXLuCqx9sKuBGf9N/FDpk4vMmjyR+hvdMzUp9Q3nVWt9k22pJ4+66ezQBfBOJ
JbR+znyG5SwHqx3zSTIYnN2nz2NWHKhaM+fdL3HqKXOwonjsBbDfTs8bowaAIU0BCqHkn97/knCn
fF7C4g75jr5GprMHynEBIENbyqoz0unw3bFRJHEfW10TemYoPy11sbDbgf7LyuI4bVeIBf6POGaD
OXvCINIt8Fbh0BueTTSOs1eRUGLEEwPTMKO3j6DaYLeInrZdNL4yYCuMO2o4FUkYqW0y0uU7jOqG
UJqGRzXJfq2JH0wfDVvpZd6JfmxlXZY78oZ/hVSp5v1KkFS0i3RZuAa4VtGxDfwCegbOafL/gs+4
RDpe9W5Z9xA6fzL0FqucsYeg3Nr1l5K1q8utO2tUdtBgZkRj3ekxUcrU1oSetavDkUsUYK196wiN
sCXxbZrs89rLCSh3TOb4YUlaDTrGcSa8DMjeKFhb/KzmojrAqo7vvsMCnronqqEDliIR6VhwrvIL
WZBm63bvud/KlOkb1lD3fjRg1TU/UCrrHd+ypVHem/Wd3uDWjxnLIWrk7hvMb1wixyBCBzJvchvx
l4B0gOjs1g+Gzi8kbp1ZqwtVxIh7238ShUD/8TVRBOxKh66AHUJUaJ2+MqNlEYATrbYaIKfaLbmp
oNCK9ycQOeararSt9fqLgPy5osALLfx61qNdWTrCGzfUCJ/ttGNHydS857seDXBl4Z9vlV2RffpX
1XJ/DRg54CRbfn26xaxm5xI5hFluvaV4E+Ux5fdXojQfoZdHotNaIFaGMfmAxrKja6qFoIHCpYWN
BGYbG6xE9k+HqHGjQVJnsz3u87WDZc0/S7clCfS6igQZwaE0BmaKjUGcG8m0IP5ocqJVCbCCzyv7
Htm/Lk5bWrHqcfsSxewCwc/jv0wZfWCFQjF76hKt7T80bisWNIE3OZv6tkpADOudi4udAlONlWME
XnLmwEE3jGIcJuI80ywYlMnmwXwSqnHx0GZ0V1lwXJG38m6OcD3RHpd88VgRlLHIpxd7t2C7RVny
DMvEM6sn+rBOGtZJsuIgZmGa+v5+l4A6J/ydxYyfprMRcPpyxQmTrN+Bh3XO86j+ORiKAZczsn/O
JV+ozlmoMA0SysTwd81NfkO9177uPWs0YpmaaBoQEMtGBwy50oFgu9+F3x+PLaR0XPVc2RVAtXPn
zCH566YUKQ1vAPu/a6MNYO96KSdyeTXVckQQVagHQVz3VaEWfNqLgfSE8EQisf2vkGMblS334Dd0
9S6ciRYD4TsxKE/4gIPxj6llU62Uur0nSCGMbb+3VTU1iEe+Y9FofxvJzWaOL2SK4UvMBqgqEg9C
S+vLOxVWknZY+vFZOE50660u5+nhc23AnfD56hppUdhu5pQgOnwTssDZIRzeHRU92AArx+JkxMdd
nEoTVz3AU8/4R1vZ9WjSY3VxX5eOIpKx9WXotSC8MxYkqBxCvxl4RFCWxHRYBPw+d1Llu8ziXMuC
LjNJNiUtobNGZ57eOMPEyhaoY7svzb8ZWcLNLvp2FtpjLvuhwB+2K0H1pULIxUSWK7qqb31oUIHf
5OYGXhIrwgPbnHrQdEYCUtqPMDAKDhh6EbSr/Mi6GJqTtXF5M/3bA9fZjZGysVLO5B6eXEoxZ7BR
6y8R84xYYGCU9nutf3D+2z4nXIEhuW35aWUQ4tHW3JImWvFVoJE0JY2M4JHiY2WuxCyfuBuISzDo
IdhsS8JagLpeyEFV7v+gK4KFoUqlLhIlnYJfmw7qHBXGTgPdwzvO/tl/iHLVM5dtG6XP/8vA32p+
9oAqwHm49r5zl10/JTR5oNY0rmU3s4J1LCMcQ/ltBhzoKosJViV2jgstCMRk79+lROq8sd8ejmml
eekHhieopGgOkPQ9XktcrepdRyjgGOW58zKgBiLom+A5R491zORA15XtwaV+wYISTLmJO+vOsUxK
lRV/X+Sn3KJlQoTJwjHfSrkdwexGbng2ras5FEv+FTxUAmjF+SXGrm67j9meNm0ZZo8F84SF0HNm
SRyVsQTRFY2i30PJNL2hhWqN8XYkaJaoDy3CoKe3yPYpslhdjrGCG+wQszeLsXdWgnKJiYKq5XwS
RCtIuFfSVJFZaZB+qsbs4aXMddbk1Kz8E+2nWFMS6vvtkg5ZwsDM5+dbWS2IrW0ZPhRKAX/SEwcm
Zdcm0eq7M0ecFnGg4ArADOf/L21c6nDxx7mtruxpGaCdGj5Am+yeTc7X1pCUzwzkup3+U5QHpwvG
WedoQF/ItpOaMAoc/Fzn0ATAcTdVZrD/feEresGjJBnBQIlZlvcwXchompKrv/Y7qYp/T+XTPfBL
ZmgSnyJyq1IBcv0jfzVgnbhLJOiQwKWVZKBe9d4GuuWUaovPS/wIk+Cfo1F439rGmNKCHwNJtCfZ
fVI61tSoYRHiGgt9c5/bM6fQAbsXLnbtWIlWUiHNdr407+2fUxAgg+3MP1Vo3HvDy+20B9Fzm+YW
pNTaxpMWilNdyWjsaPoAKnJt8ISR76GztPjiavMam/GpbfolZ78qJg2gHvdgwUkTdHRVURSaP/7A
Fn3ruifWBCTSUw49ppi6cVQZeE0+G1Vza6My8msmndX6cpeWhCi+hXvh5JOyemjUMegUGyP8O8V1
hvdZkPIPuJS7LqA0Lqq2idNaAl2y0oNAa4GdjqW4qqF1+8lNde7/npy+wLqBXDcTsJ1M+pzwhJ2i
XVP6ZfpSEqMnAstWX+TjBYAGeOCt4RHNQNI0qa1gd2VZ7UXu86GGqTl+a6CTa9F7exqVck1TvgzW
WLntTnQxmS1UxdGx+eC7bvN65UUDgaHhGGgeH1f4apblWyf4YPd11Hpgj79QthYZcXvzypHnl7lh
dFRfGplCItB4zJ8UM4NqxHtQWf+8DCmKLXRmFdQgdSgXnpj73lwVxcUhfGYWG34U/ElVHiWO8gHH
dhxzhocDnHG169RdU75J11gRuIawlrf7jD113Uerxx0po1S2z7gilE2WU5Ylz7BNjI63EbB/oYCk
qLiXMiVbpd7xa+i2Jv7ColPS9LL01P9BnM0gLGEakU9jEYbGGZgQspG8QErxeA8e6CxRqxg2MWbE
pq+7fRG4jnx+XWRd2R9ojAvSBcg4eA3p52s3OYkO2Da0cK+Fg8vN1qobcGqpwxo/0XfYkOKdx9H0
c/j7Pakxakod5H1CLtA1YJ3fFXRDiito+yVqWlGyQWs5wNdgpVu8PktYAY4jOcMW3VhAyzOve2mz
mPVEHayhtRsvRPCyLE+Un40QNFsWqWalP4YCFr3Rcgimkt0ozBFZoJHuNh5wK+4zecY2vOqH9uoH
Wg7hiXvUdxPS6zxe6hbxHMwEAv9HpdAXmGQI7nCuwKWRB5s7bq5RD6g/NJtM9gW53tvhk6zYdnkB
f9M6QF5PbSnJGhzGi04g6Oz6UjgCf0EviEmPMD4j6/N0AfCLjuye2Oguo98jk5tjE1QawmYVgB9j
/A3VshJ9yaZmq/mWSzfAFkiGV2NYVqJi6CO7mGCn/Jpp0urgiTZMOMmkaJkBkfipT9peXnzV4jeJ
f2RYM3RXKinU6Ao0nAjxV/wISLPt11upR2dZzYngC9aVbjf/2ZqlBGJ0+LvJHK8LNhOeLlfk5Z8c
PlvmhY2YOSWzAngE/SdSwJxY8daK6Z1ggXtCEpjsefM0FScKJa/Ptf5/h2XZiOeb98HeWTF83rNZ
A8sSWmKmZEq+5l0j/2YhE55xeveeIY+pvusCkVYVWMHlgSilkgnQgfySo8m7/uWBGHmgIBWcyxIM
nmQBpTTEderw+QMobIvHc+6zuU4Z0e7HnFALDLwB6PJam62MrqhNNe7PphI33+QfiB0Ss9pYllIn
kfS6VnSfQnV3HkVWApuf19e4CmaLlvTPsC02Vi2RRNZAdLAUll662pcGiM6H9SJkOszuy3Rfolx3
pxwVuSLgjUu8f7BZpn3EyKx4YVpC6gZxv7Lw8RSS4MPTzQKY4EIr4aeGMxy17IMo5A0oi3KFQgbC
VrH9taP3FQNFNVN03zV0PKyUP7bWTfg4/HNU8SKIw72scC4w8lhwCrgFWVAGIVR+YFvbaXk6sETr
KMHlNP3pFjJXlxvBXqYLOE+/BlzS7R7aQT8t83azXJnKFTWYQa5A5tqsDfZpzt4Au4wkI7jPKZwK
T64XhrKxMXSYksiczT+K7qK+r9v3ol4UN3uzrriSv6SAGsKrzFqeHuacbLX1TmSQ+k2FlT6QSEJl
8BM7Y7RPvxQDLklslIsY//rXe6KtL5VdGPmFKXfzQ0G/H/UHD7ZoZpVdsbor+0cXL2rtEyY9yTHO
6DBtOnIvrxQMLwBhwQQ1VpEQL4sHVZAd+9loMGjFS8ksSdrGGJOgi7gP6DwvieJW0KeewT7+BHh4
NxEpailGHX/RCxtkrl0WVpPHvDN2rYRzjmXF7/92700cWlbY2CZKcI/iAU6mfhyWKN3YiXarGMxa
DqUMtCFMkP8VSrWuTxrwnkNxYWVfNgmZyoksmo9HD9sWKZIunhbyCu2ij/0EBt83VbAHHNDXqXQt
iDH1KmCRTW5tW3/C70xai+hw8RUWw8Ru8w84/wrSJcXFohMq/blQ8hCRYnSImkY5ARsRCzWFgafR
/XiRJQytkkOFjgK1M6iqlISkIy+WvNCY3/h0ZmjT1KO5aU5DB5BoUU3GBfJv4akj9J3n5LM+nMwd
yw2FEoN1R372okDGrBlJsDZsOpDldbbSH4j/+613I9fjLptAjtPNJWohMV6RjpqoABn/4/xg7x8K
uSxa/h0dyHXtD6E3lbdvNbjuVEXAC3jjDIgPVMCUqfiONV7ilgHUxNSyZrq15GHZ2BKc3UY7fB4x
3P5QpbWtLAcS6DCvdd5EIKfEO7TUp8WMqfERf161adFWWtZrNBG0p/Ik4ymc17bb2r66Eev9NyxN
obRklzqps1ZWLr7u5sLsacFtfFK+KQNXLGilzN1Ph9aFlZUL2+Tc3T9irclF77gC9ux71Z3FnkCk
F23YkS6UPa11HQovPByI9x7/e/HhhHfEj9Q/2GdDVE8YqI/3QrgPD8hAxQFqTe524OQkM9fP4tnx
aJPxhlbQ61gBFQOXBCytewdxY7Bctkm1iDS7yI60S3g8i9FgWhARFg0U9uPgbzmZm2j86ERQsWh0
7yNVOsSiAfunbhOEiFINOgBYcTR6m+WE6snvs1RyF4QRRFF8jDu1LXO2CEPEjJMpMSZnwdK5KwZY
UPAEqQj2oGAN2f5C/dao4F3N6P7LSi8oGJjoKfHs3PCL2J31HSfohZt2mJrtmbqVnbq2sffl4QZX
/GW6jL1Lj3W5sTZ8Jh6MtW2K3vLM+lwVV3B14XwQ68oKWki+t1nPjl05suI7SmwYsGBIJKk7RV1Q
ioh5BQ2Ph6BvCLMroVS3D+NrWMyc6d/aG2sndo83KJV55lIngb0pETT7mabMjdc9UDoAuMM1skGg
znrpDLRiVVyWOrLti8tE/4Uch3aLrYI1Nof5REE3vYN4JCDUzEqKGU8pXsY6Ek4N8It6TgvR+FC0
8TLlr9h8ZXemGgNtQifH1ANWDO6YHNpyU4YQgX4gtss84vvIGqWbW3OTOSxwE3UtYdpcG+agBQ9L
Fz8IwUnJrJfGiez74Inw6/Yq1EBJk2ntzdK5dN7oVob7n1XWEmgf9WBBecnAczR9890SG2kQUMBS
OJ1XUAyJYT0/U5SMQlEUgLYSHFHyew7Z7p0YjzzxEcV7MgC6qrk40I2fizTPNV2vK4AgQGRB4dTI
BLYOu/rievi7IlNDLhp4o2t6r6uDilCcP6dFzDZHThxaXo4qIWrgPtRJR8dLaUVQgGopNtexAhDz
ir35uj9OvFeZwfRJ4legD7y08UYi+XjVKeJXwncB6E5624W2p5GlqqVJ386sKX4nwiHUya27N1em
og+A1YKoK4Mxz569jPrddIlesLln9dp94v0U5GRWxPFAm5oBJ1Rzph+3NxakpAIlyTmgj2jm1p+T
1BK2eadKAbMvgmqrXvj8tO1UpKx4+ld+O+O8goa+mJhidwPtyYYT62xZh5z0cd0pFiyXSiYF8K/6
KMup8NMElLUEcYCZR0cLBI87iPafEUCBGHvLsnASkX9AYPTK5JghhBYUXRgRrXYEUnbNw3mHEbTY
T/w0ZmiX7Jjgj7CX9sbxY/yqEyn19ScACULDZFCycIJd7bltGBaNwAKrJqZ+OlMj3cEkpYB5qMxc
106QJb6SNnhqJH2M6oWrdD6yJW8rrtxdk3FJnE4wABY36tYkk7jzyHsTV9uOsRn8D72cbc9tlJ7F
BBVzgWebbDqlc9bliATUxRth9JHqNbrNxF3m6X876SsRV5X95I1ORGjJBUpRfteX34qFUCJmeUBH
8S1pAMOEMRJBcGkwLxN/cHj8QT7dYYFdh4XoTysjx6wdkWtJut3Jr4JAcgfr/12H84N2AuTD812H
5F8lvII6lqfCu75JzhoursxVpvLv2gapnKNFfTmHcrnA1+3Fee27qUmZa1mMyxkhn1Gb6d8ch0UW
q9D6GAtI4HUzuiw640aBLP6Gqwk1QISVzS3zKZSoCMQ8SToHTrE4scloZaHuoyUrZ0vkrLvbkrN+
3SIvIYfHvTmow61r/7FvYszdJnpOCfAoap6/mTtP2w8rgPAwAs/C+98rra8g0cjuNvztCV/UPI1D
WhDZwqM2EoCIxhciETSVO0H56h0NcL7DaV5By9UA4hfqYaYUlwDr2DRE+/Qzsr+vIrfvWPC8JNnD
E/zW0i+2cY35b/hi2TlpSf5r0NE7IstwAsysRXXl31UhxTNWpI1j5CCwVTKHG+VR5Qd7aHdQ77m8
bdTBRIOOQYGZUpHzHU42AKmryqwReJUuO7NB5KLGKhx3Ee/I2+Oy0C1dCoGtMWXMXG5ffxHgJ5u9
UsKVj/odKETDgVU2PVo1L1VKDqrQ7i6j/xedC7rTJb9ehH1+kJmdoE33Sq1JdagdWspJVS9UwYRG
VT7A1RssHljS0zSNfsVMGLSmNi52fuHrBAZhChg32t3JFugO+u5F69ML1c71m1chySyQmYgpPaVV
Y9jPy6hZwy75UHJ9BgGFF8QfZnrvkGBw+A9Q+IUzueAXnn8Dex1WAnNiZUnEuzXDM1bRyrxx0Bgv
cX6LlHGZoF8osg6jLqmDW8v4YniSXDzlJ3UTVtxuO8ADdosFozNwQbgbjnrX6UivgpPyEt53LcdP
fvL62dLJx0go60yjjNF+TVuuew/6pG+2m74akDpQ6pvLNCXTWG889GoSeQpiUOIKPbcn3zibLVqs
DDCnubL2MIN9AT/teqeMM8lBHH4fOxjYybbvISF5ykYEdS0d6dg0QaMtckkmmGASUkrbLUoo2+2e
LTeEGA5sFqLlm5qSFR+mC/+CGeMhRQUPXsvSFBoLX/oxWH4c9I1MRnV+ftLQC4waU8lONd6ej9EH
GQWszF4s07Q9a0NUwhsAZJQNq5tu1LipNBD17MZnZG7dUOoVPa39VKO5K2nKjsm0znnijygp196T
3P9xfFCOtwlHPYAXpjqp3vvx/CxGf8+hRy9nO47YwbV+S+oS62diuj2BFf0e8c1LYb5V04FWVIz9
3G9ybPiJLpGl+5KhBLPM5tmBJSNJaanOTAcqcttNwJvDRiDUUzHl+YAILdTzKlt8ajcw0rsJS7IR
nlIRYTfbXXCznYzPVX0PcMiSJEC6qfs4d37chdwUsZpS8D4K8si8CWk6+QSyA1mPl5qcuh034EjU
zxaFWFnf/4zY45DLHhp+c3Q2TYwF46l97/Ey/1FpkULok1pxHoJX6uVxnh0xrXo61U6rGILZ3UnV
ZF2nvwvRLqAp0G/fqBB47F7VjVHEnAucjqXYYKePWEAOA96YwuzXX2AThJmnX5VvRV1/jPUtML2D
Ne3rlhuntflooyL+4SiYxQSMlawIq3DEqTCpL2vVD4oBqXO6uD3xkI165aGoQyppk9O1xyaK15c5
KR71nCHNEjC+MMlO+sTFIQTqN02SbVXJC0y66fv/SSKIbUHO7pIpjvLoz0yYEn7jrw723yex0Ii9
wBxdWr/rGMz5ZY5KHB196qd3I+VUoeVeCPAqJLMgjPg6CdwW18mgtCUvAH0I3bT80AWXX/ZwclmD
nFfic4DNvs1OE8N94J4JryBuUi4DsxqwEnxUatfHloYwub7ox0MrMXCYvdQHR1w1ehx1OkDlNyC0
QYw9LLHGlE+Wq/lPQe6JO2qdc8wSExa+ifW3IbyCkbjNpo3tpJk1LFIBu6VkB93hbvHymMqVpYvN
v5BjL0Cc7O8AQOyBXTHZMvvjjfqh5xo4506gIKSdVL8AyRTBFdHKCcxD+TKvvqM/ww8VPihx8kMo
0/J7Z3MhfCyASk+R0ePGujsE/MsFK/BIMqwkdZf7tUHJj9b6Bzs2ve+fqyjMbgXuTDcQWoZDJ7U9
JegGBEWVKPKQYpFw/5RJF2Fz+MNBDFUiq0Hu5uT9SFWenAO5XgK/JLWoEpX5xWkdmNQgaMEDIIwg
c01GMkjOeZdWvu1SPvPObA8495jN/be9r47sCwYC2zxyjglzVS0SX6eH3MVxgUMRJfERgWW/ektg
KHF2xeCNmin1Yi/hUS8tqJv+qr1QKq76rr7gZllPKzBYc6ZqChwWc9t/sEC8AIF2ffbYMvD7iYs+
LyrkUdLY0I5VWbdSej+layox7d5NkxnZ+VO4vm8bwuYkx5/oeuH2lHjrpCHxuhmy/3ciQ8TjJzp8
o0bhNFiG7gC3aUKLjnm3T+YBa8TE3cMiry/0WaGVOWkJGmHwAeMf0HVG7zt7ZAOBD8clViv+F7hN
NLxHIWe5dZdkjvqC2eaXyj44wd39pJ7gT3Z22I7GO3MIpyzvGUCd5mVd3UV60MyNOZAR6V7gmK0D
10KfTz6dnPjoUK9s69bVuAalYeUeHBrwFn9rSocd6dIrpptlOCYvjMn89ka4V0KatiiQwUmraAHX
aXfUsEx0ahrTrm22OeAEpk2INykkkF5BhHS9DAGRlNBlGahLNQPuIW5FuI6n5AEIg0PwJofuscm8
3ykS77EjirzDW8m2NoO0PUPyCqyTo2PSbPlJssoED0y/e+ND2UMiziw/44FhgUIJvfi1aGefYOIr
bkj/XM6kYj3FBMuJvs0lgOnO4elaMhPvWEMudf+hQxV3Ji6iXCr7SPPxqa8T7MbHOvEnYf1FMOTd
nD/axAFmfNz7Qz5yqGcLijeQep7H2uID2zQzHNFqkorUv1hKQ6BAwrclmtqQvkA8SJgDRXfCUqAR
LSSO02Sbj8RvQkORYm6u0HwwPEzw1OS7HULGXkpJoDZO+GztoHsdte72IZVGSRRNHrCLo56Hg44v
yuXCY664G1xr/M6c0AyfCQYQZm7He6FKsv7YF38Woy1eENj6wBA5AEuiVkANbc6Dysiu4R7u4S6G
H/GZ+CB0+b01Fz4LX3OzVfeTNNT7fsGEWKJBtyzT0TdzGOR2+uU3aeimyBIjcCEUEggMDyEqB+a5
Mmi/oUgB9Bnil0t6kR/aipbpvLUbncUA2bZQgqm/UOtIFN6wYsT3K+gsIZUbxgZEkfClZmBdRc0c
VLcm4uyj/MMRCKlLhAw9heGUhSScDOjXzDfoJmHIwCKJaqye443h1fr940qxavlUy0xutexF/m/7
BCMytumjjskDF0GmBtVNVdqPPk+oNQlO2EpBBWEaUXSg1GgDjY0Hhm4b/F+8PayKV25vU8998trY
q5Epzi0yqZlv83BDInJNzhnfEj28ypHubioOgU06awmrmGfMH7IMab9EUCeEfyHwhwIJ8NNxvkAe
sgKSj7mx8bxmticNLbtHHTIv4niZs9U7HgTduYuJk5Qnb3CAehr2gBShw/mAFyZxEjmp6YpkMhSc
/2Jyld2y/qYTwi91PZQOg/K5oInyCb2zVDui+ybgLtvyiQy+xrPwTF32vVFt8JdlLw7e++RxvyjQ
oz74BQ34dKUioL1+tmj79qXdeAMEgP9ali64mZGNS4EF4gm7dSu5cwSLDNJP+bV87fiGHMM0/aP9
AEzBoNAGxgyE3r0skY3++vRWy1saYP4zM0kzit6ID/NxlT+2S+vAjVJpuCJxaEGchq1Rh8t0fvsg
JHmAdpskKmmmXJuL2K5P/MHL7Kzy+GJyHhzj8jZ3lrYED5LL0VEi8mt0zEe/cPVLtmKZgl/a5jIL
rHCQUdOFcnPxo6pTPPBhZinWbGGfQuAUtn9QCoJYnsAMITiYX9gSlEyaiwdcrd1WhRCz5iwyywjF
uXtuZg6d2eIZUZtrS6Al2E9tTOdLt37tbekcUy26RzLJvt/O3CxFvXV8i+UOMNbby715kVZI/FjB
lu78GX/Cb/369PZGLGZz2vmy3o0gvf+vwoOQcgw727aJ+u8kLN60+b/a+39wGXMmUj+U+0/tgFkX
6NJ/Od8lrxH3KPXRj9Jsr+Dr6Bti1RhjVYkN2EYpTBlIm+IV0qiB1ygQZaIgZ8MFFDZC1liYqaJY
dqSkaPH5Wl+afdY2ZqwisrvWMpfCWZlaWR9wdeSOSNQwSFerPe+yaaMA5ujP25N2U3kS7J+Q6jQa
U6C9gHt+d+F8QbP51lk+rAbcu7iWyhv8B+i3/OvPj+qAdhAoZZZXYVDUEN48AzxdaJ4T6qUhsnbH
BfEbthjkNNNNGCgzLmT/l0Amz/HoKo5VNX5IHnWeDFU96tx2pZcUyiXN+55h88N16doD/M8/KNvH
Od/87DyXJx4oKRQYMmCq9cC927hfxOlYz2qkqN70tfG7LP3X4i7UKVTlCvG4HEJ1DOvILSZ+/1+/
N0kF8btLn0F9CrZ0X7Ok+hcY6TEbBe6/aE5tbamv73uGAD0Bg7WdMw0eVvBDgEVbFtTmzu7PDwfL
+H62BuHi045ztlRvTqrnIAmWvgTLel7575tRCw3LqFuiWwCX2JWvrRKpW/zf0OEywRfvK7jz9dSQ
Nvj97z8HhffBR2KbYzw3pQK1nKXUItie2UTgda3jANRlBOIzMIIxajBpFRYIVzfevo9rfhqOKSfz
tw5yEESZSzclIH4x5mYYl4ULeVBbdAKpYfi82rvyXMILLOddVczGiYZBxrZPEwMWQDVFLnm/+2dd
5JWuZyNkMiNYjt6pFI48nGrq3SW3SiX+8hH5tFwgyJQh4WH6iHOSp4EuyZcCHUaEG4ZaiVsgg44h
0fzSQ7nj/knI2L9qFQxmEP+EJj07JQ4i4Fx/XrU3Eg5/DQm6yqzZfHM2BK3y+5qoPx4zc5kftK5Z
KR+HlcOeF928A5SKa1Pk71+iOtmB6wCP5p/U3lhsomN+OJsiBefTapQgbn/rAZgWtLzy2P8a8etW
/1aNWEGy1WLm/16OewY+0AOR/kctWByV2qolLnYHdChyaJq0P74RbvrT5IXmUfqbRS5stwPB9Bgl
ITnMF2MNxWZ+xApRKE6xJ0wNzFqHNjicvh/IIRsOAIRBZA4hDNC2FWjZPUo8PEQ8wEsAz4HlE/YT
bKFiKZ5j07PG9MYayL6jFKcEX7b4vPHDJbiAyizTs6MFBhZn11iSXDxYJC7auvGeNnB433Bamewz
GL06huh2hgOjnV+r0re1z4+1SSKqniWHjRhfoCAvRPiwigrCHbw46Vs8OA09v8DE12mgiqiD02+Z
u9Mqur4fnoNJ18dO3AXLKeq3DYjAf67RtJJWtjzFlclEpTRGdWx0YuS1F2/dl9IyMttqQs4kji54
hTKPycRxwQRyhh6dj6vz3tXF7oPRtpAYD4JalypZJ0vu+1VFXll4C0etdK+Wcg0lcuuOkZMxCjCd
8Cnm+yzRkVP1iaQR/VS9tv70cBkCav81gD8fU2usyHLkw/v+6OPoAlz/UT+Cz/+/Hgb9mF99i+nM
1UactOOmQG5my4N1Qe46uRlyJ3HIz6O+wavgRmWacWzsRfoKbuj4TEkW9t/rl1+NSfXRtMer6PzK
yfArR/yCMq2p7Jw6saRW0Fz4YOsWnFc5iN+wT8wDks67y5gpuLWL5ogzBcGMo78eCEY4f/1+EKbk
/LeTZ2Sf4QfAljFScBbh8GQ4sbpRbQ753zBhXD2quQZkdd3AevII//RqcHsomWh64uncHl5u7Q2A
je9pcFqbFGukovjhGOIJIv0hgfxi33v3DuX7COPx2tI8GRDS/jWeMFm0DQRdhotIilTUNY61BQx3
TsOUOMEqUnbKMl3647MxLTjvIlh7KnGXVtjoRMRAnWNfCes9kJj945SpDcFmvskTs7IAv907coKk
zg8ZsoTH2pS95Wiyf7QvtgzU8IqZexCjHqPxhyMEKvbZcn0g0O+maHBA79pcmEsbf6FaZs70zXGr
qeEmLmWQ/3sJ2NcTdMxAwZy0Gqaevw3qCheluzR6e8SUsWISub46OhwphlsWewR9OaUaAktR9Rn1
/8PbSgoXEgl/P4tpMDWdIXWNR/dbhpySdfmCvTOJgZ8ykQDDwppksRhuyA8Mm8hOkJZH+Ry4maLX
dd5RU5u9ZskUXhQZ7cPKJEM8KGiW0Uj3sOPIPOrROvvBPGoTpCkmC/CwmBWnNZUKT1RlNEZsYMFW
ONQuGisv/st2+O7hi0ZbyoYHuNJLmfLF9oaLHCDYpvz20wz1rphG7vUFOJ0TQJKFtDh9O929DOkG
uvgqq5LtiTiKZ9UIeiEI/MxiIbe2whmNwcfDUc4y/XILgF4a7rlm6+437HTzvIppc7nDxOa7eNJr
Asg3Y4yIgzfc/9NFZQDBYMG3DqjDuqyiGgdqPck91Avifc6nRbF9Mlp9iHpPAUWuHe8SC/ycRbxd
senhNMZJd2G+pRK289Uk3Aw/onQr2sp4xB7Xv2eaMoMEkI9kNaSBWkZlQQU+rw0zHtz2/tVYAam1
VisEcO59B7Q1YLA6cBwoMPgLrvVEq7kYZd+r6idrAPIvtukULqwemLmWyhq6xmoixuM8JhECRLGL
D35ZLWjhylSR+EIo2REdqM7IlQtf9qpuMBuAtvrBKQ0cXMIFMhWtlJODL6piRcPVuXIoDP81Jp+y
RjWqOIXnSJxfNEAOQ0fNuaK/KJBn0sEy1EMM0uJUrnBQoTrnZC4KVT/1fjGvWmQffoxTrdhnt3sw
CtMz6bNlLtHHvP4tHIQ0y+0ze1ZRxfpH3L00SoBIJrRr8xlns8yq/Mt+luQQwWmoCh0jGxthKYrR
kvHlEZPyAztlCFh2G0oZEIRcQ+kVrgN+/cm5vOtuSISWuOKTFV4TxVLrN9AKvBKHiY2JCjv2LHA1
ZHa4ISKW2TyGPI/Kp2iyhpf3Lvn+lk0cbQjmYB/gwgUUg+a4x7R0rgkLImq6rKE3u1sT3TslUTUT
KYBZGml4+NN+P9lEVDMVruItlqubBvQFjK9x+yLdh4pPHVWUTB1CsjLbv1extLnj4w5BInXsB2s0
anI3FlxajZqBdCCKsVVavSSyKiO+Krqw6ki+RJ4IzRIaPqeoENia4juWo5VsWoLpcuEkpv0ufgC/
L+kWhVgy7yk47pktpgPSrlBQVTjLZeFgALOoGS6WhKg9/II0XxHIZUkkS5srvoZeI9OFGNbVLRuC
LD/CBwqH7ZdXFTjJAjOgaVGM2BaJ5mqTzc57dlemN+nb36zV/xDIk6ktSZ6s8Rs5wxJVbT9rDJBF
O9oHtmJAO2fbGm1IrtdNDgq6PIchWq8U1CXFAtZeH26GFQEbrGXlpK/HTI0PzoNuSiirvWZCHtv+
CX3PWm13WrR13WMYqTIzVTZVVU6jHZGfmRpKfBq4oOpxw3zeRGGkAlrJbO+MlMgYm+jzm0YjiOdi
toFZNnHZLlOeoBs7wY/56MR6ZBggv0fr9FcebiD/pcK5mvCgZ0RUpQd6bC1EqlyvILJpAFBagD6P
aX2z2oqkUjfUqos1J4iGRI+Kl8NFT/qbqrYabHGrzudOg2U8HqEBpD/rePKsUobjE70WVxkMR71c
eh3bAW2TNM7NRPCxCQkgBVImC/ey+f22c17uXmzCXz+U7DL0w9uo0De0XwnBAqwmZ9nWyRTBj4n3
Pum9//VO2VGksiaCiW8tXeNoRNayVwps+zfMFU6POD2GhjnyN+49KoOaN/pd+8NgMBEVlVfpId84
H+wMdcUSX2L1RQ4tbNrkNk8gXAa2nhAOPnmSQG33ThYHB5BtH9icYusTrrkTqTHpMwe1FOsUfzw0
RbGlbVa/XOw47zQ8+s2Y24iDT48EkGk7IkH0mmHVdNXePaNLfMD0OcSB0CdRggA2X+64GV+kd82m
rWvRNmqTUELZgfyFtlW67+IvjZyE7X0fqT3gCXWNV1/2pU1t8SkeaV1skLlW6z4gHKZaH6UlarQO
eeQunLuEX1xScXJ6cyfbsvmGRBMCBdhUtaj/u+h4RUQZcnA8OTdSHrz8Kn0E7OZxlP7pjx6+4ABy
JJGwuJIeOS8jM5Urr5ZbdKXcLfz9rltDyUoAPoZv6apcQrTeQ+Xj2Skbob+XXCWZSfXN0ZOku8+l
a74UrMxhDjtCTgIVK6WSzwJJRSgasT9gugmDTE+yYKs8bAAQidDc7jioHZ9T1/NdPJ4lvZi/pA3P
SRizk4VzIBNi0uItfjhmfVVuDV2s/kOW9l1TkG9KFGUR7EKH7RAsFrkKngw/rnf+pAvHOG0M5Iiw
xV11U0tytfSsapPbIrJbr7jxsvep81MUjYn3hYEoEpOzw4gOvst6o6qYJ+iZK6llfninbWBkdOSP
jWDHjztQZIZfHIDhFnYE3TrIctJi/jVlT0twrvB08k6HvLICj3MLiuSGGYDQXczoBPPdyEUc8Q+b
99z7ZmNcXT9A7HhzRc6gkL3fhTSb6+LlAvvwphzUXguSM9ovOy8Gs7iaieAME+wq6uV279nEZr9H
ZjBOmDJoMK0sMalMMGA1UJ1+QrR+U61YeqOBXAB8orEEmxk9oTkkRX4AVKcRutDRZC9F577CW1C9
F87LXOeL6acxP9YBoSXKM667bHDW0sWCpMF10TsdefmanQV9myRYpgXRyuUZZLJaZp2ev580aXJ1
V9UhJM1xoSNIFCmAuduO8p45ael//0+2d6HPzw6xwBSue/X0BAWUbA6ciMYC43CbPQQfCA1dwnEx
fP/hayh70E60tEw00wLMASOsPrjrj1i23rVqQh/FISWDooOum7PLPSroM4frZ29wm4goryxlWl5C
RJmkxxEMhvZvRvCmQVtUqDqmiXt5OajKXjNGrbRUPo/5BicwCaZyjfcZKSb5mkslOAqhe29mU4oE
so01oL1YYsllQ+ybpwYcup/Whkk32X+j8fH+WNV3AMlhAkxXqf93OPg6YoxIGpPJoVyG2lw8SAhH
u8HXT3GV3OD5pn1bQLL/qfYjdRrkhO8KLnsxoZo4QI+BurKpeOk4cWDjzS0U5bqgLezFgJRp2cEZ
Aa+LnO7sf7kz04WEG4vpk3U0g3P2sQoKyJf+6J0iMLS/fAiAvD35Vgr7a2HaZgdGPKqIuTl1cPku
Qicyxzjs1tBivZafR0afQsJ2i23UATsibAowdiIRf0RuDEOgGRwhhmDW4wrIYP+GiOxT+5gOTC/v
Kn8awLQHbOIGKmhBWQ4+VqllO4NmRAVmTTd15CKg5ipPRGoaI7dlkpyJb4Cks+iSFT8U0W1sXY+p
+e3RLDZNR/QzLpM3mVCizEjOq3YHfJl6SDHtMEBi02FWevjWicjailQ6Dcd6jLU8W5udNdZjmiRL
NW/QKGMQ/lECFck742xejZC2fXvLCVhIUdVLlQqYV1SPTYssCUOuzjVP0VwdmHBF1jSizK4i4GDb
6d2qZ7vVPpb4Uhpl7hhzbJCiUoYeRra4pX+yTbBqIVhZIKIa85To/c7bafG43HRYSNyiDEcUUk0Q
ryBX4VPDgSTDsu3kbZZ+1QUbyGaAJpFN+Vn9dGw7CVGM051Os7uABj7uWrxXy8A9diOeM/o6v+Rl
gD+QteECKqkGR9IZKzynLj5BtOcZEPEsjor89dw1RM9DZd3akTm00D6ixa+sKBZHAYd3jknVUT1Y
4iLpKQsswsL5GZoTXDKJGu5bXz2xN2+2BjQBxLa2J6NHgW/vU/g+i5cQeP1OjuC/OoPggUknAf5o
udraMf99UtYIr74u3510xTUvgZN5NB4xmUXP5EEJQJdYfmo1/b1NkPBhHSjHD6QMABb2d/IwALzs
QeM7mrrVBnap1KeiM2hTA8NmzqbvLwtEulC3WGjTYjRePg1saVjRQKdPk7el7YXo90eTPJnqUgmM
+tQBJkoDLW6vMBcmgliDIs3yuhz4yCEnCBVGYZxf9LSHOpHiUuB7d22g4XwyKispp4K0RbsHJ85Y
Yirrp6xFcDsq/2wqfFhq3LCwI0XBd5GyMjzYkAIuo3nhN18rrr339uQciWx4AueOfnJiP7MqD6L4
a1UMzlXtq0KReXddGx+hjgXCMNN3upW5jaXRKFgDiq5QbVMNMzJmFhdlARvdHKWZaMeg/hRWJc52
lwn6QkT2M6kTvP+XwD4FEYkOn8C7SyGeuruyoHmUR+e985m7z/DqMkUhCuVTqYuFPMUPORwymln9
gG54dv3t/ylEyWDWuV3LxvCnb/iJg+/1vTwVCg8EtT4Kays5xA2ltBZaJb58WtWUKIw1QAQt0IJA
gW4mgNtTu2fY+3j4DXQjWK2l/jvn/ntHUDmCpX4fxNUJuHeZE1oOE5aLIxNoxnafuqzFxz2AntRv
uh6+SxoGnKayUHQ7dcAzdzXMHcXxfjqwzL10b+VOQPkGYi0NWC8xwR4rv29YaHw+AqFrJLLEYclw
XVpBMlxvuuZpiqosekl27NNbfgTUPaGIJYfqN5a8fornwTD3RVmy3KMkqx2pFPOCK1g5fyrHtuFk
klFM+IaDBF0+ohbqm4HiyyAVABT+HniDa9WCzQUaEvSax2t3ZOME8rDKBaesXwNajxF8i5HmTWD9
qtPVLVJ2EYGxDj7GUYrZzut/i9zbek30/jJC6XgLyMhXgp5uzqO5kth6Mkzxp7zuU3fP1HtvbNVt
ENmtnqGMainERXC70yyPiclA9H7JHV2R5OMT2IQJ5Ds+lM/DEdEwtwLFe7xNLrQrIijMRo791QeQ
JrtflJiQlhZOi08uby/kr3zJqFfoEugb/ARnBrRJ8qAjAlFhWImXfPaHz+5ImJo5nun8AcH9/rtN
inEKew44bS3X+rpvQGb6TIoOZl2QTVQpeDcXho8agR4UlJ2YzzoqyzH1+bntXDWWkvGc295mJLII
WV6SAMJXhySBM4dE8oepaomsesZDryoWKJhyk/rWNdW7sCK7yZ6+3RYLpmrszTutNGY2ajxiTYR4
8Id4Pvhb9u5ycA4siXsEdR3olCuceKxomRbzc5rpAezeVsFyC4TQW+CJkR9t3UAoXaOdkDBvxJNW
XfTuRXxz37FxuhdusvUfmuVKfWqmaqKx4UvOe8E2i9+eVOXBOTxpxArJele6SCvoiB5Gz8hm4oXJ
7S9pFWKyKwI16qcIUVsqQCsZHAHswxfFUH1RguISDSphBptayiT1SIlOL3eCQtGRlAVDiaJcRaBv
h3e0yEN8fWPje3/0BmF+6/7tpsEc3g9lh24tXQMMKifI5kmp021CCQhxzs9EHuOR/2ZP4pQUpXRT
Yiz7c1lqzDNmn7f3ea4m7PYQkvOzPpSBC4+M3m3sWIM4ynUE1wcAgE2xttWWXkxNQAscyqyUEhta
ieVnUU3KrKmPxVm97VUjDL/no2TSDuCYYnMf+xL3W5UeePCtkCHw6H1fQQPlPK08cIWkv9vmODt0
RtumfXadaK1vB/EGZPA+FFVrSrrAO2e6fUw3sYLWxRQXkCyOtJjfqqTVl6TzI+75GzyaP2sNEbRQ
1e8L9JwqaC0WjdikZuGppwDD1hydleB4t9Sdx0as2DzB/fN70fkRfCuwDFZA5FC5XMyXGqO2rG+P
XCXEfKUC+/Kl58kH7H2ZOKb5NVj5f51j1C6eo4gTCHvoAWee7xuLtdBc4UCkWlKj3moBUKu3MhOe
zgOm/Fx7PaqFSzR/S40E/q6CfHE7SfzngIuybwb7BWijTZtsmN7I2eGRySJ5njJS7Yc72WTvzwnL
Xzo9aix1HwLO4jdLozRBvkhLT4kYcs5VxSbKmk2vGr9CWeMqD9Dr7KwISIx3C/auFK+47xjC2I4m
O8KN1fh1wnoE1k6hxIueV51c2VfUYAjw2vhja1u+OoQECHRdxaBqYp+qGrCECvInFH20SzwvEIqe
lSlIQ1fZZPSliIyyIOwBFWDzgLsYiaopbxXBlM3tabSi9oSIft9EjDjEzdTdoE7gvtj8405cbF1s
/AVSJAS93D876ePShZbn8jdQ2ESij3MoMOt078mR7U/6N4qpSBd6eA2UJgHA3Sgb4MmH3RUhiZg+
Pyk0HqXRmszCdMkEdsvXY6a5rzFTtXz/HnuIoR3qfFTdOZqBpHt89zltw5INR29LorPE795f74wI
Cj2xTuN4VXdsifdnoirQOSDcYVWG7p01dvFuI7DxfMrMTTeWfsGTok3aoAZzGZYOZURFeJoN7bhL
yVbpAR47XvSs6NnOgwn6UVF0NNMIu4Fhflp75ZH6eeXxzJm5hxjLQ1hn9HxmS63v9iAZk48GlnAQ
4rYZvN87/7ETXEAbY8i/xVlLzE0Ru6u8hQyVDKLw95S1yPGzk4+xt5x3jwoaIcNsCDmuMj6l0IAy
huh/m1IWOwUmrUOdOdBWmmIeDHxqEX3fdQu/lNq4FN9GQ5FxG4GqtO4LjqUJPMJT0ByHtCDe3ocY
SD4J3AETVC4G5SFtSZKamv4gI2WUcBVwLuj8yMUxr/L3j3Eb1G5mboAH/ehAtriPy1htajkXxgMC
aUqLdUZLYAGohd+EU02vncYLo7u1wj1CBWSYbHMi+AXbfetEEpob3DRbYXU52Z+bkKYbPjOjBlAZ
41MAO7se5vBR3t8NIpdZp3lq+p2t2FIX4/jBJC/awmtDdzXjE4JAESqqAQyiYFKnSriVujF2L5nC
zh35UV139+0dw7VJd+rGTrCq7kW6YUmIi7dNo4YRXSpmeeJEbLtHubTevIYzmf2AC0o6xgCQS673
ZdHatBfv+rUJF20npXAr41uT0tHptOcZFyQQS7roLN7fuWWhcDhaICOXEWXaCg/rtI0xaqz6JdRp
QrqSMohMrHa3syftxCKAtltHdo8tsPZkTVRo/Ik3HsIMyHZvML5sIH9ncGkZdvk/f+chihUIOh3Y
zuF/HT4um90XJlVDLWWYA5l1lkDUT2c1H+OjV3n6cWoBld2L00kwrgMOT1Ah+hb34z1GXUDt5HaO
oahRBzD5/1dnfHDw5suVIXESgwL7MgZgvbA+FUcFZHkX62I32wtqMYuvUo8td6nrkZOmidByQAZx
Ynoro/80SU58vnttLGAxYHV6BIubGUP/qu/MiLN3/hGmvq8/d9EVwHWCUREgdLUp14Qe0Dsn6y8g
NFkbrTQ3ZRswBsCUYieXQxllonuduHW9C+L709F4HAICWVqc87sf0xMLXors9DSdDWd1bqQZAGVs
5+lM5hpPgQiRvMqwz7n8cU7q0/az7nghOaGJh6HiW18QPHuBO4zvXGCe0MarDGM6WWGiVgUl4hRL
Rxoh4v3rbM5bq497HFkZSE661/3CGt1XPr6bdKX7XkDx8q0TLBVj/NVcsjf6mDItdm2/TIUlFYYI
ogl+XZ9XcqimxC6lUBim+OBLr2hQrxZRcVqQxKuNroyYfinmquzG6Z3E7dmKCEpsqJbRsOrvGnX2
mPMCtaZ7HRVsHIY+jzqajRFOZSl7jWsh9Ld3irjEXAsPS6aUH1Q47oqFuxsdv9dIuDqbCKJDUhd5
SOgJjImskSgkrnmpZLRuBFEtR8orCA9dMIp6kttFgIKVnLpoe7oJtQ4ytdA6QbF//DiK38uZxcaL
yJLZNIQDZJ2BRuDBvMunK+/rxuG9br/R/Y8GHo5QPIxVobWmpVgQa8nQX2G70VZ3qtAs2VyRfTq5
FpYpjPZsVGigoiQ8bENl9soHklQj2n8NyGlD5jitUEU91ojpGWYGkOXrpYGqrYQP2LqDBuMi0dab
uYe2Fpy3KuL3zP1GlYbThF1ychUfhktk40tQ3+Yr4PCdybHvOKHZSC/wXPR7Ckb1Y4vzDr+mL2Ss
O2X9dVk7zQ+lIGjqEO2XIxOlPdZVNjIevHrPNwix+ML4AdZ+Q5+Isz4kiUeYQ8uDi2KRp6/3iK/m
xPbAVWlb9aAq7h/zQYFuGzkk76CkTK62nIVdG2N0VGtcqV6esWofMoJkDtYuEkXN8rV2ivVRlMTO
7BO3OfZOpa6N0dH90MwbX8uc97ADukkppSqt0cg5dJrWMocdvCucC9AlasnlCGeQf59iGKm3DXA0
fbFoUX8srYByNjd2AYOJJ7y7eYlCrBC8IyuZ+djvtSIyb4GOJvGcJwKt7bSEzER6AvPbKhw7GvT/
l4PYS740mupE8oQ8WW4VVB52uPDOklNcL+UPmWVcdkCozCxXv8R8ufAcH5gYEiG7XtALDQlJljMK
qLcBVRq5bqZDy+ATrPZ0CmT2IcPrAYA0Zy1zv8yJsvYmFnQZdzpMX/MU7zPF+p5I2IED8wjaxYd1
T7bYUYKwj+IRbmoYg2/NrVRyezWiWZmipIloBcmrSDhg3jWPW0e9A9r+CeYd9bjbwx/qP2RckesB
np0WLsUz14S3+WMpxBpUi19xIvdG1NqiRQ83+mKmI7dLpvLSxtL7zBllgQxXsEVZAw+LAnxRzS8y
0GVmG9WrW0+M6Zkaf/eYTvYn9nG1RFp//0NiyV/bnwiv4Nz7O3+XpOukqa7eHS1z3MxY5EuJ0x87
Mw3dq33aCluPEZaPePn01AqFKo4LGCA1MSHXT+LH1iywiDdNs2NRS+yE8zZgAI7DkKZMEsaNAp2Q
IqbwdN+Hf2kLitg2haACtJu/SsCv7QlEm8wwPWz5uKG4z+6RZ64vVbuFtJGYc1ejju6Rqi0YzwTl
rQOXlpLzy1WbgI0X+G7DqN6nb6toz0V30XKTHYaXLNEH
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (weak1, weak0) GSR = GSR_int;
    assign (weak1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
