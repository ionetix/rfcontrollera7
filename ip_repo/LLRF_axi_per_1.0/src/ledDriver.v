`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Engineer: 		Nathan Usher
// Create Date:    17:51:05 03/29/2014
// Module Name:    ledDriver
// Description: 	Serial driver for LEDs
// Additional Comments: 
//		
//////////////////////////////////////////////////////////////////////////////////
module ledDriver(
	input clk,
	input pllFault,
	input on,
	input init,
	input ramp,
	input closed,
	input vswr,
	input dvdt,
	input low,
	input tnrMoving,
	input tnrFault,
	output reg ledClk = 0,
	output reg ledLatch = 0,
	output reg ledData = 0
);

	reg [10:0] shifter = 11'h3FF;
	reg [9:0] word = 0, nextWord = 0;
	reg [1:0] state = 0;
	reg [4:0] ctr = 0;
	reg [23:0] blinkCtr = 0;
	wire [9:0] blink;
	assign blink = blinkCtr[23] ? 10'h0 : 10'hFF;
	
	always @ (posedge clk)
	begin
		if (pllFault)
			blinkCtr <= blinkCtr + 1'b1;
		nextWord <= pllFault ? blink : {tnrFault, tnrMoving, dvdt, low, vswr, 1'b0, closed, init, ramp, on};
		case (state)
		// wait for LED status change
		0:
		begin
			shifter <= {1'b0, nextWord};
			word <= nextWord;
			ctr <= 0;
			ledClk <= 0;
			ledData <= 0;
			ledLatch <= 0;
			if (word != nextWord)
				state <= state + 1;
		end
		// shift out new LED status
		1:
		begin
			ledClk <= ~ledClk;
			ledData <= shifter[10];
			ledLatch <= 0;
			if (~ledClk)
			begin
				shifter <= {shifter[9:0], 1'b0};
			end
			else
			begin
				ctr <= ctr + 1;
				if (ctr == 10)
					state <= state + 1;
			end
		end
		// send LED latch pulse
		2:
		begin
			ctr <= 0;
			ledClk <= 0;
			ledData <= 0;
			ledLatch <= 1;
			state <= state + 1;
		end
		// wait for LED driver
		3:
		begin
			ctr <= ctr + 1;
			ledClk <= 0;
			ledData <= 0;
			ledLatch <= 0;
			if (&ctr)
				state <= state + 1;
		end
		endcase
	end
endmodule
