`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 			FRIB
// Engineer: 			Nathan Usher
// 
// Create Date:    15:58:18 02/20/2013 
// Design Name: 		FRIB LLRF PED2 - FGPDB
// Module Name:    rfIlk 
// Project Name: 		FRIB LLRF PED2
// Description: 		interlock logic
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module rfIlk(
	input clk,
	input rfLoopback,
	input extIlkOk,
	input pll0Locked,
	input pll1Locked,
	input tunerFault,
	input rfSetOn,
	input findingHome,
	input ilkRst,											// clear latched interlocks
	input ilkCtrRst,										// reset interlock counters
	output reg dacSleep = 1,
	output reg setRfOff = 0,
	output reg ilkd = 0,
	output reg rfOn = 0,
	input [7:0] ilkTurnsOffRf,							// if set to 1, the interlock will also set the rf to off
	input [15:0] ilkResetSel,							// 2 bits per interlock - 0: disable, 1: reset after delay1, 2: reset after delay2, 3: latch until operator reset
	input [31:0] fastResetTime,						// reset delay for interlocks set to 2'b01 above
	input [31:0] slowResetTime,						// reset delay for interlocks set to 2'b10 above
	output [127:0] ilkCtrs,								// 8 8-bit counters, 1 per interlock
	output reg [7:0] ilkSts = 0,						// interlock status - bad interlocks
	output reg [7:0] ilkLatches = 0,					// interlock status - latched interlocks
	output reg [7:0] firstIlk = 0,					// the interlock that shut off RF the last time RF shut off
	input [31:0] fwdAmpl,
	input [31:0] rflAmpl,
	input [31:0] cavAmpl,
	input [31:0] cavLowAmpl,
	input [31:0] cavLowTime,
	input [31:0] cavLowMinFwd,
	input [31:0] fwdRflMinRfl,
	input [15:0] fwdRflRatio1,
	input [31:0] fwdRflTime1,
	input [15:0] fwdRflRatio2,
	input [31:0] fwdRflTime2,
	input [31:0] dvdtRate,
	input [7:0] ilkRateMask,
	input [5:0] ilkRateShift,
	input [7:0] ilkRateLimit
	);

  reg [21:0] plcCtr = 0;
	reg [31:0] cavLowCtr = 0;
	reg [31:0] fwdRflCtr1 = 0;
	reg [31:0] fwdRflCtr2 = 0;
	wire [31:0] fwdRflProd1;
	wire [31:0] fwdRflProd2;
	reg [31:0] dvdtPrev = 0;
	reg [7:0] dvdtCtr = 0;
	reg [31:0] cavAmplPrev = 0;
	reg [7:0] ilkStsPrev;
	wire [7:0] ilkEdge = ilkSts & ~ilkStsPrev;
	wire [7:0] ilkRate;
	wire [7:0] ilkRstBus;
	wire [7:0] ilkCtrRstBus;
	
	// signals for resetting latched interlocks
	reg [31:0] ilkResetCtr = 0;
	wire [7:0] autoResetFast;
	wire [7:0] autoResetSlow;
	wire [7:0] ilkEnabled;
	wire [7:0] ilkDisabled;
	wire [7:0] resetSlow = (ilkResetCtr > slowResetTime) ? autoResetSlow : 0;
	wire [7:0] resetFast = (ilkResetCtr > fastResetTime) ? autoResetFast : 0;
	wire [7:0] resetIlks = ilkRstBus | resetSlow | resetFast;

	always @ (posedge clk)
	begin
		ilkStsPrev <= ilkSts;
		cavAmplPrev <= cavAmpl;
		// interlock test section
		// interlock 0: cavity low
		if (rfOn & (cavAmpl < cavLowAmpl) & (~findingHome | (fwdAmpl > cavLowMinFwd)))
			cavLowCtr <= cavLowCtr + 1;
		else
			cavLowCtr <= 0;
		ilkSts[0] <= ilkEnabled[0] & (cavLowCtr > cavLowTime);
		// interlock 1: fwdRflRatio1 high
		if (rfOn & ({fwdRflProd1, 8'h00} > {8'h00, fwdAmpl}) & (rflAmpl > fwdRflMinRfl))
			fwdRflCtr1 <= fwdRflCtr1 + 1;
		else
			fwdRflCtr1 <= 0;
		ilkSts[1] <= ilkEnabled[1] & (fwdRflCtr1 > fwdRflTime1);
		// interlock 2: fwdRflRatio2 high
		if (rfOn & ({fwdRflProd2, 8'h00} > {8'h00, fwdAmpl}) & (rflAmpl > fwdRflMinRfl))
			fwdRflCtr2 <= fwdRflCtr2 + 1;
		else if (|fwdRflCtr2)
			fwdRflCtr2 <= fwdRflCtr2 - 1;
		ilkSts[2] <= ilkEnabled[2] & (fwdRflCtr2 > fwdRflTime2);
		// interlock 3: -dV/dt
		dvdtPrev <= (dvdtRate > cavAmpl) ? 0 : cavAmpl - dvdtRate;
		if (cavAmpl != cavAmplPrev)
			dvdtCtr <= (ilkEnabled[3] & rfOn & (cavAmpl < dvdtPrev)) ? (&dvdtCtr ? dvdtCtr : dvdtCtr + 1) : 0;
		ilkSts[3] <= &dvdtCtr;
		// interlock 4: PLC
    if (extIlkOk)
      plcCtr <= 0;
    else if (~&plcCtr)
      plcCtr <= plcCtr + 1;
		ilkSts[4] <= &plcCtr;	// interlock is not maskable
		// interlock 5: Either PLL unlocked fault (provides DAC sample clock)
		ilkSts[5] <= ~pll0Locked | ~pll1Locked;	// interlock is not maskable
		// interlock 6: tuner fault
		ilkSts[6] <= tunerFault;
		// interlock 7: interlock rate interlock
		ilkSts[7] <= ilkEnabled[7] & (ilkRate > ilkRateLimit);
	
		// if an interlock is configured to set the RF to off, change the RF setting
		setRfOff <= |(ilkTurnsOffRf & ilkLatches);
		
		// latch interrupts (and clear when conditions are met)
		// most of the logic is above the always block
		if (~ilkd)
			ilkResetCtr <= 0;
		else if (~&ilkResetCtr)
			ilkResetCtr <= ilkResetCtr + 1;
		ilkLatches <= ~ilkDisabled & (ilkSts | (ilkLatches & ~resetIlks));
		
		// When RF shuts off due to interlock, latch the interlock(s) responsible
		ilkd <= |ilkLatches | |ilkSts;
		if (ilkRst)
			firstIlk <= 0;
		else if (rfOn & |ilkSts & ~|firstIlk)
			firstIlk <= ilkSts;

		// RF on/off logic
		rfOn <= rfSetOn & ~|ilkSts & ~|ilkLatches;
		
		// RF switches and DAC sleep pins for redundant RF shutoff
		dacSleep <= ilkd & ~rfLoopback;
	end

	// math functions used for interlocks above
//	multU16U16 fwdRflRatioMult1 (.clk(clk), .a(fwdRflRatio1), .b(rflAmpl[31:16]), .p(fwdRflProd1));
	
	multU16U16 fwdRflRatioMult1 (
      .CLK(clk),  // input wire CLK
      .A(fwdRflRatio1),      // input wire [31 : 0] A
      .B(rflAmpl[31:16]),      // input wire [31 : 0] B
      .P(fwdRflProd1)      // output wire [63 : 0] P
    );
	
//	multU16U16 fwdRflRatioMult2 (.clk(clk), .a(fwdRflRatio2), .b(rflAmpl[31:16]), .p(fwdRflProd2));
	
	multU16U16 fwdRflRatioMult2 (
      .CLK(clk),  // input wire CLK
      .A(fwdRflRatio2),      // input wire [31 : 0] A
      .B(rflAmpl[31:16]),      // input wire [31 : 0] B
      .P(fwdRflProd2)      // output wire [63 : 0] P
    );
	
	ilkRate ilkAvgRate (.clk(clk), .ilkMask(ilkRateMask), .ilkEdge(ilkEdge), .ilkShift(ilkRateShift), .rate(ilkRate));
	
	// interlock counters
	// also generates some signals used for resetting interrupts
	genvar x;
	generate
		for (x=0; x<8; x=x+1)
		begin: ilkCounters
			assign ilkRstBus[x] = ilkRst;
			assign ilkCtrRstBus[x] = ilkCtrRst;
			ilkCtr counters(.clk(clk), .reset(ilkCtrRstBus[x]), .ilkEdge(ilkEdge[x]), .ctr(ilkCtrs[x*16+15:x*16]));
			assign autoResetFast[x] = ilkResetSel[x*2] & ~ilkResetSel[x*2+1];
			assign autoResetSlow[x] = ilkResetSel[x*2+1] & ~ilkResetSel[x*2];
			assign ilkEnabled[x] = |ilkResetSel[x*2+1:x*2];
			assign ilkDisabled[x] = ~|ilkResetSel[x*2+1:x*2];
		end
	endgenerate
	
/*	wire [35:0] csCtrl;
	wire [255:0] csData;
	chipscope_icon csIcon (.CONTROL0(csCtrl));
	chipscope_ila csIla (.CONTROL(csCtrl), .CLK(clk), .TRIG0(csData));
	
	assign csData[7:0] = ilkEnabled;
	assign csData[15:8] = ilkDisabled;
	assign csData[23:16] = resetFast;
	assign csData[31:24] = resetSlow;
	assign csData[39:32] = autoResetFast;
	assign csData[47:40] = autoResetSlow;
	assign csData[55:48] = resetIlks;
	assign csData[71:56] = ilkResetSel;
	assign csData[79:72] = ilkTurnsOffRf;
	assign csData[87:80] = ilkSts;
	assign csData[95:88] = ilkLatches;
	assign csData[103:96] = firstIlk;
	assign csData[111:104] = ilkEdge;
	assign csData[143:112] = fwdAmpl;
	assign csData[175:144] = rflAmpl;
	assign csData[191:176] = fwdRflRatio1;
	assign csData[223:192] = fwdRflTime1;
	assign csData[231:224] = ilkCtrs[15:8];
	assign csData[244:232] = 0;
	assign csData[245] = extIlkOk;
	assign csData[246] = pll0Locked;
	assign csData[247] = pll1Locked;
	assign csData[248] = tunerFault;
	assign csData[249] = rfSetOn;
	assign csData[250] = ilkRst;
	assign csData[251] = ilkCtrRst;
	assign csData[252] = dacSleep;
	assign csData[253] = setRfOff;
	assign csData[254] = ilkd;
	assign csData[255] = rfOn;
	*/
endmodule
