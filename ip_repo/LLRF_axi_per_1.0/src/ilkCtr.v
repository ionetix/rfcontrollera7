`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 			FRIB
// Engineer: 			Nathan Usher
// 
// Create Date:    16:59:03 02/22/2013 
// Design Name: 		FRIB LLRF PED2 - FGPDB
// Module Name:    ilkCtr 
// Project Name: 		FRIB LLRF PED2
// Description: 		counts interlocks
//
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ilkCtr(
	input clk,
	input reset,
	input ilkEdge,
	output reg [15:0] ctr = 0
	);
	
	always @ (posedge clk)
	begin
		if (reset)
			ctr <= 0;
		else if (~&ctr & ilkEdge)
			ctr <= ctr + 1'b1;
	end
endmodule
