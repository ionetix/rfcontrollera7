`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/22/2018 11:19:36 AM
// Design Name: 
// Module Name: ADRC_IQ
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module ADRC_IQ(clk, reset, ctr, set, fdbk, ctrl);

	parameter saturate = 20'sh7FFFF;
	parameter chipscope = 0;
	parameter n = 20;
	parameter nx = 48;
	parameter gain = 8;		// 8 -> 36.8 us; 11 -> 5.6 ms;	to 1%

	input clk;
	input reset;
	input [4:0] ctr;
	input signed [n-1:0] set;
	input signed [n-1:0] fdbk;
	output reg signed [n-1:0] ctrl;

	reg signed [nx-gain:0] u = 0;
	reg signed [nx-1:0] x1 = 0;
	reg signed [nx-1:0] temp = 0;
	reg [nx-4:0] max = -1;

	always @ (posedge clk)
	begin
		if (reset)
		begin
			u <= 0;
			x1 <= 0;
			temp <= 0;
			ctrl <= 0;
		end 
		else
		begin
			temp <= fdbk - ctrl - $signed(x1[nx-1:gain]);
			if (ctr == 5'h03)
				x1 <= x1 + temp;
			else if (x1[nx-1] & ~x1[nx-2])
				x1 <= {2'b11, ~max};
			else if (x1[nx-2] & ~x1[nx-1])
				x1 <= max;
			u <= set - $signed({x1[nx-1], x1[nx-1:gain]});
			if (ctr == 5'h06)
			begin
				if (u < -saturate)
					ctrl <= -saturate;
				else if (u > saturate)
					ctrl <= saturate;
				else
					ctrl <= u[n-1:0];
			end
		end
	end

endmodule
