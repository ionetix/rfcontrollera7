
`timescale 1 ns / 1 ps

	module LLRF_axi_per_v1_0 #
	(
		// Users to add parameters here
        parameter saturate                       = 32'h4D9F6FEE,
        parameter samples                        = 29,
		// User parameters ends
		// Do not modify the parameters beyond this line


		// Parameters of Axi Slave Bus Interface S00_AXI
		parameter integer C_S00_AXI_DATA_WIDTH	= 32,
		parameter integer C_S00_AXI_ADDR_WIDTH	= 10
	)
	(
		// Users to add ports here
		input                                     refclk200,
        input                                     CLK_DAC_N,
        input                                     CLK_DAC_P,
        output [13:0]                             DAC_N,
        output [13:0]                             DAC_P,
        output                                    DAC_SLEEP,
        input                                     CLK12_5,
        output                                    RF_ON_MON,
        output                                    PLC_RFON,
        output                                    PLC_RFLOCKED,
        output                                    SPIDAC_CLK,
        output                                    SPIDAC_CS,
        output                                    SPIDAC_DATA,
        output                                    LEDS_CLK,
        output                                    LEDS_DATA,
        output                                    LEDS_LATCH,
      input                                     ADC_CLKBIT_N,
       input                                     ADC_CLKBIT_P,
        input                                     ADC_A0_N,
        input                                     ADC_A0_P,
        input                                     ADC_A1_N,
      input                                     ADC_A1_P,
        input                                     ADC_B0_N,
      input                                     ADC_B0_P,
        input                                     ADC_B1_N,
        input                                     ADC_B1_P,
        input                                     ADC_C0_N,
       input                                     ADC_C0_P,
        input                                     ADC_C1_N,
        input                                     ADC_C1_P,
        input                                     ADC_D0_N,
        input                                     ADC_D0_P,
        input                                     ADC_D1_N,
        input                                     ADC_D1_P,
        output                                    SW_LOOPBACK_N,
        output                                    SW_LOOPBACK_P,
        input                                     OUTPUT_EN,
        input                                     bitSlipCal,
        input                                     configDone,
        input                                     pll0locked,
        input                                     pll1locked,
        output                                    serdesCalDone,
        output                                    tunerDn,
        output                                    tunerUp,
        output                                    tunerGoHome,
        output                                    tunerSetHome,
        input                                     tunerMoving,
        input                                     tunerFault,
        input  [5:0]                              atten,
		// User ports ends
		// Do not modify the ports beyond this line


		// Ports of Axi Slave Bus Interface S00_AXI
		input wire  s00_axi_aclk,
		input wire  s00_axi_aresetn,
		input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_awaddr,
		input wire [2 : 0] s00_axi_awprot,
		input wire  s00_axi_awvalid,
		output wire  s00_axi_awready,
		input wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_wdata,
		input wire [(C_S00_AXI_DATA_WIDTH/8)-1 : 0] s00_axi_wstrb,
		input wire  s00_axi_wvalid,
		output wire  s00_axi_wready,
		output wire [1 : 0] s00_axi_bresp,
		output wire  s00_axi_bvalid,
		input wire  s00_axi_bready,
		input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_araddr,
		input wire [2 : 0] s00_axi_arprot,
		input wire  s00_axi_arvalid,
		output wire  s00_axi_arready,
		output wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_rdata,
		output wire [1 : 0] s00_axi_rresp,
		output wire  s00_axi_rvalid,
		input wire  s00_axi_rready
	);
// Instantiation of Axi Bus Interface S00_AXI
	LLRF_axi_per_v1_0_S00_AXI # (
	 
		.C_S_AXI_DATA_WIDTH(C_S00_AXI_DATA_WIDTH),
		.C_S_AXI_ADDR_WIDTH(C_S00_AXI_ADDR_WIDTH)
	) LLRF_axi_per_v1_0_S00_AXI_inst (
	.refclk200    (refclk200),
    .CLK_DAC_N    (CLK_DAC_N),
    .CLK_DAC_P    (CLK_DAC_P),
    .DAC_N        (DAC_N),
    .DAC_P        (DAC_P),
    .DAC_SLEEP    (DAC_SLEEP),
    .CLK12_5      (CLK12_5),
    .RF_ON_MON    (RF_ON_MON),
    .PLC_RFON     (PLC_RFON),
    .PLC_RFLOCKED (PLC_RFLOCKED),
    .SPIDAC_CLK   (SPIDAC_CLK),
    .SPIDAC_CS    (SPIDAC_CS),
    .SPIDAC_DATA  (SPIDAC_DATA),
    .LEDS_CLK     (LEDS_CLK),
    .LEDS_DATA    (LEDS_DATA),
    .LEDS_LATCH   (LEDS_LATCH),
    .ADC_CLKBIT_N (ADC_CLKBIT_N),
    .ADC_CLKBIT_P (ADC_CLKBIT_P),
    .ADC_A0_N     (ADC_A0_N),
    .ADC_A0_P     (ADC_A0_P),
    .ADC_A1_N     (ADC_A1_N),
    .ADC_A1_P     (ADC_A1_P),
    .ADC_B0_N     (ADC_B0_N),
    .ADC_B0_P     (ADC_B0_P),
    .ADC_B1_N     (ADC_B1_N),
    .ADC_B1_P     (ADC_B1_P),
    .ADC_C0_N     (ADC_C0_N),
    .ADC_C0_P     (ADC_C0_P),
    .ADC_C1_N     (ADC_C1_N),
    .ADC_C1_P     (ADC_C1_P),
    .ADC_D0_N     (ADC_D0_N),
    .ADC_D0_P     (ADC_D0_P),
    .ADC_D1_N     (ADC_D1_N),
    .ADC_D1_P     (ADC_D1_P),
    .SW_LOOPBACK_N(SW_LOOPBACK_N),
    .SW_LOOPBACK_P(SW_LOOPBACK_P),
    .OUTPUT_EN    (OUTPUT_EN),
    .bitSlipCal   (bitSlipCal),
    .configDone   (configDone),
    .pll0locked   (pll0locked),
    .pll1locked   (pll1locked),
    .serdesCalDone(serdesCalDone),
    .tunerDn      (tunerDn),
    .tunerUp      (tunerUp),
    .tunerGoHome  (tunerGoHome),
    .tunerSetHome (tunerSetHome),
    .tunerMoving  (tunerMoving),
    .tunerFault   (tunerFault),
    .atten        (atten),	
		.S_AXI_ACLK(s00_axi_aclk),
		.S_AXI_ARESETN(s00_axi_aresetn),
		.S_AXI_AWADDR(s00_axi_awaddr),
		.S_AXI_AWPROT(s00_axi_awprot),
		.S_AXI_AWVALID(s00_axi_awvalid),
		.S_AXI_AWREADY(s00_axi_awready),
		.S_AXI_WDATA(s00_axi_wdata),
		.S_AXI_WSTRB(s00_axi_wstrb),
		.S_AXI_WVALID(s00_axi_wvalid),
		.S_AXI_WREADY(s00_axi_wready),
		.S_AXI_BRESP(s00_axi_bresp),
		.S_AXI_BVALID(s00_axi_bvalid),
		.S_AXI_BREADY(s00_axi_bready),
		.S_AXI_ARADDR(s00_axi_araddr),
		.S_AXI_ARPROT(s00_axi_arprot),
		.S_AXI_ARVALID(s00_axi_arvalid),
		.S_AXI_ARREADY(s00_axi_arready),
		.S_AXI_RDATA(s00_axi_rdata),
		.S_AXI_RRESP(s00_axi_rresp),
		.S_AXI_RVALID(s00_axi_rvalid),
		.S_AXI_RREADY(s00_axi_rready)
	);

	// Add user logic here

	// User logic ends

	endmodule
